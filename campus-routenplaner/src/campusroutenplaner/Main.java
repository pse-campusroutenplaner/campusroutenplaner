package campusroutenplaner;



import campusroutenplaner.controller.main.BackgroundController;
import campusroutenplaner.controller.main.BuildingController;
import campusroutenplaner.controller.main.MapController;
import campusroutenplaner.controller.usercontroller.ConstructionSiteController;
import campusroutenplaner.controller.usercontroller.FavoriteController;
import campusroutenplaner.controller.usercontroller.LogInController;
import campusroutenplaner.controller.usercontroller.RoutingController;
import campusroutenplaner.model.constructionsites.ConstructionSiteManager;
import campusroutenplaner.model.database.Database;
import campusroutenplaner.model.favorites.FavoriteManager;
import campusroutenplaner.model.logIn.LogInManager;
import campusroutenplaner.model.map.CampusMap;
import campusroutenplaner.model.map.MapManager;
import campusroutenplaner.model.routing.GraphGenerator.GraphGeneratorReal;
import campusroutenplaner.model.routing.GraphGenerator.GraphGeneratorTable;
import campusroutenplaner.model.routing.RoutingManager;
import campusroutenplaner.view.ViewMain;
import javafx.application.Application;
import javafx.stage.Stage;


public class Main extends Application {

	static ViewMain view;

	public static void main(String[] args) {


		Database database = new Database("KIT.db");
		try {
		    database.sync();
		}
		catch (Exception e) {
		    e.printStackTrace();
		}
		/*
		 * System.out.println(database.login("admin", "testpw"));
		 * database.logout(); try { image = ImageIO.read(new File("bg_0.png"));
		 * database.addBackground(Double.parseDouble("0.0"),
		 * Double.parseDouble("0.0"), Double.parseDouble("0.0"), image, 0); }
		 * catch (IOException e) { e.printStackTrace(); } try { database.sync();
		 * } catch (Exception e) { e.printStackTrace(); return; }
		 */



		CampusMap campus = new CampusMap(database);
		MapManager mapManager = new MapManager(campus, database);
		MapController mapController = new MapController(mapManager);
		mapManager.loadDatabase();
		BuildingController buildingController = new BuildingController(mapManager, campus);
		LogInManager laginManger = new LogInManager(database);
		LogInController loginController = new LogInController(laginManger);

		// for user
		FavoriteManager favoriteManager = new FavoriteManager(database);
		FavoriteController favoriteController = new FavoriteController(favoriteManager);

		RoutingManager routingManager = new RoutingManager();
		RoutingController routingController = new RoutingController(routingManager, buildingController);
		BackgroundController backgroundController = new BackgroundController(mapController, buildingController);
		ConstructionSiteManager constructionSiteManager = new ConstructionSiteManager(database);
		ConstructionSiteController constructionSiteController = new ConstructionSiteController(constructionSiteManager);


		Main.view = new ViewMain(mapController, buildingController, backgroundController, favoriteController,
				routingController , constructionSiteController, loginController, database);





        launch(args);






    }



	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			view.start(primaryStage);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
