package campusroutenplaner;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by nmlad on 7/30/16.
 */
public class Logger {

    static FileWriter fileWriter;



    public static void print(String text){
        BufferedWriter bw = null;


        try {
            fileWriter = new FileWriter("res/log.log", true);
        } catch (IOException e) {
            System.out.println(e.getMessage());

            File file = new File("/res/log.log");


            try {
                file.createNewFile();
            } catch (IOException e1) {
                System.out.println(e.getMessage());
            }



        }


        try {
            bw = new BufferedWriter(fileWriter);

            DateFormat dateFormat = new SimpleDateFormat("yy/MM/dd HH:mm:ss");
            Date date = new Date();

            System.out.println(text);

            bw.write(dateFormat.format(date) +" : " + text);
            bw.newLine();
            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bw != null) try {
                bw.close();
            } catch (IOException ioe2) {

            }
        }

    }

    public void write(String text){
        BufferedWriter bw = null;


        try {
            fileWriter = new FileWriter("res/log.log", true);
        } catch (IOException e) {
          //  System.out.println(e.getMessage());

            File file = new File("/res/log.log");


            try {
                file.createNewFile();
            } catch (IOException e1) {
                System.out.println(e.getMessage());
            }



        }


        try {
            bw = new BufferedWriter(fileWriter);

            DateFormat dateFormat = new SimpleDateFormat("yy/MM/dd HH:mm:ss");
            Date date = new Date();



            bw.write(dateFormat.format(date) +" : " + text);
            bw.newLine();
            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bw != null) try {
                bw.close();
            } catch (IOException ioe2) {

            }
        }
    }
}
