package campusroutenplaner.view.admin.controller.map;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import campusroutenplaner.controller.main.MapController;
import campusroutenplaner.model.map.Background;
import campusroutenplaner.model.map.DefaultType;
import campusroutenplaner.model.map.Edge;
import campusroutenplaner.model.map.Vertex;
import campusroutenplaner.view.admin.controller.Mode;
import campusroutenplaner.view.admin.controller.map.BackgroundScale;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class AdminMapViewController {

	@FXML
	private VBox adminMapView;
	@FXML
	private Slider zoomSlider;
	@FXML
	private ScrollPane scrollPane;
	@FXML
	private Group rootGroup;
	@FXML
	private Rectangle map;
	@FXML
	private VertexViewController vertexViewController;
	@FXML
	private EdgeViewController edgeViewController;
	@FXML
	private OptionController parent;
	private int mapID;
	private int mapIDprev;
	private Image background;
	private Mode mode;
	private Group zoomgroup;
	private Map<Circle, Integer> vertices;
	private Map<Circle, Set<Line>> lines;
	private Map<Line, Integer> edges;
	private MapController controller;
	private Draughtsman draftsman;
	private Circle selectedVertex;
	private int selectedVertexID;

	@FXML
	private void initialize() {
		draftsman = new Draughtsman();
		zoomgroup = new Group();
		zoomgroup.getChildren().add(draftsman.getMeasure());
		zoomgroup.getChildren().addAll(map);
		rootGroup.getChildren().add(zoomgroup);
		zoomSliderInit();
		mapInit();
	}

	/**
	 * add ChangeListener to slider to be able to zoom
	 */
	private void zoomSliderInit() {
		zoomSlider.valueProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				zoom((Double) newValue);
			}
		});
	}

	/**
	 * specify what happens while zooming
	 * 
	 * @param new_val
	 *            zoom value
	 */
	private void zoom(Double new_val) {
		zoomgroup.setScaleX(new_val);
		zoomgroup.setScaleY(new_val);
		double r = draftsman.getCircleRadius() / ((new_val * 0.2) + 0.8);
		draftsman.setCircleRadius(r);
		for (Circle c : vertices.keySet()) {
			c.setRadius(r);
		}
	}

	private void mapInit() {
		map.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if (mode.equals(Mode.ADDVERTEX)) {
					DefaultType type = new DefaultType();
					Circle circle = draftsman.drawVertex(event.getX(), event.getY(), type);
					Vertex vertex = controller.requestAddVertex(mapID, circle.getCenterX(), circle.getCenterY());
					addVertex(circle, vertex.getId());
					event.consume();
				}
			}
		});
	}

	private void map(int mapID) {
		this.mapID = mapID;
		if (mapID < 0)
			return;
		Background b = controller.requestGetBackground(mapID);
		if (b.getPicture() != null) {
			background = SwingFXUtils.toFXImage(b.getPicture(), null);
			map.setFill(new ImagePattern(background));
		} else {
			map.setFill(draftsman.getNoMap());
		}
		if (b.getHight() > 0.0 && b.getWidth() > 0.0) {
			map.setHeight(b.getHight());
			map.setWidth(b.getWidth());
		}
		edgesInit(verticesInit());
	}

	private void mapStandards() {
		zoomSlider.setMin(1);
		zoomSlider.setValue(1);
		zoomgroup.setScaleX(1);
		zoomgroup.setScaleY(1);
		map.setCursor(Cursor.DEFAULT);
		map.setStroke(draftsman.getStroke());
	}

	private void scale() {
		parent.showScaleOPtion();
		zoomSlider.setMin(0.5);
		zoomSlider.setValue(0.5);
		zoomgroup.setScaleX(0.5);
		zoomgroup.setScaleY(0.5);
		map.setStroke(draftsman.getScaleStroke());
		map.setStrokeWidth(10);
		mode = Mode.IMAGESCALE;
		makeResizable(map);
	}

	private void makeResizable(Rectangle map) {
		BackgroundScale d = new BackgroundScale(map);
		map.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if (mode.equals(Mode.IMAGESCALE)) {
					d.mousePressed(event);
				}
			}
		});
		map.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if (mode.equals(Mode.IMAGESCALE)) {
					d.mouseDragged(event);
				}
			}
		});
		map.setOnMouseMoved(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if (mode.equals(Mode.IMAGESCALE)) {
					d.mouseOver(event);
				}
			}
		});
		map.setOnMouseReleased(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if (mode.equals(Mode.IMAGESCALE)) {
					d.mouseReleased(event);
				}
			}
		});
	}

	private void checkMapSize() {
		Set<Circle> deletedVertices = new HashSet<Circle>();
		for (Circle c : vertices.keySet()) {
			double x = c.getCenterX();
			double y = c.getCenterY();
			if (x < map.getLayoutX() || y < map.getLayoutY() || x > map.getLayoutX() + map.getWidth()
					|| y > map.getLayoutY() + map.getHeight()) {
				deletedVertices.add(c);
			}
		}
		saveMapSize(deletedVertices);
	}

	private void saveMapSize(Set<Circle> deletedVertices) {
		if (deletedVertices == null || deletedVertices.isEmpty()) {
			double marginX = map.getLayoutX();
			double marginY = map.getLayoutY();
			if (marginX != 0.0 && marginY != 0.0) {
				for (int i : vertices.values()) {
					Vertex v = controller.requestSearchVertex(mapID, i);
					controller.requestChangeVertexXYPosition(mapID, i, v.getXPos() - marginX, v.getYPos() - marginY);
				}
				map.setLayoutX(0);
				map.setLayoutY(0);
			}
			controller.requestSetHighWidth(mapID, map.getHeight(), map.getWidth());
			map(mapID);
		} else {
			graphOutOfMap(deletedVertices);
		}
	}

	private void graphOutOfMap(Set<Circle> deletedVertices) {
		try {
			Stage stage = new Stage();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("../../FXMLfiles/GraphOutOfMap.fxml"));
			Parent root = loader.load();
			MapWarning warning = loader.getController();
			warning.setMapWarning(this, deletedVertices, stage);
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * load all vertices of set mapID from controller
	 */
	private Map<Integer, Circle> verticesInit() {
		if (vertices != null) {
			zoomgroup.getChildren().removeAll(vertices.keySet());
		}
		vertices = new HashMap<Circle, Integer>();
		Map<Integer, Circle> IDs = new HashMap<Integer, Circle>();
		lines = new HashMap<Circle, Set<Line>>();
		for (int i : controller.requestVertexList(mapID)) {
			Vertex vertex = controller.requestSearchVertex(mapID, i);
			Circle circle = draftsman.drawVertex(vertex.getXPos(), vertex.getYPos(), vertex.getType());
			IDs.put(i, circle);
			addVertex(circle, i);
		}
		return IDs;
	}

	/**
	 * load all edge of set mapID from controller
	 */
	private void edgesInit(Map<Integer, Circle> IDs) {
		if (edges != null) {
			zoomgroup.getChildren().removeAll(edges.keySet());
		}
		edges = new HashMap<Line, Integer>();
		for (int i : controller.requestEdgeList(mapID)) {
			Edge edge = controller.requestSearchEdge(mapID, i);
			if (edge.getSource().getMapId() == edge.getDest().getMapId()) {
				addEdge(edge, IDs.get(edge.getSource().getId()), IDs.get(edge.getDest().getId()));
			} else {
				int ID = edge.getSource().getId();
				if (edge.getDest().getMapId() == mapID)
					ID = edge.getDest().getId();
				addEdge(edge, IDs.get(ID), null);
			}
		}
	}

	/**
	 * add circle and vertex to all hashMaps
	 * 
	 * @param circle
	 *            depiction element of vertex
	 * @param vertex
	 *            graph element
	 */
	private void addVertex(Circle circle, int vertexID) {
		dragVertex(circle);
		clickVertex(circle);
		vertices.put(circle, vertexID);
		lines.put(circle, new HashSet<Line>());
		zoomgroup.getChildren().add(circle);
	}

	/**
	 * specify what happens if vertex is dragged
	 * 
	 * @param circle
	 *            dragged element
	 */
	private void dragVertex(Circle circle) {
		circle.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent t) {
				if (mode.equals(Mode.ADDVERTEX)) {
					circle.setCenterX(t.getX());
					circle.setCenterY(t.getY());
					t.consume();
				}
			}
		});
		circle.setOnMouseReleased(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent t) {
				if (mode.equals(Mode.ADDVERTEX)) {
					if (controller.requestChangeVertexXYPosition(mapID, vertices.get(circle), t.getX(),
							t.getY()) == null || t.getY() > map.getHeight() || t.getX() > map.getWidth()) {
						Vertex v = controller.requestSearchVertex(mapID, vertices.get(circle));
						circle.setCenterX(v.getXPos());
						circle.setCenterY(v.getYPos());
					}
					t.consume();
				}
			}
		});
	}

	/**
	 * specify what happens if vertex is clicked
	 * 
	 * @param circle
	 *            clicked element
	 */
	private void clickVertex(Circle circle) {
		circle.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent t) {
				switch (mode) {
				case ADDEDGE:
					addEdgeStart((Circle) t.getSource());
					break;
				case IMAGESCALE:
					break;
				case STANDERD:
				case ADDVERTEX:
					if (t.getClickCount() == 2) {
						parent.showVertexOption();
						vertexViewController.show(circle, controller.requestSearchVertex(mapID, vertices.get(circle)));
					}
					break;
				}
				t.consume();
			}
		});
	}

	/**
	 * decide if an new edge should be request from controller or just save the
	 * circle
	 * 
	 * @param circle
	 *            clicked element
	 */
	private void addEdgeStart(Circle circle) {
		if (selectedVertex == null) {
			selectedVertex = circle;
			selectedVertexID = vertices.get(circle);
			mapIDprev = mapID;
			draftsman.vertexSelected(selectedVertex);
		} else {
			if (selectedVertex != circle) {
				Edge edge = controller.requestAddEdge(mapIDprev, selectedVertexID, mapID, vertices.get(circle));
				if (edge == null) {
					notSameType();
				} else if (mapID != mapIDprev) {
					addEdge(edge, circle, null);
				} else {
					addEdge(edge, selectedVertex, circle);
				}
			}
			draftsman.deselected(selectedVertex);
			selectedVertex = null;
			selectedVertexID = 0;
			mapIDprev = 0;
		}
	}

	private void notSameType() {
		try {
			Stage stage = new Stage();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("../../FXMLfiles/NotSameType.fxml"));
			Parent root = loader.load();
			NotSameTypeController warning = loader.getController();
			warning.setStage(stage);
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * add line and edge to the HashMaps
	 * 
	 * @param line
	 *            depiction element of the edge
	 * @param edge
	 *            graph element
	 */
	private void addEdge(Edge edge, Circle start, Circle end) {
		Line line;
		if (edge.getSource().getMapId() == edge.getDest().getMapId()) {
			line = draftsman.drawEdge(start.getCenterX(), start.getCenterY(), end.getCenterX(), end.getCenterY());
			addLine(end, line, false);
		} else {
			line = draftsman.drawEdge(start.getCenterX(), start.getCenterY(), start.getCenterX() + 10,
					start.getCenterY() + 10);
			line.endXProperty().bind(start.centerXProperty().add(10));
			line.endYProperty().bind(start.centerYProperty().add(10));
		}
		draftsman.setBlocked(line, edge.isBlocked());
		addLine(start, line, true);
		edges.put(line, edge.getId());
		clickEdge(line);
		edges.put(line, edge.getId());
		zoomgroup.getChildren().add(line);
		line.toBack();
		map.toBack();
	}

	private void addLine(Circle circle, Line line, boolean isStart) {
		lines.get(circle).add(line);
		if (isStart) {
			line.startXProperty().bind(circle.centerXProperty());
			line.startYProperty().bind(circle.centerYProperty());
		} else {
			line.endXProperty().bind(circle.centerXProperty());
			line.endYProperty().bind(circle.centerYProperty());
		}
	}

	/**
	 * specify what happens if edge is clicked
	 * 
	 * @param line
	 *            clicked element
	 */
	private void clickEdge(Line line) {
		line.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent t) {
				try {
					switch (mode) {
					case ADDVERTEX:
						break;
					case IMAGESCALE:
						break;
					case STANDERD:
					case ADDEDGE:
						parent.showEdgeOption();
						edgeViewController.show(line, controller.requestSearchEdge(mapID, edges.get(line)));
						break;
					}
					t.consume();
				} catch (NullPointerException e) {

				}
			}
		});
	}

	/**
	 * request Controller to delete vertex and removing from hashMaps
	 * 
	 * @param circle
	 *            depiction of deleting vertex
	 */
	protected void deleteVertex(Circle circle) {
		if (circle == null)
			return;
		for (Line l : lines.get(circle)) {
			deleteEdge(l);
		}
		lines.remove(circle);
		controller.requestDeleteVertex(mapID, vertices.get(circle));
		vertices.remove(circle, vertices.get(circle));
		zoomgroup.getChildren().removeAll(circle);
	}

	/**
	 * request Controller to delete edge and removing from hashMap
	 * 
	 * @param line
	 *            depiction of deleting edge
	 */
	protected void deleteEdge(Line line) {
		if (edges.containsKey(line)) {
			controller.requestDeleteEdge(mapID, edges.get(line));
			edges.remove(line);
		}
		zoomgroup.getChildren().remove(line);
	}

	protected void deleteVertices(Set<Circle> deletedVertices) {
		for (Circle c : deletedVertices) {
			if (!vertices.containsKey(c))
				c = findCircleWithSamePosiotion(c);
			deleteVertex(c);
		}
		saveMapSize(null);
	}

	private Circle findCircleWithSamePosiotion(Circle c) {
		for (Circle circle : vertices.keySet()) {
			if (circle.getCenterX() == c.getCenterX() && circle.getCenterY() == c.getCenterY())
				return circle;
		}
		return null;
	}

	protected boolean newImageNotSave() {
		return !((ImagePattern) map.getFill()).getImage().equals(background) || map.getLayoutX() != 0
				|| map.getLayoutY() != 0;
	}

	protected void finishScale() {
		setMode(Mode.STANDERD);
		mapStandards();
		checkMapSize();
	}

	protected void showImage(BufferedImage createImage) {
		if (createImage != null)
			map.setFill(new ImagePattern(SwingFXUtils.toFXImage(createImage, null)));
	}

	protected Circle reloade(Circle circle) {
		int vertexid = vertices.get(circle);
		reloade();
		for (Circle c : vertices.keySet()) {
			if (vertices.get(c) == vertexid)
				return c;
		}
		return null;
	}

	protected void reloade() {
		int mapID = this.mapID;
		show(-1);
		show(mapID);
	}

	public void show(int floorID) {
		mapStandards();
		if (mapID != floorID)
			map(floorID);
	}

	protected int getMapID() {
		return mapID;
	}

	protected Mode getMode() {
		return mode;
	}

	protected void setMode(Mode mode) {
		if (mode.equals(Mode.IMAGESCALE) && this.mode != mode)
			scale();
		this.mode = mode;
	}

	protected void setDraftsman(ToggleButton button) {
		draftsman.setMeasureButton(button);
	}

	protected void setVertexViewController(VertexViewController vertexViewController) {
		this.vertexViewController = vertexViewController;
		vertexViewController.setDraftsman(draftsman);
	}

	protected void setEdgeViewController(EdgeViewController edgeViewController) {
		this.edgeViewController = edgeViewController;
	}

	protected void setParent(OptionController parent) {
		this.parent = parent;
	}

	protected void setController(MapController controller) {
		this.controller = controller;
		map(0);
	}
}
