package campusroutenplaner.view.admin.controller.map;

import java.util.Set;

import campusroutenplaner.view.admin.controller.Mode;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class MapWarning {
	private AdminMapViewController parent;
	private Stage stage;
	private Set<Circle> deletedVertices;

    @FXML
    private void cancle(ActionEvent event) {
    	parent.setMode(Mode.IMAGESCALE);
    	stage.close();
    }

    @FXML
    private void deleteGraphSection(ActionEvent event) {
    	parent.deleteVertices(deletedVertices);
    	stage.close();
    }

	protected void setMapWarning(AdminMapViewController parent, Set<Circle> deletedVertices, Stage stage) {
		this.parent = parent;
		this.deletedVertices = deletedVertices;
		this.stage = stage;
		stage.setOnCloseRequest(e -> parent.setMode(Mode.IMAGESCALE));
	}
}
