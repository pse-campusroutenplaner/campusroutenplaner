package campusroutenplaner.view.admin.controller.map;

import campusroutenplaner.controller.main.BackgroundController;
import campusroutenplaner.controller.main.MapController;
import campusroutenplaner.view.admin.controller.AdminSceneController;
import campusroutenplaner.view.admin.controller.Mode;
import campusroutenplaner.view.admin.controller.building.BuildingViewController;
import campusroutenplaner.view.admin.controller.building.FloorListController;
import campusroutenplaner.view.admin.controller.map.AdminMapViewController;
import campusroutenplaner.view.admin.controller.map.EdgeViewController;
import campusroutenplaner.view.admin.controller.map.MapOptionController;
import campusroutenplaner.view.admin.controller.map.VertexViewController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Accordion;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleButton;

public class OptionController {

	@FXML
	private SplitPane option;
	@FXML
	private Accordion options;
	@FXML
	private TitledPane vertexOptions;
	@FXML
	private ToggleButton addVertex;
	@FXML
	private TitledPane edgeOptions;
	@FXML
	private ToggleButton addEdge;
	@FXML
	private TitledPane backgroundOptions;
	@FXML
	private VertexViewController vertexViewController;
	@FXML
	private EdgeViewController edgeViewController;
	@FXML
	private MapOptionController mapOptionController;
	@FXML
	private AdminMapViewController adminMapViewController;
	@FXML
	private FloorListController floorListController;
	@FXML
	private AdminSceneController parent;

	@FXML
	private void initialize() {
		adminMapViewController.setParent(this);
		adminMapViewController.setEdgeViewController(edgeViewController);
		adminMapViewController.setVertexViewController(vertexViewController);
		vertexViewController.setMap(adminMapViewController);
		edgeViewController.setMap(adminMapViewController);
		vertexOptions.expandedProperty().addListener((o, oldVal, newVal) -> setMode(false, false));
		edgeOptions.expandedProperty().not().addListener((o, oldVal, newVal) -> setMode(false, false));
		addVertex.selectedProperty().addListener((o, oldVal, newVal) -> setMode(newVal, false));
		addEdge.selectedProperty().addListener((o, oldVal, newVal) -> setMode(false, newVal));
		backgroundOptions.expandedProperty().addListener((o, oldVal, newVal) -> mapOptionController.close(newVal));
		mapOptionController.setMap(adminMapViewController);
		floorListController.setMap(adminMapViewController);
	}

	private void setMode(boolean addVertex, boolean addEdge) {
		hideOptions();
		adminMapViewController.setMode(Mode.STANDERD);
		this.addVertex.setSelected(addVertex);
		this.addEdge.setSelected(addEdge);
		if (addVertex) {
			adminMapViewController.setMode(Mode.ADDVERTEX);
		}
		if (addEdge) {
			adminMapViewController.setMode(Mode.ADDEDGE);
		}
	}

	@FXML
	private void campusMap(ActionEvent event) {
		floorListController.campusMap();
	}

	private void hideOptions() {
		edgeViewController.hide();
		vertexViewController.hide();
	}

	protected void showVertexOption() {
		vertexOptions.setExpanded(true);
	}

	protected void showEdgeOption() {
		edgeOptions.setExpanded(true);
	}

	protected void showScaleOPtion() {
		if (!mapOptionController.getScale().isSelected()) {
			mapOptionController.getScale().setSelected(true);
			backgroundOptions.setExpanded(true);
		}
	}

	public void unBlockedEdge(int edgeID) {
		edgeViewController.unBlockedEdge(edgeID);
	}

	public void reloade() {
		options.setExpandedPane(null);
		adminMapViewController.reloade();
	}

	public FloorListController getFloorListController() {
		return floorListController;
	}

	public void setParent(AdminSceneController parent) {
		this.parent = parent;
	}

	public void setController(MapController map, BackgroundController backgroundController) {
		adminMapViewController.setController(map);
		edgeViewController.setController(map);
		vertexViewController.setController(map);
		mapOptionController.setController(map);
		mapOptionController.setBackgroundController(backgroundController);
	}

	public void setBuildingList(BuildingViewController buildingController) {
		vertexViewController.setBuildingController(buildingController);
	}
}
