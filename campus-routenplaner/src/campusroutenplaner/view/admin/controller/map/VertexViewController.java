package campusroutenplaner.view.admin.controller.map;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import campusroutenplaner.controller.main.MapController;
import campusroutenplaner.model.map.DefaultType;
import campusroutenplaner.model.map.ElevatorType;
import campusroutenplaner.model.map.EntranceType;
import campusroutenplaner.model.map.OnError;
import campusroutenplaner.model.map.RoomType;
import campusroutenplaner.model.map.StairsType;
import campusroutenplaner.model.map.Vertex;
import campusroutenplaner.model.map.VertexType;
import campusroutenplaner.view.admin.controller.building.BuildingViewController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;

public class VertexViewController {

	@FXML
	private VBox vertexView;
	@FXML
	private Label vertexID;
	@FXML
	private CheckBox entrance;
	@FXML
	private FlowPane mapOptions;
	@FXML
	private FlowPane floorOptions;
	@FXML
	private ComboBox<String> building;
	@FXML
	private CheckBox stairway;
	@FXML
	private CheckBox elevator;
	@FXML
	private CheckBox room;
	@FXML
	private TextField roomNumber;
	@FXML
	private Label notValid;
	@FXML
	private VertexViewInfoController vertexViewInfoController;
	private int ID;
	private Set<CheckBox> check;
	private Circle circle;
	private Draughtsman draftsman;
	private AdminMapViewController map;
	private MapController controller;
	private BuildingViewController buildingController;

	@FXML
	private void initialize() {
		vertexViewInfoController.setParent(this);
		checkBoxInit();
		notValid.setVisible(false);
		building.disableProperty().bind(entrance.selectedProperty().not());
		roomNumber.visibleProperty().bind(room.selectedProperty());
		room.selectedProperty().addListener((o, oldval, newval) -> change(room, newval));
		stairway.selectedProperty().addListener((o, oldval, newval) -> change(stairway, newval));
		elevator.selectedProperty().addListener((o, oldval, newval) -> change(elevator, newval));
		entrance.selectedProperty().addListener((o, oldVal, newVal) -> entrance(newVal));
		entrance.selectedProperty().addListener((o, oldval, newval) -> buildingNumberClear(newval));
		hide();
	}

	@FXML
	private void chooseBuilding(ActionEvent event) {
		if (ID < 0 || !building.isVisible())
			return;
		notValid.setVisible(false);
		String b = building.getEditor().getText();
		VertexType v = controller.requestSearchVertex(map.getMapID(), ID).getType();
		String a = null;
		if (v.getType().equals("Entrance"))
			a = ((EntranceType) v).getBuildingNumber();
		if (b.equals(a))
			return;
		if (building.getItems().contains(b)) {
			EntranceType type = new EntranceType(b);
			controller.requestChangeVertexType(map.getMapID(), ID, type);
			draftsman.change(circle, type);
			circle = map.reloade(circle);
		} else if (b.matches(buildingController.getBuildingRegex())) {
			buildingController.userMessage(b, true, this);
		} else {
			notValid.setVisible(true);
		}

	}

	@FXML
	private void delete(ActionEvent event) {
		map.deleteVertex(circle);
		hide();
	}

	@FXML
	private void setRooumNumber(ActionEvent event) {
		notValid.setVisible(false);
		String roomName = roomNumber.getText();
		if (!roomName.matches("[-]?\\d+[.]?\\d*")) {
			notValid.setVisible(true);
		} else {
			VertexType type = controller.requestSearchVertex(map.getMapID(), ID).getType();
			LinkedList<String> aliases = null;
			if (type.getType().equals("Room"))
				aliases = ((RoomType) type).getAliases();
			RoomType room = new RoomType(roomNumber.getText(), aliases);
			controller.requestChangeVertexType(map.getMapID(), ID, room);
			circle = map.reloade(circle);
			vertexViewInfoController.setVisible(true, room);
			notValid.setVisible(false);
		}
	}

	private void buildingNumberClear(boolean isEntrance) {
		if (!isEntrance) {
			building.getEditor().clear();
		} else {
			VertexType type = controller.requestSearchVertex(map.getMapID(), ID).getType();
			if (!type.getType().equals("Entrance"))
				return;
			String i = ((EntranceType) type).getBuildingNumber();
			if (i.matches(buildingController.getBuildingRegex()))
				building.getEditor().setText(i);

		}

	}

	private void entrance(Boolean isSelected) {
		if (ID < 0)
			return;
		if (isSelected && !building.isVisible()) {
			addEntrance(buildingController.getBuilding());
			setSelected(entrance);
		}
		if (!isSelected && !controller.requestSearchVertex(map.getMapID(), ID).getType().getType().equals("Default")) {
			DefaultType type = new DefaultType();
			controller.requestChangeVertexType(map.getMapID(), ID, type);
			if (circle != null) {
				draftsman.change(circle, type);
				circle = map.reloade(circle);
			}
			building.getEditor().clear();
		}
	}

	private void change(CheckBox type, boolean set) {
		vertexViewInfoController.setVisible(false, null);
		VertexType vertextype = new DefaultType();
		if (set) {
			switch (type.getId()) {
			case "stairway":
				setSelected(stairway);
				vertextype = new StairsType();
				break;
			case "elevator":
				setSelected(elevator);
				vertextype = new ElevatorType();
				break;
			case "room":
				setSelected(room);
				vertextype = new RoomType();
				roomNumber.clear();
				vertexViewInfoController.setVisible(true, (RoomType) vertextype);
				break;
			default:
				break;
			}
		}
		if (ID != -1
				&& !controller.requestSearchVertex(map.getMapID(), ID).getType().getType().equals(vertextype.getType())
				&& !type.getId().equals("room") && !entrance.isSelected()) {
			controller.requestChangeVertexType(map.getMapID(), ID, vertextype);
			draftsman.change(circle, vertextype);
			circle = map.reloade(circle);
		}
	}

	private void checkBoxInit() {
		check = new HashSet<CheckBox>();
		check.add(entrance);
		check.add(room);
		check.add(elevator);
		check.add(stairway);
	}

	private void vertexInfo(Vertex vertex) {
		mapOptions.setDisable(!buildingController.isGround(map.getMapID()));
		building.setVisible(map.getMapID() == 0);
		floorOptions.setDisable(map.getMapID() == 0);
		VertexType type = vertex.getType();
		switch (type.getType()) {
		case "Elevator":
			setSelected(elevator);
			break;
		case "Stairs":
			setSelected(stairway);
			break;
		case "Room":
			setSelected(room);
			RoomType room = (RoomType) vertex.getType();
			roomNumber.setText(room.getName());
			vertexViewInfoController.setVisible(true, room);
			break;
		case "Entrance":
			setSelected(entrance);
			String i = ((EntranceType) type).getBuildingNumber();
			if (i.matches(buildingController.getBuildingRegex()))
				building.setValue(((EntranceType) type).getBuildingNumber());
			break;
		default:
			break;
		}
	}

	private void setSelected(CheckBox box) {
		box.setSelected(true);
		for (CheckBox b : check) {
			if (b != box) {
				b.setSelected(false);
			}
		}
	}

	private void addEntrance(String b) {
		if (b != null && building.getItems().contains(b)) {
			String buildingOfEntrance = null;
			VertexType type = controller.requestSearchVertex(map.getMapID(), ID).getType();
			if (type.getType().equals("Entrance"))
				buildingOfEntrance = ((EntranceType) type).getBuildingNumber();
			if (!b.equals(buildingOfEntrance)) {
				type = new EntranceType(b);
				controller.requestChangeVertexType(map.getMapID(), ID, type);
				draftsman.change(circle, type);
			}
		}
	}

	protected void hide() {
		notValid.setVisible(false);
		vertexView.setVisible(false);
		ID = -1;
		vertexID.setText("" + ID);
		circle = null;
		for (CheckBox b : check) {
			b.setSelected(false);
		}
		building.getEditor().clear();
		vertexViewInfoController.setVisible(false, null);
	}

	protected void show(Circle circle, Vertex vertex) {
		hide();
		vertexView.setVisible(true);
		ID = vertex.getId();
		this.vertexID.setText("" + ID);
		this.circle = circle;
		vertexInfo(vertex);
	}

	protected String addInfo(String info) {
		if (info.equals(""))
			return null;
		if (controller.requestAddAlias(map.getMapID(), ID, info).equals(OnError.SuccessfulAdd))
			return info;
		return null;
	}

	protected String deleteInfo(String info) {
		if (controller.requestRemoveAlias(map.getMapID(), ID, info).equals(OnError.SuccessfulAdd))
			return info;
		return "";
	}

	protected VBox getVertexView() {
		return vertexView;
	}

	protected void setDraftsman(Draughtsman draftsman) {
		this.draftsman = draftsman;
	}

	protected void setController(MapController controller) {
		this.controller = controller;
	}

	protected void setMap(AdminMapViewController map) {
		this.map = map;
	}

	protected void setBuildingController(BuildingViewController buildingController) {
		this.buildingController = buildingController;
		building.setItems(buildingController.getBuildingList());
	}

	public void successfulBuildingAdd(boolean successfulAdd, String building) {
		if (successfulAdd && ID != -1) {
			addEntrance(building);
			this.building.setValue(building);
		} else if (circle != null && ID != -1) {
			show(circle, controller.requestSearchVertex(map.getMapID(), ID));
		}
	}
}
