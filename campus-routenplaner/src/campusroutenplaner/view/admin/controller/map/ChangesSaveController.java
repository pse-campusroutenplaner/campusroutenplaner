package campusroutenplaner.view.admin.controller.map;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.layout.VBox;

public class ChangesSaveController {

    @FXML
    private VBox changesNotSave;
    private MapOptionController parent;

    @FXML
    void change(ActionEvent event) {
    	parent.setChanges();
    }

    @FXML
    void noChanges(ActionEvent event) {
    	parent.noChanges();
    }

	protected void setParent(MapOptionController parent) {
		this.parent = parent;
	}
    
}
