package campusroutenplaner.view.admin.controller.map;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class NotSameTypeController {

	@FXML
	private VBox notSameType;
	@FXML
	private Label vertexType;
	private Stage stage;

	@FXML
	void close(ActionEvent event) {
		stage.close();
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

}
