package campusroutenplaner.view.admin.controller.map;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import campusroutenplaner.controller.main.BackgroundController;
import campusroutenplaner.controller.main.MapController;
import campusroutenplaner.view.admin.controller.Mode;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class MapOptionController {

	@FXML
	private FlowPane mapOption;
	@FXML
	private ToggleButton scale;
	@FXML
	private ToggleButton showMeasure;
	@FXML
	private TextField measure;
	@FXML
	private Label message;
	private AdminMapViewController map;
	private MapController controller;
	private BackgroundController backgroundController;
	private BufferedImage background;
	private Stage secondaryStage;

	@FXML
	private void initialize() {
		message.setVisible(false);
		measure.disableProperty().bind(showMeasure.selectedProperty().not());
		showMeasure.selectedProperty().addListener((o, oldval, newval) -> setMesure(!newval));
		secondaryStage = new Stage();
	}

	@FXML
	private void scale(ActionEvent event) {
		if (scale.isSelected()) {
			map.setMode(Mode.IMAGESCALE);
		} else {
			map.finishScale();
		}
	}

	@FXML
	private void setChanges(ActionEvent event) {
		setChanges();
	}

	@FXML
	private void showFileChooser(ActionEvent event) {
		message.setVisible(false);
		FileChooser fileChooser = new FileChooser();
		configureFileChooser(fileChooser);
		File file = fileChooser.showOpenDialog(secondaryStage);
		if (file != null) {
			message.setText(file.getAbsolutePath());
			background = createImage();
			map.showImage(background);
		} else {
			message.setText("Keine Datei ausgewählt");
		}
	}

	private void configureFileChooser(FileChooser fileChooser) {
		fileChooser.setTitle("Kartenmaterial auswählen");
		fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
		fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("PNG", "*.png"),
				new FileChooser.ExtensionFilter("JPG", "*.jpg"));
	}

	private BufferedImage createImage() {
		try {
			return ImageIO.read(new File(message.getText()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void setMesure(boolean setRequest) {
		message.setVisible(false);
		if (!setRequest)
			return;
		String m = measure.getText();
		if (m.matches("\\d+[.]?\\d*")) {
			controller.requestSetScale(map.getMapID(), Double.parseDouble(measure.getText()) / 50);
		} else {
			message.setVisible(true);
			message.setText("Die Eingabe ist keine Nummer");
			message.setTextFill(Color.RED);
			measure.setText("" + backgroundController.requestGetBackground(map.getMapID()).getScale() * 50);
		}
	}

	private void userMessage() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("../../FXMLfiles/ChangesNotSave.fxml"));
			Parent root = loader.load();
			ChangesSaveController changesSave = loader.getController();
			changesSave.setParent(this);
			Scene scene = new Scene(root);
			secondaryStage.setScene(scene);
			secondaryStage.setOnCloseRequest(e -> noChanges());
			secondaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void setChanges() {
		message.setVisible(false);
		if (background != null)
			backgroundController.requestSaveBackgroundImage(map.getMapID(), background);
		if (showMeasure.isSelected())
			setMesure(true);
		map.finishScale();
		noChanges();
	}

	protected void close(boolean isOpen) {
		message.setVisible(false);
		if (isOpen) {
			measure.setText("" + controller.requestGetBackground(map.getMapID()).getScale() * 50);
			return;
		}
		if (!map.getMode().equals(Mode.IMAGESCALE) && !map.newImageNotSave()
				&& measure.getText().matches("\\d+[.]?\\d*") && controller.requestGetBackground(map.getMapID())
						.getScale() == Double.parseDouble(measure.getText()) / 50) {
			noChanges();
		} else {
			userMessage();
		}
	}

	protected void noChanges() {
		scale.setSelected(false);
		showMeasure.setSelected(false);
		background = null;
		map.setMode(Mode.STANDERD);
		map.reloade();
		secondaryStage.close();
	}

	protected ToggleButton getShowMeasure() {
		return showMeasure;
	}

	protected ToggleButton getScale() {
		return scale;
	}

	protected void setMap(AdminMapViewController map) {
		this.map = map;
		map.setDraftsman(showMeasure);
	}

	protected void setController(MapController controller) {
		this.controller = controller;
	}

	protected void setBackgroundController(BackgroundController backgroundController) {
		this.backgroundController = backgroundController;
		measure.setText("" + backgroundController.requestGetBackground(map.getMapID()).getScale() * 50);
	}
}
