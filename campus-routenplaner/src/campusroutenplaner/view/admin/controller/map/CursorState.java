package campusroutenplaner.view.admin.controller.map;

public enum CursorState {
	DEFAULT,
    DRAG,
    NW_RESIZE,
    SW_RESIZE,
    NE_RESIZE,
    SE_RESIZE,
    E_RESIZE,
    W_RESIZE,
    N_RESIZE,
    S_RESIZE;
}
