package campusroutenplaner.view.admin.controller.map;

import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Rectangle;

public class BackgroundScale {
	public interface OnDragResizeEventListener {
		void onDrag(Node node, double x, double y, double h, double w);

		void onResize(Node node, double x, double y, double h, double w);
	}

	private double clickX, clickY, nodeX, nodeY, nodeH, nodeW;
	private CursorState state = CursorState.DEFAULT;
	private Rectangle map;
	private OnDragResizeEventListener defaultListener;
	private int margin;

	public BackgroundScale(Rectangle map) {
		margin = 8;
		this.map = map;
		defaultListener = new OnDragResizeEventListener() {
			@Override
			public void onDrag(Node node, double x, double y, double h, double w) {
				setNodeSize(node, x, y, h, w);
			};

			@Override
			public void onResize(Node node, double x, double y, double h, double w) {
				setNodeSize(node, x, y, h, w);
			};

			private void setNodeSize(Node node, double x, double y, double h, double w) {
				node.setLayoutX(x);
				node.setLayoutY(y);
				if (node instanceof Canvas) {
					((Canvas) node).setWidth(w);
					((Canvas) node).setHeight(h);
				} else if (node instanceof Rectangle) {
					((Rectangle) node).setWidth(w);
					((Rectangle) node).setHeight(h);
				}
			}
		};
	}

	protected void mouseReleased(MouseEvent event) {
		map.setCursor(Cursor.DEFAULT);
		state = CursorState.DEFAULT;
	}

	protected void mouseOver(MouseEvent event) {
		CursorState state = currentMouseState(event);
		Cursor cursor = getCursorForState(state);
		map.setCursor(cursor);
	}

	private CursorState currentMouseState(MouseEvent event) {
		CursorState state = CursorState.DEFAULT;
		boolean left = isLeftResizeZone(event);
		boolean right = isRightResizeZone(event);
		boolean top = isTopResizeZone(event);
		boolean bottom = isBottomResizeZone(event);

		if (left && top)
			state = CursorState.NW_RESIZE;
		else if (left && bottom)
			state = CursorState.SW_RESIZE;
		else if (right && top)
			state = CursorState.NE_RESIZE;
		else if (right && bottom)
			state = CursorState.SE_RESIZE;
		else if (right)
			state = CursorState.E_RESIZE;
		else if (left)
			state = CursorState.W_RESIZE;
		else if (top)
			state = CursorState.N_RESIZE;
		else if (bottom)
			state = CursorState.S_RESIZE;
		else if (isInDragZone(event))
			state = CursorState.DRAG;

		return state;
	}

	private Cursor getCursorForState(CursorState state) {
		switch (state) {
		case NW_RESIZE:
			return Cursor.NW_RESIZE;
		case SW_RESIZE:
			return Cursor.SW_RESIZE;
		case NE_RESIZE:
			return Cursor.NE_RESIZE;
		case SE_RESIZE:
			return Cursor.SE_RESIZE;
		case E_RESIZE:
			return Cursor.E_RESIZE;
		case W_RESIZE:
			return Cursor.W_RESIZE;
		case N_RESIZE:
			return Cursor.N_RESIZE;
		case S_RESIZE:
			return Cursor.S_RESIZE;
		default:
			return Cursor.DEFAULT;
		}
	}

	protected void mouseDragged(MouseEvent event) {
		if (defaultListener != null) {
			double mouseX = parentX(event.getX());
			double mouseY = parentY(event.getY());
			if (state == CursorState.DRAG) {
				defaultListener.onDrag(map, mouseX - clickX, mouseY - clickY, nodeH, nodeW);
			} else if (state != CursorState.DEFAULT) {
				// resizing
				double newX = nodeX;
				double newY = nodeY;
				double newH = nodeH;
				double newW = nodeW;
				
				// Right Resize
				if (state == CursorState.E_RESIZE || state == CursorState.NE_RESIZE || state == CursorState.SE_RESIZE) {
					newW = mouseX - nodeX;
				}
				// Left Resize
				if (state == CursorState.W_RESIZE || state == CursorState.NW_RESIZE || state == CursorState.SW_RESIZE) {
					newX = mouseX;
					newW = nodeW + nodeX - newX;
				}

				// Bottom Resize
				if (state == CursorState.S_RESIZE || state == CursorState.SE_RESIZE || state == CursorState.SW_RESIZE) {
					newH = mouseY - nodeY;
				}
				// Top Resize
				if (state == CursorState.N_RESIZE || state == CursorState.NW_RESIZE || state == CursorState.NE_RESIZE) {
					newY = mouseY;
					newH = nodeH + nodeY - newY;
				}
				defaultListener.onResize(map, newX, newY, newH, newW);
			}
		}
	}

	protected void mousePressed(MouseEvent event) {

		if (isInResizeZone(event)) {
			setNewInitialEventCoordinates(event);
			state = currentMouseState(event);
		} else if (isInDragZone(event)) {
			setNewInitialEventCoordinates(event);
			state = CursorState.DRAG;
		} else {
			state = CursorState.DEFAULT;
		}
	}

	private void setNewInitialEventCoordinates(MouseEvent event) {
		nodeX = nodeX();
		nodeY = nodeY();
		nodeH = nodeH();
		nodeW = nodeW();
		clickX = event.getX();
		clickY = event.getY();
	}

	private boolean isInResizeZone(MouseEvent event) {
		return isLeftResizeZone(event) || isRightResizeZone(event) || isBottomResizeZone(event)
				|| isTopResizeZone(event);
	}

	private boolean isInDragZone(MouseEvent event) {
		double xPos = parentX(event.getX());
		double yPos = parentY(event.getY());
		double nodeX = nodeX() + margin;
		double nodeY = nodeY() + margin;
		double nodeX0 = nodeX() + nodeW() - margin;
		double nodeY0 = nodeY() + nodeH() - margin;

		return (xPos > nodeX && xPos < nodeX0) && (yPos > nodeY && yPos < nodeY0);
	}

	private boolean isLeftResizeZone(MouseEvent event) {
		return intersect(0, event.getX());
	}

	private boolean isRightResizeZone(MouseEvent event) {
		return intersect(nodeW(), event.getX());
	}

	private boolean isTopResizeZone(MouseEvent event) {
		return intersect(0, event.getY());
	}

	private boolean isBottomResizeZone(MouseEvent event) {
		return intersect(nodeH(), event.getY());
	}

	private boolean intersect(double side, double point) {
		return side + margin > point && side - margin < point;
	}

	private double parentX(double localX) {
		return nodeX() + localX;
	}

	private double parentY(double localY) {
		return nodeY() + localY;
	}

	private double nodeX() {
		return map.getBoundsInParent().getMinX();
	}

	private double nodeY() {
		return map.getBoundsInParent().getMinY();
	}

	private double nodeW() {
		return map.getBoundsInParent().getWidth();
	}

	private double nodeH() {
		return map.getBoundsInParent().getHeight();
	}
}
