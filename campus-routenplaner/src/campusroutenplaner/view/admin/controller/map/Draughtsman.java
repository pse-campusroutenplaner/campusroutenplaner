package campusroutenplaner.view.admin.controller.map;

import campusroutenplaner.model.map.VertexType;
import javafx.event.EventHandler;
import javafx.scene.control.ToggleButton;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

public class Draughtsman {
	private int defaultRadius = 5;
	private double circleRadius = 5;
	private Line measure;
	private Color vertex = Color.MEDIUMSEAGREEN;
	private Color blocked = Color.RED;
	private Color entrance = Color.YELLOW;
	private Color stairs = Color.CORAL;
	private Color room = Color.LIGHTSEAGREEN;

	public Draughtsman() {
		measure = new Line();
		showMeasure(true);
		measure.setStroke(Color.ORANGERED);
		measure.setStrokeWidth(7);
		measure.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent t) {
				measure.setStartX(t.getX() - 25);
				measure.setStartY(t.getY());
				measure.setEndX(t.getX() + 25);
				measure.setEndY(t.getY());
			}
		});
	}
	
	private void showMeasure(Boolean newVal) {
		if (newVal) {
			measure.setStartX(100);
			measure.setStartY(100);
			measure.setEndX(150);
			measure.setEndY(100);
		}
	}
	
	public Circle drawVertex(double x, double y, VertexType type) {
		Circle vertex = new Circle();
		vertex.setCenterX(x);
		vertex.setCenterY(y);
		vertex.setRadius(circleRadius);
		vertex.setFill(setColor(type));
		return vertex;
	}

	private Color setColor(VertexType type) {
		switch (type.getType()) {
		case "Entrance":
			return entrance;
		case "Elevator":
		case "Stairs":
			return stairs;
		case "Room":
			return room;
		default:
			return vertex;
		}
	}

	public void vertexSelected(Circle circle){
		DropShadow e = new DropShadow();
		e.setWidth(50);
		e.setHeight(50);
		e.setSpread(0.8);
		circle.setEffect(e);
	}
	
	public void deselected(Circle circle) {
		circle.setEffect(null);;
	}

	public Line drawEdge(double x1, double y1, double x2, double y2) {
		Line edge =  new Line(x1, y1, x2, y2);
		edge.setStrokeWidth(2);
		return edge;
	}
	
	public void setBlocked(Line line, boolean isblocked) {
		if (isblocked)
			line.setStroke(blocked);
	}

	public int getCircleRadius() {
		return defaultRadius;
	}

	public void setCircleRadius(double circleRadius) {
		this.circleRadius = circleRadius;
	}

	public void change(Circle circle, VertexType type) {
		circle.setFill(setColor(type));
	}
	public Color getScaleStroke() {
		return Color.BLUE;
	}
	
	public Color getStroke() {
		return Color.TRANSPARENT;
	}
	
	protected Line getMeasure() {
		return measure;
	}
	
	protected void setMeasureButton(ToggleButton button) {
		measure.visibleProperty().bind(button.selectedProperty());
		measure.visibleProperty().addListener((o, oldVal, newVal) -> showMeasure(newVal));
	}
	
	public Color getNoMap() {
		return Color.BLACK;
	}
}
