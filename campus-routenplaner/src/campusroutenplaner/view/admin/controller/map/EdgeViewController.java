package campusroutenplaner.view.admin.controller.map;

import java.io.IOException;

import campusroutenplaner.controller.main.MapController;
import campusroutenplaner.model.map.Edge;
import campusroutenplaner.view.admin.controller.constructionSites.DateSetterController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

public class EdgeViewController {

	@FXML
	private VBox edgeView;
	@FXML
	private Label edgeID;
	@FXML
	private CheckBox blocked;
	@FXML
	private CheckBox accessible;
	@FXML
	private TextField length;
	@FXML
	private Label NaN;
	@FXML
	private Group constructionSite;
	@FXML
	private Label from;
	@FXML
	private Label to;
	private Stage stage;
	private int ID;
	private Line line;
	private MapController controller;
	private AdminMapViewController map;

	@FXML
	private void initialize() {
		NaN.setVisible(false);
		stage = new Stage();
		accessible.selectedProperty().addListener((o, oldVal, newVal) -> setAccessible(newVal));
		blocked.selectedProperty().addListener((o, oldVal, newVal) -> addConstructionSite(newVal));
		hide();
	}

	@FXML
	private void changeLength(ActionEvent event) {
		NaN.setVisible(false);
		String l = length.getText();
		if (l.matches("\\d*")) {
			controller.requestChangeEdgeLength(map.getMapID(), ID, Integer.parseInt(l));
		} else {
			NaN.setVisible(true);
		}
	}

	@FXML
	private void delete(ActionEvent event) {
		if (controller.requestSearchEdge(map.getMapID(), ID).isBlocked()) {
			DateSetterController d = addConstructionSite(true);
			if (d != null)
				d.deleteConstructionSite(ID);
			stage.close();
		}
		map.deleteEdge(line);
		controller.requestDeleteEdge(map.getMapID(), ID);
		hide();
	}

	private void setAccessible(boolean isAccessible) {
		if (!controller.requestSearchEdge(map.getMapID(), ID).isAccessible() == isAccessible) {
			controller.requestChangeEdgeAccessible(map.getMapID(), ID, isAccessible);
		}
	}

	private DateSetterController addConstructionSite(boolean isBlocked) {
		if (isBlocked) {
			blocked.setSelected(false);
			try {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("../../FXMLfiles/DateSetter.fxml"));
				Parent root = loader.load();
				DateSetterController date = loader.getController();
				date.setEdgeID(ID);
				date.setStage(stage);
				date.setEdgeViewController(this);
				Scene scene = new Scene(root);
				stage.setTitle("Zeitraum der Sperre wählen");
				stage.setScene(scene);
				stage.show();
				return date;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	protected void hide() {
		edgeView.setVisible(false);
		NaN.setVisible(false);
	}

	protected void show(Line line, Edge edge) {
		hide();
		edgeView.setVisible(true);
		this.line = line;
		ID = edge.getId();
		edgeID.setText("" + ID);
		blocked.setSelected(edge.isBlocked());
		blocked.setDisable(edge.isBlocked());
		accessible.setSelected(edge.isAccessible());
		length.setText("" + ((Double) edge.getLength()).intValue());
		if (stage.isShowing())
			stage.close();
	}

	protected void unBlockedEdge(int edgeID) {
		controller.requestChangeEdgeBlocked(map.getMapID(), edgeID, false);
	}

	protected void setController(MapController controller) {
		this.controller = controller;
	}

	protected void setMap(AdminMapViewController map) {
		this.map = map;
	}

	public void successfulAdd(boolean successfulAdd) {
		if (successfulAdd && !controller.requestSearchEdge(map.getMapID(), ID).isBlocked()) {
			controller.requestChangeEdgeBlocked(map.getMapID(), ID, successfulAdd);
			if (controller.requestSearchEdge(map.getMapID(), ID).isBlocked())
				line.setStroke(Color.RED);
			blocked.setDisable(true);
		}
	}
}
