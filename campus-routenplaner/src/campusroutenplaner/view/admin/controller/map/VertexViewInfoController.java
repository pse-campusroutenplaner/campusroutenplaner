package campusroutenplaner.view.admin.controller.map;

import campusroutenplaner.model.map.RoomType;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

public class VertexViewInfoController {

    @FXML
    private VBox vertexViewInfo;
    @FXML
    private ListView<String> aliases;
    @FXML
    private TextField info;
    @FXML
    private VBox noChoise;
    @FXML
    private VertexViewController parent;
    private ObservableList<String> content;
    
    @FXML
	private void initialize() {
		noChoise.setVisible(false);
	}
    
    @FXML
    private void add(ActionEvent event) {
    	noChoise.setVisible(false);
    	String information = parent.addInfo(info.getText());
    	if (information != null)
    		content.add(information);
    	info.clear();
    }

    @FXML
    private void delete(ActionEvent event) {
    	noChoise.setVisible(false);
    	String information = aliases.getSelectionModel().getSelectedItem();
    	if (information != null) {
    		content.remove(parent.deleteInfo(information));
    	} else {
    		noChoise.setVisible(true);
    	}
    }

	protected void setParent(VertexViewController parent) {
		this.parent = parent;
	}

	protected void setVisible(boolean isRoom, RoomType room) {
		info.clear();
		vertexViewInfo.setVisible(isRoom);
		if (isRoom) {
			content = FXCollections.observableArrayList(room.getAliases());
			aliases.setItems(content);
		}
	}
}
