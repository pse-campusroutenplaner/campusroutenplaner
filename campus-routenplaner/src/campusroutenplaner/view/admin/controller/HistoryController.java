package campusroutenplaner.view.admin.controller;

import java.io.IOException;

import campusroutenplaner.model.database.Subject;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * This class offers the ability to recognize database changes. Further on
 * notify subject after every 10 minutes to synchronize;
 * 
 * @author Thieu Ha Minh
 *
 */
public class HistoryController implements Observer {

	@FXML
	private VBox history;
	@FXML
	private ListView<String> historyList;
	@FXML
	private CheckBox autosync;
	@FXML
	private AdminSceneController parent;
	private ObservableList<String> content;
	private Timeline timeline;
	private Subject subject;
	private Scene scene;

	@FXML
	private void manualSync(ActionEvent event) {
		startSync(true);
	}

	private void setTimer() {
		if (timeline == null) {
			timeline = new Timeline(new KeyFrame(Duration.minutes(10), ae -> startSync(false)));
			timeline.setCycleCount(Animation.INDEFINITE);
		}
		timeline.playFromStart();
	}

	private void doSync(Stage syncStage) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				if (syncStage.isShowing()) {
					if (subject.doSync()) {
						syncStage.close();
					} else {
						parent.multipleAdmin(syncStage);
						scene.setCursor(Cursor.DEFAULT);
					}
				}
			}
		});
	}
	
	protected boolean isUploadNeeded() {
		return subject.isUploadNeeded();
	}

	protected void startSync(boolean isManual) {
		if (autosync.isSelected() || isManual) {
			Stage syncStage = new Stage();
			try {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("../FXMLfiles/Sync.fxml"));
				Parent root = loader.load();
				Scene scene = new Scene(root);
				scene.setCursor(Cursor.WAIT);
				syncStage.setScene(scene);
				syncStage.setTitle("Synchronisiert...");
				syncStage.show();
				this.scene.setCursor(Cursor.WAIT);
				doSync(syncStage);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	protected boolean discardChanges() {
		subject.discardChanges();
		return true;
	}

	protected void setParent(AdminSceneController parent) {
		this.parent = parent;
	}

	protected void logout() {
		subject.unregister();
		timeline.stop();
	}

	@Override
	public void update(String message) {
		content.add(message);
		scene.setCursor(Cursor.DEFAULT);
	}

	@Override
	public void setSubject(Subject subject) {
		if (subject != null) {
			this.subject = subject;
			subject.register(this);
			content = FXCollections.observableArrayList("hier ist ein Ereignislog");
			historyList.setItems(content);
			historyList.setVisible(true);
			setTimer();
		}
	}

	public void setScene(Scene scene) {
		this.scene = scene;
	}
}