package campusroutenplaner.view.admin.controller.constructionSites;

import java.time.LocalDate;

import campusroutenplaner.controller.usercontroller.ConstructionSiteController;
import campusroutenplaner.model.constructionsites.ConstructionSite;
import campusroutenplaner.model.map.OnError;
import campusroutenplaner.view.admin.controller.AdminSceneController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;

/**
 * All construction sites are listed.
 * 
 * @author Thieu Ha Minh
 *
 */
public class ConstructionSiteListController {

	@FXML
	private GridPane constructionSiteList;
	@FXML
	private TableView<ConstructionSite> table;
	@FXML
	private TableColumn<ConstructionSite, String> place;
	@FXML
	private TableColumn<ConstructionSite, LocalDate> beginDate;
	@FXML
	private TableColumn<ConstructionSite, LocalDate> endDate;
	@FXML
	private Label noSelection;
	@FXML
	private DateSetterController dateSetterController;
	@FXML
	private AdminSceneController parent;
	private ConstructionSiteController controller;

	@FXML
	private void delete(ActionEvent event) {
		noSelection.setVisible(false);
		ConstructionSite c = table.getSelectionModel().getSelectedItem();
		if (c != null) {
			if (controller.requestRemoveConstructionSide(c.getId()).equals(OnError.SuccessfulRemoved))
				parent.unBlockedEdge(c.getEdgeId());
		} else {
			noSelection.setVisible(true);
		}
	}

	@FXML
	private void showDateSetter(ActionEvent event) {
		noSelection.setVisible(false);
		ConstructionSite c = table.getSelectionModel().getSelectedItem();
		if (c != null) {
			dateSetterController.show(c.getId());
			dateSetterController.setEdgeID(Integer.parseInt(c.getPlace()));
			dateSetterController.getDateSetter().setVisible(true);
		} else {
			noSelection.setVisible(true);
		}
	}

	private void initialize() {
		table.setItems(controller.requestGetConstructionSites());
		place.setCellValueFactory(new PropertyValueFactory<ConstructionSite, String>("place"));
		beginDate.setCellValueFactory(new PropertyValueFactory<ConstructionSite, LocalDate>("beginDate"));
		endDate.setCellValueFactory(new PropertyValueFactory<ConstructionSite, LocalDate>("endDate"));
		noSelection.setVisible(false);
		dateSetterController.getDateSetter().setVisible(false);
	}

	public void setParent(AdminSceneController parent) {
		this.parent = parent;
	}

	/**
	 * Setter for ConsreuctionSiteController. Is needed to get the list of
	 * construction sites.
	 * 
	 * @param controller
	 *            ConsreuctionSiteController
	 */
	public void setController(ConstructionSiteController controller) {
		this.controller = controller;
		dateSetterController.setController(controller);
		initialize();
	}
}