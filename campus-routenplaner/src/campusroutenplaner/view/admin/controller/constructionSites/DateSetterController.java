package campusroutenplaner.view.admin.controller.constructionSites;

import java.time.LocalDate;

import campusroutenplaner.controller.usercontroller.ConstructionSiteController;
import campusroutenplaner.model.constructionsites.ConstructionSite;
import campusroutenplaner.model.map.OnError;
import campusroutenplaner.view.admin.controller.map.EdgeViewController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

/**
 * @author Thieu Ha Minh
 *
 */
public class DateSetterController {

	@FXML
	private FlowPane dateSetter;
	@FXML
	private DatePicker begin;
	@FXML
	private DatePicker end;
	@FXML
	private CheckBox indeterminate;
	@FXML
	private Button finish;
	@FXML
	private Button change;
	@FXML
	private Label invalidTime;
	private Stage stage;
	private int edgeID = -1;
	private LocalDate indeterminateDate = LocalDate.of(9999, 12, 31);
	private EdgeViewController edgeView;
	private static ConstructionSiteController controller;

	@FXML
	private void finish(ActionEvent event) {
		invalidTime.setVisible(false);
		addConstructionSite(edgeID, begin.getValue(), end.getValue());
	}

	@FXML
	private void indeterminate(ActionEvent event) {
		invalidTime.setVisible(false);
		begin.setValue(null);
		end.setValue(null);
		if (change.isVisible()) {
			changeTimeScale(edgeID, LocalDate.now(), indeterminateDate);
		} else {
			addConstructionSite(edgeID, LocalDate.now(), indeterminateDate);
		}
	}

	@FXML
	private void onAction(ActionEvent event) {
		indeterminate.setSelected(false);
		if (!change.isVisible())
			finish.setVisible(true);
	}

	@FXML
	private void changeTimescale(ActionEvent event) {
		invalidTime.setVisible(false);
		changeTimeScale(edgeID, begin.getValue(), end.getValue());
	}

	private void changeTimeScale(int edgeID, LocalDate begin, LocalDate end) {
		if (edgeID == -1 && controller == null)
			return;
		if (begin == null || end == null || end.isBefore(begin)) {
			invalidTime.setVisible(true);
			return;
		}
		int ID = controller.requestGetConstructionSiteId(edgeID);
		controller.requestChangeConstructionSite(ID, begin, end, edgeID);
		edgeID = -1;
		dateSetter.setVisible(false);
	}

	private void addConstructionSite(int edgeID, LocalDate begin, LocalDate end) {
		if (begin == null || end == null || end.isBefore(begin)) {
			invalidTime.setVisible(true);
			return;
		}
		if (stage != null)
			stage.close();
		if (edgeID == -1 && controller == null)
			return;
		boolean successfulAdd = false;
		if (controller.requestAddConstructionSite(edgeID, begin, end).equals(OnError.SuccessfulAdd))
			successfulAdd = true;
		if (edgeView != null)
			edgeView.successfulAdd(successfulAdd);
		finish.setVisible(false);
		edgeID = -1;
	}

	protected void show(int constructionSiteID) {
		dateSetter.setVisible(true);
		change.setVisible(true);
		finish.setVisible(false);
		ConstructionSite c = controller.requestGetConstructionSite(constructionSiteID);
		if (c.getEndDate().equals(indeterminateDate)) {
			indeterminate.setSelected(true);
		} else {
			begin.setValue(c.getBeginDate());
			end.setValue(c.getEndDate());
		}
	}

	protected void setController(ConstructionSiteController controller) {
		DateSetterController.controller = controller;
	}

	public void deleteConstructionSite(int edgeID) {
		controller.requestRemoveConstructionSide(controller.requestGetConstructionSiteId(edgeID));
	}

	public FlowPane getDateSetter() {
		return dateSetter;
	}

	public void setEdgeID(int edgeID) {
		this.edgeID = edgeID;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public void setEdgeViewController(EdgeViewController edgeView) {
		this.edgeView = edgeView;
		change.setVisible(false);
	}
}
