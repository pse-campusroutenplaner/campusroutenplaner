package campusroutenplaner.view.admin.controller.building;

import java.io.IOException;

import campusroutenplaner.controller.main.BuildingController;
import campusroutenplaner.view.admin.controller.AdminSceneController;
import campusroutenplaner.view.admin.controller.map.VertexViewController;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * Class to show building informations in a structured TreeView
 * 
 * @author Thieu Ha Minh
 *
 */
public class BuildingViewController {
	@FXML
	private GridPane buildingView;
	@FXML
	private ComboBox<String> building;
	@FXML
	private Label notValid;
	@FXML
	private TreeView<String> informations;
	@FXML
	private TitledPane changes;
	@FXML
	private TextField newName;
	@FXML
	private TextField newAdress;
	@FXML
	private AdminSceneController parent;
	private FloorListController floorList;
	private BuildingController controller;
	private Stage userMessage;
	private String buildingRegex;
	private ObservableList<String> buildingList;
	private TreeItem<String> name;
	private TreeItem<String> adress;

	@FXML
	private void changeAdress(ActionEvent event) {
		String showingBuilding = building.getSelectionModel().getSelectedItem();
		if (showingBuilding.equals(""))
			return;
		if (controller.requestChangeBuildingAddress(showingBuilding, newAdress.getText()) != null)
			fillInformation(showingBuilding);
	}

	@FXML
	private void changeName(ActionEvent event) {
		String showingBuilding = building.getSelectionModel().getSelectedItem();
		if (showingBuilding.equals(""))
			return;
		if (controller.requestChangeBuildingName(showingBuilding, newName.getText()) != null)
			fillInformation(showingBuilding);
	}

	@FXML
	private void delete(ActionEvent event) {
		String b = building.getEditor().getText();
		if (building.getItems().contains(b)) {
			userMessage(b, false, null);
		} else {
			notValid.setVisible(true);
		}
	}

	@FXML
	private void showBuilding(ActionEvent event) {
		notValid.setVisible(false);
		changes.setExpanded(false);
		changes.setVisible(true);
		String b = building.getEditor().getText();
		if (building.getItems().contains(b)) {
			fillInformation(b);
		} else if (b.matches(buildingRegex)) {
			userMessage(b, true, null);
		} else if (!b.equals("")) {
			notValid.setVisible(true);
			changes.setVisible(false);
		}
	}

	private void fillInformation(String buildingNumber) {
		newName.clear();
		newAdress.clear();
		if (building.getItems().contains(buildingNumber)) {
			controller.requestFloorNumbers(buildingNumber);
			name = new TreeItem<String>("Name: " + controller.requestBuildingName(buildingNumber));
			adress = new TreeItem<String>("Adresse: " + controller.requestBuildingAddress(buildingNumber));
			name.getChildren().add(adress);
			name.getChildren().add(entrances(buildingNumber));
			name.getChildren().add(floors(buildingNumber));
			informations.setRoot(name);
			name.setExpanded(true);
		} else {
			informations.setRoot(null);
		}
	}

	private TreeItem<String> floors(String buildingNumber) {
		TreeItem<String> floors = new TreeItem<String>("Stockwerke");
		for (int i : controller.requestFloorNumbers(buildingNumber)) {
			TreeItem<String> floor = new TreeItem<String>("" + i);
			floors.getChildren().add(floor);
		}
		return floors;
	}

	private TreeItem<String> entrances(String buildingNumber) {
		TreeItem<String> entrances = new TreeItem<String>("Eingänge");
		for (int i : controller.requestEntrances(buildingNumber)) {
			TreeItem<String> entrance = new TreeItem<String>("" + i);
			entrances.getChildren().add(entrance);
		}
		return entrances;
	}

	private void error(boolean isAdding) {
		// TODO: Error message cant add building or delete buiding
	}

	/**
	 * close pop-up window
	 * 
	 * @param showBuilding
	 */
	protected void close(boolean showBuilding) {
		if (!showBuilding) {
			building.getEditor().clear();
			changes.setVisible(false);
		}
		fillInformation(building.getEditor().getText());
		userMessage.close();
	}

	protected void successfulDelete(boolean successfulDelete) {
		if (successfulDelete) {
			floorList.campusMap();
			close(false);
		} else {
			error(false);
			close(true);
		}
	}

	protected void successfulAdd(boolean successfulAdd, String addedBulding) {
		if (!successfulAdd) {
			error(true);
		} else {
			building.setValue(addedBulding);
		}
		close(successfulAdd);
	}

	/**
	 * Set pop-up window to start an dialog with the admin, whether the user
	 * want to delete or add an building
	 * 
	 * @param buildingNumber
	 *            of the deleting oder adding building
	 * @param isAdding
	 *            true if adding, false if deleting
	 */
	public void userMessage(String buildingNumber, boolean isAdding, VertexViewController vertex) {
		if (userMessage.isShowing())
			return;
		try {
			Parent root;
			if (isAdding) {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("../../FXMLfiles/AddBuilding.fxml"));
				root = loader.load();
				AddBuildingController add = loader.getController();
				add.setParent(this);
				add.setController(controller);
				add.setBuildingNumber(buildingNumber);
				add.setVertexViewController(vertex);
			} else {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("../../FXMLfiles/DeleteBuilding.fxml"));
				root = loader.load();
				DeleteBuilding delete = loader.getController();
				delete.setParent(this);
				delete.setController(controller);
				delete.setBuildingNumber(buildingNumber);
			}
			Scene scene = new Scene(root);
			userMessage.setScene(scene);
			userMessage.showAndWait();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean isGround(int mapID) {
		if (mapID == 0)
			return true;
		return controller.isGroundFloor(mapID);
	}

	public void tabbed(boolean isShowing) {
		if (isShowing)
			close(false);
	}

	public void show(String buildingNumber) {
		if (building.getItems().contains(buildingNumber))
			building.setValue(buildingNumber);
	}

	public String getBuilding() {
		return floorList.getBuildingNumber();
	}

	public ObservableList<String> getBuildingList() {
		return buildingList;
	}

	public String getBuildingRegex() {
		return buildingRegex;
	}

	public void setFloorList(FloorListController floorList) {
		this.floorList = floorList;
	}

	public void setParent(AdminSceneController parent) {
		this.parent = parent;
	}

	public void setController(BuildingController controller) {
		this.controller = controller;
		floorList.setController(controller);
		buildingList = controller.requestBuildingNumberList();
		building.setItems(buildingList);
		notValid.setVisible(false);
		parent.buildingList();
		buildingRegex = "\\d\\d[.]\\d\\d";
		userMessage = new Stage();
	}
}
