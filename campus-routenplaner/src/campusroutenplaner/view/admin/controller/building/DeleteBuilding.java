package campusroutenplaner.view.admin.controller.building;

import campusroutenplaner.controller.main.BuildingController;
import campusroutenplaner.model.map.OnError;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 * Pop-up window to delete buildings
 * @author Thieu Ha Minh
 *
 */
public class DeleteBuilding {

    @FXML
    private VBox deleteBuilding;
    @FXML
    private Label buildingNumber;
    private BuildingViewController parent;
    private BuildingController controller;

    @FXML
    private void cancle(ActionEvent event) {
    	parent.close(true);
    }

    @FXML
    private void delete(ActionEvent event) {
    	buildingNumber.getText();
    	boolean successfulDelete = true;
    	if (controller.requestDeleteBuilding(buildingNumber.getText()).equals(OnError.UnSuccessfulRemoved))
    		successfulDelete = false;
    	parent.successfulDelete(successfulDelete);
    }

	protected void setBuildingNumber(String buildingNumber) {
		this.buildingNumber.setText(buildingNumber);
	}

	protected void setParent(BuildingViewController parent) {
		this.parent = parent;
	}

	protected void setController(BuildingController controller) {
		this.controller = controller;
	}
}
