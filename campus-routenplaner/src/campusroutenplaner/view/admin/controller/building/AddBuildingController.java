package campusroutenplaner.view.admin.controller.building;

import campusroutenplaner.controller.main.BuildingController;
import campusroutenplaner.view.admin.controller.map.VertexViewController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 * Pop-up window to add buildings
 * 
 * @author Thieu Ha Minh
 *
 */
public class AddBuildingController {

	@FXML
	private VBox addBuilding;
	@FXML
	private Label buildingNumber;
	private BuildingViewController parent;
	private BuildingController controller;
	private VertexViewController vertexView;

	@FXML
	void addBuilding(ActionEvent event) {
		boolean successfulAdd = true;
		if (controller.requestAddBuilding(buildingNumber.getText()) == null)
			successfulAdd = false;
		if (vertexView != null)
			vertexView.successfulBuildingAdd(successfulAdd, buildingNumber.getText());
		parent.successfulAdd(successfulAdd, buildingNumber.getText());
	}

	@FXML
	void cancle(ActionEvent event) {
		if (vertexView != null)
			vertexView.successfulBuildingAdd(false, buildingNumber.getText());
		parent.close(false);
	}

	protected void setParent(BuildingViewController parent) {
		this.parent = parent;
	}

	protected void setController(BuildingController controller) {
		this.controller = controller;
	}

	protected void setBuildingNumber(String buildingNumber) {
		this.buildingNumber.setText(buildingNumber);
	}

	protected void setVertexViewController(VertexViewController vertexView) {
		this.vertexView = vertexView;
	}
}
