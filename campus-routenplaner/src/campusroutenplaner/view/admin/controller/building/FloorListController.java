package campusroutenplaner.view.admin.controller.building;

import java.util.LinkedList;

import campusroutenplaner.controller.main.BuildingController;
import campusroutenplaner.view.admin.controller.map.AdminMapViewController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

/**
 * In the options the user can switch between different maps, by choosing the
 * building and the floor
 * 
 * @author Thieu Ha Minh
 *
 */
public class FloorListController {

	@FXML
	private VBox floorList;
	@FXML
	private Button addFloorUp;
	@FXML
	private VBox floorButtons;
	@FXML
	private Button addFloorDown;
	@FXML
	private ComboBox<String> building;
	@FXML
	private VBox floors;
	@FXML
	private Label message;
	@FXML
	private ToggleButton delete;
	@FXML
	private AdminMapViewController map;
	private String buildingNumber;
	private BuildingController controller;

	@FXML
	private void initialize() {
		floors.setVisible(false);
		message.setVisible(false);
		delete.visibleProperty().bind(floors.visibleProperty());
		delete.selectedProperty().addListener((o, oldVal, newVal) -> delete(newVal));
	}

	@FXML
	private void addFloorDown(ActionEvent event) {
		message.setVisible(false);
		if (addFloorDown.getText().equals("+")) {
			int newFloorNumber = controller.requestAddFloor(buildingNumber, false);
			addFloorButton(newFloorNumber).toFront();
		} else if (addFloorDown.getText().equals("-")) {
			if (controller.requestRemoveFloor(buildingNumber, false) != null)
				floorButtons.getChildren().remove(floorButtons.getChildren().size() - 1);
			deleteFloor();
		}
	}

	// TODO: warnung das stockwerk nicht gelöscht werden konnte
	@FXML
	private void addFloorUp(ActionEvent event) {
		message.setVisible(false);
		if (addFloorDown.getText().equals("+")) {
			int newFloorNumber = controller.requestAddFloor(buildingNumber, true);
			addFloorButton(newFloorNumber).toBack();
		} else if (addFloorDown.getText().equals("-")) {
			if (controller.requestRemoveFloor(buildingNumber, true) != null)
				floorButtons.getChildren().remove(0);
			deleteFloor();
		}

	}

	@FXML
	private void showFloorofBuilding(ActionEvent event) {
		floors.setVisible(false);
		floorButtons.getChildren().clear();
		buildingNumber = building.getValue();
		if (buildingNumber != null) {
			LinkedList<Integer> floors = controller.requestFloorNumbers(buildingNumber);
			if (floors != null) {
				this.floors.setVisible(true);
				for (int i : floors) {
					addFloorButton(i).toBack();
				}
			}
		}
	}
	
	private void deleteFloor() {
		if (floorButtons.getChildren().size() != 0) {
			map.show(controller.requestMapIdOfFloor(buildingNumber, 0));
		} else {
			map.show(0);
			campusMap();
		}
	}

	private void delete(boolean deleteMode) {
		if (deleteMode) {
			addFloorDown.setText("-");
			addFloorUp.setText("-");
		} else {
			addFloorDown.setText("+");
			addFloorUp.setText("+");
		}
	}

	private Button addFloorButton(int floor) {
		Button b = new Button();
		if (floor == Integer.MAX_VALUE) {
			b.setVisible(false);
			message.setText("Ein Stockwerk konnte diesem Gebäude nicht hinzugefügt werden");
			message.setVisible(true);
		}
		b.setText("" + floor);
		floorButtons.getChildren().add(b);
		click(b);
		return b;
	}

	private void click(Button b) {
		b.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				map.show(controller.requestMapIdOfFloor(buildingNumber, Integer.parseInt(b.getText())));
			}
		});
	}

	/**
	 * If the campusMap is shown, all Button concerning floors will be hidden.
	 */
	public void campusMap() {
		floors.setVisible(false);
		building.getSelectionModel().clearSelection();
		map.show(0);
	}

	protected String getBuildingNumber() {
		return buildingNumber;
	}

	/**
	 * Setter for AdminMapViewController. Is needed to be able to show different
	 * maps.
	 * 
	 * @param map
	 *            AdminMapViewController
	 */
	public void setMap(AdminMapViewController map) {
		this.map = map;
	}

	/**
	 * Setter for BuildingController. BuildingController is needed to add floors
	 * and getting the access to all buildings
	 * 
	 * @param controller
	 *            BuildingController
	 */
	public void setController(BuildingController controller) {
		this.controller = controller;
		building.setItems(controller.requestBuildingNumberList());
	}
}
