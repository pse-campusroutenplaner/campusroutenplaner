package campusroutenplaner.view.admin.controller;

import campusroutenplaner.controller.usercontroller.LogInController;
import campusroutenplaner.model.map.OnError;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.VBox;

/**
 * Ability to set a new password 
 * @author Thieu Ha Minh
 *
 */
public class PasswordChangerController {

    @FXML
    private VBox passwordChanger;
    @FXML
    private PasswordField oldPassword;
    @FXML
    private PasswordField newPassword1;
    @FXML
    private PasswordField newPassword2;
    @FXML
    private Label message;
    @FXML 
    private AdminSceneController parent;
    
    private LogInController controller;
    
    
    @FXML
	private void initialize() {
    	message.setVisible(false);
    }

    @FXML
    private void changePW(ActionEvent event) {
    	if (!newPassword1.getText().equals(newPassword2.getText())) {
    		message.setVisible(true);
    		message.setText("Die Eingaben zum neuen Passwort stimmen nicht überein");
    	} else if (!controller.requestChangePassword(oldPassword.getText(), newPassword1.getText()).equals(OnError.SuccessfulChangePassword)) {
    		message.setVisible(true);
    		message.setText("Altes Passwort ist falsch");
    	} else {
    		message.setVisible(false);
    		oldPassword.clear();
    		newPassword1.clear();
    		newPassword2.clear();
    	}
    	
    }

	protected void setParent(AdminSceneController parent) {
		this.parent = parent;
	}

	public void setController(LogInController login) {
		controller = login;
		
	}

}
