package campusroutenplaner.view.admin.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.stage.Stage;

public class MultipleAdminController {
	private AdminSceneController admin;
	private Stage stage;

    @FXML
    private void closeProgram(ActionEvent event) {
    	stage.close();
    	admin.close();
    }

    @FXML
    private void continueWorking(ActionEvent event) {
    	stage.close();
    }
    
    protected void setAdmin(AdminSceneController admin) {
    	this.admin = admin;
    }
    
    protected void setStage(Stage stage) {
    	this.stage = stage;
    	stage.setTitle("WARNUNG");
    }
}
