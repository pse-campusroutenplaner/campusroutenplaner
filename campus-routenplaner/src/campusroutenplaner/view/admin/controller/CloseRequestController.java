package campusroutenplaner.view.admin.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.stage.Stage;

public class CloseRequestController {
	private Stage primaryStage;
	private Stage secondaryStage;
	private HistoryController parent;
	private AdminSceneController admin;

	@FXML
	void dischargeChanges(ActionEvent event) {
		if (parent.discardChanges())
			close();
	}

	@FXML
	void sync(ActionEvent event) {
		parent.startSync(true);
		close();
	}

	private void close() {
		if (primaryStage != null) {
			primaryStage.close();
		} else {
			admin.logout();
		}
		secondaryStage.close();
	}

	protected void setParent(HistoryController parent) {
		this.parent = parent;
	}

	protected void setStages(Stage primaryStage, Stage secondaryStage) {
		this.primaryStage = primaryStage;
		this.secondaryStage = secondaryStage;
	}

	protected void setLogout(AdminSceneController admin) {
		this.admin = admin;
	}

}
