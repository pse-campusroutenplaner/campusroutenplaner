package campusroutenplaner.view.admin.controller;

import java.io.IOException;

import campusroutenplaner.view.ViewMain;
import campusroutenplaner.view.admin.controller.building.BuildingViewController;
import campusroutenplaner.view.admin.controller.constructionSites.ConstructionSiteListController;
import campusroutenplaner.view.admin.controller.map.OptionController;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * Root of the administration tool. Main task: communication with the ViewMain,
 * so all Controller can be setted.
 * 
 * @author Thieu Ha Minh
 *
 */
public class AdminSceneController {

	@FXML
	private SplitPane adminScene;
	@FXML
	private TabPane tabs;
	@FXML
	private Tab map;
	@FXML
	private Tab buiding;
	@FXML
	private Tab constructionSites;
	@FXML
	private Button help;
	@FXML
	private OptionController optionController;
	@FXML
	private BuildingViewController buildingViewController;
	@FXML
	private ConstructionSiteListController constructionSiteListController;
	@FXML
	private HistoryController historyController;
	@FXML
	private PasswordChangerController passwordChangerController;
	private Stage secondaryStage;
	private ViewMain parent;

	@FXML
	private void initialize() {
		help.setVisible(false);
		constructionSiteListController.setParent(this);
		optionController.setParent(this);
		buildingViewController.setParent(this);
		buildingViewController.setFloorList(optionController.getFloorListController());
		passwordChangerController.setParent(this);
		historyController.setParent(this);
		map.selectedProperty().addListener((o, oldval, newval) -> optionController.reloade());
		buiding.selectedProperty().addListener((o, oldval, newval) -> buildingViewController.tabbed(newval));
	}

	@FXML
	private void logoutRequest() {
		if (historyController.isUploadNeeded()) {
			syncRequest(null);
		} else {
			logout();
		}
	}

	private void syncRequest(Stage primaryStage) {
		try {
			if (secondaryStage == null)
				secondaryStage = new Stage();
			if (secondaryStage.isShowing())
				return;
			FXMLLoader loader = new FXMLLoader(getClass().getResource("../FXMLfiles/CloseRequest.fxml"));
			Parent root = loader.load();
			Scene scene = new Scene(root);
			secondaryStage.setScene(scene);
			secondaryStage.show();
			CloseRequestController closeRequest = loader.getController();
			closeRequest.setParent(historyController);
			closeRequest.setStages(primaryStage, secondaryStage);
			closeRequest.setLogout(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void logout() {
		historyController.logout();
		parent.logout();
	}

	protected void close() {
		parent.close();
	}

	protected void multipleAdmin(Stage warning) {
		if (warning == null)
			warning = new Stage();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("../FXMLfiles/MultipleAdmin.fxml"));
			Parent root = loader.load();
			MultipleAdminController warningController = loader.getController();
			warningController.setStage(warning);
			warningController.setAdmin(this);
			Scene scene = new Scene(root);
			warning.setScene(scene);
			warning.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// public void showBuilding(String buildingNumber) {
	// tabs.getSelectionModel().select(buiding);
	// buildingViewController.show(buildingNumber);
	// }

	public void buildingList() {
		optionController.setBuildingList(buildingViewController);
	}

	public void unBlockedEdge(int edgeID) {
		optionController.unBlockedEdge(edgeID);
	}

	public OptionController getOptionController() {
		return optionController;
	}

	public BuildingViewController getBuildingViewController() {
		return buildingViewController;
	}

	public ConstructionSiteListController getConstructionSiteListController() {
		return constructionSiteListController;
	}

	public HistoryController getHistoryController() {
		return historyController;
	}

	public PasswordChangerController getPasswordChangerController() {
		return passwordChangerController;
	}

	public void setParent(ViewMain parent) {
		this.parent = parent;
	}

	public void setStage(Stage primaryStage) {
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				event.consume();
				if (historyController != null) {
					if (historyController.isUploadNeeded()) {
						syncRequest(primaryStage);
					} else {
						close();
					}
				}
			}
		});

	}
}