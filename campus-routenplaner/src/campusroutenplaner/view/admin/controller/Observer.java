package campusroutenplaner.view.admin.controller;

import campusroutenplaner.model.database.Subject;

/**
 * Created by nmladenov on 7/16/16.
 */
public interface Observer  {
    //method to update the observer, used by subject
    public void update(String message);
    public void setSubject(Subject sub);
}
