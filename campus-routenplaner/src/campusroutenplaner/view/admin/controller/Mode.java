package campusroutenplaner.view.admin.controller;

/**
 * Different modi of the map
 * @author Thieu Ha Minh
 *
 */
public enum Mode {
	ADDVERTEX,
	ADDEDGE,
	IMAGESCALE,
	STANDERD
	
}
