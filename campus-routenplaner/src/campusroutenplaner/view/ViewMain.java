package campusroutenplaner.view;

import java.io.IOException;
import java.util.LinkedList;

import campusroutenplaner.controller.main.BackgroundController;
import campusroutenplaner.controller.main.BuildingController;
import campusroutenplaner.controller.usercontroller.ConstructionSiteController;
import campusroutenplaner.controller.usercontroller.FavoriteController;
import campusroutenplaner.controller.usercontroller.LogInController;
import campusroutenplaner.controller.main.MapController;
import campusroutenplaner.controller.usercontroller.RoutingController;
import campusroutenplaner.model.database.Subject;
import campusroutenplaner.model.map.Edge;
import campusroutenplaner.view.admin.controller.AdminSceneController;
import campusroutenplaner.view.user.viewcontroller.AdminLogin;
import campusroutenplaner.view.user.viewcontroller.UserScene;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ViewMain extends Application {

	private Parent root;
	private Scene scene;
	private Stage primaryStage;
	private Stage secondaryStage;

	private Subject observable;
	private MapController map;
	private BuildingController building;
	private BackgroundController background;
	private RoutingController routing;
	private FavoriteController favorite;
	private UserScene user;
	private ConstructionSiteController constructionSite;
    private LogInController login;

	public ViewMain(
			MapController map, 
			BuildingController building, 
			BackgroundController background,
			FavoriteController favorite, 
			RoutingController routing, 
			ConstructionSiteController constructionSite,
			LogInController login,
			Subject obsevable) {
		this.map = map;
        this.background = background;
		this.building = building;
		this.favorite = favorite;
		this.routing = routing;
		this.constructionSite = constructionSite;
		this.login = login;
		this.observable = obsevable;
	}

	private void setController(AdminSceneController admin, Scene scene) {
		admin.setParent(this);
		admin.setStage(primaryStage);
		admin.getHistoryController().setSubject(observable);
		admin.getHistoryController().setScene(scene);
		admin.getBuildingViewController().setController(building);
		admin.getOptionController().setController(map, background);
		admin.getConstructionSiteListController().setController(constructionSite);
		admin.getPasswordChangerController().setController(login);
	}

	private void setController(UserScene user) {
		user.setParent(this);
		user.getFavouriteController().setFavoriteController(favorite);
		user.getFavouriteController().setBuildingController(building);
		user.getFavouriteController().setRoutingController(routing);
		user.getFloorListController().setBuildingController(building);
		user.getRouteOptionController().setBuildingController(building);
		user.getRouteOptionController().setRoutingController(routing);
		user.getUserMapViewController().setBackgroundController(background);
		user.getUserMapViewController().setBuildingController(building);
		primaryStage.setOnCloseRequest(e -> close());
		
		LinkedList<Edge> edges = new LinkedList<Edge>(); // only for test
		for (int i = 0; i < map.requestEdgeList(0).size(); i++) {
			edges.add(map.requestSearchEdge(0, map.requestEdgeList(0).get(i)));
		}
		user.setTest(edges);
	}

	@Override
	public void start(Stage primaryStage) {
		try {
			this.primaryStage = primaryStage;
			userScene();
			primaryStage.setOnCloseRequest(e -> close());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void close() {
		Platform.exit();  
		System.exit(0);
	}

	public void succefullLogin() {
		secondaryStage.close();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("admin/FXMLfiles/AdminScene.fxml"));
			root = loader.load();
			scene = new Scene(root);
			setController((AdminSceneController) loader.getController(), scene);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void back() {
		secondaryStage.close();
	}

	public void logout() {
		try {
			userScene();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void trylogin() {
		try {
			if (secondaryStage == null || !secondaryStage.isShowing()) {
				secondaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader(getClass().getResource("user/view/AdminLogin.fxml"));
				Parent root = loader.load();
				Scene scene = new Scene(root);
				secondaryStage.setScene(scene);
				secondaryStage.show();
				AdminLogin mylogin = loader.getController();
				mylogin.setParent(this);
				user.setAdminLogin(mylogin);
				mylogin.setLoginController(login);
				mylogin.matchLanguage();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void userScene() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("user/view/MainView.fxml"));
			root = loader.load();
			user = loader.getController();
			setController(user);
			scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
