package campusroutenplaner.view.user.viewcontroller;

import javafx.fxml.FXML;

import java.util.LinkedList;
import java.util.Locale;
import java.util. ResourceBundle;



import campusroutenplaner.model.map.Edge;
import campusroutenplaner.view.ViewMain;

public class UserScene {
	
	@FXML private RouteOption routeOptionController;
	@FXML private Favourite favouriteController;
	@FXML private UserMapView userMapViewController;
	@FXML private FloorList floorListController;

	private LanguageSetter languageSetterController;
	private AdminLogin adminLoginController;
	private ViewMain parent;
//	public int flag = 0; // flag for 3 searchParamMode
	public String languageFlag = "DE";
	private ResourceBundle rb;
	public LinkedList<Edge> myEdge; // only for test
	
	@FXML private void initialize() {
		rb = ResourceBundle.getBundle("campusroutenplaner.view.user.viewcontroller/resource", Locale.GERMANY);		
		routeOptionController.init(this,rb);
		favouriteController.init(this,rb);
		userMapViewController.init(this,rb);
		floorListController.init(this);	
		languageSetterController = new LanguageSetter();
		languageSetterController.init(this);
		adminLoginController = new AdminLogin();
		adminLoginController.init(this,rb);
	}

	public void setParent(ViewMain parent) {
		this.parent = parent;
	}

	protected ViewMain getParent() {
		return parent;
	}

	public RouteOption getRouteOptionController() {
		return routeOptionController;
	}

	public Favourite getFavouriteController() {
		return favouriteController;
	}


	public UserMapView getUserMapViewController() {
		return userMapViewController;
	}


	public FloorList getFloorListController() {
		return floorListController;
	}


	public LanguageSetter getLanguageSetterController() {
		return languageSetterController;
	}

	public AdminLogin getAdminLoginController() {
		return adminLoginController;
	}
	
	
	public void setAdminLogin(AdminLogin login) {
		adminLoginController = login;	
		adminLoginController.init(this,rb);				
	}
	
	public void setResourceBundle(ResourceBundle rb2) {
		this.rb = rb2;		
	}
	
	// only for test, should be deleted
	public void setTest(LinkedList<Edge> edges) { 
		this.myEdge = edges;
		
	}







	
	

//	public String loadNewItem() {
//		String addItem = "";
//		if(routeOptionController.searchParamMode == "Route") {
//			addItem = routeOptionController.pane2buildingSource.getText() + ":" 
//                    + routeOptionController.pane2buildingSourceRoom.getText() + "<>" 
//                    + routeOptionController.pane2buildingDes.getText() + ":"
//                    + routeOptionController.pane2buildingDesRoom.getText();
//			return addItem;
//		} else if (routeOptionController.searchParamMode == "Suche") {
//			flag = 1;
//			addItem =routeOptionController.pane1buildingSource.getText() + ":"
//          		    + routeOptionController.pane1buildingSourceRoom.getText();	
//			return addItem;
//		} else {
//			flag = 2;
//			addItem = routeOptionController.pane3buildingSource.getText() + ":"
//            		+ routeOptionController.pane3buildingSourceRoom.getText() + "<>" 
//            		+ routeOptionController.pane3buildingStop.getText() +":"
//            		+ routeOptionController.pane3buildingStopRoom.getText() + "<>" 
//            		+ routeOptionController.pane3buildingDes.getText() + ":"
//                    + routeOptionController.pane3buildingDesRoom.getText();
//			return addItem;
//		}
//	}
	

}
