package campusroutenplaner.view.user.viewcontroller;

import java.util.ResourceBundle;

import campusroutenplaner.controller.usercontroller.LogInController;
import campusroutenplaner.model.map.OnError;
import campusroutenplaner.view.ViewMain;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class AdminLogin {
	@FXML private AnchorPane adminLogin;
	@FXML private TextField adminName;
	@FXML private PasswordField passWort;
	@FXML private Button login;
	@FXML private Button back;
	@FXML private Label errorInfo;
	private ViewMain parent;
	private ResourceBundle myRB;
	private UserScene userScene;
	private LogInController loginController;
	
	/**
	 * when user click the button "login", it will check account's information
	 * @param event
	 */
	@FXML
	private void checkInfo(ActionEvent event) {
		if (loginController.requestLogIn(adminName.getText(), passWort.getText()) == OnError.SuccessfulLogin) {
			errorInfo.setText("");
			parent.succefullLogin();
		} else {
			errorInfo.setText(this.myRB.getString("loginInfo")); 
		}
	}
	
	public void back(ActionEvent event){
		parent.back();
	}
	
	public void init(UserScene myUserScene, ResourceBundle rb) {
		this.userScene = myUserScene;
		this.myRB = rb;
	}
	
	public void setParent(ViewMain parent) {
		this.parent = parent;
	}
	
	public void setLoginController(LogInController parent) {
		this.loginController = parent;
	}
	

	public void setResource(ResourceBundle rb) {
		this.myRB = rb;			
	}

	public void matchLanguage() {
		adminName.setPromptText(myRB.getString("adminName"));
		passWort.setPromptText(myRB.getString("passWort"));
		login.setText(myRB.getString("login"));
		back.setText(myRB.getString("back"));		
	}


	

}
