package campusroutenplaner.view.user.viewcontroller;
import java.util.Locale;
import java.util. ResourceBundle;

public class LanguageSetter {
	private UserScene mainView;

	public void init(UserScene mainView) {
		this.mainView = mainView;		
	}

	/**
	 * depended on the parameter "lang" to load the language's resource. There are 4 language file for this program
	 * @param lang
	 */
	public void changelanguage(String lang) {
		
		if (lang == "EN") {
			ResourceBundle rb = ResourceBundle.getBundle("campusroutenplaner.view.user.viewcontroller/resource", Locale.US);
			setLanguage(rb);
			mainView.setResourceBundle(rb);
		} else if (lang == "ZH") {
			ResourceBundle rb = ResourceBundle.getBundle("campusroutenplaner.view.user.viewcontroller/resource", Locale.CHINA);
			setLanguage(rb);
			mainView.setResourceBundle(rb);
		} else if (lang == "DE") {
			ResourceBundle rb = ResourceBundle.getBundle("campusroutenplaner.view.user.viewcontroller/resource", Locale.GERMANY);
			setLanguage(rb);			
			mainView.setResourceBundle(rb);
		} else if (lang == "BG") {
			Locale Bulgaria = new Locale("bg","BG");
			ResourceBundle rb = ResourceBundle.getBundle("campusroutenplaner.view.user.viewcontroller/resource", Bulgaria);
			setLanguage(rb);	
			mainView.setResourceBundle(rb);
		}	
	}
	
	/**
	 * set the string or symbol on GUI to the corresponding variant in the language file
	 * @param rb
	 */
	public void setLanguage(ResourceBundle rb) {
		mainView.getRouteOptionController().loginAdmin.setText(rb.getString("loginAdmin"));
		mainView.getRouteOptionController().routeTypes.setPromptText(rb.getString("routeTypes"));
		mainView.getRouteOptionController().setRouteTypesItems(rb);
		mainView.getRouteOptionController().start3.setText(rb.getString("label1"));
		mainView.getRouteOptionController().stop3.setText(rb.getString("label2"));
		mainView.getRouteOptionController().end3.setText(rb.getString("label3"));
		mainView.getRouteOptionController().start2.setText(rb.getString("label1"));
		mainView.getRouteOptionController().end2.setText(rb.getString("label3"));
		mainView.getRouteOptionController().end1.setText(rb.getString("label3"));
		mainView.getRouteOptionController().pane1buildingSource.setPromptText(rb.getString("building"));
		mainView.getRouteOptionController().pane1buildingSourceRoom.setPromptText(rb.getString("room"));
		mainView.getRouteOptionController().pane2buildingSource.setPromptText(rb.getString("building"));
		mainView.getRouteOptionController().pane2buildingSourceRoom.setPromptText(rb.getString("room"));
		mainView.getRouteOptionController().pane2buildingDes.setPromptText(rb.getString("building"));
		mainView.getRouteOptionController().pane2buildingDesRoom.setPromptText(rb.getString("room"));
		mainView.getRouteOptionController().pane3buildingSource.setPromptText(rb.getString("building"));
		mainView.getRouteOptionController().pane3buildingSourceRoom.setPromptText(rb.getString("room"));
		mainView.getRouteOptionController().pane3buildingDes.setPromptText(rb.getString("building"));
		mainView.getRouteOptionController().pane3buildingDesRoom.setPromptText(rb.getString("room"));
		mainView.getRouteOptionController().pane3buildingStop.setPromptText(rb.getString("building"));
		mainView.getRouteOptionController().pane3buildingStopRoom.setPromptText(rb.getString("room"));
		mainView.getRouteOptionController().startRouting.setText(rb.getString("startRoute"));
		mainView.getUserMapViewController().reset.setText(rb.getString("reset"));
		mainView.getUserMapViewController().backToCampus.setText(rb.getString("backToCampus"));
		mainView.getUserMapViewController().zoom.setText(rb.getString("zoom"));
		mainView.getFavouriteController().setResource(rb);
		mainView.getUserMapViewController().setResource(rb);
		mainView.getFloorListController().floorListLable.setText(rb.getString("floorListLable"));
		if (mainView.getRouteOptionController().journeyInfo.getText() != "") {
			mainView.getRouteOptionController().journeyInfo.setText(rb.getString("journeyTimeInfo"));			
		}
		if (mainView.getRouteOptionController().hintInfo.getText() != "") {
			mainView.getRouteOptionController().hintInfo.setText(rb.getString("hintInfo"));			
		}
	}
}
