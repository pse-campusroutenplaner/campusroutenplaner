package campusroutenplaner.view.user.viewcontroller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Circle;

public class TimeView {
	@FXML public Label time;
	@FXML public Label timeTitle;
	@FXML public StackPane timeView; 
	private UserMapView userMapView;
	
	
	/**
	 * if user click one circle and this circle is the same with the circle which representative the first 
	 * vertex in the result of the route, then display the time of the route
	 * @param myCircle
	 *        the first vertex object in the result of the route
	 * @param t
	 */	
	public void show(Circle myCircle, MouseEvent t) {
		Circle circle = (Circle) t.getSource();
		if (circle.equals(myCircle)) {
		timeView.setLayoutX(t.getX());
		timeView.setLayoutY(t.getY());
	    timeView.setVisible(true);
		timeView.toFront();
		}
	}



	public void initialize(UserMapView userMapView2) {
		this.userMapView = userMapView2;		
	}	

}
