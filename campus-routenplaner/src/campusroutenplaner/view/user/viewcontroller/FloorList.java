package campusroutenplaner.view.user.viewcontroller;
import campusroutenplaner.model.map.*;
import campusroutenplaner.controller.main.BuildingController;

import java.net.URL;
import java.util.LinkedList;
import java.util.Observable;
import java.util.ResourceBundle;










import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TreeItem;
import javafx.scene.layout.AnchorPane;

public class FloorList {
	private UserScene mainView;
	@FXML public AnchorPane floorList;
	@FXML public ListView<String> floors;
	@FXML public Label floorListLable; 
	
	public int selectedNum;
	private BuildingController buildingController;
	private Building clickedBuilding;
	private ObservableList<String> floorsItemList;
	
	
//	ObservableList<String> floorsItemList = 
//			FXCollections.observableArrayList("0","1","2","3","4","-1","-2"); //should read data from controller
//	ObservableList<String> floorsItemList = 
//			FXCollections.observableArrayList(); //should read data from controller
	
	/**
	 * initialize and add listener of the ListView, when user selected any floor's number, call method changeFloorMap to 
	 * display the corresponding background of floor and the route.
	 * @param clickedBuilding
	 */
	public void initialize() {
		floorList.setVisible(false);
		floorsItemList = FXCollections.observableArrayList("0");
		floors.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
//		floors.getSelectionModel().select(0); //set default selected item 0.Stock
//		floors.getFocusModel().focus(0);
		//get the selected nummber of Etage "String newValue"
		floors.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
		    @Override
		    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
		    	if (oldValue != newValue && newValue != null) {
			    	selectedNum =  Integer.parseInt(newValue); //get the selected item, string to int
			    	System.out.println("ListView selection changed from oldValue = " 
			                + oldValue + " to newValue = " + newValue);	
			    	changeFloorMap(selectedNum, clickedBuilding);	    		
		    	}
		    }
		});				
	}
	
	/**
	 * set all floor number of the clicked building in Structure ListView
	 * @param clickedBuilding
	 */
	public void setFloorItems(Building clickedBuilding) {
		floorsItemList.clear();
		floors.getItems().clear();
		this.clickedBuilding = clickedBuilding;
		LinkedList<Integer> floorsItems = new LinkedList<>();
		floorsItems = buildingController.requestFloorNumbers(this.clickedBuilding.getNumber());		
        for (int i = 0; i < floorsItems.size(); i++) {
        	floorsItemList.add(i, floorsItems.get(i).toString());
        } 
        floors.setItems(floorsItemList);
	}
	
	public void setSelectedFloorNumber(int floorNumber, Building clickedBuilding) {
		int indexFloor = -1;
		this.clickedBuilding = clickedBuilding;
		LinkedList<Integer> floorsItems = new LinkedList<>();
		floorsItems = buildingController.requestFloorNumbers(this.clickedBuilding.getNumber());		
        for (int i = 0; i < floorsItems.size(); i++) {
        	if (floorsItems.get(i) == floorNumber) {  // find the index of floor0
        		indexFloor = i;
        	}
        } 
        floors.requestFocus();
		floors.getSelectionModel().select(indexFloor); //set default selected item 0.Stock		
	}
	
	
	
	/**
	 * change the background for the floor which user selected,if there is a segment route on this floor, it will be directly displayed.
	 * @param background
	 * @param building
	 */
	public void changeFloorMap(int selectedNum, Building building) {  //depending on the mapID of floor
		int mapID = buildingController.requestMapIdOfFloor(building.getNumber(), selectedNum);
		mainView.getUserMapViewController().changeMap(mapID);  //change background image
		mainView.getUserMapViewController().showRoute(mapID); //show corresponding route
	}
	


	
	public void init(UserScene mainView4) {
		this.mainView = mainView4;
		
	}
	
	public void setBuildingController(BuildingController myBuildingController) {
		this.buildingController = myBuildingController;		
	}
	

}
