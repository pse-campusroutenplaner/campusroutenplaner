package campusroutenplaner.view.user.viewcontroller;

import campusroutenplaner.view.user.viewhelper.Draughtsman;
import campusroutenplaner.model.map.*;
import campusroutenplaner.controller.main.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ResourceBundle;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

public class UserMapView {

	@FXML
	private TimeView timeViewController;
	@FXML
	private VBox userMapView;
	@FXML
	private Group rootGroup;
	@FXML
	private ImageView map;
	@FXML
	private ScrollPane mapRoot;
	@FXML
	public Button reset;
	@FXML
	public Button moreShow;
	@FXML
	public Button backToCampus;
	@FXML
	private Slider zoomSlider;
	@FXML
	public Label zoom;
	private UserScene mainView;
	public Group zoomGroup;
	private ArrayList<Circle> isShowNodeList;
	private ArrayList<Line> isShowEdgeList;
	private int myMapID = 0;
	private String clickStatus = "Default"; // 2 Status, "Default" and
											// "InBuilding"
	private String clickBuilding = "";
	// Default, SingleBuilding, BuildingInRoute, InFoundBuilding, InBuildingDes
	private Draughtsman myDraughtsman;
	private BackgroundController backgroundController;
	private BuildingController buildingController;
	public Background background;
	public Image backgroundImage;
	private ResourceBundle rb;

	private int timeViewFlag = 0; // flag for whether need to add to ZoomGroup
	public Building foundBuilding;
	public Building buildingDes;
	public Building buildingStop;
	public Vertex buildingStopRoom;
	public LinkedList<Edge> routeEdgeList;
	private LinkedList<Edge> tempEdgeList;
	private Circle circleStart;
	private LinkedList<Building> throughBuildingList;
	private Building throughBuilding;

	/**
	 * initialize 4 lists and one Group
	 */
	public void initialize() {
		timeViewController.initialize(this);
		isShowNodeList = new ArrayList<Circle>();
		isShowEdgeList = new ArrayList<Line>();
		routeEdgeList = new LinkedList<Edge>();
		tempEdgeList = new LinkedList<Edge>();
		throughBuildingList = new LinkedList<Building>();
		
		zoomGroup = new Group();
		zoomSliderInit();
		zoomGroup.getChildren().add(map);
		rootGroup.getChildren().add(zoomGroup);
		myDraughtsman = new Draughtsman();
		circleStart = new Circle();

	}

	/**
	 * set the minimal and maximal value of zoomSlider and add the listener, if
	 * get the new value, directly call the method zoom
	 */
	public void zoomSliderInit() { // here should get the init value
		zoomSlider.setMin(0.2);
		zoomSlider.setMax(3);
		zoomSlider.setValue(1);
		zoomSlider.valueProperty().addListener((o, oldVal, newVal) -> zoom((Double) newVal));
	}

	/**
	 * depending on the current value of the zoomSlider to change the scaleValue
	 */
	public void zoom(double scaleValue) {
		zoomGroup.setScaleX(scaleValue);
		zoomGroup.setScaleY(scaleValue);
	}

	/**
	 * when user click the button "zum CampusMap", the change the map to campus
	 * and display the segment of route in the campus
	 */
	public void goToCampusMap(ActionEvent event) {
		cleanShowShape();
		backgroundImage = SwingFXUtils.toFXImage(backgroundController.requestGetBackground(0).getPicture(), null);
		map.setImage(backgroundImage);
		map.setFitHeight(backgroundController.requestGetBackground(0).getHight());
		map.setFitWidth(backgroundController.requestGetBackground(0).getWidth());

		zoomSlider.setValue(1);
		showRoute(0);
		// showBuildingEntrance();
		if (clickStatus == "InBuilding") {
			mainView.getFloorListController().floorList.setVisible(true);
			clickStatus = "Default";
		}

		mainView.getFloorListController().floorList.setVisible(false);
	}

	/**
	 * with the mapID to change the background to the floor0 of the clicked
	 * building and set the floor's number of this clicked building, also
	 * display them
	 * 
	 * @param mapID
	 */
	public void buildingToFloor0(int mapID) { // for double click one building
												// node,not direct click button
		// System.out.println("buildingToFloor0 mapID is: " + mapID);
		myMapID = mapID;
		cleanShowShape();
		backgroundImage = SwingFXUtils.toFXImage(backgroundController.requestGetBackground(mapID).getPicture(), null);
		map.setImage(backgroundImage);
		map.setFitHeight(backgroundController.requestGetBackground(mapID).getHight());
		map.setFitWidth(backgroundController.requestGetBackground(mapID).getWidth());
		zoomSlider.setValue(1);
		if (clickStatus == "Default") { // set Items in FloorList
			if (clickBuilding == "foundBuilding") {
				mainView.getFloorListController().setFloorItems(foundBuilding);
				if (routeEdgeList != null || routeEdgeList.size() != 0) {
					mainView.getFloorListController().setSelectedFloorNumber(0, foundBuilding);
				}
			} else if (clickBuilding == "buildingDes") {
				mainView.getFloorListController().setFloorItems(buildingDes);
				if (routeEdgeList != null || routeEdgeList.size() != 0) {
					mainView.getFloorListController().setSelectedFloorNumber(0, buildingDes);
				}

			} else if (clickBuilding == "buildingStop") {
				mainView.getFloorListController().setFloorItems(buildingStop);
				if (routeEdgeList != null || routeEdgeList.size() != 0) {
					mainView.getFloorListController().setSelectedFloorNumber(0, buildingStop);
				}
			} else if (clickBuilding == "throughBuilding") {
				mainView.getFloorListController().setFloorItems(throughBuilding);
				if (routeEdgeList != null || routeEdgeList.size() != 0) {
					mainView.getFloorListController().setSelectedFloorNumber(0, throughBuilding);
				}				
			}
			mainView.getFloorListController().floorList.setVisible(true);
			clickStatus = "InBuilding";
		} else if (clickStatus == "InBuilding") {
			mainView.getFloorListController().floorList.setVisible(false);
			clickStatus = "Default";
		}
		// myMapID = mapID;
		// if (routeEdgeList != null || routeEdgeList.size() != 0) {
		// mainView.getFloorListController().setSelectedFloorNumber(0);
		// System.out.println("mapID in buildingToFloor0():" + myMapID);
		//// showRoute(myMapID);
		// }
	}

	/**
	 * with the mapID to change the background
	 * 
	 * @param mapID
	 */
	public void changeMap(int mapID) { // depend on mapID to change the map
		myMapID = mapID;
		cleanShowShape();
		backgroundImage = SwingFXUtils.toFXImage(backgroundController.requestGetBackground(mapID).getPicture(), null);
		map.setImage(backgroundImage);
		map.setFitHeight(backgroundController.requestGetBackground(mapID).getHight());
		map.setFitWidth(backgroundController.requestGetBackground(mapID).getWidth());
	}

	/**
	 * all of attribute go back to default value
	 */
	public void reset() {
		changeMap(0); // directly load campusMap
		if (routeEdgeList != null) {
			routeEdgeList.clear(); // clear the route's result
		}
		if (tempEdgeList != null) {
			tempEdgeList.clear();
		}
		if (throughBuildingList != null) {
			throughBuildingList.clear();
		}
		mainView.getFloorListController().floorList.setVisible(false);
		mainView.getRouteOptionController().hintInfo.setText("");
        mainView.getRouteOptionController().journeyInfo.setText("");
		clickStatus = "Default";
		myMapID = 0;
		clickBuilding = "";
		timeViewFlag = 0;
		zoomSlider.setValue(1);
		cleanShowShape();
		foundBuilding = null;
		buildingDes = null;
		buildingStop = null;
		throughBuilding = null;
		mainView.getRouteOptionController().journeyInfo.setText("");
		mainView.getRouteOptionController().hintInfo.setText("");
		

		// floorOmapID = -1;

	}

	/**
	 * when user click "reset" button, list and flag go back to default value
	 * 
	 * @param event
	 */
	public void resetShow(ActionEvent event) { // for button "zurückstellen"
		reset();
	}

	/**
	 * clean the displayed shapes, when the new shapes should be displayed
	 */
	public void cleanShowShape() {
		for (Circle node : isShowNodeList) { // clean displayed circle
			zoomGroup.getChildren().remove(node);
		}

		for (Line edge : isShowEdgeList) { // clean displayed line
			zoomGroup.getChildren().remove(edge);
		}

		isShowNodeList.clear();
		isShowEdgeList.clear();
		hideRouteTime();
	}

	/**
	 * through the floor0mapID to set the correct flag "clickBuilding", the
	 * program know user click which building
	 * 
	 * @param floor0mapID
	 */
	public void setClickedBuilding(int floor0mapID) {

        if (floor0mapID == buildingController.requestMapIdOfFloor(foundBuilding.getNumber(), 0)) {
			clickBuilding = "foundBuilding";
		} else if (buildingDes != null && floor0mapID == buildingController.requestMapIdOfFloor(buildingDes.getNumber(), 0)) {
			clickBuilding = "buildingDes";
		} else if (buildingStop != null && floor0mapID == buildingController.requestMapIdOfFloor(buildingStop.getNumber(), 0)) {
			clickBuilding = "buildingStop";
		}
		
	    if (throughBuilding != null) {
			if (floor0mapID == buildingController.requestMapIdOfFloor(throughBuilding.getNumber(), 0)) {
		       clickBuilding = "throughBuilding";
			}
	    }
	}

	private Circle endVertex(Circle circle) {
		if (tempEdgeList.getLast().equals(routeEdgeList.getLast()))
			return myDraughtsman.drawFlag(this, circle.getCenterX(), circle.getCenterY());
		return circle;
	}

	/**
	 * Iterate the entrance list of foundBuilding, to display all the entrances
	 */
	public void showBuildingEntrance() { // parameter is
											// "foundBuilding:Building" , in
											// this methode should Traversing
											// all Entrance of building
		cleanShowShape();
		if (myMapID != 0) { // change the background to campus
			myMapID = 0;
			backgroundImage = SwingFXUtils.toFXImage(backgroundController.requestGetBackground(0).getPicture(), null);
			map.setImage(backgroundImage);
			map.setFitHeight(backgroundController.requestGetBackground(0).getHight());
			map.setFitWidth(backgroundController.requestGetBackground(0).getWidth());
		}

		System.out.println("foundBuilding in view:" + foundBuilding);

		for (Vertex vertex : foundBuilding.getEntrances()) { // loop definition
																// of all
																// entrances and
																// display
																// directly
			Circle circle = new Circle();
			int floorOmapID = buildingController.requestMapIdOfFloor(foundBuilding.getNumber(), 0);
			if (foundBuilding.isBlueprinted()) {
				circle = myDraughtsman.drawVertexExtra(this, vertex.getXPos(), vertex.getYPos(), 8.0, "YELLOW",floorOmapID); // last parameter is mapid of floor0										
			} else {
				circle = myDraughtsman.drawVertex(this, vertex.getXPos(), vertex.getYPos(), 8.0, "RED");
			}

//			if (foundBuilding.getEntrances().size() == 1)
//				circle = myDraughtsman.drawFlag(this, vertex.getXPos(), vertex.getYPos());
			zoomGroup.getChildren().add(circle);
			isShowNodeList.add(circle);
		}

	}

	
	public void showThroughBuilding() {
		if (throughBuildingList != null && throughBuildingList.size()>0) {
			for (int i = 0; i < throughBuildingList.size(); i++) {
				if (!throughBuildingList.get(i).equals(foundBuilding) && !throughBuildingList.get(i).equals(buildingDes)
						&& !throughBuildingList.get(i).equals(buildingStop)) {
					throughBuilding = throughBuildingList.get(i);
					int floorOmapID = buildingController.requestMapIdOfFloor(throughBuildingList.get(i).getNumber(), 0);
					for (Vertex vertex : throughBuildingList.get(i).getEntrances()) {
						Circle circleThrough = new Circle();						
						circleThrough = myDraughtsman.drawVertexExtra(this, vertex.getXPos(), vertex.getYPos(), 8.0, "YELLOW", floorOmapID);
						System.out.println("throughBuilding floorOmapID:" + floorOmapID);
						zoomGroup.getChildren().add(circleThrough);
						isShowNodeList.add(circleThrough);
					}
				}
				
			}
			
		}

				
	}
	
	/**
	 * result of the route is stored in routeEdgeList use the myMapID flag to
	 * set the current mapID of displayed background, when user click "los"
	 * button to show first segment of the route, the myMapID is the same with
	 * the first edge's source's mapID. when user click floor number, we can get
	 * the new mapID of the selected floor, then iterate the routeEdgeList, find
	 * the matched edge and vertex, which we should new display, then show the
	 * route with circle and line
	 * 
	 * @param mapID
	 */
	public void showRoute(int mapID) { // list of edge have to be stored in
										// this.routeEdgeList, show first
										// segment
		// tempEdgeList.clear();



		VertexType typeStair;
		Circle circleStair = new Circle();
		for (int i = 0; i < routeEdgeList.size(); i++) { // check stair node and
															// display
			if (routeEdgeList.get(i) != null) {
				typeStair = routeEdgeList.get(i).getSource().getType();
				if (routeEdgeList.get(i).getSource().getMapId() == mapID
						|| routeEdgeList.get(i).getDest().getMapId() == mapID) {
					if ((routeEdgeList.get(i).getSource().getMapId() == mapID && typeStair instanceof StairsType)
							|| (routeEdgeList.get(i).getSource().getMapId() == mapID
									&& typeStair instanceof ElevatorType)) {
						circleStair = myDraughtsman.drawVertex(this, routeEdgeList.get(i).getSource().getXPos(),
								routeEdgeList.get(i).getSource().getYPos(), 8.0, "RED");
						zoomGroup.getChildren().add(circleStair);
						isShowNodeList.add(circleStair);
					} else if ((routeEdgeList.get(i).getDest().getMapId() == mapID && typeStair instanceof StairsType)
							|| (routeEdgeList.get(i).getDest().getMapId() == mapID
									&& typeStair instanceof ElevatorType)) {
						circleStair = myDraughtsman.drawVertex(this, routeEdgeList.get(i).getDest().getXPos(),
								routeEdgeList.get(i).getDest().getYPos(), 8.0, "RED");
						zoomGroup.getChildren().add(circleStair);
						isShowNodeList.add(circleStair);
					}
				}
			}
		}

		System.out.println("foundBuilding:" + foundBuilding);
		System.out.println("buildingDes:" + buildingDes);
		System.out.println("buildingStop:" + buildingStop);
		System.out.println("Throughbuilding:" + throughBuilding);
		if (myMapID != mapID) {
			myMapID = mapID;
			changeMap(myMapID);
		}
		if (myMapID != 0) {
			clickStatus = "InBuilding";
		}

		for (int i = 0; i < routeEdgeList.size(); i++) { // direct show all line
															// in one segment
			System.out.println("routeEdgeList-index" + i + ":" + routeEdgeList.get(i));
		}
		System.out.println("-----------------------------------------------");
		for (int i = 0; i < routeEdgeList.size(); i++) { // direct show all line
															// in one segment
			if (routeEdgeList.get(i) != null) {
				if (routeEdgeList.get(i).getSource().getMapId() == myMapID
						&& routeEdgeList.get(i).getDest().getMapId() == myMapID) {
					tempEdgeList.add(routeEdgeList.get(i)); // temp store the
															// edge for display
															// 2 node in one
															// segment
					Line line = new Line();
					line = myDraughtsman.drawEdge(routeEdgeList.get(i).getSource(), routeEdgeList.get(i).getDest(),
							"RED");
					zoomGroup.getChildren().add(line);
					isShowEdgeList.add(line);
				}
			}
		}

		if (myMapID != 0 && tempEdgeList.size() != 0 && tempEdgeList != null) { // if
																				// node
																				// is
																				// in
																				// floor,show
																				// normal
																				// circle
			System.out.println("myMapID:" + myMapID);
			for (int i = 0; i < tempEdgeList.size(); i++) {
				if (buildingStopRoom != null) {
					Circle stopRoom = new Circle();
					if (buildingStopRoom.getId() == tempEdgeList.get(i).getSource().getId()) {
						stopRoom = myDraughtsman.drawVertex(this, tempEdgeList.get(i).getSource().getXPos(),
								tempEdgeList.get(i).getSource().getYPos(), 8.0, "RED");
						zoomGroup.getChildren().add(stopRoom);
						isShowNodeList.add(stopRoom);
					} else if (buildingStopRoom.getId() == tempEdgeList.get(i).getDest().getId()) {
						stopRoom = myDraughtsman.drawVertex(this, tempEdgeList.get(i).getDest().getXPos(),
								tempEdgeList.get(i).getDest().getYPos(), 8.0, "RED");
						zoomGroup.getChildren().add(stopRoom);
						isShowNodeList.add(stopRoom);
					}
				}
				System.out.println("1-tempEdgeList-index" + i + ":" + tempEdgeList.get(i)); // only
																							// for
																							// test
			}
			Circle circle1 = new Circle();
			if (tempEdgeList.size() > 1) {
				if (tempEdgeList.getFirst().getSource().getId() != tempEdgeList.get(1).getSource().getId()
						&& tempEdgeList.getFirst().getSource().getId() != tempEdgeList.get(1).getDest().getId()) {
					circle1 = myDraughtsman.drawVertex(this, tempEdgeList.getFirst().getSource().getXPos(),
							tempEdgeList.getFirst().getSource().getYPos(), 8.0, "RED");
					// System.out.println("circle1:" + tempEdgeList.getFirst());
					// System.out.println("circle1:" +
					// tempEdgeList.getFirst().getSource());
				} else {
					circle1 = myDraughtsman.drawVertex(this, tempEdgeList.getFirst().getDest().getXPos(),
							tempEdgeList.getFirst().getDest().getYPos(), 8.0, "RED");
					// System.out.println("circle1:" + tempEdgeList.getFirst());
					// System.out.println("circle1:" +
					// tempEdgeList.getFirst().getDest());
				}
			} else if (tempEdgeList.size() == 1) {
				circle1 = myDraughtsman.drawVertex(this, tempEdgeList.getFirst().getSource().getXPos(),
						tempEdgeList.getFirst().getSource().getYPos(), 8.0, "RED");
			}
			if (tempEdgeList.getFirst().equals(routeEdgeList.getFirst())) {
				circleStart = circle1; // store the first node for time show
			}

			zoomGroup.getChildren().add(circle1);
			isShowNodeList.add(circle1);

			Circle circle2 = new Circle();
			// System.out.println(tempEdgeList.size()-2);
			if (tempEdgeList.size() > 1) {
				if (tempEdgeList.getLast().getDest().getId() != tempEdgeList.get(tempEdgeList.size() - 2).getSource()
						.getId()
						&& tempEdgeList.getLast().getDest().getId() != tempEdgeList.get(tempEdgeList.size() - 2)
								.getDest().getId()) {
					circle2 = myDraughtsman.drawVertex(this, tempEdgeList.getLast().getDest().getXPos(),
							tempEdgeList.getLast().getDest().getYPos(), 8.0, "GREEN");
					circle2 = endVertex(circle2);
					System.out.println("circle2:" + tempEdgeList.getLast());
					System.out.println("circle2:" + tempEdgeList.getLast().getDest());
				} else {
					circle2 = myDraughtsman.drawVertex(this, tempEdgeList.getLast().getSource().getXPos(),
							tempEdgeList.getLast().getSource().getYPos(), 8.0, "GREEN");
					circle2 = endVertex(circle2);
					System.out.println("circle2:" + tempEdgeList.getLast());
					System.out.println("circle2:" + tempEdgeList.getLast().getSource());
				}
			} else if (tempEdgeList.size() == 1) {
				circle2 = myDraughtsman.drawVertex(this, tempEdgeList.getFirst().getDest().getXPos(),
						tempEdgeList.getFirst().getDest().getYPos(), 8.0, "RED");
			}
			zoomGroup.getChildren().add(circle2);
			isShowNodeList.add(circle2);
		} else if (myMapID == 0 && tempEdgeList.size() != 0 && tempEdgeList != null) { // if
																						// node
																						// is
																						// in
																						// campus,show
																						// extra
																						// circle
			for (int i = 0; i < routeEdgeList.size(); i++) { //find throughBuilding
				if (routeEdgeList.get(i) != null) {
					if (routeEdgeList.get(i).getSource().getMapId() != 0)  {
						Building tempBuilding = buildingController.requestGetBuilding(routeEdgeList.get(i).getSource().getMapId());
						if (tempBuilding != null) {
							if (throughBuildingList.size() == 0) {
								throughBuildingList.add(tempBuilding);
							} else if (throughBuildingList.size() > 0 && !tempBuilding.equals(throughBuildingList.getLast())) {
								throughBuildingList.add(tempBuilding);
							}
						}					 						
					} else if (routeEdgeList.get(i).getDest().getMapId() != 0) {
						Building tempBuilding = buildingController.requestGetBuilding(routeEdgeList.get(i).getDest().getMapId());
						if (tempBuilding != null) {
							if (throughBuildingList.size() == 0) {
								throughBuildingList.add(tempBuilding);
							} else if (throughBuildingList.size() > 0 && !tempBuilding.equals(throughBuildingList.getLast())) {
								throughBuildingList.add(tempBuilding);
							}
						}							
					}
				}
			}
			showThroughBuilding(); //display entrance of the through Building
			System.out.println("--------------------------------------");
			System.out.println("myMapID:" + myMapID);
			for (int i = 0; i < tempEdgeList.size(); i++) { // only for test
				System.out.println("2-tempEdgeList-index" + i + ":" + tempEdgeList.get(i));
			}
			Circle circle3 = new Circle(); // start node in campus
			System.out.println("tempEdgeList.size:" + tempEdgeList.size());
			if (tempEdgeList.size() > 1) {
				VertexType type3;
				if (tempEdgeList.getFirst().getSource().getId() != tempEdgeList.get(1).getSource().getId()
						&& tempEdgeList.getFirst().getSource().getId() != tempEdgeList.get(1).getDest().getId()) {
					type3 = tempEdgeList.getFirst().getSource().getType(); // only
																			// EntranceType
																			// have
																			// getBuildingNumber
				} else {
					type3 = tempEdgeList.getFirst().getDest().getType();
				}

				if (type3 instanceof EntranceType) {
					if (((EntranceType) type3).getBuildingNumber().equals(foundBuilding.getNumber())
							&& foundBuilding.isBlueprinted()) {
						int floorOmapID = buildingController.requestMapIdOfFloor(foundBuilding.getNumber(), 0);
						if (tempEdgeList.getFirst().getSource().getId() != tempEdgeList.get(1).getSource().getId()
								&& tempEdgeList.getFirst().getSource().getId() != tempEdgeList.get(1).getDest()
										.getId()) {
							circle3 = myDraughtsman.drawVertexExtra(this, tempEdgeList.getFirst().getSource().getXPos(),
									tempEdgeList.getFirst().getSource().getYPos(), 8.0, "YELLOW", floorOmapID);
							// System.out.println("circle3:" +
							// tempEdgeList.getFirst().getSource().getId());

							// System.out.println("circle3:" +
							// tempEdgeList.getFirst());
							// System.out.println("circle3:" +
							// tempEdgeList.getFirst().getSource());
							// System.out.println("circle3 x:" +
							// tempEdgeList.getFirst().getSource().getXPos());
							// System.out.println("circle3 y:" +
							// tempEdgeList.getFirst().getSource().getYPos());

						} else {
							circle3 = myDraughtsman.drawVertexExtra(this, tempEdgeList.getFirst().getDest().getXPos(),
									tempEdgeList.getFirst().getDest().getYPos(), 8.0, "YELLOW", floorOmapID);
							// System.out.println("circle3:" +
							// tempEdgeList.getFirst().getDest().getId());
							// System.out.println("circle3:" +
							// tempEdgeList.getFirst());
							// System.out.println("circle3:" +
							// tempEdgeList.getFirst().getDest());
							// System.out.println("circle3 x:" +
							// tempEdgeList.getFirst().getDest().getXPos());
							// System.out.println("circle3 y:" +
							// tempEdgeList.getFirst().getDest().getYPos());
						}
					} else {
						if (tempEdgeList.getFirst().getSource().getId() != tempEdgeList.get(1).getSource().getId()
								&& tempEdgeList.getFirst().getSource().getId() != tempEdgeList.get(1).getDest()
										.getId()) {
							circle3 = myDraughtsman.drawVertex(this, tempEdgeList.getFirst().getSource().getXPos(),
									tempEdgeList.getFirst().getSource().getYPos(), 8.0, "RED");
						} else {
							circle3 = myDraughtsman.drawVertex(this, tempEdgeList.getFirst().getDest().getXPos(),
									tempEdgeList.getFirst().getDest().getYPos(), 8.0, "RED");
						}
					}
					if (tempEdgeList.getFirst().equals(routeEdgeList.getFirst())) {
						circleStart = circle3; // store the first node for time
												// show
					}
				}
				if (buildingStop != null) { // stop node in campus
					for (int i = 0; i < tempEdgeList.size(); i++) { // can show
																	// many
																	// entry of
																	// the stop
						Circle circle5 = new Circle();
						VertexType type5 = tempEdgeList.get(i).getSource().getType();
						VertexType type6 = tempEdgeList.get(i).getDest().getType();
						if (type5 instanceof EntranceType) {
							if (((EntranceType) type5).getBuildingNumber().equals(buildingStop.getNumber())) { // find
																												// the
																												// node
																												// which
																												// is
																												// the
																												// stop's
																												// entry
								if (buildingStop.isBlueprinted()) {
									int floorOmapID = buildingController.requestMapIdOfFloor(buildingStop.getNumber(),
											0);
									circle5 = myDraughtsman.drawVertexExtra(this,
											tempEdgeList.get(i).getSource().getXPos(),
											tempEdgeList.get(i).getSource().getYPos(), 8.0, "YELLOW", floorOmapID);
									System.out.println("buildingStop floorOmapID:" + floorOmapID);
									// zoomGroup.getChildren().add(circle5);
									// isShowNodeList.add(circle5);
								} else {
									circle5 = myDraughtsman.drawVertex(this, tempEdgeList.get(i).getSource().getXPos(),
											tempEdgeList.get(i).getSource().getYPos(), 8.0, "RED");
									// zoomGroup.getChildren().add(circle5);
									// isShowNodeList.add(circle5);
								}
							}
						}

						if (type6 instanceof EntranceType) {
							if (((EntranceType) type6).getBuildingNumber().equals(buildingStop.getNumber())) { // find
																												// the
																												// node
																												// which
																												// is
																												// the
																												// stop's
																												// entry
								if (buildingStop.isBlueprinted()) {
									int floorOmapID = buildingController.requestMapIdOfFloor(buildingStop.getNumber(),
											0);
									circle5 = myDraughtsman.drawVertexExtra(this,
											tempEdgeList.get(i).getDest().getXPos(),
											tempEdgeList.get(i).getDest().getYPos(), 8.0, "YELLOW", floorOmapID);
									System.out.println("buildingStop floorOmapID:" + floorOmapID);
									// zoomGroup.getChildren().add(circle5);
									// isShowNodeList.add(circle5);
								} else {
									circle5 = myDraughtsman.drawVertex(this, tempEdgeList.get(i).getDest().getXPos(),
											tempEdgeList.get(i).getDest().getYPos(), 8.0, "RED");
									// zoomGroup.getChildren().add(circle5);
									// isShowNodeList.add(circle5);
								}
							}
						}
						zoomGroup.getChildren().add(circle5);
						isShowNodeList.add(circle5);
					}
				}
			} else if (tempEdgeList.size() == 1) {
				System.out.println("goto circle3");
				VertexType type31;
				VertexType type32;
				type31 = tempEdgeList.get(0).getSource().getType();
				type32 = tempEdgeList.get(0).getDest().getType();
				if (type31 instanceof EntranceType && type32 instanceof EntranceType) {
					if (((EntranceType) type31).getBuildingNumber().equals(foundBuilding.getNumber())
							&& foundBuilding.isBlueprinted()) {
						int floorOmapID = buildingController.requestMapIdOfFloor(foundBuilding.getNumber(), 0);
						circle3 = myDraughtsman.drawVertexExtra(this, tempEdgeList.getFirst().getSource().getXPos(),
								tempEdgeList.getFirst().getSource().getYPos(), 5.0, "YELLOW", floorOmapID);
					} else if (((EntranceType) type31).getBuildingNumber().equals(foundBuilding.getNumber())
							&& !foundBuilding.isBlueprinted()) {
						circle3 = myDraughtsman.drawVertex(this, tempEdgeList.getFirst().getSource().getXPos(),
								tempEdgeList.getFirst().getSource().getYPos(), 5.0, "RED");
					} else if (((EntranceType) type32).getBuildingNumber().equals(foundBuilding.getNumber())
							&& foundBuilding.isBlueprinted()) {
						int floorOmapID = buildingController.requestMapIdOfFloor(foundBuilding.getNumber(), 0);
						circle3 = myDraughtsman.drawVertexExtra(this, tempEdgeList.getFirst().getDest().getXPos(),
								tempEdgeList.getFirst().getDest().getYPos(), 5.0, "YELLOW", floorOmapID);
					} else if (((EntranceType) type32).getBuildingNumber().equals(foundBuilding.getNumber())
							&& !foundBuilding.isBlueprinted()) {
						circle3 = myDraughtsman.drawVertex(this, tempEdgeList.getFirst().getDest().getXPos(),
								tempEdgeList.getFirst().getDest().getYPos(), 5.0, "RED");
					}
				}
			}
			zoomGroup.getChildren().add(circle3);
			isShowNodeList.add(circle3);

			Circle circle4 = new Circle(); // end node in campus
			Circle circle4Flag = null;
			if (tempEdgeList.size() > 1) {
				VertexType type4;
				if (tempEdgeList.getLast().getDest().getId() != tempEdgeList.get(tempEdgeList.size() - 2).getSource()
						.getId()
						&& tempEdgeList.getLast().getDest().getId() != tempEdgeList.get(tempEdgeList.size() - 2)
								.getDest().getId()) {
					type4 = tempEdgeList.getLast().getDest().getType();
				} else {
					type4 = tempEdgeList.getLast().getSource().getType();
				}
				if (type4 instanceof EntranceType) { // only EntranceType have
														// getBuildingNumber
					if (((EntranceType) type4).getBuildingNumber().equals(buildingDes.getNumber())
							&& buildingDes.isBlueprinted()) {
						int floorOmapID = buildingController.requestMapIdOfFloor(buildingDes.getNumber(), 0);
						if (tempEdgeList.getLast().getDest().getId() != tempEdgeList.get(tempEdgeList.size() - 2)
								.getSource().getId()
								&& tempEdgeList.getLast().getDest().getId() != tempEdgeList.get(tempEdgeList.size() - 2)
										.getDest().getId()) {
							circle4 = myDraughtsman.drawVertexExtra(this, tempEdgeList.getLast().getDest().getXPos(),
									tempEdgeList.getLast().getDest().getYPos(), 8.0, "YELLOW", floorOmapID);
							circle4Flag = endVertex(circle4);
							// System.out.println("circle4:" +
							// tempEdgeList.getLast());
							// System.out.println("circle4:" +
							// tempEdgeList.getLast().getDest());
						} else {
							circle4 = myDraughtsman.drawVertexExtra(this, tempEdgeList.getLast().getSource().getXPos(),
									tempEdgeList.getLast().getSource().getYPos(), 8.0, "YELLOW", floorOmapID);
							circle4Flag = endVertex(circle4);
							// System.out.println("circle4:" +
							// tempEdgeList.getLast());
							// System.out.println("circle4:" +
							// tempEdgeList.getLast().getSource());
						}
					} else {
						if (tempEdgeList.getLast().getDest().getId() != tempEdgeList.get(tempEdgeList.size() - 2)
								.getSource().getId()
								&& tempEdgeList.getLast().getDest().getId() != tempEdgeList.get(tempEdgeList.size() - 2)
										.getDest().getId()) {
							circle4 = myDraughtsman.drawVertex(this, tempEdgeList.getLast().getDest().getXPos(),
									tempEdgeList.getLast().getDest().getYPos(), 8.0, "GREEN");
							circle4 = endVertex(circle4);
						} else {
							circle4 = myDraughtsman.drawVertex(this, tempEdgeList.getLast().getSource().getXPos(),
									tempEdgeList.getLast().getSource().getYPos(), 8.0, "GREEN");
							circle4 = endVertex(circle4);
						}
					}
				}
			} else if (tempEdgeList.size() == 1) {
				System.out.println("goto circle4");
				VertexType type41;
				VertexType type42;
				type41 = tempEdgeList.get(0).getSource().getType();
				type42 = tempEdgeList.get(0).getDest().getType();
				if (type41 instanceof EntranceType && type42 instanceof EntranceType) {
					if (((EntranceType) type41).getBuildingNumber().equals(buildingDes.getNumber())
							&& buildingDes.isBlueprinted()) {
						int floorOmapID = buildingController.requestMapIdOfFloor(buildingDes.getNumber(), 0);
						circle4 = myDraughtsman.drawVertexExtra(this, tempEdgeList.getFirst().getSource().getXPos(),
								tempEdgeList.getFirst().getSource().getYPos(), 5.0, "YELLOW", floorOmapID);
					} else if (((EntranceType) type41).getBuildingNumber().equals(buildingDes.getNumber())
							&& !buildingDes.isBlueprinted()) {
						circle4 = myDraughtsman.drawVertex(this, tempEdgeList.getFirst().getSource().getXPos(),
								tempEdgeList.getFirst().getSource().getYPos(), 5.0, "RED");
					} else if (((EntranceType) type42).getBuildingNumber().equals(buildingDes.getNumber())
							&& buildingDes.isBlueprinted()) {
						int floorOmapID = buildingController.requestMapIdOfFloor(buildingDes.getNumber(), 0);
						circle4 = myDraughtsman.drawVertexExtra(this, tempEdgeList.getFirst().getDest().getXPos(),
								tempEdgeList.getFirst().getDest().getYPos(), 5.0, "YELLOW", floorOmapID);
					} else if (((EntranceType) type42).getBuildingNumber().equals(buildingDes.getNumber())
							&& !buildingDes.isBlueprinted()) {
						circle4 = myDraughtsman.drawVertex(this, tempEdgeList.getFirst().getDest().getXPos(),
								tempEdgeList.getFirst().getDest().getYPos(), 5.0, "RED");
						System.out.println("circle4:" + tempEdgeList.getFirst().getDest().getXPos() + ":"
								+ tempEdgeList.getFirst().getDest().getYPos());
					}
					circle4 = endVertex(circle4);
				}
			}
			zoomGroup.getChildren().add(circle4);
			System.out.println("zoomGroup add circle4");
			isShowNodeList.add(circle4);
			if (circle4Flag != null && !circle4Flag.equals(circle4)) {
				zoomGroup.getChildren().add(circle4Flag);
				isShowNodeList.add(circle4Flag);
				circle4.toFront();
			}
		}

		tempEdgeList.clear();
	}

	/**
	 * when the first segment of route is in building, then display the right
	 * FloorList
	 */
	public void showFloorList(int firstMapID) {
		mainView.getFloorListController().setFloorItems(foundBuilding);
		clickBuilding = "foundBuilding";
		mainView.getFloorListController().floorList.setVisible(true);
		clickStatus = "InBuilding";
		int floorNumber = buildingController.requestFloorNumber(foundBuilding.getNumber(), firstMapID);
		mainView.getFloorListController().setSelectedFloorNumber(floorNumber, foundBuilding);

	}

	/**
	 * when the user right click the start circle of the route, display the time
	 * of the route at the same location
	 */
	public void showRouteTime(MouseEvent mouseEvent) {// parameter is
														// "routeEdgeList:List<Edge>"

		double length = 0.0;
		int time = 0;
		for (Edge edge : routeEdgeList) {
			if (edge != null) {
				length = length + edge.getLength();
			}
		}
		switch (mainView.getRouteOptionController().routeStyle) { // depending
																	// on the
																	// routeStyle
																	// and
																	// calculate
																	// the time
																	// L/v
		case "walk":
			time = (int) length / 60;
			break;
		case "bike":
			time = (int) length / 167;
			break;
		case "accessible":
			time = (int) length / 60;
		}
		if (time < 1) {
			timeViewController.timeTitle.setText(rb.getString("timeTitle"));
			timeViewController.time.setText(rb.getString("timeInfo"));
		} else {
			timeViewController.timeTitle.setText(rb.getString("timeTitle"));
			timeViewController.time.setText(time + " " + "min");
		}

		timeViewController.show(circleStart, mouseEvent); // if click the first
															// node,dann show
															// the RouteTime

		if (timeViewFlag == 0) {
			zoomGroup.getChildren().add(timeViewController.timeView);
			timeViewFlag = 1;
		}
		circleStart.toFront(); // circleStart display always in front
	}

	/**
	 * when the user left click the start circle of the route, hide the time of
	 * the route at the same location
	 */
	public void hideRouteTime() {
		timeViewController.timeView.setVisible(false); // clean displayed time
														// of route
		zoomGroup.getChildren().remove(timeViewController.timeView);
		timeViewFlag = 0;
	}

	public void init(UserScene mainView3, ResourceBundle rb) {
		this.mainView = mainView3;
		this.rb = rb;
	}

	/**
	 * set BackgroundController and load the default backgroundImage
	 */
	public void setBackgroundController(BackgroundController myBackgroundController) {
		this.backgroundController = myBackgroundController;
		Image backgroundImage = SwingFXUtils.toFXImage(backgroundController.requestGetBackground(0).getPicture(), null);
		map.setImage(backgroundImage);
		map.setFitHeight(backgroundController.requestGetBackground(0).getHight());
		map.setFitWidth(backgroundController.requestGetBackground(0).getWidth());
	}

	public void setBuildingController(BuildingController myBuildingController) {
		this.buildingController = myBuildingController;
	}

	public void setResource(ResourceBundle rb2) {
		this.rb = rb2;
	}

}
