package campusroutenplaner.view.user.viewcontroller;
import campusroutenplaner.controller.main.*;
import campusroutenplaner.controller.usercontroller.RoutingController;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

public class RouteOption implements Initializable{
	
	@FXML private HBox routeOption;
	@FXML public Button loginAdmin;
	@FXML public ComboBox<String> routeTypes;
	@FXML private ComboBox<String> languages;
	@FXML private Button walkBtn;
	@FXML private Button bikeBtn;
	@FXML private Button accessibleBtn;
	@FXML private Pane paneRoute;
	@FXML private Pane paneSuche;
	@FXML private Pane paneZwischen;
	@FXML public Button startRouting;
	@FXML public Label hintInfo;
	@FXML public Label journeyInfo;
	@FXML public TextField pane2buildingSource;
	@FXML public TextField pane2buildingSourceRoom;
	@FXML public TextField pane2buildingDes;
	@FXML public TextField pane2buildingDesRoom;
	@FXML public TextField pane1buildingSource;
	@FXML public TextField pane1buildingSourceRoom;
	@FXML public TextField pane3buildingSource;
	@FXML public TextField pane3buildingStop;
	@FXML public TextField pane3buildingDes;
	@FXML public TextField pane3buildingDesRoom;
	@FXML public TextField pane3buildingStopRoom;
	@FXML public TextField pane3buildingSourceRoom;
	@FXML public Label start3;
	@FXML public Label end3;
	@FXML public Label stop3;
	@FXML public Label end1;
	@FXML public Label start2;
	@FXML public Label end2;
	
	private UserScene mainView;
	private ResourceBundle myRB;
	public ObservableList<String> routeTypesItemList;
	public String searchParamMode = "Route";
    public String routeStyle = "walk";
    private BuildingController buildingController; 
    private RoutingController routingController; 


	/**
	 * initialize the items in ComboBox languages and routeTypes
	 */	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		ObservableList<String> languageItemList = FXCollections.observableArrayList("DE","EN","BG","ZH");
		routeTypesItemList = FXCollections.observableArrayList("Route","Suche","Route mit Zwischenstopp");
		languages.setItems(languageItemList);
		routeTypes.setItems(routeTypesItemList);				
	}
	
	/**
	 * set the current language resource and update the items in routeTypes
	 * @param rb
	 */	
	public void setRouteTypesItems(ResourceBundle rb) { //
		myRB = rb;
		routeTypesItemList = FXCollections.observableArrayList(myRB.getString("routeTypes1"),myRB.getString("routeTypes2"),myRB.getString("routeTypes3"));
		routeTypes.setItems(routeTypesItemList);		
	}
	
	/**
	 * when user selected one item in language ComboBox, get the language's value and call languageSetterController to set the language
	 * @param event
	 */	
	@FXML public void languageItemChanged(ActionEvent event){
		mainView.getLanguageSetterController().changelanguage(languages.getValue().toString());
		
	}
	
	/**
	 * when user click the button "adminAnmelden", a new Scene for login opened
	 * @param event
	 */	
	@FXML public void AdminLogin(ActionEvent event) throws Exception{
		mainView.getParent().trylogin();
	}
	
	/**
	 * when user click the button of "walk", set the current routeStyle to walk
	 * @param event
	 */	
	@FXML public void setWalk(ActionEvent event){
		this.routeStyle = "walk";
		System.out.print(this.routeStyle);
	}
	
	
	/**
	 * when user click the button of "bike", set the current routeStyle to bike
	 * @param event
	 */	
	@FXML public void setBike(ActionEvent event){
		this.routeStyle = "bike";	
		System.out.print(this.routeStyle);
	}
	
	/**
	 * when user click the button of "accessible", set the current routeStyle to accessible
	 * @param event
	 */	
	@FXML public void setAccessible(ActionEvent event){
		this.routeStyle = "accessible";	
		System.out.print(this.routeStyle);
	}
	
	/**
	 * depend on the value of routeTypes to show different TextField for user
	 * @param event
	 */	
	@FXML public void routeTypesChanged(ActionEvent event){  // show different TextField for user
		switch (routeTypes.getSelectionModel().getSelectedIndex()) {
		case 0: paneRoute.setVisible(true);  // index0 -> Route
		              paneSuche.setVisible(false);
		              paneZwischen.setVisible(false);
		              this.searchParamMode = "Route";
		              break;
		case 1: paneRoute.setVisible(false); // index1 -> Suche
                      paneSuche.setVisible(true);
                      paneZwischen.setVisible(false);
                      this.searchParamMode = "Suche";
                      break;
		case 2: paneRoute.setVisible(false); //index2 -> Route mit Zwischenstop
	                  paneSuche.setVisible(false);
	                  paneZwischen.setVisible(true);
	                  this.searchParamMode = "Route mit Zwischenstopp";
	                  break;
        default: break;
		}		
	}
	
	/**
	 * when user click the button "Los", send the route information to routingController and call method in 
	 * routingController to calculate the shortest path. The result value of route is one edge's list, it 
	 * will be stored in userMapView, then the userMapView directly display the first segment of the route's result 
	 * on the corresponding background. if the user input information of building is leer, there will be the hint to display.
	 * @param event
	 */	
	@FXML public void getRoutingParaAndRouting(ActionEvent event){  // los-Button Click


		mainView.getUserMapViewController().reset();
//		mainView.getFloorListController().floorList.setVisible(false);
		switch (this.searchParamMode) {
		case "Route": 
			if (pane2buildingSource.getText().trim().isEmpty() || pane2buildingDes.getText().trim().isEmpty()) {
                journeyInfo.setText("");
				hintInfo.setText(myRB.getString("hintInfo"));
				} else {
				hintInfo.setText("");
                journeyInfo.setText(myRB.getString("journeyTimeInfo"));
					System.out.println(pane2buildingSource.getText()+","+pane2buildingSourceRoom.getText()+","+pane2buildingDes.getText()+"," +pane2buildingDesRoom.getText());
					mainView.getUserMapViewController().routeEdgeList = routingController.requestRouting(pane2buildingSource.getText(),pane2buildingSourceRoom.getText(),
							pane2buildingDes.getText(), pane2buildingDesRoom.getText(),routeStyle); // get list of edge
					System.out.println(mainView.getUserMapViewController().routeEdgeList);
					mainView.getUserMapViewController().foundBuilding = buildingController.requestSearchBuilding(pane2buildingSource.getText());
					mainView.getUserMapViewController().buildingDes = buildingController.requestSearchBuilding(pane2buildingDes.getText());
//					System.out.println(mainView.getUserMapViewController().foundBuilding);
//					System.out.println(mainView.getUserMapViewController().buildingDes);
					if (mainView.getUserMapViewController().routeEdgeList ==  null || mainView.getUserMapViewController().routeEdgeList.size() == 0) {
                        journeyInfo.setText("");
						hintInfo.setText(myRB.getString("notFoundInfo"));	 // location or route not found											
					} else if (mainView.getUserMapViewController().routeEdgeList.size() != 0) {
						System.out.println("routeEdgeList.size:" + mainView.getUserMapViewController().routeEdgeList.size());
						int firstMapID = mainView.getUserMapViewController().routeEdgeList.getFirst().getSource().getMapId();
						mainView.getUserMapViewController().showRoute(firstMapID);	
						if(firstMapID != 0) {
							mainView.getUserMapViewController().showFloorList(firstMapID);						
						}
					}
				}
			break;
		case "Suche": 
			if (pane1buildingSource.getText().trim().isEmpty()) {
                journeyInfo.setText("");
				hintInfo.setText(myRB.getString("hintInfo"));
				} else {
					hintInfo.setText("");
					mainView.getUserMapViewController().foundBuilding = buildingController.requestSearchBuilding(pane1buildingSource.getText()); //get searched Building
					if(pane1buildingSourceRoom.getText().trim().isEmpty()){
//						mainView.getUserMapViewController().foundBuilding = buildingController.requestSearchBuilding(pane1buildingSource.getText()); //get searched Building
						if (mainView.getUserMapViewController().foundBuilding == null) {
                            journeyInfo.setText("");
							hintInfo.setText(myRB.getString("notFoundInfo"));
						} else {
//							System.out.println(mainView.getUserMapViewController().foundBuilding);
//							System.out.println("getEntrances" + mainView.getUserMapViewController().foundBuilding.getEntrances().size());
							mainView.getUserMapViewController().showBuildingEntrance(); //display all Entrance of the building							
						}
					} else { //suche "building + room" 
						if (mainView.getUserMapViewController().foundBuilding == null) {
                            journeyInfo.setText("");
							hintInfo.setText(myRB.getString("notFoundInfo"));
						} else {
							mainView.getUserMapViewController().showBuildingEntrance();
							mainView.getUserMapViewController().routeEdgeList = routingController.requestSimpleRouting(pane1buildingSource.getText(), pane1buildingSourceRoom.getText(), routeStyle);
							if (mainView.getUserMapViewController().routeEdgeList == null || mainView.getUserMapViewController().routeEdgeList.size() == 0) {
                                journeyInfo.setText("");
								hintInfo.setText(myRB.getString("notFoundInfo"));						
							}							
						}
					}
					}
			break;
		case "Route mit Zwischenstopp":
			if (pane3buildingSource.getText().trim().isEmpty() || pane3buildingDes.getText().trim().isEmpty() || pane3buildingStop.getText().trim().isEmpty()) {
				hintInfo.setText(myRB.getString("hintInfo"));
				} else {
		            hintInfo.setText("");
                    journeyInfo.setText(myRB.getString("journeyTimeInfo"));
					System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
					mainView.getUserMapViewController().routeEdgeList = routingController.requestRoutingWithStop(pane3buildingSource.getText(),pane3buildingSourceRoom.getText(),
					pane3buildingStop.getText(), pane3buildingStopRoom.getText(),
					pane3buildingDes.getText(), pane3buildingDesRoom.getText(),routeStyle); //   get list of edge
					if (mainView.getUserMapViewController().routeEdgeList == null || mainView.getUserMapViewController().routeEdgeList.size() == 0) {
                        journeyInfo.setText("");
						hintInfo.setText(myRB.getString("notFoundInfo"));					
					} else if (mainView.getUserMapViewController().routeEdgeList.size() != 0) {
						mainView.getUserMapViewController().foundBuilding = buildingController.requestSearchBuilding(pane3buildingSource.getText());
						mainView.getUserMapViewController().buildingStop = buildingController.requestSearchBuilding(pane3buildingStop.getText());
						mainView.getUserMapViewController().buildingDes = buildingController.requestSearchBuilding(pane3buildingDes.getText());
						mainView.getUserMapViewController().buildingStopRoom = buildingController.requestGetVertexOfRoom(mainView.getUserMapViewController().buildingStop,
								pane3buildingStopRoom.getText());
						int firstMapID = mainView.getUserMapViewController().routeEdgeList.getFirst().getSource().getMapId();
						mainView.getUserMapViewController().showRoute(firstMapID);	
						if (firstMapID != 0) {
							mainView.getUserMapViewController().showFloorList(firstMapID);
						}
						System.out.println("foundBuilding:"+ mainView.getUserMapViewController().foundBuilding);
						System.out.println("buildingStop"+ mainView.getUserMapViewController().buildingStop);
						System.out.println("buildingDes"+ mainView.getUserMapViewController().buildingDes);
					}								
				 }
			break;
			default: break;			
			}
		
	}


	public void init(UserScene mainView1, ResourceBundle resources) {
		this.mainView = mainView1;
		myRB = resources;
	}	
	
	public void setBuildingController(BuildingController myBuildingController) { 
		this.buildingController = myBuildingController;
	}
	
	public void setRoutingController(RoutingController myRoutingController){ 
		this.routingController = myRoutingController;		
	}



	
	

}
