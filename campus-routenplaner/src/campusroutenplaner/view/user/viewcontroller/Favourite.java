package campusroutenplaner.view.user.viewcontroller;
import campusroutenplaner.controller.main.BuildingController;
import campusroutenplaner.controller.usercontroller.FavoriteController;
import campusroutenplaner.controller.usercontroller.RoutingController;

import java.util.LinkedList;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

public class Favourite{
	
	private UserScene mainView;
	@FXML private AnchorPane favourite;
	@FXML private Button fav;
	@FXML private Button list;
	@FXML private Button delete;
	@FXML private TreeView<String> localFav;
	
	private TreeItem<String> root;
	private TreeItem<String> sampleRoutenList;
	private TreeItem<String> placeList;
	private TreeItem<String> complexRoutenList;
	
	private LinkedList<String> places;
	private LinkedList<String[]> routes;
	private LinkedList<String[]> routesWithStop;
	private ResourceBundle myRB;
	private FavoriteController favoriteController;
	private BuildingController buildingController;
	private RoutingController routingController;



	/**
	 * set the resource of selected language
	 */
		public void setResource(ResourceBundle rb) {
			myRB = rb;			
		}

		
		/**
		 * update the display of three string list
		 * 
		 * @param myPlaces
		 *            string list of places
		 * @param routes
		 *            string list of routes
		 * @param routesWithStop
		 *            string list of routes with stopover
		 */
	   public void updateList(LinkedList<String> myPlaces,LinkedList<String[]> routes, LinkedList<String[]> routesWithStop) {
		   root = new TreeItem<>(myRB.getString("Favoriten"));
		   sampleRoutenList = new TreeItem<>(myRB.getString("fav1"));
		   placeList = new TreeItem<>(myRB.getString("fav2"));
		   complexRoutenList = new TreeItem<>(myRB.getString("fav3"));
		   root.getChildren().add(sampleRoutenList);
		   root.getChildren().add(placeList);
		   root.getChildren().add(complexRoutenList);
		   
		   for (String item : myPlaces) { //iteration of place's list
			   TreeItem<String> myItem = new TreeItem<>(item);
			   placeList.getChildren().add(myItem);			   
			}
		    for (String[] list: routes) { //iteration of route's list
		    	TreeItem<String> myItem = new TreeItem<>(list[0] + "<>" +  list[1]);
		    	sampleRoutenList.getChildren().add(myItem);		    	
		    }
		    for (String[] list: routesWithStop) { //iteration of routesWithStop's list
		    	TreeItem<String> myItem = new TreeItem<>(list[0] + "<>" +  list[1] + "<>" + list[2]);
		    	complexRoutenList.getChildren().add(myItem);		    	
		    }
		   localFav.setRoot(root);
	  }
	   
		/**
		 * when the user click the button of "showFavorite", display the current items of three string list
		 * @param event
		 */
	   @FXML public void setShowFavouriteItems(ActionEvent event) {
		   places = favoriteController.requestLoadFavoritePlacesFromDatabase();
		   routes = favoriteController.requestLoadFavoriteRoutesFromDatabase();
		   routesWithStop = favoriteController.requestLoadFavoriteRoutesWithStopFromDatabase();	   		   
		   updateList(places,routes,routesWithStop);
		   localFav.setVisible(true);
	   }
	   
	   
		/**
		 * when the user choose one item and click the button of "deleteFavorite", remove the current item and delete from the DB
		 * @param event
		 */
	   @FXML public void deleteFav(ActionEvent event) {
		   TreeItem<String> seletedItem = localFav.getSelectionModel().getSelectedItem();
		   if(seletedItem != root && seletedItem != sampleRoutenList && seletedItem != placeList && seletedItem != complexRoutenList){	
			   if (seletedItem.getParent().equals(placeList)) { // delete the data in DB
				   favoriteController.requestRemoveFavoritePlace(seletedItem.getValue().toString());				   
			   } else if (seletedItem.getParent().equals(sampleRoutenList)) {
				   String[] str1 = seletedItem.getValue().toString().split("<>"); //split the item in sampleRoutenList
				   favoriteController.requestRemoveFavoriteRoute(str1[0],str1[1]);
			   } else if (seletedItem.getParent().equals(complexRoutenList)) {
				   String[] str2 = seletedItem.getValue().toString().split("<>"); //split the item in complexRoutenList
				   favoriteController.requestRemoveFavoriteRouteWithStop(str2[0],str2[1],str2[2]);
			   }
			   seletedItem.getParent().getChildren().remove(seletedItem); // delete from view show
		   }		   		   		   		   		  		   
	   }
	   
		/**
		 * after the user input route information and click the button of "addFavorite", call the favoriteController to store the item in DB
		 * @param event
		 */
	   @FXML public void addFav(ActionEvent event) {
		   mainView.getRouteOptionController().hintInfo.setText("");
		   switch (mainView.getRouteOptionController().searchParamMode) {
		   case "Route":  
			   String source = mainView.getRouteOptionController().pane2buildingSource.getText();
			   String sourceRoom = mainView.getRouteOptionController().pane2buildingSourceRoom.getText();
			   String des = mainView.getRouteOptionController().pane2buildingDes.getText();
			   String desRoom = mainView.getRouteOptionController().pane2buildingDesRoom.getText();
			   if (!source.isEmpty() && !des.isEmpty()) {
				   if (!sourceRoom.isEmpty() && !desRoom.isEmpty()) {
					   favoriteController.requestAddFavoriteRoute(source + ":" + sourceRoom, des + ":" + desRoom);				   
				   } else if (sourceRoom.isEmpty() && !desRoom.isEmpty()) {
					   favoriteController.requestAddFavoriteRoute(source, des + ":" + desRoom);
				   } else if (!sourceRoom.isEmpty() && desRoom.isEmpty()) {
					   favoriteController.requestAddFavoriteRoute(source + ":" + sourceRoom, des);
				   } else if (sourceRoom.isEmpty() && desRoom.isEmpty()) {
					   favoriteController.requestAddFavoriteRoute(source, des);
				   }
			   } else {
				   mainView.getRouteOptionController().hintInfo.setText(myRB.getString("hintInfo"));
			   }	
			   break;
		   case "Suche":
			   String building = mainView.getRouteOptionController().pane1buildingSource.getText();
			   String room = mainView.getRouteOptionController().pane1buildingSourceRoom.getText();
			   if (!building.isEmpty()) {
				   if (!room.isEmpty()) {
					   favoriteController.requestAddFavoritePlace(building + ":" + room);
				   } else {
					   favoriteController.requestAddFavoritePlace(building);
				   }				   
			   } else {
				   mainView.getRouteOptionController().hintInfo.setText(myRB.getString("hintInfo"));
			   }
			   break;
		   case "Route mit Zwischenstopp": 
			   String source1 = mainView.getRouteOptionController().pane3buildingSource.getText();
			   String sourceRoom1 = mainView.getRouteOptionController().pane3buildingSourceRoom.getText();
			   String des1 = mainView.getRouteOptionController().pane3buildingDes.getText();
			   String desRoom1 = mainView.getRouteOptionController().pane3buildingDesRoom.getText();
			   String stop = mainView.getRouteOptionController().pane3buildingStop.getText();
			   String stopRoom = mainView.getRouteOptionController().pane3buildingStopRoom.getText();
			   if (!source1.isEmpty() && !des1.isEmpty() && !stop.isEmpty()) {
				   if (sourceRoom1.isEmpty() && desRoom1.isEmpty() && stopRoom.isEmpty()) {
					   favoriteController.requestAddFavoriteRouteWithStop(source1, stop, des1);
				   } else if (!sourceRoom1.isEmpty() && !desRoom1.isEmpty() && !stopRoom.isEmpty()) {
					   favoriteController.requestAddFavoriteRouteWithStop(source1 + ":" + sourceRoom1, 
							   stop + ":" + stopRoom, des1 + ":" + desRoom1);
				   } else if (!sourceRoom1.isEmpty() && desRoom1.isEmpty() && stopRoom.isEmpty()) {
					   favoriteController.requestAddFavoriteRouteWithStop(source1 + ":" + sourceRoom1, stop, des1);
				   } else if (!sourceRoom1.isEmpty() && !desRoom1.isEmpty() && stopRoom.isEmpty()) {
					   favoriteController.requestAddFavoriteRouteWithStop(source1 + ":" + sourceRoom1, 
							   stop, des1 + ":" + desRoom1);
				   } else if (!sourceRoom1.isEmpty() && desRoom1.isEmpty() && !stopRoom.isEmpty()) {
					   favoriteController.requestAddFavoriteRouteWithStop(source1 + ":" + sourceRoom1, 
							   stop + ":" + stopRoom, des1);				   
				   } else if (sourceRoom1.isEmpty() && !desRoom1.isEmpty() && stopRoom.isEmpty()) {
					   favoriteController.requestAddFavoriteRouteWithStop(source1, 
							   stop, des1 + ":" + desRoom1);
				   } else if (sourceRoom1.isEmpty() && desRoom1.isEmpty() && !stopRoom.isEmpty()) {
					   favoriteController.requestAddFavoriteRouteWithStop(source1, 
							   stop + ":" + stopRoom, des1);
				   } else if (sourceRoom1.isEmpty() && !desRoom1.isEmpty() && !stopRoom.isEmpty()) {
					   favoriteController.requestAddFavoriteRouteWithStop(source1, 
							   stop + ":" + stopRoom, des1 + ":" + desRoom1);
				   }				   
			   } else {
				   mainView.getRouteOptionController().hintInfo.setText(myRB.getString("hintInfo"));
			   }
			   break;
		   default: break;				   
		   }
	   }

		/**
		 * when user double click one item in favorite list, call the routingController to calculate the shortest path and userMapView should show
		 * the right shape 
		 * @param mouseEvent
		 */	   
	   @FXML public void favouriteRoute(MouseEvent mouseEvent) {
		   if(mouseEvent.getClickCount() == 2) {
			   mainView.getUserMapViewController().reset();				
			   TreeItem<String> seletedItem = localFav.getSelectionModel().getSelectedItem();
 			   String myString = seletedItem.getValue().toString();  //get selected Items and String analysis	
 			  if (seletedItem.getParent().equals(placeList)) { //simple search
 				 String[] str1 = myString.split(":");
 				 mainView.getUserMapViewController().foundBuilding = buildingController.requestSearchBuilding(str1[0]);
 				 if (str1.length == 1) { 					 
 					 if (mainView.getUserMapViewController().foundBuilding == null) {
 						 mainView.getRouteOptionController().hintInfo.setText(myRB.getString("notFoundInfo"));
					 } else {						
						 mainView.getUserMapViewController().showBuildingEntrance(); //display all Entrance of the building							
					 }
 				  } else if (str1.length == 2) {
 					 if (mainView.getUserMapViewController().foundBuilding == null) {
 						 mainView.getRouteOptionController().hintInfo.setText(myRB.getString("notFoundInfo"));
						} else {
							mainView.getUserMapViewController().showBuildingEntrance();
							mainView.getUserMapViewController().routeEdgeList = routingController.requestSimpleRouting(str1[0], str1[1], mainView.getRouteOptionController().routeStyle);
							if (mainView.getUserMapViewController().routeEdgeList == null || mainView.getUserMapViewController().routeEdgeList.size() == 0) {
								mainView.getRouteOptionController().hintInfo.setText(myRB.getString("notFoundInfo"));						
							}							
						}
 				  }
 			  } else if (seletedItem.getParent().equals(sampleRoutenList)) { //simple route
 				  String[] str1 = seletedItem.getValue().toString().split("<>");
 				  String[] subStr1 = str1[0].split(":");
 				  String[] subStr2 = str1[1].split(":"); 
 				  String source = "";
 				  String sourceRoom = "";
 				  String des = "";
 				  String desRoom = "";
 				  if (subStr1.length == 1) {
 					 source = subStr1[0];
 					 sourceRoom = "";
 					 if (subStr2.length == 1) {
 						des = subStr2[0];
 						desRoom = "";
 					 } else if (subStr2.length == 2) {
 						des = subStr2[0];
 						desRoom = subStr2[1];
 					 }
 				  } else if (subStr1.length == 2) {
 					 source = subStr1[0];
 					 sourceRoom = subStr1[1];
 					 if (subStr2.length == 1) {
 						des = subStr2[0];
 						desRoom = "";
 					 } else if (subStr2.length == 2) {
 						des = subStr2[0];
 						desRoom = subStr2[1];
 					 }
 				  }
 				 mainView.getUserMapViewController().routeEdgeList = routingController.requestRouting(source,sourceRoom,des,desRoom,mainView.getRouteOptionController().routeStyle);
 				 mainView.getUserMapViewController().foundBuilding = buildingController.requestSearchBuilding(source);
				 mainView.getUserMapViewController().buildingDes = buildingController.requestSearchBuilding(des);
				 if (mainView.getUserMapViewController().routeEdgeList ==  null || mainView.getUserMapViewController().routeEdgeList.size() == 0) {
					 mainView.getRouteOptionController().hintInfo.setText(myRB.getString("notFoundInfo"));	 // location or route not found											
				 } else if (mainView.getUserMapViewController().routeEdgeList.size() != 0) {					 
					 int firstMapID = mainView.getUserMapViewController().routeEdgeList.getFirst().getSource().getMapId();
					 mainView.getUserMapViewController().showRoute(firstMapID);
					 if(firstMapID != 0) {
							mainView.getUserMapViewController().showFloorList(firstMapID);						
					 }
				 }
 			  } else if (seletedItem.getParent().equals(complexRoutenList)) { //route with overstop			  
 				  String[] str1 = seletedItem.getValue().toString().split("<>");
				  String[] subStr1 = str1[0].split(":");
				  String[] subStr2 = str1[1].split(":"); 
				  String[] subStr3 = str1[2].split(":");
				  String source = "";
				  String sourceRoom = "";
				  String stop = "";
				  String stopRoom = "";
				  String des = "";
				  String desRoom = "";
				  if (subStr1.length == 1 && subStr2.length == 1 && subStr3.length == 1) {
					  source = subStr1[0];
					  sourceRoom = "";
					  stop = subStr2[0];
					  stopRoom = "";
					  des = subStr3[0];
					  desRoom = "";					  
				  } else if (subStr1.length == 1 && subStr2.length == 1 && subStr3.length == 2) {
					  source = subStr1[0];
					  sourceRoom = "";
					  stop = subStr2[0];
					  stopRoom = "";
					  des = subStr3[0];
					  desRoom = subStr3[1];					  
				  } else if (subStr1.length == 1 && subStr2.length == 2 && subStr3.length == 2) {
					  source = subStr1[0];
					  sourceRoom = "";
					  stop = subStr2[0];
					  stopRoom = subStr2[1];
					  des = subStr3[0];
					  desRoom = subStr3[1];	
				  } else if (subStr1.length == 1 && subStr2.length == 2 && subStr3.length == 1) {
					  source = subStr1[0];
					  sourceRoom = "";
					  stop = subStr2[0];
					  stopRoom = subStr2[1];
					  des = subStr3[0];
					  desRoom = "";					  
				  } else if (subStr1.length == 2 && subStr2.length == 1 && subStr3.length == 1) {
					  source = subStr1[0];
					  sourceRoom = subStr1[1];
					  stop = subStr2[0];
					  stopRoom = "";
					  des = subStr3[0];
					  desRoom = "";
				  } else if (subStr1.length == 2 && subStr2.length == 2 && subStr3.length == 1) {
					  source = subStr1[0];
					  sourceRoom = subStr1[1];
					  stop = subStr2[0];
					  stopRoom = subStr2[1];
					  des = subStr3[0];
					  desRoom = "";
				  } else if (subStr1.length == 2 && subStr2.length == 1 && subStr3.length == 2) {
					  source = subStr1[0];
					  sourceRoom = subStr1[1];
					  stop = subStr2[0];
					  stopRoom = "";
					  des = subStr3[0];
					  desRoom = subStr3[1];					  
				  } else if (subStr1.length == 2 && subStr2.length == 2 && subStr3.length == 2) {
					  source = subStr1[0];
					  sourceRoom = subStr1[1];
					  stop = subStr2[0];
					  stopRoom = subStr2[1];
					  des = subStr3[0];
					  desRoom = subStr3[1];					  
				  }
				  mainView.getUserMapViewController().routeEdgeList = routingController.requestRoutingWithStop(source, sourceRoom, stop, stopRoom, des, desRoom,
						  mainView.getRouteOptionController().routeStyle); // get list of edge
				 if (mainView.getUserMapViewController().routeEdgeList == null || mainView.getUserMapViewController().routeEdgeList.size() == 0) {
					 mainView.getRouteOptionController().hintInfo.setText(myRB.getString("notFoundInfo"));					
				 } else if (mainView.getUserMapViewController().routeEdgeList.size() != 0) {
					 mainView.getUserMapViewController().foundBuilding = buildingController.requestSearchBuilding(source);
					 mainView.getUserMapViewController().buildingStop = buildingController.requestSearchBuilding(stop);
					 mainView.getUserMapViewController().buildingDes = buildingController.requestSearchBuilding(des);
					 mainView.getUserMapViewController().buildingStopRoom = buildingController.requestGetVertexOfRoom(mainView.getUserMapViewController().buildingStop,
							 stopRoom);
					 int firstMapID = mainView.getUserMapViewController().routeEdgeList.getFirst().getSource().getMapId();
					 mainView.getUserMapViewController().showRoute(firstMapID);	
					 if (firstMapID != 0) {
						 mainView.getUserMapViewController().showFloorList(firstMapID);
					 }
				 }			
 				  
 			  }
 			  
		   }		   
		  		   
	   }
	   

	   public void setFavoriteController(FavoriteController myFavoriteController) {
		   this.favoriteController = myFavoriteController;		   
	   }
	   
	   public void setBuildingController(BuildingController myBuildingController) {
		   this.buildingController = myBuildingController;
	   }
	   
	   public void setRoutingController(RoutingController myRoutingController) {
		   this.routingController = myRoutingController;
	   }
	   
		public void init(UserScene mainView2, ResourceBundle rb) {
			this.mainView = mainView2;
			this.myRB = rb;
		}	

}
