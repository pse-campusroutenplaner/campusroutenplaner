package campusroutenplaner.view.user.viewhelper;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;




import campusroutenplaner.model.map.Vertex;
import campusroutenplaner.view.user.viewcontroller.UserMapView;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;


public class Draughtsman {

	UserMapView myUserMapView = new UserMapView();
	
	/**
	 * draw the normal circle for vertex
	 */
	public Circle drawVertex(UserMapView userMapView,double axisX, double axisY, double radius, String color) {
		myUserMapView  = userMapView;
		Circle node = new Circle();
	    node.setCenterX(axisX);
	    node.setCenterY(axisY);
	    node.setRadius(radius);
	    node.setFill(Paint.valueOf(color));
	    node.setOnMouseClicked(new EventHandler<MouseEvent>() {
	        @Override
	        public void handle(MouseEvent t) {
	            if (t.getButton().equals(MouseButton.PRIMARY)) {
	                if (t.getClickCount() %2 == 1) {
	                	myUserMapView.hideRouteTime();
	                }
	            } else if (t.getButton().equals(MouseButton.SECONDARY)) {
	            	if (t.getClickCount() %2 == 1) {
	            		myUserMapView.showRouteTime(t);		            		
	            	}
	            }
	            t.consume();
	        }
	    });

	    return node;				
	}
	
	
	/**
	 * depending on the start and end vertex to draw the line for edge
	 */
	public Line drawEdge(Vertex start, Vertex end,  String color) {
//		System.out.println(start.getXPos());
		Line line = new Line();
//		System.out.println(line);
		line.setStartX(start.getXPos());
		line.setStartY(start.getYPos());
		line.setEndX(end.getXPos());
		line.setEndY(end.getYPos());
		line.setStroke(Paint.valueOf(color));
		line.setStrokeWidth(6);
//	    System.out.println("getCenter: " + line.getStartX() + " " + line.getStartY() + " " + line.getEndX() + " " + line.getEndY());
		return line;
	}
		

	/**
	 * draw the special circle for vertex and set mouse event handle
	 * when user double left click the circle, will go to the corresponding floor0 image
	 * when user one left click this circle, will hide the time view.
	 * when user one right click this circle, will display the route time
	 * @param floor0mapID
	 *         mapID of floor0 in the building, which have this vertex
	 */
	public Circle drawVertexExtra(UserMapView userMapView, double axisX, double axisY, double radius, String color, int floor0mapID) {
		myUserMapView  = userMapView;
		Circle node = new Circle();
		    node.setCenterX(axisX);
		    node.setCenterY(axisY);
		    node.setRadius(radius);
		    node.setFill(Paint.valueOf(color));
//		    System.out.println(node.getTranslateX() + " " + node.getTranslateY());
		    System.out.println("node getCenter: " + node.getCenterX() + " " + node.getCenterY());


		    node.setOnMouseClicked(new EventHandler<MouseEvent>() {
		        @Override
		        public void handle(MouseEvent t) {
		            if (t.getButton().equals(MouseButton.PRIMARY)) {
		                if (t.getClickCount() %2 == 0) {
//		                	int mapID = myUserMapView.getMapIDofFloor0(node.getCenterX(),node.getCenterY());
		                	myUserMapView.setClickedBuilding(floor0mapID);
		                	myUserMapView.buildingToFloor0(floor0mapID);
		                }else if(t.getClickCount() %2 == 1){
//		                    System.out.println("Circle Position: "+node.getCenterX()+" "+node.getCenterY());
		            		myUserMapView.hideRouteTime();	
		                }
		            }else if (t.getButton().equals(MouseButton.SECONDARY)) {
		            	if (t.getClickCount() %2 == 1) {
		            		myUserMapView.showRouteTime(t);		            		
		            	}
		            }
		            t.consume();
		        }
		    });		    

		    return node;
		}	
	
	public Circle drawFlag(UserMapView userMapView,double axisX, double axisY) {
		myUserMapView  = userMapView;
		Circle node = new Circle();
	    node.setCenterX(axisX);
	    node.setCenterY(axisY);
	    node.setRadius(50);
		BufferedImage i = null;
		try {
			i = ImageIO.read(new File("res/img/user/flag.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Image img = SwingFXUtils.toFXImage(i, null);
		node.setFill(new ImagePattern(img));
		return node;		
	}
	
}
