package campusroutenplaner.controller.main;

import campusroutenplaner.Logger;
import campusroutenplaner.model.map.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

public class BuildingController {
	private CampusMap campus;
	private MapManager mapManager;
	private ObservableList<String> buildingNumbers = FXCollections.observableArrayList();

	public BuildingController(MapManager mapManager, CampusMap campus) {

		// campusMap load database
		this.mapManager = mapManager;
		this.campus = campus;

		for (Building b : campus.getBuildings()) {
			buildingNumbers.add(b.getNumber());
		}
		Collections.sort(buildingNumbers);
	}

	public Building requestSearchBuilding(String searchedString) {
		return campus.searchBuilding(searchedString);
	}

	public Building requestSearchBuildingByNumber(String number) {
		return campus.searchBuildingByNumber(number);
	}


    /**
     * adds a building with
     * @param number
     * @return the new building, or null, when an there is an error;
     *
     * <p>
     *    Errors: pattern doesn't match, building exists
     *
     * </p>
     */
	public Building requestAddBuilding(String number) {

		Building toReturn = campus.addBuilding(number);

		if(toReturn == null){
			return null;
		}

		String buildingNumber = toReturn.getNumber();
		buildingNumbers.add(buildingNumber);
		Collections.sort(buildingNumbers);

		return toReturn;
	}

	/**
	 * deletes a building with number:
	 * 
	 * @param number
	 */
	public OnError requestDeleteBuilding(String number) {

		Building building = campus.searchBuildingByNumber(number);
		if(building == null){
			return OnError.NumberDoesNotExist;
		}

		String buildingNumber = building.getNumber();


		buildingNumbers.remove(buildingNumber);



		for(Vertex vertex: building.getEntrances()){
            mapManager.changeVertex(vertex.getMapId(), vertex.getId(), vertex.getXPos(), vertex.getYPos(), new DefaultType());
		}



        //removing all floors
		for( int i = building.getFloors().size()-1 ; i >= 0 ; i--){
		    int mapId = building.getFloors().get(i).get(0);

            mapManager.removeFloor(mapId);

            mapManager.deleteBackground(mapId);

        }


		return campus.removeBuilding(number);

	}

    /**
     * creates a floor to building with
     * @param number
     * @param up
     * @return the number of the floor or Integer.MAX_Value on error
     */
	public int requestAddFloor(String number, boolean up) {

		Building building = campus.searchBuildingByNumber(number);
		if (building == null) {
            System.out.println("Building not found");
            return Integer.MAX_VALUE;
		}

		ArrayList<Integer> newFloor = new ArrayList<>();

		int floorNumber;

		// fid the floor number
		if (building.getFloors().isEmpty()) { // building has no floors

			floorNumber = 0;

		} else { // building has floors

			if (up) {
				floorNumber = building.getFloors().getLast().get(1);
				floorNumber++;

			} else {
				floorNumber = building.getFloors().getFirst().get(1);
				floorNumber--;

			}

		}

		int newFloorId = mapManager.addFloor(number, floorNumber);

		newFloor.add(newFloorId);
		newFloor.add(floorNumber);

		if (up) {
			building.getFloors().addLast(newFloor);
		} else {
			building.getFloors().addFirst(newFloor);
		}

		return floorNumber;
	}

	/**
	 * removes a floor from the building with
	 * 
	 * @param numberBuilding
	 * @param up;
	 *            true for one floor up; false for one floor down
	 * @return the changed building; null when this building is not found;
	 *
	 *         <p>
	 *         if you try to remove the 0 floor, you get nothing.
	 *         </p>
	 */
	public Building requestRemoveFloor(String numberBuilding, boolean up) {

		Building building = campus.searchBuildingByNumber(numberBuilding);
		if (building == null) {
            System.out.println("Cannot delete: no such Building");
            return null;
        }

        if (building.getFloors().isEmpty() ) {
            System.out.println("Cannot delete: Floor is empty");
            return null;
        }

        int floorId;
        int floorNumber;

        if (up) {
            floorId = building.getFloors().getLast().get(0);
            floorNumber = building.getFloors().getLast().get(1);
            
            // checks if we are trying to remove the ground floor
            if (floorNumber == 0 && building.getFloors().size() > 1) {
            	Logger.print("Cannot delete 0 floor, when there are floors underneath");
                return null;
            }

            building.getFloors().removeLast();
        } else {
            floorId = building.getFloors().getFirst().get(0);
            floorNumber = building.getFloors().getFirst().get(1);

            // checks if we are trying to remove the ground floor
           if (floorNumber == 0 && building.getFloors().size() > 1) {
                Logger.print("Cannot delete 0 floor, when there are floors overneath");
                return null;
            }


            building.getFloors().removeFirst();
        }



        
        if(floorNumber == 0){
        	requestSetBlueprinted(building.getNumber(), false);
        }
        
        
        mapManager.removeFloor(floorId);

        return building;

    }



    /**
     * changes the old adress to
     * @param number
     * @param address
     * @return the changed building and null if this building doesn't exist
     */
    public Building requestChangeBuildingAddress(String number, String address){
        Building build = campus.searchBuildingByNumber(number);
        if(build == null){
            return null;
        }
       return campus.changeBuilding(number, build.getName(), address );
    }


    /**
     * assigns a vertex to this building
     * @param - the number of the building
     * @param
     * @return the Vertex and null if the vertex or building doesn't exists
     */
   /* public Vertex requestAssignEntranceToBuilding(String number, int vertexId){
        Vertex vert = mapManager.getVertex(0, vertexId);

        if(vert == null){
            return null;
        }


        Building build = campus.searchBuildingByNumber(number);

        if(build == null){
            return null;
        }

        if(build.getEntrances().contains(vert)){
			System.out.println("Building has this as an entrance");
			return null;
		}

        return campus.assignVertexToBuildingEntrances(vert,build);
    }*/
    
    
   


    //requestBuldingNumberList():List<String>
    public ObservableList<String> requestBuildingNumberList(){
        return buildingNumbers;
    }





    public LinkedList<Integer> requestEntrances(String buildingNumber){

        LinkedList<Integer> numebrList = new LinkedList<>();

        if(campus.getEntrances(buildingNumber) == null){
            return null;
        }

        for(Vertex v : campus.getEntrances(buildingNumber)){
            numebrList.add(v.getId());
        }

        return numebrList;
    }




    /**
     *
     * @param buildingNumber
     * @return name of this building and null, if this buidling doesn't exist
     */
    public String requestBuildingName(String buildingNumber){
        return campus.searchBuildingByNumber(buildingNumber)!=null ?  campus.searchBuildingByNumber(buildingNumber).getName() : null;
    }


    /**
     *
     * @param buildingNumber
     * @return address of this building and null, if this buidling doesn't exist
     */
    public String requestBuildingAddress(String buildingNumber){
        return campus.searchBuildingByNumber(buildingNumber)!=null ?  campus.searchBuildingByNumber(buildingNumber).getAddress() : null;
    }
    
    
    /**
     * 
     * @param room
     * @return the vertex for this, null ohterwise
     */
    public Vertex requestGetVertexOfRoom(Building building, String room){


    	
    	if (building == null || room == null) {
    		return null;
    	}
    	
    	
    	
    	int vertexId =  campus.getVertexIdofRoom(building.getNumber(), room);



		if(vertexId <= 0){
			System.out.println("Room name not found(1)");
			return null;
		}


        System.out.println("VertexId of this room is " + vertexId);


        System.out.println("Searching room in the building "+building.getNumber());
        for(ArrayList<Integer> floors : building.getFloors()){

    		Vertex vert = mapManager.getVertex(floors.get(0), vertexId);
    		
    			if(vert !=null){
                    System.out.println("Vertex is found "+vert);
    				return vert;
    			}


        }


        System.out.println("Room name not found"); //TODO:
        return null;
    	
    
    }








	/**
	 * returns the mapId of floor with number floorNumber
	 *
	 * @param buildingNuber
	 * @param floorNumber
	 * @return -1 if building not found, or floor not found
	 */
	public int requestMapIdOfFloor(String buildingNuber, int floorNumber) {

		Building building = campus.searchBuildingByNumber(buildingNuber);

		if (building == null) {
			return -1;
		}

		for (ArrayList<Integer> floor : building.getFloors()) {
			if (floor.get(1) == floorNumber) {
				return floor.get(0);
			}
		}

		return -1;
	}

	/**
	 * this method returns a list with the numbers of all floors
	 *
	 * @param buildingNumber
	 * @return a LinkedList with all floors, null, when building not existing
	 */
	public LinkedList<Integer> requestFloorNumbers(String buildingNumber) {
		LinkedList<Integer> floorsNumbers = new LinkedList<>();

		Building build = campus.searchBuildingByNumber(buildingNumber);

		if (build == null) {
			return null;
		}

		for (ArrayList<Integer> floor : build.getFloors()) {
			floorsNumbers.add(floor.get(1));
		}



		return floorsNumbers;
	}

	/**
	 * chnages the name of a building with
	 *
	 * @param number
	 *            and
	 * @param name
	 * @return the new bulding, or null if there is no building with such a
	 *         number
	 */
	public Building requestChangeBuildingName(String number, String name) {
		Building build = campus.searchBuildingByNumber(number);
		if (build == null) {
			return null;
		}
		return campus.changeBuilding(number, name, build.getAddress());
	}


	/**
	 * checks if the map with
	 * @param floorId is on the ground floor
	 * @return true, if floor number is 0, false otherwise, or on error
     */
	public boolean isGroundFloor(int floorId){
		Map map = mapManager.getMap(floorId);

		if(map == null || !(map instanceof FloorMap)){
			return false;
		}

		if(((FloorMap) map).getFloorNumber() == 0){
			return true;
		}

		return false;
	}


    protected void requestSetBlueprinted(String buildingNumber, boolean blueprinted) {
        Building build = campus.searchBuildingByNumber(buildingNumber);

        if(build == null){
            System.out.println("Cannot set blueprinted to nonexisting building: "+buildingNumber);
            return;
        }

        campus.setBlueprinted(buildingNumber, blueprinted);
    }


    /**
     * finds the floorNumber in
     * @param buildingNumber with
     * @param mapID
     * @return -1 on error
     */
    public int requestFloorNumber(String buildingNumber, int mapID){
        Building build = campus.searchBuildingByNumber(buildingNumber);

        if(build == null){
            System.out.println("requestFloorNumber: Cannot find nonexisting building: "+buildingNumber);
            return -1;
        }



        for(ArrayList<Integer> floors : build.getFloors()){
            if(floors.get(0) == mapID){
                return floors.get(1);
            }
        }


        return -1;
    }


    /**
     * getting a whole building by the mapId of one floor
     * @param mapId
     * @return null on error
     */
    public Building requestGetBuilding(int mapId){
        Map map = mapManager.getMap(mapId);

        if(map == null || mapId == 0){
            return null;
        }

        Assert.assertTrue(map instanceof FloorMap);

        String buildingNumber = ((FloorMap) map).getBuildingNumber();

        Building building = requestSearchBuildingByNumber(buildingNumber);
        Assert.assertNotNull(building);

        return building;





    }
}
