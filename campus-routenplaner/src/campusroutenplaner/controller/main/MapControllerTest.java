package campusroutenplaner.controller.main;

import static org.junit.Assert.*;

import campusroutenplaner.model.map.*;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import campusroutenplaner.model.database.Database;
import sun.security.x509.EDIPartyName;

import java.util.LinkedList;

public class MapControllerTest {

	private static MapManager mapManager;

	private static MapController mapController;

	

    @BeforeClass
    public  static void setUpBeforeClass(){
        Database database = new Database("test/usercontroller/mapControllerKITtest.db");
        CampusMap campus = new CampusMap(database);

        mapManager = new MapManager(campus, database);
        mapManager.loadDatabase();
        mapController = new  MapController(mapManager);
    }

	@Before 
	public void before() {

    }
	
	@Test
	public void requestAddVertex() {


		assertNotEquals( null , mapController.requestAddVertex(0, 0, 0));
        assertEquals( null , mapController.requestAddVertex(-1, -1, -1));


	}
	
	
	
	
	@Test
	public void requestSearchVertex() {


		
		assertNotEquals(null, mapController.requestAddVertex(0, 0, 0));
		
		assertEquals(null, mapController.requestSearchVertex(0, 0));

        assertNull(mapController.requestSearchVertex(20, 0));

        assertNull(mapController.requestSearchVertex(-1, 0));

	}
	
	
	@Test
	public void requestSearchEdge() {


        int vert1 = mapController.requestAddVertex(0, 0, 0).getId();
        int vert2 = mapController.requestAddVertex(0, 0, 0).getId();

        Edge edge = mapController.requestAddEdge(0, vert1, 0 , vert2);

		assertEquals(edge, mapController.requestSearchEdge(0, edge.getId()));


        mapController.requestDeleteEdge(0, edge.getId());
        mapController.requestDeleteVertex(0, vert1);
        mapController.requestDeleteVertex(0, vert2);




	}


	@Test
    public void deleteVertexTest(){
	    Vertex vertex = mapController.requestAddVertex(0, 0, 0);

        assertEquals(vertex, mapController.requestSearchVertex(0, vertex.getId()));

        assertEquals(vertex.getId(), mapController.requestDeleteVertex(0, vertex.getId()));

        assertSame(-1, mapController.requestDeleteVertex(0, vertex.getId()));

    }


    @Test
    public void changeVertexTypeInvalidParam(){
       //mapController.requestAddVertex(0, 0, 0);
       assertNull( mapController.requestChangeVertexType(-1, 10, null));
        assertNull( mapController.requestChangeVertexType(20, 10, new DefaultType()));
    }
    @Test
    public void changeVertexType(){
       Vertex vertex  =  mapController.requestAddVertex(1, 0, 0);
       Vertex actual =      mapController.requestChangeVertexType(0, vertex.getId() , new StairsType());

        assertNotEquals(vertex,actual);
    }



    @Test
    public void addEdgeTestInvalidParameters(){


        assertNull(mapController.requestAddEdge(-1, -1, -1 , 0));
        assertNull(mapController.requestAddEdge(20, 5, 1 , 0));
    }

    @Test
    public void addEdgeValid(){

        int vert1 = mapController.requestAddVertex(0, 0, 0).getId();
        int vert2 = mapController.requestAddVertex(0, 0, 0).getId();

        assertNotNull(mapController.requestAddEdge(0, vert1, 0 , vert2));
    }


    @Test
    public void changeXYPosInvalid(){
        assertNull(mapController.requestChangeVertexXYPosition(0, 1, -1, -1));
       assertNull(mapController.requestChangeVertexXYPosition(20, 5, 0, 0));
    }



    @Test
    public void changeXYPos(){
        Vertex vertex =  mapController.requestAddVertex(0, 0, 0);
        double x = vertex.getXPos();
        Vertex changed = mapController.requestChangeVertexXYPosition(0, vertex.getId(), 1, 1);
        assertNotEquals(x, changed.getXPos());
    }




    @Test
    public void deleteEdgeInvalid(){
        assertEquals(-1, mapController.requestDeleteEdge(-1, -1));
    }



    @Test
    public void deleteEdge(){
        assertEquals(-1, mapController.requestDeleteEdge(-1, -1));



        int vert1 = mapController.requestAddVertex(0, 0, 0).getId();
        int vert2 = mapController.requestAddVertex(0, 0, 0).getId();

        Edge edge = mapController.requestAddEdge(0, vert1, 0 , vert2);

        assertEquals(edge.getId(), mapController.requestDeleteEdge(0, edge.getId()));
    }


    @Test
    public void changeEdgeLenghtInvalid(){
        assertNull(mapController.requestChangeEdgeLength(-1, -1, -1));
        assertNull(mapController.requestChangeEdgeLength(20, 4, 2));
    }


    @Test
    public void changeEdgeLenght(){
        int vert1 = mapController.requestAddVertex(0, 0, 0).getId();
        int vert2 = mapController.requestAddVertex(0, 0, 0).getId();

        Edge edge = mapController.requestAddEdge(0, vert1, 0 , vert2);

        assertNotEquals(edge.getLength(), mapController.requestChangeEdgeLength(0, edge.getId(), 5));

    }



    @Test
    public void changeEdgeAccessibleInvalid(){
        assertNull(mapController.requestChangeEdgeAccessible(-1, -1, false));
        assertNull(mapController.requestChangeEdgeAccessible(20, 5, false));
    }

    @Test
    public void changeEdgeAccessible(){
        int vert1 = mapController.requestAddVertex(0, 0, 0).getId();
        int vert2 = mapController.requestAddVertex(0, 0, 0).getId();

        Edge edge = mapController.requestAddEdge(0, vert1, 0 , vert2);

        assertEquals(true, edge.isAccessible());

        assertEquals(false, mapController.requestChangeEdgeAccessible(0, edge.getId(), false).isAccessible());
    }



    @Test
    public void getVertexListInvalidParam(){
        assertNull(mapController.requestVertexList(-1));
    }

    @Test
    public void getVertexList(){
        assertNotNull(mapController.requestVertexList(0));
    }



    @Test
    public void getEdgesListInvalidParam(){
        assertNull(mapController.requestVertexList(-1));
    }

    @Test
    public void getEdgesList(){
        assertNotNull(mapController.requestVertexList(0));
    }




	@Test
    public void changeEdgeBlockedInvalidTest(){
	    assertNull(mapController.requestChangeEdgeBlocked(-1, -1, false));
	    assertNull(mapController.requestChangeEdgeBlocked(20, 100, false));
    }

    @Test
    public void changeEdgeBlocked(){
        int vert1 = mapController.requestAddVertex(0, 0, 0).getId();
        int vert2 = mapController.requestAddVertex(0, 0, 0).getId();

        Edge edge = mapController.requestAddEdge(0, vert1, 0 , vert2);

        assertFalse(edge.isBlocked());


        Edge changed = mapController.requestChangeEdgeBlocked(0, edge.getId(), true);
        assertNotNull(edge);
        assertNotEquals(false, changed.isAccessible());

    }





    @Test
    public void addAliasesTest(){
        Vertex vertex = mapController.requestAddVertex(1, 0 ,0);

        Vertex changed = mapController.requestChangeVertexType(vertex.getMapId(), vertex.getId(), new RoomType("123"));

        mapController.requestAddAlias(vertex.getMapId(), vertex.getId(), "niki");
        mapController.requestAddAlias(vertex.getMapId(), vertex.getId(), "qk");

        LinkedList<String> aliases = new LinkedList<>();
        aliases.add("niki");
        aliases.add("qk");

        assertEquals(aliases, ((RoomType) changed.getType()).getAliases());

    }

    @Test
    public void addAliasesNullTest(){
        mapController.requestAddAlias(-1, -1, "niki");
    }


    @Test
    public void removeAliasTest(){
        Vertex vertex = mapController.requestAddVertex(1, 0 ,0);

        Vertex changed = mapController.requestChangeVertexType(vertex.getMapId(), vertex.getId(), new RoomType("123"));

        mapController.requestAddAlias(vertex.getMapId(), vertex.getId(), "niki");
        mapController.requestAddAlias(vertex.getMapId(), vertex.getId(), "qk");

        LinkedList<String> aliases = new LinkedList<>();
        aliases.add("niki");
        aliases.add("qk");

        assertEquals(aliases, ((RoomType) changed.getType()).getAliases());



        mapController.requestRemoveAlias(changed.getMapId(), changed.getId(), "qk");
        aliases.remove("qk");

        assertEquals(aliases, ((RoomType) changed.getType()).getAliases());

    }

    @Test
    public void removeAliasNullTest(){
        mapController.requestRemoveAlias(-1, -1, "niki");
    }


    @Test
    public void getEdgesTest(){
        int vert1 = mapController.requestAddVertex(2, 0, 0).getId();
        int vert2 = mapController.requestAddVertex(2, 0, 0).getId();

        Edge edge = mapController.requestAddEdge(2, vert1, 2 , vert2);

        LinkedList<Integer> edgesList = new LinkedList<>();
        edgesList.add(edge.getId());

        assertEquals(edgesList, mapController.requestEdgeList(2));

        mapController.requestDeleteEdge(2, edge.getId());
    }

    @Test
    public void getEdgesListNoMapId(){
        assertEquals(null, mapController.requestEdgeList(-2));
    }

    @Test
    public void getBuildingNumber(){
        assertEquals("50.34", mapController.requestGetBuildingNumber(1));

        assertEquals(null, mapController.requestGetBuildingNumber(0));

    }



    @Test
    public void changeBackgroundTest(){
        mapController.changeBackground(5, null, 123, 23, 23);
    }

    @Test
    public void saveBackgroundTest(){
        mapController.requestSaveBackgroundImage(2, null);
    }


    @Test
    public void requestGetBackground(){
        assertNull(mapController.requestGetBackground(-1));
        mapController.requestGetBackground(2);
    }


    @Test
    public void setWidthAndHeight(){
        mapController.requestSetHighWidth(-1, 0, 0);
        mapController.requestSetHighWidth(5, 0, 0);
    }

    @Test
    public void setScale(){
        mapController.requestSetScale(0, 5);
        mapController.requestSetScale(-4, 5);
    }





	
	
	
	@After
	public void after() {

	}

}
