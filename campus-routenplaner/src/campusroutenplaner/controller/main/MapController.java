package campusroutenplaner.controller.main;

import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.regex.Pattern;

import campusroutenplaner.Logger;
import campusroutenplaner.model.map.*;

public class MapController {

	private MapManager mapManager;
	
	public MapController(MapManager mapManager) {
		this.mapManager = mapManager;
	}

	
	/**
	 * add vertex with
	 * @param mapId
	 * @param xPos
	 * @param yPos
	 * @return the vertex, or null if the paramerets are <b>NULL</b>
	 */
	public Vertex requestAddVertex(int mapId, double xPos, double yPos) {
		

		if ( (mapId >= 0) && (xPos >= 0.0) && (yPos >= 0.0) ) {
		    return mapManager.addVertex(mapId, xPos, yPos);
		}

        System.out.println("Adding vertex with invalid parameters");
        return null;
		    
	}


    public Vertex requestSearchVertex(int mapId, int vertexId){
        return mapManager.getVertex(mapId, vertexId);
    }

    public Edge requestSearchEdge(int mapId, int edgeId){
        return mapManager.getEdge(mapId, edgeId);
    }
	
	/**
	 * deletes and edge
	 *
	 * @param mapId
	 * @param id
	 * @return -1 when vertex has edges
	 */
	public int requestDeleteVertex(int mapId, int id) {
		
		return mapManager.deleteVertex(mapId, id);
	}
	
	
	

	
	
	/**
	 * With this methode we can request to change the type of vertex. If the parameters are correct, it will be called up the method changeVertex() from MapManager.
	 * In case that the change is not successful, it will be returned a NULL
	 *
	 * <p> You cannot change the type of a Entrance vertex, that has edges!!!</p>
	 * @return Vertex
	 */
	public Vertex requestChangeVertexType(int mapId, int id, VertexType type) {

        if(mapId >= 0 && id > 0 && type!= null) {
            Vertex vertex = mapManager.getVertex(mapId, id);


            if (vertex == null) {
                Logger.print("Searching for a nonexisting vertex; requestChangeVertexType mapId: " + mapId + ", id: " + id);
                return null;
            }





            return mapManager.changeVertex(mapId, id, vertex.getXPos(), vertex.getYPos(), type);
        }

        System.out.println("changeVertrexType: invalid parameters");
        return null;
	}
	
	
	/**
	 * Change x/y position of vertex.If the parameters are correct, it will be called up the method changeVertex() from MapManager.
	 * Otherweise it will be returned a <b>NULL</b>
	 * @return Vertex
	 */
	public Vertex requestChangeVertexXYPosition(int mapId, int id, double xPos, double yPos) {


		
        if ( (xPos >= 0.0) && (yPos >= 0.0) ) {
          Vertex vertex = mapManager.getVertex(mapId, id);

          if(vertex == null){
              System.out.println("mapController: changeVertexXY no vertex");
              return null;
          }





	      return mapManager.changeVertex(mapId, id, xPos, yPos, vertex.getType());
       }

        System.out.println("ChangeVertexXYPos: invalid parameters");
        return null;
	}
	
   
	
	

	
	

	/**
	 * with this method we can request to create a new edge between two vertices.
	 * If parameters are not correct, then it will be returned null.
	 * @param sourceMapId
	 * @param sourceVertexId
	 * @param destMapId
	 * @param destVertexId
	 * @return Edge or NULL on not false arguments
     */
	 public Edge requestAddEdge(int sourceMapId, int sourceVertexId, int destMapId, int destVertexId) {

		 if ( (sourceMapId >= 0) && (sourceVertexId > 0) && (destMapId >= 0) && (destVertexId > 0) ) {
		
		 	return mapManager.addEdge(sourceMapId, sourceVertexId, destMapId, destVertexId);
	     }

		 System.out.println("Cannot create an edge between sourceMapId: "+ sourceMapId + " sourceVertId: "+ sourceVertexId + " destMapId: "+  destMapId + " desVertId "+ destVertexId );
		 return null;
	
	}





	 /**
	 * request to get  List of vertices for specific map .If mapId is not correct, return defaultList.
     * @param mapId
     * @return LinkedList<Integer>
     */		
     public LinkedList<Integer> requestVertexList(int mapId) {



         if (mapId >= 0) {

             return mapManager.getVertices(mapId);
         }

         System.out.println("requestVertexList: mapId invalid");
         return null;
		
     }

    public LinkedList<Integer> requestEdgeList(int mapId){

        if (mapId >= 0) {
            return mapManager.getEdgeList(mapId);
        }

        System.out.println("requestEdgeList: mapId invalid");
        return null;
    }
	
	
	/**
	 * Request to delete Edge. It will called up the method deleteEdge from MapManager.
	 * In case that the handover parameters are not correct, it will be returned -1.
	 * @param mapId
	 * @param edgeId
	 * @return int
	 */
	public int requestDeleteEdge(int mapId, int edgeId) {
		
		if ( (mapId >= 0) && (edgeId >= 0)) {
			return mapManager.deleteEdge(mapId, edgeId);
		}
		return -1;
	}
	
	



	/**
	 * changes the length of the edge with
	 * @param mapId
	 * @param id
	 * @param length
     * @return the changed edge, or NULL by error
     */
   public Edge requestChangeEdgeLength(int mapId, int id, double length) {

	   if ( (mapId >= 0) && (id >= 0) && (length >= 0) ){

           Edge edge = mapManager.getEdge(mapId, id);

           if(edge == null){
               System.out.println("Edge with mapId: "+mapId + " and id: "+ id + " not found");
               return null;
           }
		

			return mapManager.changeEdge(mapId, id, length, edge.isAccessible(), edge.isBlocked());
		}

       System.out.println("changeEdgelenght: wrong parameter");
	   return null;
  }



	
	/**
	 * Changes accessible of the edge with mapId and id, to
	 * @param accessible
	 *@return Edge if successful, NULL otherwise
	 */
  public Edge requestChangeEdgeAccessible(int mapId, int id, boolean accessible) {

	  if ( (mapId >= 0) && (id >=0) ) {

			Edge edge = mapManager.getEdge(mapId, id);

            if(edge == null){
                System.out.println("Edge with mapId: "+mapId + " and id: "+ id + " not found");
                return null;
            }

			return mapManager.changeEdge(mapId, id, edge.getLength(), accessible, edge.isBlocked());
		}

      System.out.println("change Edge accessible wrong parameters");
      return null;

  }

	/**
	 * changes the edgo with
	 * @param mapId
	 * @param id to
	 * @param blocked
     * @return the changed edge, or NULL  otherwise
     */
	public Edge requestChangeEdgeBlocked( int mapId, int id, boolean blocked) {
		if ( (mapId >= 0) && (id > 0) ) {
		    Edge edge = mapManager.getEdge(mapId, id);

            if(edge == null){
                System.out.println("Edge not existing, mapId: "+ mapId + " id: "+ id);
                return null;
            }

			return mapManager.changeEdge(mapId, id, edge.getLength(), edge.isAccessible(), blocked);
		 }

		System.out.println("Edge with mapId: "+mapId + " and id: "+ id + " not found");
	  return null;
   }



	
	protected Background changeBackground(int mapID, BufferedImage img, double high, double width, double scale) {

		return mapManager.changeBackground(mapID, img, high, width, scale);
	}
	
	
	protected Background requestSaveBackgroundImage(int mapId, BufferedImage picture)  {
	      return mapManager.saveBackgroundImage(mapId, picture);
	   }
	
	/**
	 * It will be called up the method getBackground() from MapManager
	 *@return Background
	 */
	public Background requestGetBackground(int mapId) {
		if (mapId >= 0) {
			return mapManager.getBackground(mapId);
			}
	   return null;
	}

    public void requestSetHighWidth(int mapId, double high, double width){
    	//high und width werden gar nicht benutzt
        Background bg = requestGetBackground(mapId);
        if(bg == null){
            return;
        }
        changeBackground(mapId, bg.getPicture(), high, width, bg.getScale());
    }
    
    public void requestSetScale(int mapID, double scale) {
    	Background bg = requestGetBackground(mapID);
        if(bg == null){
            return;
        }
        changeBackground(mapID, bg.getPicture(), bg.getHight(), bg.getWidth(), scale);
    }



    
    public OnError requestAddAlias(int mapId, int id, String alias) {
    	
    	Vertex vertex = mapManager.getVertex(mapId, id);
    	
    	if (vertex != null) {

    	    VertexType type = vertex.getType();

            if(type instanceof RoomType){
                LinkedList<String> aliases;
                aliases = ((RoomType) vertex.getType()).getAliases();
                aliases.add(alias);
                ((RoomType) type).setAliases(aliases);
            }



        	mapManager.changeVertex(mapId, id, vertex.getXPos(), vertex.getYPos(), type);
        	return OnError.SuccessfulAdd;
    	}
    	
    	return OnError.UnSuccessfulAdd;
    }



    
   public OnError requestRemoveAlias(int mapId, int id, String alias) {
    	
    	Vertex vertex = mapManager.getVertex(mapId, id);
    	
    	if (vertex != null) {
    		  LinkedList<String> aliases = new LinkedList<>();
              if(vertex.getType() instanceof RoomType){
                  aliases.addAll(((RoomType) vertex.getType()).getAliases());
                  aliases.remove(alias);
                  ((RoomType) vertex.getType()).setAliases(aliases);
              }
        	mapManager.changeVertex(mapId, id, vertex.getXPos(), vertex.getYPos(), vertex.getType());
        	return OnError.SuccessfulAdd;
    	}
    	
    	return OnError.UnSuccessfulAdd;
    }





    protected String requestGetBuildingNumber(int floorId){
        Map map = mapManager.getMap(floorId);

        if(map instanceof FloorMap){
            return ((FloorMap) map).getBuildingNumber();
        }

        return null;
    }
   
	
}
