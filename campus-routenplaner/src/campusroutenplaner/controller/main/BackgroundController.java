package campusroutenplaner.controller.main;

import campusroutenplaner.model.map.Background;
import campusroutenplaner.model.map.Building;

import java.awt.image.BufferedImage;

public class BackgroundController {

	private MapController mapController;
    private BuildingController buildingController;

	public BackgroundController(MapController mapController, BuildingController buildingController) {
		this.mapController = mapController;
		this.buildingController = buildingController;
	}
	

	/**
	 * It will called up the method requestBackgroundImage from MapController
	 *@return Background
	 */
	public  Background requestGetBackground(int mapId) {
	     return mapController.requestGetBackground(mapId);
	}
	

	
	
	
	public Background requestSetHighWidth(int mapId, double hight, double width){
		
		Background bg = mapController.requestGetBackground(mapId);
		
		if(bg == null){
			return null;
		}
		
		return mapController.changeBackground(mapId, bg.getPicture(), hight, width, bg.getScale());
	}
	
	
	/**
	 * It will called up the method requestBackgroundImage from MapController
	 *@return Background
	 */
//	  public Background requestGetBackgroundImage(Building building, int floorNumber) {

//		  return mapController.requestGetBackgroundImage(building, floorNumber);
//	   }
	
	/**
	 * It will called up the method requestLoadBackgroundImage from MapController
	 *@return Background
	 */
   public Background requestSaveBackgroundImage(int mapId, BufferedImage picture)  {


	   Background background  = mapController.requestSaveBackgroundImage(mapId, picture);

      if (background != null){
    	  if (mapId == 0) {
    		  return background;
    	  }
 

         buildingController.requestSetBlueprinted(mapController.requestGetBuildingNumber(mapId),true);
         

      }

        return background;

   }
	
	/**
	 * It will called up the method requestGetBackgroundImage from MapController to become the image, 
	 * that must be shifted.Then it will called up the method shift from Background.
	 * In case that the parameters are not correct , then it will be returned the same image without changes.
	 *@return Background
	 */
	//public Background requestShift(int mapId, int x, int y) {
	//   if ( (mapId >= 0) && (x >= 0) && (y >= 0) ) {
	
	//	    Background background = mapController.requestGetBackgroundImage(mapId);
	//		Background shiftedBackground = background.shift(x, y);
	//		return shiftedBackground;
     //    }
	//	return background;
	//  
//	}
	
	
	/**
	 * It will called up the method requestGetBackgroundImage from MapController to become the image, 
	 * that must be zoomed.Then it will called up the method shift from Background.
	 * In case that the parameters are not correct , then it will be returned the same image without changes.
	 *@return Background
	 */
//	 public Background requestZoom(int mapId, int scale) {
		
	//  if ( (mapId >= 0 ) && (scale >= 0) && (scale <= 100) ) {
	
	// 		Background background = mapController.requestGetBackgroundImage(mapId);
	//		Background zoomed = background.zoom(scale);
	//		return zoomed;
	//	}
	// return background;	 
	
   
	
   public Background requestSetScale(int mapId, double scale){
	   Background bg = mapController.requestGetBackground(mapId);
	   
	   if(bg == null){
		   return null;
	   }
	   
	   return mapController.changeBackground(mapId, bg.getPicture(), bg.getHight() , bg.getWidth(), scale);
	   
	  
   }
   
   
	
}
