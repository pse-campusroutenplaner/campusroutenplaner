package campusroutenplaner.controller.main;

import static org.junit.Assert.*;

import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import campusroutenplaner.model.database.Database;
import campusroutenplaner.model.map.Background;
import campusroutenplaner.model.map.CampusMap;
import campusroutenplaner.model.map.MapManager;

public class BackgroundControllerTest {

	private BackgroundController backgroundController;
	private MapController mapController;
    private BuildingController buildingController;
    private MapManager mapManager;
    private Database data;
    private CampusMap campus;
    private Background background;
    private BufferedImage image;
    
    @Before
    public void before() {
		data = new Database("test/model/KIT(testCopy).db");
		background = new Background();
		
		image = new BufferedImage(200,200,BufferedImage.TYPE_INT_RGB);
		campus = new CampusMap(data);
    	mapManager = new MapManager(campus, data);
    	mapController = new MapController(mapManager);
    	buildingController = new BuildingController(mapManager, campus);
    	
    	backgroundController = new BackgroundController(mapController, buildingController);
    	
    	
    	mapManager.loadDatabase();
    	
    	
    }
	
	
	
	
	
	
	
	
	
	
	@Test
	public void requestGetBackground() {
		
		assertEquals(null, backgroundController.requestGetBackground(99));
		
		
	}
	
	@Test
	public void requestSetHighWidth() {
		
		assertEquals(backgroundController.requestSetHighWidth(0, 0, 0), backgroundController.requestSetHighWidth(0, 0, 0));
		assertEquals(null, backgroundController.requestSetHighWidth(999, 0, 0));

		
	}
	
	
	@Test
	public void requestSaveBackgroundImage() {
		
		Background background = backgroundController.requestSaveBackgroundImage(0, image);
		
		assertEquals(background.getPicture(), backgroundController.requestSaveBackgroundImage(0, image).getPicture());
		mapManager.addFloor("50.50", 1);
		
		Background backgroundTwo = backgroundController.requestSaveBackgroundImage(1, image);
		
		assertEquals(backgroundTwo.getPicture(), backgroundController.requestSaveBackgroundImage(1, image).getPicture());

		
		
	}
	
	
	@Test
	public void requestSetScale() {
		
		background = backgroundController.requestSetScale(0, 0);
		
		assertEquals(background, backgroundController.requestSetScale(0, 0));
		
		assertEquals(null, backgroundController.requestSetScale(99, 0));


		
	}
	
	@After
	public void after() {
		data.forgetDB();
	}
	
	

}
