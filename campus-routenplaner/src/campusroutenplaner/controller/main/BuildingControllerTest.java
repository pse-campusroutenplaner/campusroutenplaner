package campusroutenplaner.controller.main;

import campusroutenplaner.model.database.Database;
import campusroutenplaner.model.map.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import sun.awt.image.BufferedImageDevice;

import java.util.LinkedList;

import static org.junit.Assert.*;

/**
 * Created by nmlad on 8/14/16.
 */
public class BuildingControllerTest {

    private static BuildingController buildingController;
    private static MapManager mapManager;

    @BeforeClass
    public static void setUpBeforeClass(){
        Database database = new Database("test/usercontroller/buildingControllerTest.db");
        CampusMap campus = new CampusMap(database);


        mapManager = new MapManager(campus, database);
        mapManager.loadDatabase();

       campus.addBuilding("00.00");
        campus.addBuilding("50.34");

    //    Vertex vertex  = mapManager.addVertex(0, 0, 0);
    //    mapManager.changeVertex(vertex.getMapId(), vertex.getId(), 0, 0, new EntranceType("50.34"), new LinkedList<>());


        buildingController = new BuildingController(mapManager, campus);


    }


    @Test
    public void addBuildingTest(){

        Building building;

        //succ add
        building = buildingController.requestAddBuilding("11.11");

        assertNotNull(building);

        assertTrue(buildingController.requestBuildingNumberList().contains("11.11"));




        building = buildingController.requestAddBuilding("11.11");
        assertNull(building);


        buildingController.requestDeleteBuilding("11.11");

    }

    @Test
    public void addBuildingInvalidNumber(){
        Building building;

        building = buildingController.requestAddBuilding("11,11");
        assertNull(building);
        assertFalse(buildingController.requestBuildingNumberList().contains("11,11"));

        building = buildingController.requestAddBuilding("11.1");
        assertNull(building);
        assertFalse(buildingController.requestBuildingNumberList().contains("11.1"));

        building = buildingController.requestAddBuilding("1.11");
        assertNull(building);
        assertFalse(buildingController.requestBuildingNumberList().contains("1.11"));


    }


    @Test
    public void deleteBuilding(){
        System.out.println(buildingController.requestBuildingNumberList());
        assertTrue(buildingController.requestBuildingNumberList().contains("00.00"));


        buildingController.requestAddFloor("00.00", true);
        buildingController.requestAddFloor("00.00", true);

        int floorNumber0 = 0;
        int floorNumber1 = 1;

        int vertId;
        Vertex vertex = mapManager.addVertex(0, 0, 0);
        vertId = vertex.getId();
        mapManager.changeVertex(0, vertId, 0 , 0, new EntranceType("00.00"));



        mapManager.addVertex(buildingController.requestMapIdOfFloor("00.00", floorNumber0), 0, 0);


        vertId = mapManager.addVertex(buildingController.requestMapIdOfFloor("00.00", floorNumber1), 0, 0).getId();
        LinkedList<String> aliases = new LinkedList<>();
        aliases.add("niki");
        mapManager.changeVertex(buildingController.requestMapIdOfFloor("00.00", floorNumber1), vertId, 0, 0, new RoomType("212").setAliases(aliases));


        assertEquals(OnError.SuccessfulRemoved, buildingController.requestDeleteBuilding("00.00"));
        assertNull(buildingController.requestEntrances("00.00"));

        buildingController.requestAddBuilding("00.00");
    }

    @Test
    public void deleteNotExistingBuildng(){
        assertEquals(OnError.NumberDoesNotExist, buildingController.requestDeleteBuilding("11.11"));
    }






    @Test
    public void addFloorToNonExistingBuilding(){
        assertEquals(Integer.MAX_VALUE, buildingController.requestAddFloor("20.20", true));
    }

    @Test
    public void add0Floor(){


        assertEquals(0, buildingController.requestAddFloor("00.00", true));

        Building building = buildingController.requestSearchBuildingByNumber("00.00");

        assertEquals(1, building.getFloors().size());


        buildingController.requestRemoveFloor("00.00", true);
    }

    @Test
    public void addFloor(){
        buildingController.requestAddFloor("00.00", true);


        assertEquals(1, buildingController.requestAddFloor("00.00", true));


        Building building = buildingController.requestSearchBuildingByNumber("00.00");


        assertEquals(2, building.getFloors().size());



        assertEquals(-1, buildingController.requestAddFloor("00.00", false));

        assertEquals(3, building.getFloors().size());




        buildingController.requestRemoveFloor("00.00", true);
        buildingController.requestRemoveFloor("00.00", false);
        buildingController.requestRemoveFloor("00.00", true);


    }





    @Test
    public void removeFloorEmptyFloors(){
        assertNull(buildingController.requestRemoveFloor("00.00", true));
    }

    @Test
    public void removeFloorTest(){
        Building building;

        //adding 0
        buildingController.requestAddFloor("00.00", true);
        //adding 1
        buildingController.requestAddFloor("00.00", true);


        //trying to delete 0
        building = buildingController.requestRemoveFloor("00.00", false);
        assertNull(building);


        //adding -1
        buildingController.requestAddFloor("00.00", false);





        //delete floor 1
        building = buildingController.requestRemoveFloor("00.00", true);
        assertNotNull(building);
        assertEquals(2, building.getFloors().size());


        //try to delete flor 0
        building = buildingController.requestRemoveFloor("00.00", true);
        assertNull(building);



        //deleting floor -1
        building =buildingController.requestRemoveFloor("00.00", false);
        assertNotNull(building);
        assertEquals(1, building.getFloors().size());



        //delete floor 0
        building = buildingController.requestRemoveFloor("00.00", true);
        assertNotNull(building);
        assertEquals(0, building.getFloors().size());



        building = buildingController.requestSearchBuildingByNumber("00.00");
        assertEquals(0, building.getFloors().size());
        assertEquals(false, building.isBlueprinted());
    }





    @Test
    public void searchBuildingByString(){
        assertEquals("50.34", buildingController.requestSearchBuilding("Fasanengarten 5").getNumber());
    }

    @Test
    public void searchBuildingByBuilding(){
        assertEquals("50.34", buildingController.requestSearchBuildingByNumber("50.34").getNumber());
    }





    @Test
    public void changeBuildingAddress(){
        Building building = buildingController.requestChangeBuildingAddress("50.34", "Fasanengarten 7");

        assertEquals("Fasanengarten 7", building.getAddress());


        buildingController.requestChangeBuildingAddress("50.34", "Fasanengarten 5");
    }

    @Test
    public void changeNonExistingBuildingAddress(){
        Building building = buildingController.requestChangeBuildingAddress("20.20", "Fasanengarten 7");

        assertEquals(null, building);
    }


    @Test
    public void getBuildingList(){
        assertEquals("50.34", buildingController.requestBuildingNumberList().get(0));
        assertEquals("00.00", buildingController.requestBuildingNumberList().get(1));
    }


    @Test
    public void getEntrancesTest(){
        assertEquals(1, buildingController.requestEntrances("50.34").size());
        assertSame(1,  buildingController.requestEntrances("50.34").get(0));

        assertNull(buildingController.requestEntrances("20.20"));
    }


    @Test
    public void getMapIdOfBuilding(){
        buildingController.requestAddFloor("50.34", true);

        assertNotEquals(-1, buildingController.requestMapIdOfFloor("50.34", 0));
        assertEquals(-1, buildingController.requestMapIdOfFloor("20.20", 0));
        assertEquals(-1, buildingController.requestMapIdOfFloor("00.00", 2));

    }




    @Test
    public void getNonExistingBuildingNameTest(){
        assertNull(buildingController.requestBuildingName("20.20"));
    }

    @Test
    public void getBuildingNameTest(){
        assertEquals("Infobau", buildingController.requestBuildingName("50.34"));
    }


    @Test
    public void getNonExistingBuildingAddressTest(){
        assertNull(buildingController.requestBuildingAddress("20.20"));
    }

    @Test
    public void getBuildingAdressTest(){
        assertEquals("Fasanengarten 5", buildingController.requestBuildingAddress("50.34"));
    }





    @Test
    public void getVertexIdOfRoomBuildingNull(){
        assertNull(buildingController.requestGetVertexOfRoom(null, null));
    }

    @Test
    public void getVertexIdOfRoomNoRoom(){
        assertNull(buildingController.requestGetVertexOfRoom(buildingController.requestSearchBuildingByNumber("50.34"),  "pedal"));
    }


    @Test
    public void getVertexIdofRoomValid(){
        assertNotNull(buildingController.requestGetVertexOfRoom(buildingController.requestSearchBuildingByNumber("50.34"),   "niki"));
    }

    @Test
    public void getVertexIdofRoomNotValid(){
        assertNull(buildingController.requestGetVertexOfRoom(buildingController.requestSearchBuildingByNumber("50.34"),   "pedal"));
    }


    @Test
    public void requestFloorNumbersNullBuilding(){
        assertNull(buildingController.requestFloorNumbers("20.20"));
    }

    @Test
    public void getFloorNumbers(){
        LinkedList<Integer> floors = new LinkedList<>();




        //adding 0
        buildingController.requestAddFloor("00.00", true);

        //adding 1
        buildingController.requestAddFloor("00.00", true);

        //adding -1
        buildingController.requestAddFloor("00.00", false);

        floors.add(-1);
        floors.add(0);
        floors.add(1);

        assertEquals(floors, buildingController.requestFloorNumbers("00.00"));


        buildingController.requestRemoveFloor("00.00", true);
        buildingController.requestRemoveFloor("00.00", false);
        buildingController.requestRemoveFloor("00.00", true);

    }





    @Test
    public void changeBuildingName(){
        Building building = buildingController.requestChangeBuildingName("50.34", "Ime");

        assertEquals("Ime", building.getName());


        buildingController.requestChangeBuildingName("50.34", "Infobau");
    }

    @Test
    public void changeNonExistingBuildingName(){
        Building building = buildingController.requestChangeBuildingName("20.20", "Fasanengarten 7");

        assertEquals(null, building);
    }

    @Test
    public void isGroundFloorErrorTest(){
        assertFalse(buildingController.isGroundFloor(0));
    }

    @Test
    public void isGroundFloorTest() {
        assertTrue(buildingController.isGroundFloor(1));
    }


    @Test
    public void isGroundFloorTestOtherFloor() {
        assertFalse(buildingController.isGroundFloor(2));
    }




    @Test
    public void setBlueprintedTestNonExisitngBuilding(){
        buildingController.requestSetBlueprinted("20.20", false);
    }
    @Test
    public void setBlueprintedTest(){
        buildingController.requestSetBlueprinted("00.00", true);

        assertTrue(buildingController.requestSearchBuildingByNumber("00.00").isBlueprinted());

        buildingController.requestSetBlueprinted("00.00", false);

    }



    @Test
    public void getFloorNumberNonExistingBuilding(){
        assertSame(-1, buildingController.requestFloorNumber("20.20", 5));
    }
    @Test
    public void getFloorNumberNonExistingFloor(){
        assertSame(-1, buildingController.requestFloorNumber("00.00", 5));
    }
    @Test
    public void getFloorNumber(){
        assertEquals(1,buildingController.requestFloorNumber("50.34", 2));
    }


    @Test
    public void getBuildTest(){
        Building build = buildingController.requestSearchBuildingByNumber("50.34");

        int mapId = buildingController.requestMapIdOfFloor("50.34", 0);

        assertEquals(build, buildingController.requestGetBuilding(mapId));


        assertNull(buildingController.requestGetBuilding(25));

    }

    @AfterClass
    public static void cleanUpAfterClass(){
        buildingController = null;
    }



}
