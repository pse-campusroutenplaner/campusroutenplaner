package campusroutenplaner.controller.usercontroller;

import campusroutenplaner.model.logIn.LogInManager;
import campusroutenplaner.model.map.OnError;

public class LogInController {

	LogInManager logInMan;
	
	/*
	 * Constructor
	 * @param logIn
	 */
	public LogInController(LogInManager logIn) {
		this.logInMan = logIn;
	}
	
	
	
	/**
	 * this method call up the method logIn from the Class LogInManager
	 * @param name
	 * @param password
	 * @return Enum
	 */
	public OnError requestLogIn(String name, String password) {
		 
		return logInMan.checkLogIn(name, password);
		
	}
	
	/**
	 * this method call up the method ChangePassword from the Class LogInManager
	 * @param oldPassword
	 * @param newPassword
	 * @return Enum 
	 */
	public OnError requestChangePassword (String oldPassword, String newPassword) {
		
		return logInMan.changePassword(oldPassword, newPassword);
	}
	
	/**
	 * this method call up the method changeName from the Class LogInManager
	 * @param oldName
	 * @param newName
	 * @return Enum
	 */
	//public OnError requestChangeName(String oldName, String newName) {
		
	//	return logInMan.changeName(oldName, newName);
//	 }
	
	
	
	
}
