package campusroutenplaner.controller.usercontroller;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.Set;

import com.sun.org.apache.bcel.internal.generic.NEW;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import campusroutenplaner.model.constructionsites.ConstructionSite;
import campusroutenplaner.model.constructionsites.ConstructionSiteManager;
import campusroutenplaner.model.map.Edge;
import campusroutenplaner.model.map.OnError;

public class ConstructionSiteController {

	private ConstructionSiteManager constrMan;
	private ObservableList<ConstructionSite> constructionSiteObservable = FXCollections.observableArrayList();
	/**
	 * Constructor Controller
	 * 
	 * @param constrMan
	 */
	public ConstructionSiteController(ConstructionSiteManager constrMan) {
		this.constrMan = constrMan;
		constructionSiteObservable.addAll(constrMan.getConstructionSite());
	}


	/**
	 * call up the method addConstructionSite from ConstructionSiteManager.
	 * 
	 * @param beginDate
	 * @param endDate
	 * @param edgeId
	 * @return OnError
	 */
	public OnError requestAddConstructionSite(int edgeId, LocalDate start, LocalDate end) {

		ConstructionSite temp;
		
		if (!start.equals(null) && (!end.equals(null))) {
			 temp = constrMan.addConstructionSite(start, end, edgeId);
			 if (temp != null) {
					constructionSiteObservable.add(temp);
					return OnError.SuccessfulAdd;
				}	
	 
		}
			
			
		return OnError.UnSuccessfulAdd;

	}
	
	public OnError requestAddIndefiniteConstructionSite(LocalDate startDate, int edgeId) {
		
		if (!startDate.equals(null)) {
			
		}
		
		return OnError.SuccessfulAddIdenfinite;
		
	}

	/**
	 * call up the method changeConstructionSite from ConstructionSiteManager.
	 * 
	 * @param constructionSiteId
	 * @param fromDate
	 * @param toDate
	 * @param edgeId
	 * @return ConstructionSite
	 */
	public ConstructionSite requestChangeConstructionSite(int constructionSiteId, LocalDate fromDate, LocalDate toDate,
			int edgeId) {
		
		ConstructionSite constructionSite = requestGetConstructionSite(constructionSiteId);
		ConstructionSite changed = constrMan.changeConstructionSite(constructionSiteId, fromDate, toDate, edgeId);
		if (changed != null) {
			constructionSiteObservable.remove(constructionSite);
			constructionSiteObservable.add(changed);
		}
		return changed;
	}

	/**
	 * call up the method getConstructionSite from ConstructionSiteManager.
	 * 
	 * @param constructionId
	 * @return ConstructionSite
	 */
	public ConstructionSite requestGetConstructionSite(int constructionId) {
		return constrMan.getConstructionSite(constructionId);
	}

	/**
	 * call up the method removeConstructionSite from ConstructionSiteManager.
	 * 
	 * @param constructionSiteId
	 * @return Enum
	 */
	public OnError requestRemoveConstructionSide(int constructionSiteId) {
		
		
		if ( constrMan.removeConstructionSite(constructionSiteId) == OnError.SuccessfulRemoved) {
			for (ConstructionSite temp : constructionSiteObservable) {
				if (temp.getId() == constructionSiteId) {
					constructionSiteObservable.remove(temp);
					return OnError.SuccessfulRemoved;
				}
			}
		}
		return OnError.UnSuccessfulRemoved;
	}

	/**
	 * call up the method getConstructionSites from ConstructionManager
	 * 
	 * @return
	 */
	public ObservableList<ConstructionSite> requestGetConstructionSites() {
		return constructionSiteObservable;
	}

	/**
	 * 
	 * call up the method getConstructionSiteId from the class
	 * ConstructionSiteManager
	 * 
	 * @param edgeId
	 * 
	 * @return int
	 * 
	 */

	public int requestGetConstructionSiteId(int edgeId) {
		return constrMan.getConstructionSiteId(edgeId);
	}
}
