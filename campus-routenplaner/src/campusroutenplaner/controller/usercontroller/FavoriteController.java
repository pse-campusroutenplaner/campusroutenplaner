package campusroutenplaner.controller.usercontroller;



import java.util.LinkedList;

import campusroutenplaner.model.favorites.FavoriteManager;
import campusroutenplaner.model.map.OnError;

public class FavoriteController {
	
	private FavoriteManager favoriteManager;
	
	public FavoriteController (FavoriteManager favoriteManager) {
		this.favoriteManager = favoriteManager;
	}

	/**
	 * call up the method loadFavoritesFromDatabase from FavoriteManager
	 */
	public LinkedList<String> requestLoadFavoritePlacesFromDatabase() {
		return favoriteManager.loadFavoritePlaces();
	}

	/**
	 * call up the method loadFavoriteRoutesFromDatabase from FavoriteManager
	 * @return
	 */
	public LinkedList<String[]> requestLoadFavoriteRoutesFromDatabase() {
		return favoriteManager.loadFavoriteRoutes();
	}
	
	/**
	 * call up the method loadFavoriteRoutesWithStopFromDatabase from FavoriteManager
	 * @return
	 */
	public LinkedList<String[]> requestLoadFavoriteRoutesWithStopFromDatabase() {
		return favoriteManager.loadFavoriteRoutesWithStop();
	}
	
	
	/**
	 * call up the method getFavoritePlaces from FavoriteManager
	 * @return
	 */
	public LinkedList<String> requestGetFavoritePlaces() {
		return favoriteManager.getPlaces();
	}
	
	/**
	 * call up the method getFavoriteRoutes from FavoriteManager
	 * @return
	 */
	public LinkedList<String[]> requestGetFavoriteRoutes() {
		return favoriteManager.getRoutes();
	}
	
	/**
     * call up the method getFavoriteRoutesWithStop from FavoriteManager
	 * @return
	 */
	public LinkedList<String[]> requestGetFavoriteRoutesWithStop() {
		return favoriteManager.getRoutesWithStop();
	}
	
	
	/**
	 * call up the method getFavoritePlace from FavoriteManager
	 * If the commited paramer is null, return null.
	 * @param name
	 * @return
	 */
	public String requestGetFavoritePlace(String name) {
	
		if (!name.equals("")) {
			return favoriteManager.getFavoritePlace(name);
		}
		
		return null;
	}
	
	/**
	 * call up the method getFavoriteRoute from FavoriteManager
	 * If the commited parameters are null, return null
	 * @param source
	 * @param destination
	 * @return
	 */
	public String[] requestGetFavoriteRoute(String source, String destination) {
		if ( (!source.equals("")) && (!destination.equals("")) ) {
			return favoriteManager.getFavoriteRoute(source, destination);
		}
		
		return null;
	}
	
	/**
	 * call up the method getFavoriteRouteWithStop from FavoriteManager
	 * If the commited parameters are null, return null
	 * @param source
	 * @param stop
	 * @param destination
	 * @return
	 */
	public String[] requestGetFavoriteRouteWithStop(String source, String stop, String destination) {
		
		if ( (!source.equals("")) && (!stop.equals("")) && (!destination.equals("")) ) {
			return favoriteManager.getFavoriteRouteWithStopp(source, stop, destination);
		}
		return  null;
	}
	
	/**
	 * call up the method addFavoritePlace from FavoriteManager
	 * If the commited parameter is null, it will be returned ToAddFavoriteParametersEqualsNull;
	 * @param name
	 * @return Enum
	 */
	public OnError requestAddFavoritePlace(String name) {
		
		if (!name.equals("")) {
			return favoriteManager.addFavoritePlace(name);
		}
		return OnError.ToAddFavoriteNameEqualsNull;
	}
	
	/**
	 * call up the method addFavoriteRoute from FavoriteManager
	 * if the commited parameters are null, it will be returned ToAddFavoriteParametersEqualsNull;
	 * @param source
	 * @param destination
	 * @return Enum
	 */
	public OnError requestAddFavoriteRoute(String source, String destination) {
		
		if ( (!source.equals("")) && (!destination.equals("")) ) {
			return favoriteManager.addFavoriteRoute(source, destination);
		}
		return OnError.ToAddFavoriteParametersEqualsNull;
	}
	
	/**
	 * call up the method addFavoriteRouteWithStop from FavoriteManager
	 * if the commited parameters are null, it will be returned ToAddFavoriteParametersEqualsNull;
	 * @param source
	 * @param stop
	 * @param destination
	 * @return Enum
	 */
	public OnError requestAddFavoriteRouteWithStop(String source, String stop, String destination) {
		
		if( (!source.equals("")) && (!stop.equals("")) && (!destination.equals("")) ) {
			return favoriteManager.addFavoriteRouteWithStopp(source, stop, destination);
		}
		return OnError.ToAddFavoriteParametersEqualsNull;
	}
	
	/**
	 * call up the method removeFavoritePlace from FavoriteManager
	 * if the commited parameter is null, it will be returned ToRemoveFavoriteParametersEqualsNull;
	 * @param name
	 * @return Enum
	 */
	public OnError requestRemoveFavoritePlace(String name) {
		
		if (!name.equals("")) {
			return favoriteManager.removeFavoritePlace(name);
		}
		return OnError.ToRemoveFavoriteNameEqualsNull;
	}
	
	/**
	 * call up the method removeFavoriteRoute from FavoriteManager
	 * if the commited parameters are null, it will be returned ToRemoveFavoriteParametersEqualsNull;
	 * @param source
	 * @param destination
	 * @return Enum
	 */
	public OnError requestRemoveFavoriteRoute(String source, String destination) {
		if ( (!source.equals("")) && (!destination.equals("")) ) {
			return favoriteManager.removeFavoriteRoute(source, destination);
		}
		return OnError.ToRemoveFavoriteParametersEqualsNull;
	}
	
	/**
	 * call up the method removeFavoriteRouteWithStop from FavoriteManager
	 *  if the commited parameters are null, it will be returned ToRemoveFavoriteParametersEqualsNull;
	 * @param source
	 * @param stop
	 * @param destination
	 * @return Enum
	 */
	public OnError requestRemoveFavoriteRouteWithStop(String source, String stop, String destination) {
		
		if( (!source.equals("")) && (!stop.equals("")) && (!destination.equals("")) ) {
			return favoriteManager.removeFavoriteRouteWithStop(source, stop, destination);
		}
		return OnError.ToRemoveFavoriteParametersEqualsNull;
	}
	
	
	
	
	
	
}
