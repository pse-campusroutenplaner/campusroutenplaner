package campusroutenplaner.controller.usercontroller;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import campusroutenplaner.model.constructionsites.ConstructionSite;
import campusroutenplaner.model.constructionsites.ConstructionSiteManager;
import campusroutenplaner.model.database.Database;
import campusroutenplaner.model.map.Edge;
import campusroutenplaner.model.map.OnError;
import campusroutenplaner.model.map.Vertex;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ConstructionSiteControllerTest {
	
	
	
	private ObservableList<ConstructionSite> constructionSiteObservable = FXCollections.observableArrayList();

	
	private ConstructionSiteManager constrMan;
	private ConstructionSiteController constructionSiteController;
	private Database data;
	private LocalDate begin, end, defaultDate;
	private Edge edge, edgeTwo;
	private Vertex v1, v2;
	
	
	
	
	
	@Before
	public void before() {
		
		
		data = new Database("test/model/KIT(testCopy).db");

		constrMan = new ConstructionSiteManager(data);

		constructionSiteController = new ConstructionSiteController(constrMan);
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		begin = LocalDate.parse("2015-11-12", formatter);
		end = LocalDate.parse("2016-12-16", formatter);
        defaultDate = LocalDate.parse("2030-01-01", formatter);
        
		v1 = new Vertex();
		v2 = new Vertex();
		
		edge = new Edge(1, v1, v2);
    	edgeTwo = new Edge(2, v1, v2);
		
	}
	
	
	
	@Test
	public void test() {
		
		
		constrMan.getConstructionSite().clear();

		 
		
		assertEquals(OnError.SuccessfulAdd, constructionSiteController.requestAddConstructionSite(edgeTwo.getId(), begin, end));
		assertEquals(OnError.UnSuccessfulAdd, constructionSiteController.requestAddConstructionSite(edgeTwo.getId(), defaultDate, defaultDate));

	}
	
	
	
	@Test
	public void requestIdentify() {

		assertEquals(OnError.SuccessfulAddIdenfinite, constructionSiteController.requestAddIndefiniteConstructionSite(begin, edge.getId()));

	}
	
	@Test
	public void requestChange() {

//		ConstructionSite defaultConstr = );

		assertEquals(0, constrMan.getConstructionSite().size());
		assertEquals(OnError.SuccessfulAdd, constructionSiteController.requestAddConstructionSite(edgeTwo.getId(), begin, end));
		assertEquals(1, constrMan.getConstructionSite().size());

		
		
		assertEquals(constrMan.getConstructionSite(1), constructionSiteController.requestChangeConstructionSite(constrMan.getConstructionSite(1).getId() , constrMan.getConstructionSite(1).getBeginDate(), constrMan.getConstructionSite(1).getEndDate(), constrMan.getConstructionSite(1).getEdgeId()));

	}
	
	
	
	@Test
	public void requestGet() {
		
		assertEquals(OnError.SuccessfulAdd, constructionSiteController.requestAddConstructionSite(edgeTwo.getId(), begin, end));

		
		assertEquals(constrMan.getConstructionSite(1), constrMan.getConstructionSite(1));
	}
	
	
	
	@Test
	public void requestRemove() {
		
		assertEquals(OnError.SuccessfulAdd, constructionSiteController.requestAddConstructionSite(edgeTwo.getId(), begin, end));

		
		assertEquals(OnError.SuccessfulRemoved, constructionSiteController.requestRemoveConstructionSide(1));
		assertEquals(OnError.UnSuccessfulRemoved, constructionSiteController.requestRemoveConstructionSide(1));

		
	}
	
	
	
	
	@Test
	public void requestGetConstructionSites() {
		
		assertEquals(OnError.SuccessfulAdd, constructionSiteController.requestAddConstructionSite(edgeTwo.getId(), begin, end));

		assertEquals(constructionSiteController.requestGetConstructionSites(), constructionSiteController.requestGetConstructionSites());
		
	}
	
	
	
	@Test
	public void requestGetConstructionSiteId() {
		
		assertEquals(OnError.SuccessfulAdd, constructionSiteController.requestAddConstructionSite(edgeTwo.getId(), begin, end));

		assertEquals(constructionSiteController.requestGetConstructionSiteId(edgeTwo.getId()), constructionSiteController.requestGetConstructionSiteId(edgeTwo.getId()));
		
	}
	
	
	
	@After
	public void after() {
		data.forgetDB();
	}

}
