package campusroutenplaner.controller.usercontroller;
 
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
 
@RunWith(value = Suite.class)
@Suite.SuiteClasses(value = { ConstructionSiteControllerTest.class, FavoriteControllerTest.class, LogInControllerTest.class, RoutingControllerTest.class})

public class AllTest {
 
    
}