package campusroutenplaner.controller.usercontroller;

import campusroutenplaner.model.database.Database;
import campusroutenplaner.model.logIn.LogInManager;
import campusroutenplaner.model.map.OnError;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by nmlad on 8/15/16.
 */
public class LogInControllerTest {

    private static LogInController logInController;
    private static Database database;

    @BeforeClass
    public static void setUpBeforeClass(){

        database = new Database("test/model/mapManagerTestKit.db");
        LogInManager logInManager = new LogInManager(database);
        logInController = new LogInController(logInManager);

    }





    @Test
    public void succLogin(){
        assertEquals(OnError.SuccessfulLogin, logInController.requestLogIn("admin", "1"));
    }

    @Test
    public void unsuccLogin(){
        assertEquals(OnError.UnSuccessfulLogin, logInController.requestLogIn("admin", "2"));
    }

    @Test
    public void succChangePassword(){
        assertEquals(OnError.SuccessfulChangePassword, logInController.requestChangePassword("1", "2") );
        logInController.requestChangePassword("2", "1");
    }


    @Test
    public void unsuccChangePassword(){
        assertEquals(OnError.UnSuccessfulChangePassword, logInController.requestChangePassword("3", "2") );

    }


    @After
    public void tearDown(){

    }
}
