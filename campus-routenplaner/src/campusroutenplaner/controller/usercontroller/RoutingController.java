package campusroutenplaner.controller.usercontroller;

import java.util.LinkedList;

import campusroutenplaner.Logger;
import campusroutenplaner.controller.main.BuildingController;
import campusroutenplaner.model.map.Building;
import campusroutenplaner.model.map.Edge;
import campusroutenplaner.model.map.Vertex;
import campusroutenplaner.model.routing.RouteType;
import campusroutenplaner.model.routing.RoutingManager;

/**
 * Created by nmladenov on 7/5/16.
 */
public class RoutingController {

	private RoutingManager routingMan;
	private BuildingController buildingCont;

	public RoutingController(RoutingManager routingMan, BuildingController buildingCont) {
		this.routingMan = routingMan;
		this.buildingCont = buildingCont;
	}

	/**
	 * take strings sourcebuilding and destinationBuilding. Find those buildings.If such buildings are not found , then return empty list.
	 * Otherweise take the strings sourceroom and destinationroom and find the Vertex as Object. If this objects are equals null
	 * then nothing to add in sourceList and destList.Otherweise add the object in the corresponding list. The idea is, that dijkstra must receive only lists,because the method cover several cases at once.
	 * Then proof what kind of route needs to be found.
	 * it must called up the method routing(respectly routingAcccess) from the class RoutingManager
	 * 
	 * @param sourceBuilding
	 * @param sourceRoom
	 * @param destinationBuilding
	 * @param destinationRoom
	 * @return LinkedList<Vertex>
	 */
	public LinkedList<Edge> requestRouting(String sourceBuilding, String sourceRoom, String destinationBuilding,
			String destinationRoom, String routeType) {
		Logger.print("Nachaloto na routing");
		
	   Building startBuilding = buildingCont.requestSearchBuilding(sourceBuilding);
       Building destBuilding = buildingCont.requestSearchBuilding(destinationBuilding);

        System.out.println("Field for sourceBuilding: ["+sourceBuilding + "]");
        System.out.println("Field for destinationBuilding: ["+destinationBuilding + "]");
       System.out.println("Field for sourceRoom: ["+sourceRoom+"]");
		System.out.println("Field for destinationRoom: ["+destinationRoom+"]");

		if (startBuilding == null || destBuilding == null) {
            System.out.println("Building(s) not found");
            return null;
		}
		
		LinkedList<Vertex> sourceList = new LinkedList<Vertex>();
		LinkedList<Vertex> destList = new LinkedList<Vertex>();
		
		RouteType routeTYpe = RouteType.valueOf(routeType);
		
		if ((sourceRoom.equals("")) && (destinationRoom.equals(""))) {
			
			if  (routeTYpe == RouteType.bike) {
				return routingMan.routingBike(startBuilding.getEntrances(), sourceList, destBuilding.getEntrances(), destList);
			}
			
			if (routeTYpe == RouteType.accessible) {
				return routingMan.routingAccessilbe(startBuilding.getEntrances(), sourceList, destBuilding.getEntrances(), destList);
			}
			
            Logger.print("Routing building <-> building");
            return routingMan.routing(startBuilding.getEntrances(), sourceList, destBuilding.getEntrances(), destList);
		}


		Vertex source = null;
		Vertex destination = null;

		if(!sourceRoom.equals("")){
			source = buildingCont.requestGetVertexOfRoom(startBuilding, sourceRoom);


            if(source == null){
                System.out.println("Source not found");
                return null;
            }
		}

		if(!destinationRoom.equals("")){
			destination = buildingCont.requestGetVertexOfRoom(destBuilding, destinationRoom);


            if(destination == null){
                System.out.println("Dest not found");
                return null;
            }

		}
		



		
		if (source != null) {
            System.out.println("sourceList.add "+source);
            sourceList.add(source);
		}
		if (destination != null) {
            System.out.println("destList.add "+ destination);
            destList.add(destination);
		}
		
		

        Logger.print("Route type: "+routeTYpe);

		if (routeTYpe == RouteType.bike) {
			if ( (!sourceList.isEmpty()) || (!destList.isEmpty()) ){
					return new LinkedList<Edge>(); 
			}
			
		   return routingMan.routingAccessilbe(startBuilding.getEntrances(), sourceList, destBuilding.getEntrances(), destList);

	    }
		  
		if (routeTYpe == RouteType.accessible) {
			Logger.print("ACCESSIBLE");
			 return  routingMan.routingAccessilbe(startBuilding.getEntrances(), sourceList, destBuilding.getEntrances(), destList);
		}
		Logger.print("Obiknoven");
		System.out.println("Get" + startBuilding.getEntrances() + " End: "+ destBuilding.getEntrances());
		return routingMan.routing(startBuilding.getEntrances(), sourceList, destBuilding.getEntrances(), destList);

	}

	/**
	 * take strings sourcebuilding ,stopOverBuilding and destinationBuilding. Find those buildings.If such buildings are not found , then return empty list.
	 * Otherweise take the strings sourceroom , stopOverRoom and destinationroom and find the Vertex as Object. If this objects are equals null
	 * then nothing to add in sourceList and destList.Otherweise add the object in the corresponding list. The idea is, that dijkstra must receive only lists,because the method cover several cases at once.
	 * Then proof what kind of route needs to be found.
	 * it must called up the method routingWithStop(respectly routingWithStopAcccess) from the class RoutingManager
	 * @param sourceBuilding
	 * @param sourceRoom
	 * @param stopOverBuilding
	 * @param stopOverRoom
	 * @param destinationBuilding
	 * @param destinationRoom
	 * @param routeType
	 * @return LinkedList
	 */
	public LinkedList<Edge> requestRoutingWithStop(String sourceBuilding, String sourceRoom, String stopOverBuilding,
			String stopOverRoom, String destinationBuilding, String destinationRoom, String routeType) {

		
		
		//Logger.print("Sourcebuilding " + sourceBuilding + " sourceroom " + sourceRoom + " stopoverbuilding " + stopOverBuilding + " stopoverroom " + stopOverRoom + " destinationBuilding " + destinationBuilding + " destinationroom " + destinationRoom);
		
		Building start = buildingCont.requestSearchBuilding(sourceBuilding);
		Building stop = buildingCont.requestSearchBuilding(stopOverBuilding);
		Building destination = buildingCont.requestSearchBuilding(destinationBuilding);
		
		
		
		
		
		if ((start == null) || (stop == null) || (destination == null)) {
			return null; 
		}


		LinkedList<Vertex> sourceList = new LinkedList<Vertex>();
		LinkedList<Vertex> stopList = new LinkedList<Vertex>();
		LinkedList<Vertex> destList = new LinkedList<Vertex>();


		RouteType routeTYpe = RouteType.valueOf(routeType);
		
		if ( (sourceRoom.equals("")) && (stopOverRoom.equals("")) && (destinationRoom.equals("")) ) {
			
			if  (routeTYpe == RouteType.bike) {
				return routingMan.routingWithStopAccessible(start.getEntrances(), sourceList, stop.getEntrances(), stopList, destination.getEntrances(), destList);
			}
			
			if (routeTYpe == RouteType.accessible) {
				return routingMan.routingWithStopAccessible(start.getEntrances(), sourceList, stop.getEntrances(), stopList, destination.getEntrances(), destList);
			}
			
			return routingMan.routingWithStop(start.getEntrances(), sourceList, stop.getEntrances(), stopList, destination.getEntrances(), destList);
		}
		
		
		Logger.print("idvash li pone tuka ");
		Vertex source = null;
		Vertex stopOver = null;
		Vertex dest = null;
		
		Logger.print("DESTINATIONLIST 2" + destList.size());

		
		
		
		
		if(!sourceRoom.equals("")) {
			source = buildingCont.requestGetVertexOfRoom(start, sourceRoom);
			
			if(source == null){
                System.out.println("Source not found");
                return null;
            }
			
		
		}
		
		
		
		
		if(!stopOverRoom.equals("")) {
			
			stopOver = buildingCont.requestGetVertexOfRoom(stop, stopOverRoom);
			Logger.print("STOPOVERBUIDLING " + stopOver);
			
			if(stopOver == null){
				Logger.print("STOPOVERBUIDLING " + stopOver);
                System.out.println("stopOver not found");
                return null;
            }
			
			

		}

		
		if(!destinationRoom.equals("")) {
			
			dest = buildingCont.requestGetVertexOfRoom(destination, destinationRoom);
			//Logger.print("dest "+ dest.getId());
			if(dest == null){
                System.out.println("dest not found");
                return null;
            }
			
		}
		
		
		

		
		
		if (source != null) {

            sourceList.add(source);
		}
		
		
		if (stopOver != null) {

			stopList.add(stopOver);
		}
		

		


		if (dest != null) {

            destList.add(dest);
		}
		


		
		if (routeTYpe == RouteType.bike) {
			
			if ( (!sourceList.isEmpty()) ||  (!stopList.isEmpty()) || (destList.isEmpty())  ) {
				return new LinkedList<Edge>();
			}
			return routingMan.routingWithStopAccessible(start.getEntrances(), sourceList, stop.getEntrances(), stopList,
					destination.getEntrances(), destList);
		}

		if (routeTYpe == RouteType.accessible) {
			return routingMan.routingWithStopAccessible(start.getEntrances(), sourceList, stop.getEntrances(), stopList,
					destination.getEntrances(), destList);
		}
		
		
		Logger.print("STIGASH LI DO TUKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		Logger.print("STOPLIST AFTER ADD" + stopOverRoom.isEmpty());

        return routingMan.routingWithStop(start.getEntrances(), sourceList, stop.getEntrances(), stopList,
				destination.getEntrances(), destList);

	}
	
	
	public LinkedList<Edge> requestSimpleRouting(String building, String room, String rootType) {
		
		Building start = buildingCont.requestSearchBuilding(building);
		
		if (start == null) {
			return null;
		}
		
		Vertex source = buildingCont.requestGetVertexOfRoom(start, room);
		LinkedList<Vertex> roomList = new LinkedList<Vertex>();
		
		if (room != null) {
			roomList.add(source);
		}
		
		routingMan.simpleRouting(start.getEntrances(), roomList);
		
		
		RouteType routeTYpe = RouteType.valueOf(rootType);
		
		if (routeTYpe == RouteType.bike) {
			if ( (!roomList.isEmpty())  ) {
				return new LinkedList<Edge>();
			}
			return routingMan.simpleRoutingAccessible(start.getEntrances(), roomList);
		}

		if (routeTYpe == RouteType.accessible) {
			return routingMan.simpleRoutingAccessible(start.getEntrances(), roomList);
		}
		System.out.println("YYYEEES");
		return routingMan.simpleRouting(start.getEntrances(), roomList);
	}
	

}
