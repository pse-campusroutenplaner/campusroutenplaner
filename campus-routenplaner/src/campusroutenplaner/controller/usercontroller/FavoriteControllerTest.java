package campusroutenplaner.controller.usercontroller;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import campusroutenplaner.model.database.Database;
import campusroutenplaner.model.favorites.FavoriteManager;
import campusroutenplaner.model.map.OnError;

public class FavoriteControllerTest {

	
	
	private FavoriteController favorit;
	private FavoriteManager favoritMan;
	private Database data;
	
	
	
	@Before
	public void before() {
		data = new Database("test/model/KIT(testCopy).db");
		favoritMan = new FavoriteManager(data);
		favorit = new FavoriteController(favoritMan);
	}
	
	
	@SuppressWarnings("deprecation")
	@Test
	public void requestLoadFromDB() {

		data.forgetDB();
		assertEquals(new LinkedList<String>(), favorit.requestLoadFavoritePlacesFromDatabase());
		assertEquals(new LinkedList<String>(), favorit.requestLoadFavoriteRoutesWithStopFromDatabase());
		assertEquals(new LinkedList<String>(), favorit.requestLoadFavoriteRoutesFromDatabase());		
		
		


	}

	@SuppressWarnings("deprecation")
	@Test
	public void requesGet() {
		assertEquals(new LinkedList<String>(), favorit.requestGetFavoritePlaces());
		assertEquals(new LinkedList<String>(), favorit.requestGetFavoriteRoutes());
		assertEquals(new LinkedList<String>(), favorit.requestGetFavoriteRoutesWithStop());
		assertEquals(new LinkedList<String>(), favorit.requestGetFavoriteRoutesWithStop());
		assertEquals(null, favorit.requestGetFavoritePlace(""));
		assertEquals(new String(), favorit.requestGetFavoritePlace("name"));

		assertEquals(null, favorit.requestGetFavoriteRoute("name", "name"));
		assertEquals(null, favorit.requestGetFavoriteRoute("", ""));
		assertEquals(null, favorit.requestGetFavoriteRouteWithStop("name", "name", "name"));

		assertEquals(null, favorit.requestGetFavoriteRouteWithStop("", "", ""));

	}

	@Test
	public void requesAdd() {

		assertEquals(OnError.SuccessfulAdd, favorit.requestAddFavoritePlace("jallo"));
		
		assertEquals(OnError.ToAddFavoriteNameEqualsNull, favorit.requestAddFavoritePlace(""));

		assertEquals(OnError.SuccessfulAdd, favorit.requestAddFavoriteRoute("jallo", "jallo"));
		
		assertEquals(OnError.ToAddFavoriteParametersEqualsNull, favorit.requestAddFavoriteRoute("", ""));

		assertEquals(OnError.SuccessfulAdd, favorit.requestAddFavoriteRouteWithStop("jallo", "jallo", "jallo"));
		assertEquals(OnError.ToAddFavoriteParametersEqualsNull, favorit.requestAddFavoriteRouteWithStop("", "", ""));


	}
	
	
	@Test
	public void requestRemove() {
		
		assertEquals(OnError.SuccessfulAdd, favorit.requestAddFavoritePlace("jallo"));

		assertEquals(OnError.SuccessfulRemoved, favorit.requestRemoveFavoritePlace("jallo"));
		assertEquals(OnError.ToRemoveFavoriteNameEqualsNull, favorit.requestRemoveFavoritePlace(""));

		assertEquals(OnError.SuccessfulAdd, favorit.requestAddFavoriteRoute("jallo", "jallo"));
		assertEquals(OnError.SuccessfulRemoved, favorit.requestRemoveFavoriteRoute("jallo", "jallo"));
		assertEquals(OnError.ToRemoveFavoriteParametersEqualsNull, favorit.requestRemoveFavoriteRoute("", ""));

		assertEquals(OnError.SuccessfulAdd, favorit.requestAddFavoriteRouteWithStop("jallo", "jallo", "jallo"));
		assertEquals(OnError.SuccessfulRemoved, favorit.requestRemoveFavoriteRouteWithStop("jallo", "jallo", "jallo"));
		assertEquals(OnError.ToRemoveFavoriteParametersEqualsNull, favorit.requestRemoveFavoriteRouteWithStop("", "", ""));



		
	}



		
	@After
	public void after() {
		data.forgetDB();
	}


	
	
	
	
	
	

}
