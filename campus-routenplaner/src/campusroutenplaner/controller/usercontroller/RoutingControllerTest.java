package campusroutenplaner.controller.usercontroller;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import campusroutenplaner.controller.main.BuildingController;
import campusroutenplaner.model.database.Database;
import campusroutenplaner.model.map.Building;
import campusroutenplaner.model.map.CampusMap;
import campusroutenplaner.model.map.MapManager;
import campusroutenplaner.model.map.Vertex;
import campusroutenplaner.model.routing.RoutingManager;

public class RoutingControllerTest {

	private RoutingController routingController;
	private RoutingManager routingMan;
	private BuildingController buildingCont;
	private MapManager mapManager;
	private CampusMap campus;
	private Database data;
	private String string, stringTwo;
	private LinkedList<Vertex> entrance;
	
	
	@Before
	public void before() {
		
		data = new Database("test/model/KIT(testCopy).db");
		
		campus = new CampusMap(data);
		mapManager = new MapManager(campus, data);
		
		routingMan = new RoutingManager();
		buildingCont = new BuildingController(mapManager, campus);
		
		routingController = new RoutingController(routingMan, buildingCont);
		string = new String();
		stringTwo = "build";

		
		//campus.addBuilding("50.50");
		 entrance = new LinkedList<Vertex>();

		
	}
	
	
	
	
	
	
	@Test
	public void requestRouting() {



		

		
	    campus.addBuilding("50.50");
	    data.addBuilding("50.50", "", "");
		
		Building building = new Building("50.50");
		
//		//int i = data.addFloor(0, "50.50");
//		int i = mapManager.addFloor("50.50", 0);
//		
////		Vertex entranceOne = mapManager.addVertex(0, 0, 0);
////		Vertex entranceTwo = mapManager.addVertex(0, 1, 1);
////		
////		Vertex room = mapManager.addVertex(0, 2, 2);
////		
////		Vertex roomTwo = mapManager.addVertex(0, 3, 3);
//		//int roomId = room.getId();
//		//int roomTwoId = roomTwo.getId();
////	    data.addVertex(2, 2, "room", 1);
////		data.addVertex(3, 3, "room", 1);
//		
//		data.addRoom(2, 2, i, "ggg");
//		data.addRoom(3, 3, i, "ggt");
//		
	    mapManager.loadDatabase();
//
//		
//		//System.out.println("Erster RoomId ist: " + data.getVertexIdOfRoom("50.50", "NameOne"));
//		
//		String roomName = "ggg";
//		String roomTwoName = "ggt";
//		String room = "hallo";


		assertEquals(entrance, routingController.requestRouting(string, string, string, string, "bike"));
		assertEquals(null, routingController.requestRouting(stringTwo, string, stringTwo, string, "bike"));
		assertEquals(entrance, routingController.requestRouting("50.50", string, "50.50", string, "bike"));
		assertEquals(entrance, routingController.requestRouting("50.50", string, "50.50", string, "accessible"));
		assertEquals(entrance, routingController.requestRouting("50.50", string, "50.50", string, "walk"));
		assertEquals(null, routingController.requestRouting("50.50", stringTwo, "50.50", stringTwo, "walk"));
		assertEquals(null, routingController.requestRouting("50.50", stringTwo, "50.50", string, "walk"));
		


	
  }
	
	@Test
	public void routingController() {
		
		

	    campus.addBuilding("50.50");
	    campus.addBuilding("50.66");
	    data.addBuilding("50.50", "", "");
	    data.addBuilding("50.66", "", "");

		Building building = new Building("50.50");
		Building buildingTwo = new Building("50.66");
		
		//int i = data.addFloor(0, "50.50");
		int i = mapManager.addFloor("50.50", 0);
		
//		Vertex entranceOne = mapManager.addVertex(0, 0, 0);
//		Vertex entranceTwo = mapManager.addVertex(0, 1, 1);
//		
//		Vertex room = mapManager.addVertex(0, 2, 2);
//		
//		Vertex roomTwo = mapManager.addVertex(0, 3, 3);
		//int roomId = room.getId();
		//int roomTwoId = roomTwo.getId();
//	    data.addVertex(2, 2, "room", 1);
//		data.addVertex(3, 3, "room", 1);
		
		data.addRoom(2, 2, i, "ggg");
		data.addRoom(3, 3, i, "ggt");
		
	    mapManager.loadDatabase();

		
		//System.out.println("Erster RoomId ist: " + data.getVertexIdOfRoom("50.50", "NameOne"));
		
		String roomName = "ggg";
		String roomTwoName = "ggt";
		String room = "hallo";
		
		
		
		assertEquals(null, routingController.requestRouting("50.50", "ggg", "50.50", "hallo", "walk"));
		assertEquals(null, routingController.requestRouting("50.50", "ggg", "50.50", "hallo", "walk"));

		assertEquals(null, routingController.requestRouting("50.66", "ggg", "50.66", "hy", "bike"));
		

		assertEquals(entrance, routingController.requestRouting("50.50", "ggg", "50.50", "ggt", "walk"));

		assertEquals(entrance, routingController.requestRouting("50.50", "ggg", "50.50", "ggg", "bike"));

		assertEquals(null, routingController.requestRouting("50.66", "ggg", "50.66", "ggg", "bike"));
		
		assertEquals(null, routingController.requestRouting("50.66", "ggg", "50.66", "gggd", "bike"));

		
		assertEquals(entrance, routingController.requestRouting("50.50", "ggg", "50.50", "ggt", "accessible"));
		assertEquals(entrance, routingController.requestRouting("50.50", "ggg", "50.50", "ggt", "accessible"));
		assertEquals(entrance, routingController.requestRouting("50.50", "ggg", "50.50", "ggt", "accessible"));

		
		
		assertEquals(entrance, routingController.requestRouting("50.50", "ggg", "50.50", "ggt", "bike"));
		assertEquals(entrance, routingController.requestRouting("50.50", "ggg", "50.50", "ggt", "bike"));
		assertEquals(entrance, routingController.requestRouting("50.50", "ggg", "50.50", "ggt", "bike"));
		
		
	}
	
	
	
	
	
	
	@Test
	public void def() {
		
	    mapManager.loadDatabase();

		
		  campus.addBuilding("50.50");
		    campus.addBuilding("50.66");
		    data.addBuilding("50.50", "", "");
		    data.addBuilding("50.66", "", "");

			Building building = new Building("50.50");
			Building buildingTwo = new Building("50.66");
			
			//int i = data.addFloor(0, "50.50");

			int i = mapManager.addFloor("50.50", 0);
			data.addRoom(2, 2, i, "ggg");
			data.addRoom(3, 3, i, "ggt");
//		
		
		assertEquals(null, routingController.requestRouting("50.50", "ggg", "50.50", "hallo", "walk"));
		assertEquals(null, routingController.requestRouting("50.50", "ggg", "50.50", "hallo", "walk"));

		assertEquals(null, routingController.requestRouting("50.66", "ggg", "50.66", "hy", "bike"));

	}
	
	
	
	
	@Test
	public void requestRoutingWithStop() {
		
		
		

	    campus.addBuilding("50.50");
	    campus.addBuilding("50.66");
	    data.addBuilding("50.50", "", "");
	    data.addBuilding("50.66", "", "");

		Building building = new Building("50.50");
		Building buildingTwo = new Building("50.66");
		
		//int i = data.addFloor(0, "50.50");
		int i = mapManager.addFloor("50.50", 0);
		
//		Vertex entranceOne = mapManager.addVertex(0, 0, 0);
//		Vertex entranceTwo = mapManager.addVertex(0, 1, 1);
//		
//		Vertex room = mapManager.addVertex(0, 2, 2);
//		
//		Vertex roomTwo = mapManager.addVertex(0, 3, 3);
		//int roomId = room.getId();
		//int roomTwoId = roomTwo.getId();
//	    data.addVertex(2, 2, "room", 1);
//		data.addVertex(3, 3, "room", 1);
		
		data.addRoom(2, 2, i, "ggg");
		data.addRoom(3, 3, i, "ggt");
		
	    mapManager.loadDatabase();

		
		//System.out.println("Erster RoomId ist: " + data.getVertexIdOfRoom("50.50", "NameOne"));
		
		String roomName = "ggg";
		String roomTwoName = "ggt";
		String room = "hallo";

		assertEquals(entrance, routingController.requestRoutingWithStop("50.50", "ggg", "50.50", "ggt", "50.50", "ggt", "walk"));

		assertEquals(entrance, routingController.requestRoutingWithStop("50.50", "ggg", "50.50", "ggg", "50.50", "ggg", "bike"));

		assertEquals(null, routingController.requestRoutingWithStop("50.66", "ggg", "50.66", "ggg", "50.66", "ggg", "bike"));
		
		assertEquals(entrance, routingController.requestRoutingWithStop("50.50", "ggg", "50.50", "ggt", "50.50", "ggt", "accessible"));
		assertEquals(entrance, routingController.requestRoutingWithStop("50.50", "ggg", "50.50", "ggt", "50.50", "ggt", "accessible"));
		
		assertEquals(null, routingController.requestRoutingWithStop("50.50", "ggg", "50.50", "hallo", "50.50", "hallo", "walk"));
		assertEquals(null, routingController.requestRoutingWithStop("50.50", "ggg", "50.50", "hallo", "50.50", "hallo",  "walk"));

		assertEquals(null, routingController.requestRoutingWithStop("50.66", "ggg", "50.66", "hy", "50.66", "hy", "bike"));

	}
	
	
	
	
	@Test
	public void requestRoutingAccesiibleTwo() {
		
		
		

		

		
	    campus.addBuilding("50.50");
	    data.addBuilding("50.50", "", "");
		
		Building building = new Building("50.50");
		
//		//int i = data.addFloor(0, "50.50");
//		int i = mapManager.addFloor("50.50", 0);
//		
////		Vertex entranceOne = mapManager.addVertex(0, 0, 0);
////		Vertex entranceTwo = mapManager.addVertex(0, 1, 1);
////		
////		Vertex room = mapManager.addVertex(0, 2, 2);
////		
////		Vertex roomTwo = mapManager.addVertex(0, 3, 3);
//		//int roomId = room.getId();
//		//int roomTwoId = roomTwo.getId();
////	    data.addVertex(2, 2, "room", 1);
////		data.addVertex(3, 3, "room", 1);
//		
//		data.addRoom(2, 2, i, "ggg");
//		data.addRoom(3, 3, i, "ggt");
//		
	    mapManager.loadDatabase();
//
//		
//		//System.out.println("Erster RoomId ist: " + data.getVertexIdOfRoom("50.50", "NameOne"));
//		
//		String roomName = "ggg";
//		String roomTwoName = "ggt";
//		String room = "hallo";


		assertEquals(entrance, routingController.requestRoutingWithStop(string, string, string, string, string, string, "bike"));
		assertEquals(null, routingController.requestRoutingWithStop(stringTwo, string, stringTwo, string, stringTwo, string,  "bike"));
		assertEquals(entrance, routingController.requestRoutingWithStop("50.50", string, "50.50", string, "50.50", string, "bike"));
		assertEquals(entrance, routingController.requestRoutingWithStop("50.50", string, "50.50", string, "50.50", string, "accessible"));
		assertEquals(entrance, routingController.requestRoutingWithStop("50.50", string, "50.50", string, "50.50", string, "walk"));
		assertEquals(null, routingController.requestRoutingWithStop("50.50", stringTwo, "50.50", stringTwo, "50.50", stringTwo, "walk"));
		assertEquals(null, routingController.requestRoutingWithStop("50.50", stringTwo, "50.50", string, "50.50", string, "walk"));


		assertEquals(null, routingController.requestRoutingWithStop("50.50", "ggg", "50.50", "hallo", "50.50", "hallo", "walk"));
		assertEquals(null, routingController.requestRoutingWithStop("50.50", "ggg", "50.50", "hallo", "50.50", "hallo",  "walk"));

		assertEquals(null, routingController.requestRoutingWithStop("50.66", "ggg", "50.66", "hy", "50.66", "hy", "bike"));

		
		
		//Vertex erster = buildingCont.requestGetVertexOfRoom(building, "ggg");
		
		//System.out.println("ERSTER: " + erster);
	//	System.out.println("roomname " + campus.getVertexIdofRoom(building.getNumber(), roomName));
		
		//System.out.println("roomTwoname " + campus.getVertexIdofRoom(building.getNumber(), roomTwoName));

//		campus.assignVertexToBuildingEntrances(room, building);
//		campus.assignVertexToBuildingEntrances(entranceTwo, building);
//		campus.assignVertexToBuildingEntrances(entranceOne, building);
//		campus.assignVertexToBuildingEntrances(roomTwo, building);

		
//
		
		
	}
	
	
	
	
	@Test
	public void simpleRoutingTest() {

		
	    campus.addBuilding("50.50");
	    data.addBuilding("50.50", "", "");
		
		Building building = new Building("50.50");
		

	    mapManager.loadDatabase();


		assertEquals(entrance, routingController.requestSimpleRouting(string, string, "bike"));
		assertEquals(null, routingController.requestSimpleRouting(stringTwo, string, "bike"));
		assertEquals(entrance, routingController.requestSimpleRouting("50.50", string, "bike"));
		assertEquals(null, routingController.requestSimpleRouting("50.50", string, "accessible"));
		assertEquals(null, routingController.requestSimpleRouting("50.50", string, "walk"));
		assertEquals(null, routingController.requestSimpleRouting("50.50", stringTwo, "walk"));
		assertEquals(null, routingController.requestSimpleRouting("50.50", stringTwo, "walk"));



	}
	
	
	
	
	
	
	@After
	public void after() {
		data.forgetDB();
	}

}
