package campusroutenplaner.model.database;
import java.sql.Connection;
import java.util.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class GlobalDB {
    Connection conn = null;
    Statement db;
    public GlobalDB() throws Exception {
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        //conn = DriverManager.getConnection("jdbc:mysql://mysql1.gear.host/kitcampus:3306", "kitcampus", "Rn5Jfe6jmE*");
        conn = DriverManager.getConnection("jdbc:mysql://mysql1.gear.host/kitcampus?allowMultiQueries=true&keepAlive=true&user=kitcampus&password=Rn5Jfe6jmE*");
        db = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
        System.out.println("Connected to global database successfully");
    }
    
    public Date getLastChange() throws SQLException {
        ResultSet rs = db.executeQuery("select Max(Datum) from Changes");
        rs.next();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return df.parse(rs.getString("Max(Datum)"));
        } catch (Exception e) {
            return new Date(0);
        }
    }
    
	public void command(String command) {
		try {
		    System.out.println(command);
			db.executeUpdate(command);
		} catch (SQLException e) {
			System.out.println("SQL-Error");
			e.printStackTrace();
		}
	}
	
	public void closeConn() {
	    try {
            conn.close();
        } catch (SQLException e) {
        }
	}

	public ResultSet show(String command) {
		ResultSet rs;
		try {
			rs = db.executeQuery(command);
			return rs;
		} catch (Exception e) {
		    try {
		        System.out.println("Connection lost. Reconnecting to globale database");
		        conn.close();
		        Class.forName("com.mysql.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection("jdbc:mysql://mysql1.gear.host/kitcampus?allowMultiQueries=true&keepAlive=true&user=kitcampus&password=Rn5Jfe6jmE*");
                rs = conn.createStatement().executeQuery(command);
                return rs;
            } catch (Exception e1) {
                System.out.println("Problem with reconnecting to gdb");
                e1.printStackTrace();
            }
		    
		}
		return null;
}

}
