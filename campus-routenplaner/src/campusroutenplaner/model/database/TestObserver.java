package campusroutenplaner.model.database;

import campusroutenplaner.view.admin.controller.Observer;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.layout.VBox;

public class TestObserver implements Observer {
    @FXML
    private VBox history;
    @FXML
    private ObservableList<String> content;
    @SuppressWarnings("unused")
    private Subject subject;
    @Override
    public void update(String message) {
    }

    @Override
    public void setSubject(Subject subject) {
        if (subject != null) {
            this.subject = subject;
            subject.register(this);
        }
    }
}
