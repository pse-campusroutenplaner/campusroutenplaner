package campusroutenplaner.model.database;

import static org.junit.Assert.*;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

import campusroutenplaner.model.database.Database;
import campusroutenplaner.view.admin.controller.Observer;


public class DatabaseTest {
	private static Database db;

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();


	@BeforeClass
	public static void setUpBeforeClass(){
	}
	@Before
	public void setUpBeforeTest() {
		db = new Database("test/model/databaseTest.db");
	}
	@Test
	
	public void constructorTest(){
		if (db != null) {
			db.forgetDB();
		}
		db = new Database("test/model/databaseTest.db");
		assertNotNull(db);
	}
	@Test
	public void syncedTest() {
	    assertFalse(db.isUploadNeeded());
	    changeLoginTest();
	    db.addAlias(0, "test");
	    assertTrue(db.isUploadNeeded());
	}
	@Test
	public void constructorErrorTest(){
		exit.expectSystemExit();
		db = new Database("");
	}
	@Test
	public void addOneFavTest() {
		db.addFav("test1");
		assertTrue(db.checkIfFavoritePlaceExist("test1"));
	}
	@Test
	public void addOneFavErrorTest() {
		db = new Database("test/model/brokenDB.db");
		assertFalse(db.addFav("test1"));
	}
	@Test
	public void addTwoFavTest() {
		db.addFav("test1", "test2");
		assertTrue(db.checkIfFavoriteRouteExist("test1", "test2"));
	}
	@Test
	public void addTwoFavErrorTest() {
		db = new Database("test/model/brokenDB.db");
		assertFalse(db.addFav("test1", "test2"));
	}
	@Test
	public void addThreeFavTest() {
		db.addFav("test1", "test2", "test3");
		assertTrue(db.checkIfFavoriteRouteWithStopExist("test1", "test2", "test3"));
	}
	@Test
	public void addThreeFavErrorTest() {
		db = new Database("test/model/brokenDB.db");
		assertFalse(db.addFav("test1", "test2", "test3"));
	}
	@Test
	public void addBackgroundTest() {
		BufferedImage image = new BufferedImage(1, 1, BufferedImage.TYPE_4BYTE_ABGR);
		db.addBackground(0, 0, 0, image, 9998);
		assertNotNull(db.getBackground(9998));
	}
	@Test
	public void addBackgroundErrorTest() {
		db.addBackground(0, 0, 0, null, 7777);
		assertNull(db.getBackground(7777));
	}
	@Test
	public void addBuildingTest() {
		assertTrue(db.addBuilding("01.10", "", ""));
		assertTrue(db.checkIfBuildingNumberExist(1.1));
	}
	@Test
	public void addBuildingErrorTest() {
		db = new Database("test/model/brokenDB.db");
		assertFalse(db.addBuilding("test", "", ""));
	}
	@Test
	public void addFloorTest() {
		db.addFloor(0, "1.1");
		assertTrue(db.getFloors().size() == 1);
	}
	@Test
	public void addFloorErrorTest() {
		db = new Database("test/model/brokenDB.db");
		assertEquals(db.addFloor(0, "1.1"), 0);
	}
	@Test
	public void addVertexTest() {
		int i = db.addVertex(0, 0, "s", 0);
		assertNotNull(db.getVertex(i));
	}
	@Test
	public void addVertexErrorTest() {
		db = new Database("test/model/brokenDB.db");
		assertEquals(db.addVertex(0, 0, "s", 0), 0);
	}
	@Test
	public void addEntranceTest() {
		int i = db.addEntrance(0, 0, 0, 1);
		assertNotNull(db.getVertex(i));
	}
	@Test
	public void addEntranceErrorTest() {
		db = new Database("test/model/brokenDB.db");
		assertEquals(db.addEntrance(0, 0, 0, 1), 0);
	}
	@Test
	public void addEdgeTest() {
		db.addEdge(0, 0, 0, false);
		assertTrue(db.getEdges().size() == 1);
    }
    @Test
    public void addEdgeAccessTest() {
        db.addEdge(0, 0, 0, true);
        assertTrue(db.getEdges().size() == 1);
    }
	@Test
	public void addEdgeErrorTest() {
		db = new Database("test/model/brokenDB.db");
		assertEquals(db.addEdge(0, 0, 0, false), 0);
	}
	@Test
	public void addRoomTest() {
		int i = db.addRoom(0, 0, 0, "");
		assertNotNull(db.getVertex(i));
	}
	@Test
	public void addRoomErrorTest() {
		db = new Database("test/model/brokenDB.db");
		assertEquals(db.addRoom(0, 0, 0, ""), 0);
	}
	@Test
	public void addConstrSiteTest() {
		db.addConstrSite("2015-12-11", "2019-13-11", 0);
		assertTrue(db.getConstructionSites().size() == 1);
	}
	@Test
	public void addConstrSiteErrorTest() {
		db = new Database("test/model/brokenDB.db");
		assertEquals(db.addConstrSite("2015-12-11", "2015-13-11", 0), 0);
	}
	@Test
	public void addAliasTest() {
		db.addAlias(0, "alili");
		assertTrue(db.getAliases().size() == 1);
	}
	@Test
	public void assignEntranceToBuildingTest() {
	    db.addBuilding("01.10", "", "");
	    int i = db.addVertex(1, 1, "default", 0);
	    db.assignEntranceToBuilding(i, 1.1);
	    assertTrue(db.checkIfVertexIsAssignToBuilding(i));
	}
    @Test
    public void assignEntranceToBuildingErrorTest() {
        db = new Database("test/model/brokenDB.db");
        assertFalse(db.assignEntranceToBuilding(1, 1.1));
    }
    @Test
    public void changeBackgroundTest() {
        BufferedImage image = new BufferedImage(1, 1, BufferedImage.TYPE_4BYTE_ABGR);
        db.addBackground(0, 0, 0, image, 9998);
        db.changeBackground(1, 1, 1, image, 9998);
        String[] bg = db.getBackgrounds().get(0);
        assertTrue(bg[1].equals("1.0"));
    }
    @Test
    public void changeBackgroundErrorTest() {
        BufferedImage image = new BufferedImage(1, 1, BufferedImage.TYPE_4BYTE_ABGR);
        db.addBackground(0, 0, 0, image, 9998);
        db.changeBackground(1, 1, 1, null, 9998);
        String[] bg = db.getBackgrounds().get(0);
        assertTrue(bg[1].equals("0.0"));
    }
    @Test
    public void changeBuildingNameTest() {
        db.addBuilding("01.10", "Test", "Test");
        db.changeBuildingName("01.10", "testchange");
        String[] building = db.getBuildings().get(0);
        assertTrue(building[2].equals("testchange"));
    }
    @Test
    public void changeBuildingAddressTest() {
        db.addBuilding("01.10", "Test", "Test");
        db.changeBuildingAddress("01.10", "testchange");
        String[] building = db.getBuildings().get(0);
        assertTrue(building[0].equals("testchange"));
    }
    @Test
    public void changeFloorTest() {
        int i = db.addFloor(0, "01.10");
        db.changeFloor(1, "02.20", i);
        String[] floor = db.getFloors().get(0);
        assertTrue(floor[2].equals("2.2"));
    }
    @Test
    public void changeVertexTest() {
        int i = db.addVertex(0, 0, "default", 0);
        db.changeVertex(1, 1, "default", 1, i, null, -1);
        int b = db.getVertexFloor(i); 
        assertTrue(b == 1);
    }
    @Test
    public void changeRoomTest() {
        int i = db.addVertex(0, 0, "default", 0);
        db.changeVertex(1, 1, "Room", 1, i, "test", -1);
        String[] vertex = db.getVertices().get(0);
        assertTrue(vertex[6].equals("test"));
    }
    @Test
    public void changeEntranceTest() {
        int i = db.addVertex(0, 0, "default", 0);
        db.changeVertex(1, 1, "Entrance", 1, i, null, 01.10);
        assertTrue(db.checkIfVertexIsAssignToBuilding(i));
    }

    @Test
    public void changeVertexErrorTest() {
        assertFalse(db.changeVertex(0, 0, "nonvalidtype", 0, 0, null, 0));
    }
    @Test
    public void changeEdgeTest() {
        int i = db.addEdge(0, 0, 0, false);
        db.changeEdge(1, 1, 1, true, i);
        String[] edge = db.getEdges().get(0);
        assertTrue(edge[4].equals("1"));
    }
    @Test
    public void changePasswordTest() {
        db.insert("Informations", "Username,Password", "'test','test'");
        db.changePassword("test", "new");
        assertTrue(db.login("test", "new"));
    }
    @Test
    public void changeLoginTest() {
        db.insert("Informations", "Username,Password", "'test','test'");
        db.changeLogin("test", "new");
        assertTrue(db.login("new", "test"));
    }
    @Test
    public void changeLoginErrorTest() {
        db = new Database("test/model/brokenDB.db");
        assertFalse(db.changeLogin("test", "new"));
    }
    @Test
    public void changePasswordErrorTest() {
        db = new Database("test/model/brokenDB.db");
        assertFalse(db.changePassword("test", "new"));
    }
    @Test
    public void changeConstructionSiteStartDateTest() {
        int i = db.addConstrSite("2018-12-11", "2019-13-11", 0);
        db.changeConstructionSiteStartDate("2010-12-11", i);
        assertTrue(db.checkEdgeBlocked(0));
    }
    @Test
    public void changeConstructionSiteExpDateTest() {
        int i = db.addConstrSite("2010-12-11", "2011-13-11", 0);
        db.changeConstructionSiteExpDate("2050-12-11", i);
        assertTrue(db.checkEdgeBlocked(0));
    }
    @Test
    public void checkIfFavoritePlaceExistTest() {
        db.addFav("Test");
        assertTrue(db.checkIfFavoritePlaceExist("Test"));
    }
    @Test
    public void checkIfFavoritePlaceExistErrorTest() {
        db = new Database("test/model/brokenDB.db");
        db.addFav("Test");
        assertFalse(db.checkIfFavoritePlaceExist("Test"));
    }
    @Test
    public void checkEdgeBlockedTest() {
        int i = db.addConstrSite("2010-12-11", "2011-13-11", 0);
        db.changeConstructionSiteExpDate("2050-12-11", i);
        assertTrue(db.checkEdgeBlocked(0));
    }
    @Test
    public void checkEdgeBlockedErrorTest() {
        db = new Database("test/model/brokenDB.db");
        assertFalse(db.checkEdgeBlocked(0));
    }
    @Test
    public void checkIfVertexIsAssignToBuildingTest() {
        db.addBuilding("01.10", "", "");
        int i = db.addEntrance(0, 0, 0, 1.1);
        assertTrue(db.checkIfVertexIsAssignToBuilding(i));
    }
    @Test
    public void checkIfVertexIsAssignToBuildingErrorTest() {
        db = new Database("test/model/brokenDB.db");
        assertFalse(db.checkIfVertexIsAssignToBuilding(0));
    }
    @Test
    public void checkIfBuildingNameExistTest() {
        db.addBuilding("01.10", "test", "test2");
        db.checkIfBuildingNameExist("test");
    }
    @Test
    public void checkIfBuildingNameExistErrorTest() {
        db = new Database("test/model/brokenDB.db");
        assertFalse(db.checkIfBuildingNameExist("..."));
    }
    @Test
    public void checkIfBuildingAddressExistTest() {
        db.addBuilding("01.10", "test", "test2");
        db.checkIfBuildingAddressExist("test2");
    }
    @Test
    public void checkIfBuildingAddressExistErrorTest() {
        db = new Database("test/model/brokenDB.db");
        assertFalse(db.checkIfBuildingAddressExist("..."));
    }
    @Test
    public void checkIfBuildingNumberExistTest() {
        db.addBuilding("01.10", "test", "test2");
        db.checkIfBuildingNumberExist(1.1);
    }
    @Test
    public void checkIfBuildingNumberExistErrorTest() {
        db = new Database("test/model/brokenDB.db");
        assertFalse(db.checkIfBuildingNumberExist(1.10));
    }
    @Test
    public void checkBluePrintedTest() {
        db.addFloor(0, "01.10");
        assertTrue(db.checkBluePrinted("01.10"));
    }
    @Test
    public void checkBluePrintedFalseTest() {
        assertFalse(db.checkBluePrinted("01.10"));
    }
    @Test
    public void checkBluePrintedErrorTest() {
        db = new Database("test/model/brokenDB.db");
        assertFalse(db.checkBluePrinted("1.1"));
    }
    @Test
    public void checkIfFavoriteRouteExistTest() {
        db.addFav("test", "test1");
        assertTrue(db.checkIfFavoriteRouteExist("test", "test1"));
    }
    @Test
    public void checkIfFavoriteRouteExistErrorTest() {
        db = new Database("test/model/brokenDB.db");
        assertFalse(db.checkIfFavoriteRouteExist("aaa","bbb"));
    }
    @Test
    public void checkIfFavoriteRouteWithStopExistTest() {
        db.addFav("t1", "t2", "t3");
        assertTrue(db.checkIfFavoriteRouteWithStopExist("t1", "t2", "t3"));
    }
    @Test
    public void checkIfFavoriteRouteWithStopExistErrorTest() {
        db = new Database("test/model/brokenDB.db");
        assertFalse(db.checkIfFavoriteRouteWithStopExist("t1", "t2", "t3"));
    }
    @Test
    public void deleteAliasTest() {
        db.addAlias(0, "asd");
        db.deleteAlias("asd");
        int i = db.getAliases().size();
        assertEquals(i, 0);
    }
    @Test
    public void deleteExpiredTest() {
        db.addConstrSite("2010-12-11", "2011-13-11", 0);
        db.deleteExpired();
        int i = db.getConstructionSites().size();
        assertEquals(i, 0);
    }
    @Test
    public void deleteExpiredErrorTest() {
        db.addConstrSite("sadasd", "asdasjd", 0);
        db.deleteExpired();
        int i = db.getConstructionSites().size();
        assertEquals(i, 1);
    }
    @Test
    public void deleteFavTest1() {
        db.addFav("1");
        db.deleteFav("1");
        assertFalse(db.checkIfFavoritePlaceExist("1"));
    }
    @Test
    public void deleteFavFalseTest1() {
        assertFalse(db.deleteFav("1"));
    }
    @Test
    public void deleteFavTest2() {
        db.addFav("1", "2");
        db.deleteFav("1", "2");
        assertFalse(db.checkIfFavoriteRouteExist("1", "2"));  
    }
    @Test
    public void deleteFavFalseTest2() {
        assertFalse(db.deleteFav("1", "2"));
    }
    @Test
    public void deleteFavTest3() {
        db.addFav("1", "2","3");
        db.deleteFav("1","2","3");
        assertFalse(db.checkIfFavoriteRouteWithStopExist("1","2","3"));
    }
    @Test
    public void deleteFavFalseTest3() {
        assertFalse(db.deleteFav("1", "2","3"));
    }
    @Test
    public void deleteBGTest() {
        BufferedImage image = new BufferedImage(1, 1, BufferedImage.TYPE_4BYTE_ABGR);
        db.addBackground(0, 0, 0, image, 9998);
        db.deleteBackGround(9998);
        assertEquals(db.getBackgrounds().size(), 0);
    }
    @Test
    public void deleteBuildingTest() {
        assertTrue(db.addBuilding("01.10", "", ""));
        db.deleteBuilding("01.10");
        assertFalse(db.checkIfBuildingNumberExist(1.1));
    }
    @Test
    public void deleteBuildingErrorTest() {
        assertTrue(db.addBuilding("01.10", "", ""));
        db.deleteBuilding("10.10");
        assertTrue(db.checkIfBuildingNumberExist(1.1));
    }
    @Test
    public void deleteBuildingErrorTest2() {
        db = new Database("test/model/brokenDB.db");
        assertFalse(db.deleteBuilding("10.10"));
    }
    @Test
    public void deleteFloorTest() {
        int i = db.addFloor(0, "01.10");
        db.deleteFloor(i);
        assertTrue(db.getFloors().size()==0);
    }
    @Test
    public void deleteVertexTest() {
        int i = db.addVertex(0, 0, "default", 0);
        db.deleteVertex(i);
        assertEquals(db.getVertices().size(), 0);
    }
    @Test
    public void deleteEdgeTest() {
        int i = db.addEdge(0, 0, 0, false);
        db.deleteEdge(i);
        assertEquals(db.getEdges().size(), 0);
    }
    @Test
    public void deleteConstrSiteTest() {
        int i = db.addConstrSite("2014-12-11", "2019-13-11", 0);
        assertTrue(db.getConstructionSites().size() == 1);
        assertTrue(db.deleteConstrSite(i));
        assertEquals(db.getConstructionSites().size(), 0);
    }
    @Test
    public void deleteConstrSiteErrorTest() {
        db = new Database("test/model/brokenDB.db");
        assertFalse(db.deleteConstrSite(2));
    }
    @Test
    public void getEdgesTest() {
        addEdgeTest();
    }
    @Test
    public void getBuildingsTest() {
        addBuildingTest();
    }
    @Test
    public void getChangesTest() {
        Observer o = new TestObserver();
        o.setSubject(db);
        db.insert("Informations", "Username,Password", "'test','testpw'");
        assertTrue(db.login("test", "testpw"));
        db.addVertex(0, 0, "default", 0);
        assertEquals(db.getChanges().size(), 1);
        db.unregister();
    }
    @Test
    public void getFloorsTest() {
        addFloorTest();
    }
    @Test
    public void getAliasesTest() {
        addAliasTest();
    }
    @Test
    public void getSpecificAliasesTest() {
        db.addAlias(0, "test");
        db.addAlias(0, "test 2");
        assertEquals(db.getAliases(0).size(),2);
    }
    @Test
    public void getBackgroundsTest() {
        addBackgroundTest();
    }
    @Test
    public void getBackgroundsErrorTest() {
        db = new Database("test/model/brokenDB.db");
        assertEquals(0, db.getBackgrounds().size());
    }
    @Test
    public void getVertexFloorTest() {
        changeVertexTest();
    }
    @Test
    public void getVertexFloorErrorTest() {
        db = new Database("test/model/brokenDB.db");
        assertEquals(0, db.getVertexFloor(0));
    }
    @Test
    public void getReprIDTest() {
        db.addEdge(0, 9, 0, false);
        assertEquals(9, db.getReprID(0));
    }
    @Test
    public void getReprIDTest2() {
        db.addEdge(19, 1, 0, false);
        assertEquals(19, db.getReprID(1));
    }
    @Test
    public void getReprIDErrorTest() {
        db = new Database("test/model/brokenDB.db");
        assertEquals(0, db.getReprID(0));
    }
    @Test
    public void getBackgroundTest() {
        addBackgroundTest();
    }
    @Test
    public void getNoBackgroundTest() {
        assertEquals(null, db.getBackground(1999));
    }   
    @Test
    public void getBackgroundErrorTest() {
        addBackgroundErrorTest();
    }
    @Test
    public void getConstructionSitesTest() {
        addConstrSiteTest();
    }
    @Test
    public void getConstructionSitesErrorTest() {
        db = new Database("test/model/brokenDB.db");
        assertNull(db.getConstructionSites());
    }
    @Test
    public void getVerticesTest() {
        addVertexTest();
    }
    @Test
    public void getVertexTest() {
        int i = db.addVertex(0, 0, "default", 99);
        assertEquals(db.getVertex(i)[4], "99");
    }
    @Test
    public void getVertexErrorTest() {
        db = new Database("test/model/brokenDB.db");
        assertNull(db.getVertex(0));
    }
    @Test
    public void getBuildingFavsTest() {
        db.addFav("1");
        assertEquals(1,db.getBuildingFavs().size());
    }
    @Test
    public void getFavoritePlaceTest() {
        db.addFav("1");
        assertEquals("1", db.getFavoritePlace("1"));
    }
    @Test
    public void getFavoritePlaceErrorTest() {
        db = new Database("test/model/brokenDB.db");
        assertEquals("", db.getFavoritePlace("..."));
    }
    @Test
    public void getRouteFavsTest() {
        db.addFav("1", "2");
        assertEquals(1,db.getRouteFavs().size());
    }
    @Test
    public void getStopRouteFavsTest() {
        db.addFav("1", "2", "3");
        assertEquals(1,db.getStopRouteFavs().size());
    }
    @Test
    public void getVertexIdOfRoomTest() {
        int f = db.addFloor(0, "01.10");
        int v = db.addRoom(0, 0, f, "test");
        assertEquals(v, db.getVertexIdOfRoom("01.10", "test"));
    }
    @Test
    public void getVertexIdOfRoomTest2() {
        int v = db.addRoom(0, 0, 0, "test");
        assertEquals(v, db.getVertexIdOfRoom(null, "test"));
    }
    @Test
    public void getVertexIdOfRoomErrorTest() {
        db = new Database("test/model/brokenDB.db");
        assertEquals(0, db.getVertexIdOfRoom("01.10", "test"));
    }
    @Test
    public void getFavoriteRouteWithStopTest() {
        db.addFav("1", "2", "3");
        assertEquals("2", db.getFavoriteRouteWithStop("1", "2", "3")[1]);
    }
    @Test
    public void getFavoriteRouteWithStopErrorTest() {
        db = new Database("test/model/brokenDB.db");
        assertNull(db.getFavoriteRouteWithStop("1", "2", "3"));
    }
    @Test
    public void getFavoriteRouteTest() {
        db.addFav("1", "2");
        assertEquals("2", db.getFavoriteRoute("1", "2")[1]);
    }
    @Test
    public void getFavoriteRouteErrorTest() {
        db = new Database("test/model/brokenDB.db");
        assertNull(db.getFavoriteRoute("1", "2"));
    }
    @Test
    public void searchBuildingTest() {
        db.addBuilding("01.10", "test", "adress");
        assertEquals("01.10", db.searchBuilding("adress"));
    }
    @Test
    public void searchBuildingTest2() {
        db.addBuilding("11.10", "test", "adress");
        assertEquals("11.10", db.searchBuilding("test"));
    }
    @Test
    public void searchBuildingTest3() {
        db.addBuilding("01.11", "test", "adress");
        assertEquals("01.11", db.searchBuilding("adress"));
    }
    @Test
    public void searchBuildingErrorTest() {
        db = new Database("test/model/brokenDB.db");
        assertEquals("", db.searchBuilding(null));
    }
    @Test
    public void insertTest() {
        db.insert("Aliases", "id,alias", "0,'test'");
        assertEquals(1, db.getAliases().size());
    }
    @Test
    public void loginTest() {
        changeLoginTest();
    }
    @Test
    public void loginErrorTest() {
        assertFalse(db.login("", ""));
    }
    @Test
    public void logoutTest() {
        db.logout();
        changeLoginTest();
    }
    @Test
    public void forgetDBTest() {
        db.addAlias(0, "lias");
        db.forgetDB();
        assertEquals(0, db.getAliases().size());
    }
    public void syncDownloadTest() throws SQLException, ParseException, IOException {
        db.sync();
        assertNotEquals(0, db.getChanges());
    }
    @Test
    public void syncTest() throws Exception {
        syncDownloadTest();
        GlobalDB gdb = new GlobalDB();
        gdb.command("delete from Changes");
        gdb.closeConn();
        db.sync();
        GlobalDB gdb2 = new GlobalDB();
        assertTrue(gdb2.show("select Username from Informations").next());
    }
    @Test
    public void syncLate() throws SQLException, ParseException, IOException {
        db.insert("Changes", "Datum,Aenderung", "'2050-11-11 11:11:11','Test'");
        db.sync();
        assertEquals(1, db.getChanges().size());
    }
    
	@After 
	public void tearDown(){
		if (db != null) {
			db.forgetDB();
		}
		db = null;
	}	
	@AfterClass
	public static void cleanUp(){
		if (db != null) {
			db.forgetDB();
		}
		}
}
