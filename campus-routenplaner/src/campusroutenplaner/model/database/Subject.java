package campusroutenplaner.model.database;


import campusroutenplaner.view.admin.controller.Observer;

/**
 * Created by nmladenov on 7/16/16.
 */
public interface Subject {

    //methods to register and unregister observers
    public void register(Observer obj);
    public void unregister();

    //method to notify observers of change
    public void notifyObserver(String message);
	public boolean doSync();
	
	public void discardChanges();
	public boolean isUploadNeeded();
}
