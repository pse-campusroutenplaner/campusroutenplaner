package campusroutenplaner.model.database;

import campusroutenplaner.view.admin.controller.Observer;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.text.DateFormat;

import com.sun.org.apache.xpath.internal.SourceTree;
import org.apache.commons.io.FileUtils;
import org.apache.commons.net.ftp.FTPClient;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.imageio.ImageIO;

/**
 * Datenbank-Schnittstelle.
 */
public class Database implements Subject {

	/** Is the map getting created right now.89 */
	boolean creating;

	/** The globale Database. */
	GlobalDB gdb;

	/** The connection. */
	Connection c;

	/** Command to local Database. */
	Statement db;

	FTPClient ftpclient = new FTPClient();
	private String name;

	/**
	 * Instantiates a new database.
	 */
	public Database(String databaseName) {
		this.name = "databse";

		try {
			creating = true;
			Class.forName("org.sqlite.JDBC");
			// default is KIT.db
			c = DriverManager.getConnection("jdbc:sqlite:res/" + databaseName);
			db = c.createStatement();
			System.out.println("Opened database successfully");
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.out.println("Could not connect to local database. Exiting...");
			System.exit(0);
		}
	}
	/**
	 * Adds the change. Should only be called when Admins are changing the
	 * database.
	 *
	 * @param change
	 *            the change
	 */
	private void addChange(String change) {
		if (creating) {
			return;
		}
		notifyObserver(change);
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());// changed
		try {
			change = change.replace("'", "\"");
			//System.out.println(change);
			db.executeUpdate("INSERT INTO Changes(Datum,Aenderung) VALUES ('" + date + "','" + change + "')");
		} catch (SQLException e) {
		}
	}

	/**
	 * Adds a favorite with a stopover.
	 *
	 * @param v1
	 *            Knoten 1
	 * @param v2
	 *            Knoten 2
	 * @param v3
	 *            Knoten 3
	 */
	public boolean addFav(String v1, String v2, String v3) {
		if(insert("Favorites", "Vertex1,Vertex2,Vertex3", "'" + v1 + "','" + v2 + "','" + v3 + "'")) {
		    return true;
		}
		return false;
	}

	/**
	 * Adds a favorite route.
	 *
	 * @param v1
	 *            Knoten 1
	 * @param v2
	 *            Knoten 2
	 */
	public boolean addFav(String v1, String v2) {
		if(insert("Favorites", "Vertex1,Vertex2", "'" + v1 + "','" + v2 + "'")) {
		    return true;
		}
		return false;
	}

	/**
	 * Adds a favorite-building.
	 *
	 * @param v1
	 *            Knoten 1
	 */
	public boolean addFav(String v1) {
		if(insert("Favorites", "Vertex1", "'" + v1 + "'")) {
		return true;
		}
		return false;
	}

	private void addBackground(double width, double height, double scale, String picturePath, int floorID) {
		insert("Backgrounds", "Width,Heigth,Scale,Picture,FloorID",
				width + "," + height + "," + scale + ",'" + picturePath + "'," + floorID);
	}

	/**
	 * adds a background
	 * 
	 * @param width
	 * @param height
	 * @param scale
	 * @param image
	 * @param floorID
	 */
	public void addBackground(double width, double height, double scale, BufferedImage image, int floorID) {
	    deleteBackGround(floorID);
		String name = "res/img/maps/bg_" + floorID + ".png";
		File outputfile = new File(name);
		try {
			ImageIO.write(image, "png", outputfile);
		} catch (Exception e) {
			System.out.println("Error, can't use the image");
		}
		addBackground(width, height, scale, name, floorID);
	}
	/**
	 * Adds a building.
	 *
	 * @param number
	 *            Gebaeudenummer
	 * @param name
	 *            Gebaeudename
	 * @param adress
	 *            Adresse
	 */
	public boolean addBuilding(String number, String name, String adress) {
		if (insert("Buildings", "Number,Name,Adress", "'" + number + "','" + name + "','" + adress + "'")) {
		    return true;
		}
		return false;
	}

	/**
	 * Adds a floor.
	 *
	 * @param number
	 *            Welches Stockwerk(1.Stock?, 2.Stock?...)
	 * @param buildingNumber
	 *            Geb�udenummer
	 * @return Die ID, die dem Eintrag zugewiesen wurde
	 */
	public int addFloor(int number, String buildingNumber) {
	    double bN = Double.parseDouble(buildingNumber);
		insert("Floors", "Number,Building", number + ",'" + bN + "'");
		try {
			return show("ID", "Floors", "Number=" + number + " and Building='" + bN + "'").getInt(1);
		} catch (SQLException | NullPointerException e) {
			System.out.println("Error: Couldn't get ID");
			return 0;
		}
	}

	/**
	 * Adds a vertex.
	 *
	 * @param xPos
	 *            xPos
	 * @param yPos
	 *            yPos
	 * @param type
	 *            Typ des Knotens
	 * @param floorID
	 *            Die MapID, auf der der Knoten ist
	 * @return Die ID, die dem Eintrag zugewiesen wurde
	 */
	public int addVertex(double xPos, double yPos, String type, int floorID) {
		insert("Vertices", "xPos,yPos,Type,Floor", xPos + "," + yPos + ",'" + type + "'," + floorID);
		try {
			ResultSet rs = show("select max(ID) from Vertices");
			rs.next();
			return rs.getInt("max(ID)");
		} catch (SQLException | NullPointerException e) {
			System.out.println("Error: Couldn't get ID");
			return 0;
		}
	}

	public int addRoom(double xPos, double yPos, int floorID, String name) {
		insert("Vertices", "xPos,yPos,Type,Floor,Name", xPos + "," + yPos + ",'Room'," + floorID + ",'" + name + "'");
		try {
			ResultSet rs = show("select max(ID) from Vertices");
			rs.next();
			return rs.getInt("max(ID)");
		} catch (SQLException | NullPointerException e) {
			System.out.println("Error: Couldn't get ID");
			return 0;
		}
	}

	public int addEntrance(double xPos, double yPos, int floorID, double conn) {
		insert("Vertices", "xPos,yPos,Type,Floor,Connection", xPos + "," + yPos + ",'Entrance'," + floorID + ","+ conn);
		try {
			ResultSet rs = show("select max(ID) from Vertices");
			rs.next();
			return rs.getInt("max(ID)");
		} catch (SQLException | NullPointerException e) {
			System.out.println("Error: Couldn't get ID");
			return 0;
		}
	}



	/**
	 * Adds an edge.
	 *
	 * @param vert1
	 *            the vert 1
	 * @param vert2
	 *            the vert 2
	 * @param length
	 *            the length
	 * @param accessible
	 *            Ob die Kante barrierefrei ist
	 * @return Die ID, die dem Eintrag zugewiesen wurde
	 */
	public int addEdge(int vert1, int vert2, double length, boolean accessible) {
		if (accessible) {
			insert("Edges", "Vertex1,Vertex2,Length,Barrierfree", vert1 + "," + vert2 + "," + length + ",1");
		} else {
			insert("Edges", "Vertex1,Vertex2,Length,Barrierfree", vert1 + "," + vert2 + "," + length + ",0");
		}
		try {
			return show("ID", "Edges", "Vertex1=" + vert1 + " and Vertex2=" + vert2).getInt(1);
		} catch (SQLException | NullPointerException e) {
			System.out.println("Error: Couldn't get ID");
			return 0;
		}
	}

	/**
	 * Adds a constr site.
	 *
	 * @param startDate
	 *            the start date
	 * @param expDate
	 *            the expiration date
	 * @param edge
	 *            Die betroffene Kante
	 * @return Die ID, die dem Eintrag zugewiesen wurde
	 */
	public int addConstrSite(String startDate, String expDate, int edge) {
        try {
            insert("ConstructionSites", "Edge,StartDate,ExpDate", edge + ",'" + startDate + "','" + expDate + "'");
			return show("ID", "ConstructionSites",
					"Edge=" + edge + " and StartDate='" + startDate + "' and ExpDate='" + expDate + "'").getInt(1);
		} catch (Exception e) {
			System.out.println("Error: Couldn't get ID, adding  the ConstrSite failed");
			return 0;
		}
	}

	public void addAlias(int vertexID, String alias) {
		insert("Aliases", "ID,Alias", vertexID+",'"+alias+"'");
	}
	public boolean assignEntranceToBuilding(int vertexID, double buildingNumber) {
	    ResultSet rs2 = show("select * from Vertices where ID="+vertexID);
	    try {
            if (rs2.next()) {
                if (checkIfBuildingNumberExist(buildingNumber)) {
                    changeVertex(rs2.getDouble("xPos"), rs2.getDouble("yPos"), "Entrance", rs2.getInt("Floor"), vertexID, null, buildingNumber);
                }
            }
        } catch (SQLException | NullPointerException e) {
            return false;
        }
	    return false;
	}
	/**
	 * change background
	 * 
	 * @param width
	 * @param height
	 * @param scale
	 * @param bg
	 * @param floorID
	 */
	public void changeBackground(double width, double height, double scale, BufferedImage bg, int floorID) {
		String name = "res/img/maps/bg_" + floorID + ".png";
		File outputfile = new File(name);
		try {
			ImageIO.write(bg, "png", outputfile);
		    update("Backgrounds", "Width=" + width + ",Heigth=" + height + ",Scale=" + scale, "FloorID=" + floorID);
		} catch (Exception e) {
			System.out.println("Error, can't use the image");
		}
	}

//	public void changeBackground(double width, double height, double scale, int floorID) {
//		update("Backgrounds", "Width=" + width + ",Heigth=" + height + ",Scale=" + scale, "FloorID=" + floorID);
//	}

	/**
	 * Change building.
	 * 
	 * @param number
	 *            the number
	 * @param name
	 *            the name
	 * @param adress
	 *            the adress
	 */
//	public void changeBuilding(String number, String name, String adress) {
//		update("Buildings", "Name='" + name + "',Adress='" + adress + "', Number='" + number + "'",
//				"Number='" + number + "'");
//	}
	
//	public boolean changeBuildingNumber(String old, String newString) {
//	    update("Buildings", "Number='"+newString+"'", "where Number='" + old + "'");
//        return true;
//	}
	public boolean changeBuildingName(String number, String newString) {
	    double bN = Double.parseDouble(number);
        update("Buildings", "Name='"+newString+"'", "Number=" + bN);
        return true;
	    }
	public boolean changeBuildingAddress(String number, String newString) {
        double bN = Double.parseDouble(number);
        update("Buildings", "Adress='"+newString+"'", "Number=" + bN);
        return true;
	    }

	/**
	 * Change floor.
	 *
	 * @param number
	 *            the number
	 * @param buildingNumber
	 *            the building number
	 * @param ID
	 *            the id
	 */
	public void changeFloor(int number, String buildingNumber, int ID) {
		update("Floors", "Building='" + buildingNumber + "',Number=" + number, "ID=" + ID);
	}

	/**
	 * Change vertex.
	 *
	 * @param xPos
	 *            the x pos
	 * @param yPos
	 *            the y pos
	 * @param type
	 *            the type
	 * @param floorID
	 *            the floor ID
	 * @param ID
	 *            the id
	 */
	public boolean changeVertex(double xPos, double yPos, String type, int floorID, int ID, String name, double buildingNumber) {
		if (name == null && buildingNumber == -1) {
			if(update("Vertices", "xPos=" + xPos + ",Type='" + type + "',yPos=" + yPos+", Floor="+floorID, "ID=" + ID)) {
			    return true;
			}
		} else if (type.equals("Room") && name != null) {
			if(update("Vertices", "xPos=" + xPos + ",Type='" + type + "',yPos=" + yPos + ",name='" + name + "', Floor="+floorID,
					"ID=" + ID)) {
			    return true;
			}
		} else if (type.equals("Entrance")) {
			if(update("Vertices", "xPos=" + xPos + ",Type='" + type + "',yPos=" + yPos + ",Connection=" + buildingNumber + ", Floor="+floorID,
					"ID=" + ID)) {
			    return true;
			}
		} else {
			System.out.println("Database: Wrong type");
		}
		return false;
	}

	/**
	 * Change edge.
	 *
	 * @param vert1
	 *            the vert 1
	 * @param vert2
	 *            the vert 2
	 * @param length
	 *            the length
	 * @param accessible
	 *            the accessible
	 * @param ID
	 *            the id
	 */
	public void changeEdge(int vert1, int vert2, double length, boolean accessible, int ID) {
		int i = 0;
		if (accessible) {
			i = 1;
		}
		update("Edges", "Vertex1=" + vert1 + ",Vertex2=" + vert2 + ",Length=" + length + ", Barrierfree=" + i,
				"ID=" + ID);
	}

	public Boolean changePassword(String oldpw, String newpw) {
		ResultSet rs = show("select Password from Informations");
		try {
			rs.next();
			if (rs.getString("Password").equals(oldpw)) {
				update("Informations", "Password='" + newpw + "'", null);
				return true;
			}
		} catch (SQLException | NullPointerException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	   public Boolean changeLogin(String oldname, String newname) {
	        ResultSet rs = show("select Username from Informations");
	        try {
	            rs.next();
	            if (rs.getString("Username").equals(oldname)) {
	                update("Informations", "Username='" + newname + "'", null);
	                return true;
	            }
	        } catch (SQLException | NullPointerException e) {
	            e.printStackTrace();
	        }
	        return false;
	    }

	/**
	 * Change constr site.
	 *
	 * @param startDate
	 *            the start date
	 * @param expDate
	 *            the exp date
	 * @param edge
	 *            the edge
	 * @param ID
	 *            the id
	 */
//	public void changeConstrSite(String startDate, String expDate, int edge, int ID) {
//		update("ConstructionSites", "StartDate='" + startDate + "',ExpDate='" + expDate + "',Edge=" + edge, "ID=" + ID);
//	}
	public boolean changeConstructionSiteStartDate(String startDate, int ID) {
        update("ConstructionSites", "StartDate='" + startDate + "'","ID=" + ID);
        return true;
	}
    public boolean changeConstructionSiteExpDate(String expDate, int ID) {
        update("ConstructionSites", "ExpDate='" + expDate + "'","ID=" + ID);
        return true;
    }
//    public boolean changeConstructionSiteEdge(int edgeID, int ID) {
//        update("ConstructionSites", "Edge='" + edgeID + "'","ID=" + ID);
//        return true;
//    }

	public boolean checkIfFavoritePlaceExist(String place) {
		ResultSet rs = show(
				"select * from Favorites where Vertex2 is null and Vertex3 is null and Vertex1='" + place + "'");
		try {
			if (rs.next()) {
				return true;
			}
		} catch (SQLException | NullPointerException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	
	public boolean checkEdgeBlocked(int edgeID) {
		ResultSet rs = show("select * from ConstructionSites where Edge="+edgeID);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			if (rs.next()) {
			    Date start = df.parse(rs.getString("StartDate"));
			    Date finish = df.parse(rs.getString("ExpDate"));
			    Date current = new Date();
			    if (current.after(start) && current.before(finish)) {
			        return true;
			    }
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

//	public boolean checkIfVertexExist(int mapID, int vertexID) {
//		try {
//			if (show("select * from Vertices where ID=" + vertexID + " and Floor=" + mapID).next()) {
//				return true;
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return false;
//	}

	public boolean checkIfVertexIsAssignToBuilding(int vertexID) {
		try {
			if (show("select * from Vertices where ID=" + vertexID + " and Connection is not null").next()) {
				return true;
			}
		} catch (SQLException | NullPointerException e) {
			e.printStackTrace();
		}
		return false;
	}


	public boolean checkIfBuildingNameExist(String name) {
		try {
			if (show("select * from Buildings where Name='" + name + "'").next()) {
				return true;
			}
		} catch (SQLException | NullPointerException e) {
		}
		return false;
	}
     public boolean checkIfBuildingNumberExist(double buildinNumber) {
    	 String buildingNumber = "" + buildinNumber;
	        try {
	            ResultSet rs = c.createStatement().executeQuery("select * from Buildings where Number='" + buildingNumber + "'");
	            if (rs.next()) {
	                return true;
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	        return false;
	    }

	public boolean checkIfBuildingAddressExist(String address) {
		try {
			if (show("select * from Buildings where Adress='" + address + "'").next()) {
				return true;
			}
		} catch (SQLException | NullPointerException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean checkBluePrinted(String number) {
		try {
			if (show("select * from Floors where Building="+number).next()) {
				return true;
			}
		} catch (SQLException | NullPointerException e) {
			return false;
		}
		return false;
	}


	public boolean checkIfFavoriteRouteExist(String source, String destination) {
		ResultSet rs = show("select * from Favorites where Vertex2 is '" + destination
				+ "' and Vertex3 is null and Vertex1='" + source + "'");
		try {
			if (rs.next()) {
				return true;
			}
		} catch (SQLException | NullPointerException e) {
			e.printStackTrace();
		}
		return false;
	}



	public boolean checkIfFavoriteRouteWithStopExist(String source, String dest, String stopOver) {
		ResultSet rs = show("select * from Favorites where Vertex2 is '" + dest + "' and Vertex3 is '" + stopOver
				+ "' and Vertex1='" + source + "'");
		try {
			if (rs.next()) {
				return true;
			}
		} catch (SQLException | NullPointerException e) {
			e.printStackTrace();
		}
		return false;
	}

    @Override
    public boolean isUploadNeeded() {
        try {
            gdb = new GlobalDB();

            if (gdb.getLastChange() == null || gdb.getLastChange().before(getLastChange())) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        gdb.closeConn();
        return false;
    }

//	public boolean checkIfConstruSiteExist(int id) {
//	    ResultSet rs = show("select * from ConstructionSites where ID="+id);
//	    try {
//            if (rs.next()) {
//                return true;
//            }
//        } catch (SQLException e) {
//            return false;
//        }
//	    return false;
//	}
	public void deleteAlias(String alias) {
		delete("Aliases","alias='"+alias+"'");
	}
	public void deleteAlias(int vertexID) {
        delete("Aliases", "id='" + vertexID + "'");
    }

	public void deleteExpired() {
	    ArrayList<String[]> constrSites = RStoArray(show("select ID,ExpDate from ConstructionSites"));
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date d = new Date();
        try {
            for (int i = 0;i < constrSites.size(); i++) {
            	Date expDate = df.parse(constrSites.get(i)[1]);
                if (d.after(expDate)) {
                    delete("ConstructionSites", "ID=" + constrSites.get(i)[0]);
                }
            }
        } catch (Exception e) {
        	System.out.println("Error, unpearseable date");
        }
	}


	/**
	 * Delete fav.
	 *
	 * @param v1
	 *            the v 1
	 * @param v2
	 *            the v 2
	 * @param v3
	 *            the v 3
	 */
	public boolean deleteFav(String v1, String v2, String v3) {
        if (checkIfFavoriteRouteWithStopExist(v1, v2, v3)) {
		delete("Favorites", "Vertex1='" + v1 + "' and Vertex2='" + v2 + "' and Vertex3='" + v3 + "'");
		return true;
        }
        return false;
	}

	/**
	 * Delete fav.
	 *
	 * @param v1
	 *            the v 1
	 * @param v2
	 *            the v 2
	 */
	public boolean deleteFav(String v1, String v2) {
        if (checkIfFavoriteRouteExist(v1, v2)) {
		delete("Favorites", "Vertex1='" + v1 + "' and Vertex2='" + v2 + "' and Vertex3 is null");
		return true;
        }
        return false;
	}

	/**
	 * Delete fav.
	 *
	 * @param v1
	 *            the v 1
	 */
	public boolean deleteFav(String v1) {
	    if (checkIfFavoritePlaceExist(v1)) {
		delete("Favorites", "Vertex1='" + v1 + "' and Vertex2 is null and Vertex3 is null");
		return true;
	    }
	    return false;
	}

	/**
	 * Delete back ground.
	 *
	 * @param floorID
	 *            the floor ID
	 */
	public void deleteBackGround(int floorID) {
		delete("Backgrounds", "FloorID=" + floorID);
	}

	/**
	 * Delete building.
	 *
	 * @param buildingNumber
	 *            the building number
	 */
	public boolean deleteBuilding(String bN) {
//		if (buildingNumber.startsWith("0")) {
//			buildingNumber = buildingNumber.substring(1);
//		}
//		else if (buildingNumber.endsWith("0")) {
//			buildingNumber = buildingNumber.substring(0,buildingNumber.length()-1);
//		}
	    double buildingNumber = Double.parseDouble(bN);
	    ResultSet rs = show("SELECT Number FROM Buildings WHERE Number = "+buildingNumber);
	    try {
            if (rs.next()) {
            delete("Buildings", "Number=" + buildingNumber);
            return true;
            }
        } catch (SQLException | NullPointerException e) {
        }
	    return false;
	}

	/**
	 * Delete floor. 50
	 *
	 * @param floorID
	 *            the floor ID
	 */
	public void deleteFloor(int floorID) {
		delete("Floors", "ID=" + floorID);
	}

	/**
	 * Delete vertex.
	 *
	 * @param ID
	 *            the id
	 */
	public void deleteVertex(int ID) {
		delete("Vertices", "ID=" + ID);
		deleteAlias(ID);
	}

	/**
	 * Delete edge.
	 *
	 * @param ID
	 *            the id
	 */
	public void deleteEdge(int ID) {
		delete("Edges", "ID=" + ID);
	}

	/**
	 * Delete constr site.
	 *
	 * @param ID
	 *            the id
	 */
	public boolean deleteConstrSite(int ID) {
		if(delete("ConstructionSites", "ID=" + ID)) {
		    return true;
		}
		return false;
	}


	/**
	 * Gets the edges.
	 *
	 * @return 2DimArray of all Edges
	 */
	public ArrayList<String[]> getEdges() {
		return RStoArray(show("select * from Edges"));
	}

	/**
	 * Gets the buildings.
	 *
	 * @return 2DimArray of all Buildings
	 */
	public ArrayList<String[]> getBuildings() {
		return RStoArray(show("select * from Buildings"));
	}

	public ArrayList<String[]> getChanges() {
		return RStoArray(show("select * from Changes"));
	}

	/**
	 * Gets the floors.
	 *
	 * @return 2DimArray of all Floors
	 */
	public ArrayList<String[]> getFloors() {
		return RStoArray(show("select * from Floors"));
	}
	public ArrayList<String[]> getAliases(int vertexID) {
		return RStoArray(show("select * from Aliases where ID="+vertexID));
	}
	public ArrayList<String[]> getAliases() {
		return RStoArray(show("select * from Aliases"));
	}
	/**
	 * Gets the backgrounds.
	 * 
	 * @return 2DimArray of all Backgrounds
	 */
	public ArrayList<String[]> getBackgrounds() {
		ResultSet rs = show("select * from Backgrounds");
		ArrayList<String[]> BGs = new ArrayList<String[]>();
		try {
			while (rs.next()) {
				String[] bg = { rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5) };
				BGs.add(bg);
			}
		} catch (SQLException | NullPointerException e) {
			e.printStackTrace();
		}

		return BGs;
	}
	public int getVertexFloor(int vID) {
		ResultSet rs = show("select Floor from Vertices where ID="+vID);
		try {
			if (rs.next()) {
				return rs.getInt("Floor");
			}
		} catch (SQLException | NullPointerException e) {
		}
		return 0;
	}
	public int getReprID(int vertexID) {
		ResultSet rs = show("select Vertex1,Vertex2 from Edges where Length=0 and (Vertex1="+vertexID+" or Vertex2 = "+vertexID+")");
		try {
			if (rs.next()) {
				if (rs.getInt("Vertex1")==vertexID) {
					return rs.getInt("Vertex2");
				}
				else if (rs.getInt("Vertex2")==vertexID) {
					return rs.getInt("Vertex1");
				}
			}
		} catch (SQLException | NullPointerException e) {
		}
		return 0;
	}
//	public String getBuilding(int floorID) {
//		ResultSet rs = show("select Building from Floors where ID="+floorID);
//		try {
//			if(rs.next()) {
//				return rs.getString("Building");
//			}
//		} catch (SQLException e) {
//			
//		}
//		return null;
//	}
	public BufferedImage getBackground(int floorID) {
		ResultSet rs = show("select * from Backgrounds where FloorID=" + floorID);
		try {
			if (rs.next()) {
			    String pic = rs.getString("Picture");
				return ImageIO.read(new File(pic));
			} else {
				System.out.println("Error, that FloorID has no background");
			}
		} catch (Exception e) {
			System.out.print("Error, no file associated with database-entry");
		}
		return null;
	}
	/**
	 * Gets the construction sites.
	 *
	 * @return 2DimArray of all ConstructionSites
	 */
	public ArrayList<String[]> getConstructionSites() {
	    deleteExpired();
		try {
			return RStoArray(c.createStatement().executeQuery("select * from ConstructionSites"));
		} catch (SQLException | NullPointerException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Gets the log.
	 *
	 * @return 2DimArray of all changes
	 */
//	public ArrayList<String[]> getLog() {
//		return RStoArray(show("select * from Changes"));
//	}

	/**
	 * Gets the vertices.
	 *
	 * @return 2DimArray of all Vertices
	 */
	public ArrayList<String[]> getVertices() {
		return RStoArray(show("select * from Vertices"));
	}

	public String[] getVertex(int id) {
		try {
		return RStoArray(show("*", "Vertices", "ID=" + id)).get(0);
		}
		catch(Exception e) {
			return null;
		}
	}

	/**
	 * Gets the building favorites.
	 *
	 * @return 2DimArray of all buildingFavorites
	 */
	public ArrayList<String[]> getBuildingFavs() {
		return RStoArray(show("select * from Favorites where Vertex2 is null and Vertex3 is null"));
	}

	public String getFavoritePlace(String place) {
		ResultSet rs = show(
				"select * from Favorites where Vertex2 is null and Vertex3 is null and Vertex1='" + place + "'");
		try {
			if (rs.next()) {
				return place;
			}
		} catch (SQLException | NullPointerException e) {
			e.printStackTrace();
		}
		return "";
	}
	/**
	 * Gets the route favorites.
	 *
	 * @return 2DimArray of all routeFavorites
	 */
	public ArrayList<String[]> getRouteFavs() {
		return RStoArray(show("select * from Favorites where Vertex2 is not null and Vertex3 is null"));
	}
	public int getVertexIdOfRoom(String bN, String roomname) {
		ResultSet rs;
		if (bN != null) {
	        double buildingnumber = Double.parseDouble(bN);
//			rs = show("select ID from Vertices where Name='" + roomname
//					+ "' and Floor in (select ID from Floors where Building='" + buildingnumber + "')");
			rs = show("select Vertices.ID from Vertices inner join Floors on Floors.ID = Vertices.Floor left join Aliases"
					+ " on Vertices.id = Aliases.id where Floors.building ='"+buildingnumber+"' and (Vertices.Name='"+roomname+"' or upper(aliases.alias) like upper('%"+roomname+"%'))");
		} else {
			rs = show("select ID from Vertices where Name='" + roomname+ "'");
		}
		try {
			if (rs.next()) {
				return rs.getInt("ID");
			}
		} catch (SQLException | NullPointerException e) {
			e.printStackTrace();
		}
		return 0;
	}
	/**
	 * Gets the route with stop favorites.
	 *
	 * @return 2DimArray of all routeWithStopFavorites
	 */
	public ArrayList<String[]> getStopRouteFavs() {
		return RStoArray(show("select * from Favorites where Vertex2 is not null and Vertex3 is not null"));
	}

//	public String[] getConstructionSite(int id) {
//	    ResultSet rs = show("*", "ConstructionSites", "ID="+id);
//	    try {
//            if (rs.next()) {
//                String[] rsa = {rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4)};
//                return rsa;
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        System.out.println("ConstructionSite nicht gefunden");
//	    return null;
//	}	
//	public List<Integer> getMapIDs() {
//		ArrayList<String[]> floors = getFloors();
//		ArrayList<Integer> floorIDs = new ArrayList<Integer>();
//		floorIDs.add(0);
//		for (int i = 0; i < floors.size(); i++) {
//			floorIDs.add(Integer.parseInt(floors.get(i)[1]));// Koennte falsch
//																// sein, wie
//																// werden Floors
//																// gespeichert?
//		}
//		return floorIDs;
//	}

	public String[] getFavoriteRouteWithStop(String source, String dest, String stopOver) {
		ResultSet rs = show("select * from Favorites where Vertex2 is '" + dest + "' and Vertex3 is '" + stopOver
				+ "' and Vertex1='" + source + "'");
		try {
			if (rs.next()) {
			    String[] rtrn = {source, dest, stopOver};
				return rtrn;
			}
		} catch (SQLException | NullPointerException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String[] getFavoriteRoute(String source, String destination) {
		ResultSet rs = show("select * from Favorites where Vertex2 is '" + destination
				+ "' and Vertex3 is null and Vertex1='" + source + "'");
		try {
			if (rs.next()) {
				String[] returnS = {source, destination};
				return returnS;
			}
		} catch (SQLException | NullPointerException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * Gets the last change.
	 *
	 * @return the last change to the local database
	 * @throws SQLException
	 *             the SQL exception
	 * @throws ParseException
	 */
	private Date getLastChange() throws SQLException, ParseException {
		ResultSet rs = db.executeQuery("select Max(Datum) from Changes");
		rs.next();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Date d = df.parse(rs.getString("Max(Datum)"));
			return d;
		} catch (NullPointerException e) {
			return df.parse("1111-11-11 11:11:11");
		}
	}

	private String getColumnNames(ResultSet rs) throws SQLException {
		ResultSetMetaData rsmd = rs.getMetaData();
		StringBuilder command = new StringBuilder(rsmd.getColumnName(1));
		for (int n = 2; n <= rsmd.getColumnCount(); n++) {
			command.append("," + rsmd.getColumnName(n));
		}
		return command.toString();
	}

	private String[] getColumnNamesArr(ResultSet rs) throws SQLException {
		ResultSetMetaData rsmd = rs.getMetaData();
		String[] command = new String[rsmd.getColumnCount()];
		for (int n = 1; n <= rsmd.getColumnCount(); n++) {
			command[n - 1] = rsmd.getColumnName(n);
		}
		return command;
	}


	/**
	 * Search a building.
	 *
	 * @param name
	 *            the name
	 * @return the BuildingNumber
	 */
	public String searchBuilding(String name) {
		try {
			ResultSet buildings = db.executeQuery("select Number from Buildings where Adress='" + name + "' or Name='"
					+ name + "' or Number='" + name + "'");
			if (buildings.next()) {
			    String bNumber = buildings.getString("Number");
	            if (bNumber.length() < 5) {
	                if (bNumber.length() == 3) {
	                    bNumber = "0"+ bNumber +"0";
	                }
	                else if(bNumber.split("\\.")[0].length() == 1) {
	                    bNumber = "0"+bNumber;
	                }
	                else {
	                    bNumber = bNumber + "0";
	                }
	            }
				return bNumber;
			}


            name = name.substring(0, 1).toUpperCase() + name.substring(1);
            buildings = db.executeQuery("select Number from Buildings where Adress='" + name + "' or Name='"
                    + name + "' or Number='" + name + "'");
            if (buildings.next()) {
                String bNumber = buildings.getString("Number");
                if (bNumber.length() < 5) {
                    if (bNumber.length() == 3) {
                        bNumber = "0"+ bNumber +"0";
                    }
                    else if(bNumber.split("\\.")[0].length() == 1) {
                        bNumber = "0"+bNumber;
                    }
                    else {
                        bNumber = bNumber + "0";
                    }
                }
                return bNumber;
            }


		} catch (SQLException | NullPointerException e) {
			System.out.println("Error: Database.searchBuilding");
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * Search a room.
	 *
	 * @param name
	 *            the name
	 * @return the string
	 */
//	public String searchRoom(String name, int mapID) {
//		try {
//			ResultSet rs = db.executeQuery("select ID from Vertices where Name='" + name + "' and FloorID=" + mapID);
//			if (rs.next()) {
//				return rs.getString("ID");
//			}
//		} catch (SQLException e) {
//			System.out.println("Error: Database.searchRoom");
//			e.printStackTrace();
//		}
//		return "";
//	}

	/**
	 * Show: View specified table-entrys.
	 *
	 * @param columns
	 *            select
	 * @param table
	 *            from
	 * @param condition
	 *            where
	 * @return Die Zeilen als ResultSet
	 */
	private ResultSet show(String columns, String table, String condition) {
		return show("select " + columns + " from " + table + " where " + condition);
	}



	/**
	 * Insert: Adds a new entry to a table
	 *
	 * @param table
	 *            Die Tabelle in die eingef�gt werden soll
	 * @param columns
	 *            Die Spalten die ver�ndert werden sollen, m�ssen von der
	 *            Reihenfolge zu insertions passen Format:
	 *            spalte1,spalte2,spalte3...
	 * @param insertions
	 *            Die Eintr�ge Format: 'eintrag1','eintrage2','eintrag3'...
	 */
	protected boolean insert(String table, String columns, String insertions) {
		if(command("INSERT INTO " + table + "(" + columns + ") VALUES (" + insertions + ");")) {
		   addChange("Added the entrys " + insertions + " to the table " + table + " at " + columns); 
		   return true;
		}
		return false;
	}
	

	/**
	 * Update: Updates specified entrys.
	 *
	 * @param table
	 *            Die Tabelle in der geupdatet wird
	 * @param columns
	 *            Welche Spalten geupdatet werden Format: geb�udename =
	 *            'informatikgeb�ude', nummer = '50.70'
	 * @param condition
	 *            Die Bedingung die Zeile erf�llen muss Format: nummer = '50.40'
	 */
	private boolean update(String table, String columns, String condition) {
		if (condition == null) {
		    if(command("update " + table + " set " + columns)) {
		        addChange("Set " + columns + " in the table " + "table");
		        return true;
		    }
			
		} else {
			if(command("update " + table + " set " + columns + " where " + condition)) {
			    addChange("Set " + columns + " in the table " + "table, if " + condition);
			    return true;
			}
		}
		return false;
	}

	/**
	 * Deletes an entry.
	 *
	 * @param table
	 *            Die Tabelle in der gel�scht wird
	 * @param condition
	 *            Die Bedingung zum l�schen Format: nummer = '50.70'
	 */
	private boolean delete(String table, String condition) {
		if (condition == null) {
			command("delete from " + table);
			addChange("Deleted all entrys in " + table);
		}
		if(command("delete from " + table + " where " + condition)) {
		    addChange("Deleted the entrys in " + table + ", where " + condition);
		    return true;
		}
		return false;
	}

	/**
	 * Command: Execute a command in the local database.
	 * 
	 * @param command
	 *            Der Befehl
	 */
	private boolean command(String command) {
		try {
			System.out.println(command);
			/*if (creating) {
				notifyObserver(command); 
			}*/

			db.executeUpdate(command);
			return true;
		} catch (SQLException | NullPointerException e) {
			System.out.println("SQL-Error");
			//e.printStackTrace();
		}
		return false;
	}

	/**
	 * Show: Shows specified table-entrys.
	 *
	 * @param command
	 *            the command
	 * @return Die Zeilen als ResultSet
	 */
	private ResultSet show(String command) {
		ResultSet rs;
		try {
			// System.out.println(command);
			rs = db.executeQuery(command);
			return rs;
		} catch (SQLException e) {
			System.out.println("Error: Database.show(): " + command);
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * newDate() updates the entry LastChange to the current time and date.
	 * REMOVED, unnecessary
	 * 
	 * public void newDate() { String date = new
	 * SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()); try {
	 * db.executeUpdate("update Informations set LastChange=" + date); } catch
	 * (SQLException e) { System.out.println("Error: Database.newDate(");
	 * e.printStackTrace(); } }
	 */
	
    /**
     * Login checks the admin's username and password and if an admin is already
     * logged in. On succesful login creating goes to false and database-actions
     * will get logged.
     *
     * @param username
     *            Username
     * @param password
     *            Password
     * @return if Login succeded or not
     */
    public Boolean login(String username, String password) {
        ResultSet info = show("select * from Informations");
        try {
            info.next();
            if (info.getString("Username").equals(username) && info.getString("Password").equals(password)
                    && !info.getBoolean("LoggedIn")) {
                //update("Informations", "LoggedIn=1", null);

                creating = false;
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    
	/**
	 * Logout from admin-mode. Actions won't get logged anymore.
	 */
	public void logout() {
		command("update Informations set LoggedIn=0");
		creating = true;
	}

	private boolean isDouble(String test) {
		try {
			Double.parseDouble(test);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * Turns a resultset to a 2DimArray
	 *
	 * @param rs
	 *            the ResultSet
	 * @return 2DimArray
	 */
	private ArrayList<String[]> RStoArray(ResultSet rs) {
		ArrayList<String[]> arr = new ArrayList<String[]>();
		try {
			int n = 0;
			while (rs.next()) {
				arr.add(new String[rs.getMetaData().getColumnCount()]);
				for (int i = 0; i < rs.getMetaData().getColumnCount(); i++) {
					if (rs.getObject(i + 1) != null) {
						arr.get(n)[i] = (String) rs.getObject(i + 1).toString();
					}
				}
				n++;
			}
		} catch (Exception e) {
			System.out.println("Error converting ResultSet to Array");
			e.printStackTrace();
		}
		return arr;
	}
/*
	private String[] rsToCommandArray(String table, ResultSet rs) throws SQLException {
		String columns = getColumnNames(rs);
		String[] colArr = getColumnNamesArr(rs);
		ArrayList<String[]> data = RStoArray(rs);
		if (data.size() == 0) {
			return null;
		}
        String[] commands = new String[data.size()];
		for (int i = 0; i < data.size(); i++) {
			StringBuilder command;
			if (isDouble(data.get(i)[0])) {
				command = new StringBuilder("INSERT INTO " + table + "(" + columns + ") VALUES (" + data.get(i)[0]);
			} else {
				command = new StringBuilder(
						"INSERT INTO " + table + "(" + columns + ") VALUES ('" + data.get(i)[0] + "'");
			}
			for (int n = 1; n < data.get(i).length; n++) {
				if (isDouble(data.get(i)[n])) {
					command.append("," + data.get(i)[n]);
				} else if (data.get(i)[n] == null) {
					int f = command.indexOf(colArr[n]);
					command.delete(f - 1, f + colArr[n].length());
				} else {
					command.append(",'" + data.get(i)[n] + "'");
				}
			}
			command.append(")");
			commands[i] = command.toString();
			commands[i] = commands[i].replace("'true'", "1");
			commands[i] = commands[i].replace("'false'", "0");
		}
		return commands;
	}*/
	
	   private String[] rsToFastCommandArray(String table, ResultSet rs) throws SQLException {
	        String columns = getColumnNames(rs);
	        String[] colArr = getColumnNamesArr(rs);
	        ArrayList<String[]> data = RStoArray(rs);
	        if (data.size() == 0) {
	            return null;
	        }
	        String[] commands = new String[(data.size()/100)+1];
            StringBuilder command;
            boolean fRun = true;
            if (isDouble(data.get(0)[0])) {
                command = new StringBuilder("INSERT INTO " + table + "(" + columns + ") VALUES (" + data.get(0)[0]);
            } else {
                command = new StringBuilder(
                        "INSERT INTO " + table + "(" + columns + ") VALUES ('" + data.get(0)[0] + "'");
            }
            int b = -1;
	        for (int i = 0; i < data.size(); i++) {
	            if (!fRun) {
	            if (isDouble(data.get(i)[0])) {
	                command = new StringBuilder("INSERT INTO " + table + "(" + columns + ") VALUES (" + data.get(i)[0]);
	            } else {
	                command = new StringBuilder("INSERT INTO " + table + "(" + columns + ") VALUES ('" + data.get(i)[0] + "'");
	            }}
	            for (int n = 1; n < data.get(i).length; n++) {
	                if (isDouble(data.get(i)[n])) {
	                    command.append("," + data.get(i)[n]);
	                } else if (data.get(i)[n] == null) {
	                    int f = command.indexOf(colArr[n]);
	                    command.delete(f - 1, f + colArr[n].length());
	                } else {
	                    command.append(",'" + data.get(i)[n] + "'");
	                }
	            }
	            command.append("); ");
	            fRun = false;
	            if (i%100==0) {
	                b++;
	            }
	            if (commands[b]==null) {
	                commands[b]="";
	            }
	            commands[b] = commands[b] + command.toString();
	            
	        }
	        for (int n = 0; n < commands.length; n++) {
	        	if (commands[n] != null) {
	            commands[n] = commands[n].replace("'true'", "1");
	            commands[n] = commands[n].replace("'false'", "0");
	            }
	        }
	        return commands;
	    }

	/**
	 * sync: Compare the local database with the global database, update the
	 * older one. ungetestet, Probleme zu erwarten mit Autoincrement
	 *
	 * @throws SQLException
	 *             the SQL exception
	 * @throws ParseException
	 * @throws IOException 
	 */
	public boolean sync() throws SQLException, ParseException, IOException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date late = df.parse("2017-01-01 11:11:11");
	    if (getLastChange().after(late)) {
	        System.out.println("Du versuchst gerade, Aenderungen von 2017 hochzuladen.");
	        return false;
	    }

		try {
			gdb = new GlobalDB();
		} catch (Exception e) {
			System.out.println("No synchronizing possible.");
            return false;
		}

        System.out.println("Synchronising from "+ getLastChange());


//BLOCK SYNCHRONIZING
//	if (true) {return true;}

        FTPCommunication.ftpConnect(ftpclient, "campusroute.bplaced.net", "campusroute_pse", "p1s2e316");
		String[] tables = {"Buildings", "Vertices", "ConstructionSites", "Floors", "Edges", "sqlite_sequence", "aliases", "Changes", "Informations", "Backgrounds"};
		if (gdb.getLastChange() != null && gdb.getLastChange().after(getLastChange())) {// Globale
																						// Datenbank
																						// runterladen


            if(!creating){
                return false;
            }

            System.out.println("Making copy of KIT.db to KITtemp.db");
            File file = new File("res/KIT.db");
            File copy = new File("res/KITtemp.db");


            try {
                FileUtils.copyFile(file, copy);
                System.out.println("Copied succesfully");
            } catch (IOException e) {
                e.printStackTrace();
            }


            System.out.println("Updating local database.");
			command(
                    "DELETE FROM Changes; DELETE FROM Buildings; DELETE FROM Vertices; DELETE FROM ConstructionSites; DELETE FROM Backgrounds; DELETE FROM Floors; DELETE FROM Edges; DELETE FROM Informations; DELETE FROM sqlite_sequence; DELETE FROM aliases;");
			for (int i = 0; i < 10; i++) {
			    String[] commands;
			    if (tables[i].equals("Changes")) {
			        commands = rsToFastCommandArray(tables[i], gdb.show("select Datum,Aenderung from changes order by Datum desc limit 20 "));
			    }
			    else {
				commands = rsToFastCommandArray(tables[i], gdb.show("select * from " + tables[i]));
			    }
                if (commands != null) {
    				for (int n = 0; n < commands.length; n++) {
    				    if (commands[n] != null) {
    					command(commands[n]);
    					gdb.show("select Number from Buildings");
    				    }
    				}
					if(tables[i].equals("Backgrounds")) {
						ResultSet rs = gdb.show("select Picture from Backgrounds");
						while(rs.next()) {
							FTPCommunication.downloadFile(ftpclient, rs.getString(1), rs.getString(1));
						}
					}
                }
			}
		} else if (gdb.getLastChange() == null || gdb.getLastChange().before(getLastChange())) { // Lokale
																									// Datenbank
																									// hochladen

			System.out.println("Updating global database.");


//BLOCK UPDATE GLOBAL DATABASE
   //  if (true) {return true;}



			gdb.command(
					"DELETE FROM Changes; DELETE FROM Buildings; DELETE FROM Vertices; DELETE FROM ConstructionSites; DELETE FROM Backgrounds; DELETE FROM Floors; DELETE FROM Edges; DELETE FROM Informations; DELETE FROM sqlite_sequence; DELETE FROM Aliases");
            for (int i = 0; i < 10; i++) {
                String[] commands;
                if (tables[i].equals("Changes")) {
                    commands = rsToFastCommandArray(tables[i],
                            show("select Datum,Aenderung from changes order by Datum desc limit 20 "));
                } else {
                    commands = rsToFastCommandArray(tables[i], show("select * from " + tables[i]));
                }
				if (commands != null) {
					for (int n = 0; n < commands.length; n++) {
					    if (commands[n] != null) {
						  gdb.command(commands[n]);
					    }
					}
					if(tables[i].equals("Backgrounds")) {
						ResultSet rs = show("select Picture from Backgrounds");
						while(rs.next()) {
							FTPCommunication.uploadFile(ftpclient, rs.getString(1), rs.getString(1));
						}
					}
				}
			}
		}
		FTPCommunication.ftpDisConnect(ftpclient);
		gdb.closeConn();
        return true;
	}

	private Observer observer;
	
	public void forgetDB() {
		command("DELETE FROM Changes; DELETE FROM Buildings; DELETE FROM Vertices; DELETE FROM ConstructionSites;"
				+ " DELETE FROM Backgrounds; DELETE FROM Floors; DELETE FROM Edges; DELETE FROM Informations; DELETE FROM sqlite_sequence; DELETE FROM aliases; DELETE FROM favorites");
		try {
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void discardChanges() {
	    Boolean created = false;
	    if (!creating) {
	        creating = true;
	        created = true;
	    }
	    delete("Changes", null);

	    if (created) {
	        creating = false;
	    }
	}

	@Override
	public void register(Observer obj) {
		if (obj != null) {
			observer = obj;
		}

	}

	@Override
	public void unregister() {
		observer = null;
		logout();
	}

	@Override
	public void notifyObserver(String message) {
	    if (observer != null) {
			observer.update(message);
	    }
	}

	@Override
	public boolean doSync() {
	
		try {

			if(sync()){
			    notifyObserver("Synchronisierung erfolgreich");
                return true;
            }




		} catch (Exception e) {
		}
        return false;
	}

}