package campusroutenplaner.model.favorites;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import campusroutenplaner.model.database.Database;
import campusroutenplaner.model.map.OnError;

public class FavoriteManagerTest {

	private FavoriteManager favorit;
	private Database data;
	
	
	
	@Before
	public void beforeTest() {
		
		
		data = new Database("test/model/KIT(testCopy).db");
		
		favorit = new FavoriteManager(data);
	}
	
	
	@Test
	public void addFavoritePlace() {
		
		String place = "Coolsdddddf";
		
		assertEquals(OnError.SuccessfulAdd, favorit.addFavoritePlace(place));

		data.addFav(place);
		
		
		assertEquals(favorit.getFavoritePlace(place), favorit.getFavoritePlace(place));

		assertEquals(OnError.UnSuccessfulAdd, favorit.addFavoritePlace(place));
		
		assertEquals( OnError.SuccessfulRemoved, favorit.removeFavoritePlace(place));
		
		assertEquals( OnError.UnSuccessfulRemoved, favorit.removeFavoritePlace(place));

		data.deleteFav(place);
				
	}
	
	
	
	
	
	
	@SuppressWarnings("deprecation")
	@Test
	public void addFavoriteRoute() {
		
		String place = "Cooldddssdfd";
		
		String placeTwo = "Folgsde";
		
		assertEquals(OnError.SuccessfulAdd, favorit.addFavoriteRoute(place, placeTwo));
		
		assertEquals(OnError.UnSuccessfulAdd, favorit.addFavoriteRoute(place, placeTwo));

		assertEquals( favorit.getFavoriteRoute(place, placeTwo), favorit.getFavoriteRoute(place, placeTwo));
		
		assertEquals( OnError.SuccessfulRemoved, favorit.removeFavoriteRoute(place, placeTwo));
		
		assertEquals( OnError.UnSuccessfulRemoved, favorit.removeFavoriteRoute(place, placeTwo) );
		
		data.deleteFav(place, placeTwo);
				
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void addFavoriteRouteWithStop() {
		
		String place = "Cooldddssdfd";
		
		String placeTwo = "Folgsde";
		
		String placeThree = "FFFFFFFF";
		
		assertEquals(OnError.SuccessfulAdd, favorit.addFavoriteRouteWithStopp(place, placeTwo, placeThree));
		
		assertEquals(OnError.UnSuccessfulAdd, favorit.addFavoriteRouteWithStopp(place, placeTwo, placeThree));
		
		assertEquals(favorit.getFavoriteRouteWithStopp(place, placeTwo, placeThree), favorit.getFavoriteRouteWithStopp(place, placeTwo, placeThree));
		
		assertEquals( OnError.SuccessfulRemoved, favorit.removeFavoriteRouteWithStop(place, placeTwo, placeThree));
		
		assertEquals( OnError.UnSuccessfulRemoved, favorit.removeFavoriteRouteWithStop(place, placeTwo, placeThree));

		
		data.deleteFav(place, placeTwo, placeThree);
				
	}
	
	
	
	@Test
	public void loadFavoritePlaces() {
	

		String place = "route";

		data.addFav(place);

		assertEquals( favorit.loadFavoritePlaces(), favorit.loadFavoritePlaces());
	}
	
	@Test
	public void loadFavoriteRoutes() {
		String place = "route";
		String placeTwo = "name";

		data.addFav(place, placeTwo);
		data.addFav(placeTwo, placeTwo);

		assertEquals( favorit.loadFavoriteRoutes(), favorit.loadFavoriteRoutes());
	}
	
	
	@Test
	public void loadFavoriteRoutesWtihStop() {
	


		String place = "wooow";
		String placeTwo = "ttttt";
		String placeThree = "dffdfdf";
		


		data.addFav(place, placeTwo, placeThree);
		assertEquals( favorit.loadFavoriteRoutesWithStop(), favorit.loadFavoriteRoutesWithStop());
	}
	
	
	

	@Test
	public void getFavoritePlacesAndRoutesAndRoutesWtihStop() {
		


		assertEquals( favorit.getPlaces(), favorit.getPlaces());
		assertEquals( favorit.getRoutes(), favorit.getRoutes());
		assertEquals(favorit.getRoutesWithStop(), favorit.getRoutesWithStop());
	}
	
	
	@After
	public void after() {
		data.forgetDB();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
