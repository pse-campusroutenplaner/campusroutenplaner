package campusroutenplaner.model.favorites;

import java.util.LinkedList;

import campusroutenplaner.model.database.Database;
import campusroutenplaner.model.map.OnError;

public class FavoriteManager {

	private LinkedList<String> places = new LinkedList<>();
	private LinkedList<String[]> routes = new LinkedList<String[]>();
	private LinkedList<String[]> routesWithStop = new LinkedList<String[]>();
	
	private Database database;
	
	public FavoriteManager(Database database) {
	   this.database = database;
	}
	
	
	/**
	 * check if such favorite place already exist. In this case  it will be returned UnsuccessfulAdd. Otherweise must  the new favorite place 
	 * to be saved both in the favorite place list and in the database and then it will be returned SuccessfulAdd.
	 * @param place
	 * @return Enum
	 */
	public OnError addFavoritePlace(String place) {
		
			if (!database.checkIfFavoritePlaceExist(place) ) {	
				if (database.addFav(place)) {
					places.add(place);
					return OnError.SuccessfulAdd;
				}
			}
			return OnError.UnSuccessfulAdd;
    }
	
	
	/**
	 * if the favorite route doesn't exist, it will be saved both in the list with favorite routes and in the database, then it will be returned SuccessfulAdd.
	 * In case that a route with such parameters already exist, it will be returned UnsuccessfulAdd;
	 * @param source
	 * @param dest
	 * @return Enum
	 */
	public OnError addFavoriteRoute(String source, String dest) {
		

			if (!database.checkIfFavoriteRouteExist(source, dest)) {
				if (database.addFav(source, dest)) {
				 
					String[] routeToAdding = {source, dest};
					routes.add(routeToAdding);
					return OnError.SuccessfulAdd;
				}
			}
			return OnError.UnSuccessfulAdd;
	}
	
	
	/**
	 * if the favorite route doesn't exist, it will be saved both in the list with favorite routes and in the database, then it will be returned SuccessfulAdd.
	 * In case that a route with such parameters already exist, it will be returned UnSuccessfulAdd;
	 * @param source
	 * @param stop
	 * @param dest
	 * @return Enum
	 */
	public OnError addFavoriteRouteWithStopp(String source, String stop, String dest) {
		
			if (!database.checkIfFavoriteRouteWithStopExist(source, stop , dest)) {
				if (database.addFav(source, stop, dest)) {
				 
					String[] routeToAdding = {source, stop, dest};
					routesWithStop.add(routeToAdding);
					return OnError.SuccessfulAdd;
				}
			}
			return OnError.UnSuccessfulAdd;

	}

	/**
	 * get favorite place from database. If Favorite place not found, return null
	 */
	public String getFavoritePlace(String name) {
	
       return database.getFavoritePlace(name);
	}
	
	/**
	 * get favorite route from database.If favorite Route not found ,return null 
	 */
	public String[] getFavoriteRoute(String source, String destination) {
		
		return database.getFavoriteRoute(source, destination);

    }
	
	/**
	 * get favorite route from database. If favorite route with stop not found, return null;
	 */
	 public String[] getFavoriteRouteWithStopp(String source, String stoppOver, String destination) {
	 
		 return database.getFavoriteRouteWithStop(source, stoppOver, destination);
	 }
	
	
	/**
	 * take all  favorite places from the database and save it in the list places, if it is empty (with the idea to eliminate a second call-up
	 * of this method, because all elements will be contained twice.(just for safety)
	 * @return LinkedList<String>
	 */
	public LinkedList<String> loadFavoritePlaces() {
		places.clear();
		for (int i = 0; i < database.getBuildingFavs().size(); i++){
			
				places.add(database.getBuildingFavs().get(i)[0]);
		}
		return places;
		
	}
	
	
	/**
	 * take all  favorite routes from the database and save it in the list routes, if it is empty (with the idea to eliminate a second call-up
	 * of this method, because all elements will be contained twice.(just for safety)
	 * @return LinkedList<String[]>
	 * 
	 */
	public LinkedList<String[]> loadFavoriteRoutes() {
		routes.clear();
		for( int i = 0; i < database.getRouteFavs().size(); i++) {
				

			String temp1 = database.getRouteFavs().get(i)[0];
			String temp2 = database.getRouteFavs().get(i)[1];
			String[] toReturn = {temp1, temp2};
			
				routes.add(toReturn);
		}
	  return routes;
	}
	
	/**
	 *
	 * take all  favorite routes from the database and save it in the list routesWithStop, if it is empty (with the idea to eliminate a second call-up
	 * of this method, because all elements will be contained twice.(just for safety)
	 * @return LinkedList<String[]>
	 */
	public LinkedList<String[]> loadFavoriteRoutesWithStop() {
		routesWithStop.clear();
		
		for ( int i = 0; i < database.getStopRouteFavs().size(); i++) {
			
			
			String temp1 = database.getStopRouteFavs().get(i)[0];
			String temp2 = database.getStopRouteFavs().get(i)[1];
			String temp3 = database.getStopRouteFavs().get(i)[2];
			String[] toReturn = {temp1, temp2, temp3};
			
			routesWithStop.add(toReturn);
		}
		
		return routesWithStop;

	}
	
	/**
	 * remove a favorite place , if such place exist, then remove from database and from the places list, then return SuccessfulRemove.
	 * if such place doesn't exist, remove nothing and return UnSuccessfulRemove;
	 * @param place
	 * @return Enum
	 */
	public OnError removeFavoritePlace(String place) {
		
			if (database.checkIfFavoritePlaceExist(place)) {
				 if (database.deleteFav(place)) {
					for (int i = 0; i < places.size(); i++) {
						if (places.get(i).equals(place)) {
							places.remove(i);
							return OnError.SuccessfulRemoved;
						}
					}
			   
		    	}
			}
		return OnError.UnSuccessfulRemoved;
	}


	/**
	 *  remove a favorite route, if such favorite route exist and return SuccessfulRemove.
	 *  If such favorite route doesn't exist,remove nothing and return UnSuccessfulRemove;
	 * @param source
	 * @param dest
	 * @return Enum
	 */
	public OnError removeFavoriteRoute(String source, String dest) {
		
		if (database.checkIfFavoriteRouteExist(source, dest)) {
			if (database.deleteFav(source, dest)) {
				for ( int i = 0; i < routes.size(); i++) {
					String[] tempRoute = routes.get(i);
						if ( tempRoute[0].equals(source) && tempRoute[1].equals(dest)) {
							routes.remove(i);
							tempRoute = null;
							return OnError.SuccessfulRemoved;
					}
				}
			}
		}
		return OnError.UnSuccessfulRemoved;
	}
	
	/**
	 * remove a favorite route with stop, if such favorite route exist, then return SuccessfulRemove.
	 * If such favorite route doesn't exist, remove nothing and return UnSuccessfulRemove;
	 * @param source
	 * @param stop
	 * @param dest
	 * @return Enum
	 */
	public OnError removeFavoriteRouteWithStop(String source, String stop, String dest) {
		
		if (database.checkIfFavoriteRouteWithStopExist(source, stop, dest)) {
			if (database.deleteFav(source, stop, dest)) {
				for ( int i = 0; i < routesWithStop.size(); i++) {
					String[] tempRoute = routesWithStop.get(i);
					if ( tempRoute[0].equals(source) && tempRoute[1].equals(stop) && tempRoute[2].equals(dest)) {
						routesWithStop.remove(i);
						tempRoute = null;
						return OnError.SuccessfulRemoved;
					}
				}
			}
		}

		return OnError.UnSuccessfulRemoved;
	}
	
	

	/**
	 * get the list with favorite places
	 * @return LinkedList<String>
	 */
	public LinkedList<String> getPlaces() {
		return places;
	}


	/**
	 * get the list with the favorite routes 
	 * @return LinkedList<LinkedList<String>>
	 */
	public LinkedList<String[]> getRoutes() {
		return routes;
	}


	/**
	 * get the list with the favorite routes with stop
	 * @return LinkedList<LinkedList<String>>
	 */
	public LinkedList<String[]> getRoutesWithStop() {
		return routesWithStop;
	}
	
}
