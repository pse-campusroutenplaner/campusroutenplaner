package campusroutenplaner.model.routing;

public enum RouteType {
	accessible, bike, walk;
}
