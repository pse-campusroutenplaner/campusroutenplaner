package campusroutenplaner.model.routing.GraphGenerator;

import campusroutenplaner.controller.main.BuildingController;
import campusroutenplaner.controller.main.MapController;
import campusroutenplaner.controller.usercontroller.RoutingController;
import campusroutenplaner.model.database.Database;
import campusroutenplaner.model.map.CampusMap;
import campusroutenplaner.model.map.Edge;
import campusroutenplaner.model.map.EntranceType;
import campusroutenplaner.model.map.MapManager;
import campusroutenplaner.model.routing.RoutingManager;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

/**
 * Created by nmlad on 8/18/16.
 */
public class DijkstraStressTest1 {

    private static Database database ;
    private static CampusMap campus;
    private static MapManager mapManager ;
    private static MapController mapController ;
    private static BuildingController buildingController ;
    private static RoutingManager routingManager ;
    private static RoutingController routingController ;

    private static LinkedList<Integer> floors;
    int floorId = 25;

    @BeforeClass
    public static  void setUpBefore(){

        File origin = new File("res/test/model/Stress.db");
        File copy = new File("res/test/model/StressCopy8.db");

        System.out.println("Creating copy...");
        try {
            FileUtils.copyFile(origin, copy);
        } catch (IOException e) {
            System.out.println("Copy could not be created.");
            e.printStackTrace();
            System.exit(127);
        }


        database = new Database("test/model/StressCopy8.db");

        campus = new CampusMap(database);
        mapManager = new MapManager(campus, database);
        mapManager.loadDatabase();
        mapController = new MapController(mapManager);
        buildingController = new BuildingController(mapManager, campus);

        routingManager = new RoutingManager();
        routingController = new RoutingController(routingManager, buildingController);





    }




    @Test
    public void test200(){
        String buildingNumber = "23.99";
        int n=200;


        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(n, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());
    }

    @Test
    public void test300() {
        String buildingNumber = "23.98";
        int n=300;


        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(n, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());


    }

    @Test
    public void test400(){
        String buildingNumber = "23.97";
        int n=400;


        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(n, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());
    }

    @Test
    public void test500(){
        String buildingNumber = "23.96";
        int n=500;


        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(n, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());
    }
    @Test
    public void test600(){
        String buildingNumber = "23.95";
        int n=600;


        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(n, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());
    }





    @Test
    public void test700(){
        String buildingNumber = "23.01";
        int n=700;


        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(n, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());
    }

    @Test
    public void test800(){
        String buildingNumber = "23.02";
        int n=800;


        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(n, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());
    }

    @Test
    public void test900(){
        String buildingNumber = "23.03";
        int n=900;


        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(n, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());
    }

    @Test
    public void test1000(){
        String buildingNumber = "23.04";
        int n=1000;


        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(n, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());
    }

    @Test
    public void test1100(){
        String buildingNumber = "23.05";
        int n=1100;


        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(n, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }
    @Test
    public void test1200(){
        String buildingNumber = "23.06";
        int n=1200;


        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(n, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }

    @Test
    public void test1300(){
        String buildingNumber = "23.07";
        int n=1300;


        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(n, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());
    }
    @Test
    public void test1400(){
        String buildingNumber = "23.08";
        int n=1400;


        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(n, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }
    @Test
    public void test1500(){
        String buildingNumber = "23.09";
        int n=1500;


        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(n, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }
    @Test
    public void test1600(){
        String buildingNumber = "23.10";
        int n=1600;

        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(n, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }

    @Test
    public void test1700(){
        String buildingNumber = "23.11";
        int n=1700;

        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(n, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }

    @Test
    public void test1800(){
        String buildingNumber = "23.12";
        int n=1800;

        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(n, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }

    @Test
    public void test1900(){
        String buildingNumber = "23.13";
        int n=1900;

        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(n, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }

    @Test
    public void test2000(){
        String buildingNumber = "23.14";
        int n=2000;

        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(n, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }

    @Test
    public void test2100(){
        String buildingNumber = "23.15";
        int n=2100;

        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(n, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }





    private void genGraph(int n, String buildingNumber){


      //  buildingController.requestDeleteBuilding(buildingNumber);


        buildingController.requestAddBuilding(buildingNumber);
        mapController.requestChangeVertexType(0, 66, new EntranceType(buildingNumber));

        int floorNumber = buildingController.requestAddFloor(buildingNumber, true);
        floorId =  buildingController.requestMapIdOfFloor(buildingNumber, floorNumber);

        GraphGeneratorReal generatorReal = new GraphGeneratorReal(mapController, 50, 150, 0.3, 120, floorId);
        generatorReal.generateGraph(n);




    }






    @AfterClass()
    public static void cleanUpAfter(){

        System.out.println("Deleting copy...");
        File toDelete = new File("res/test/model/StressCopy1.db");
        if(!toDelete.delete()){
            System.out.println("Copy could not be deleted.");
            System.exit(126);
        }
    }
}
