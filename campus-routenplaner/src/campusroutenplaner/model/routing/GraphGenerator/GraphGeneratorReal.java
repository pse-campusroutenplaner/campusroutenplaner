package campusroutenplaner.model.routing.GraphGenerator;

import campusroutenplaner.Logger;
import campusroutenplaner.controller.main.MapController;
import campusroutenplaner.model.database.Database;
import campusroutenplaner.model.map.CampusMap;
import campusroutenplaner.model.map.MapManager;
import campusroutenplaner.model.map.RoomType;
import campusroutenplaner.model.map.Vertex;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;


/**
 * Created by nmlad on 8/17/16.
 */
public class GraphGeneratorReal {


    private MapController mapController;
    private double rangeMin;
    private double rangeMax;
    private Random rand;
    private int floorId;
    private double probability;
    private double radius;
    private double biggestX = 0;
    private double biggestY = 0;
    private int biggestId = 0;

    /**
     *
     * @param mapController
     * @param rangeMin
     * @param rangeMax
     * @param probability an edge not to be created
     * @param floorId
     */
    public GraphGeneratorReal(MapController mapController, int rangeMin, int rangeMax, double probability, double radius, int floorId){
        this.rangeMin = rangeMin;
        this.rangeMax = rangeMax;
        this.mapController = mapController;
        rand = new Random();
        this.floorId = floorId;
        this.probability = probability;
        this.radius = radius;
    }



    public void generateGraph(int n){


        createVertices(n);
        createEdges();

        //find the first vertex and the vertex with max xPos and yPos
        int idFirst = Integer.MAX_VALUE;
        int idLast = Integer.MIN_VALUE;

        for(int i : mapController.requestVertexList(floorId)){
            if(i > idLast){
                idLast = i;
            }

            if(i < idFirst){
                idFirst = i;
            }
        }

        mapController.requestChangeVertexType(floorId, idFirst, new RoomType("1"));
        mapController.requestChangeVertexType(floorId, biggestId, new RoomType("2"));

    }

    private void createEdges() {

        for(int i :mapController.requestVertexList(floorId)){

            System.out.println("-------------- Creating edges for " + i);

            Vertex vertex = mapController.requestSearchVertex(floorId, i);


            //calculate the radius
            double radiusLow = 0;
            double radiusHigh = radius;



            //go through all the vertices
            for(int j :mapController.requestVertexList(floorId)) {

                Vertex neighbour = mapController.requestSearchVertex(floorId, j);

                double xPos = neighbour.getXPos();
                double yPos = neighbour.getYPos();

                //  (x - center_x)^2 + (y - center_y)^2 < radius^2
                if (vertex.getId() == neighbour.getId()) {
                    continue;
                }


                //if the vertex with id=j is in the radius
                if ((xPos - vertex.getXPos()) * (xPos - vertex.getXPos()) +
                        (yPos - vertex.getYPos()) * (yPos - vertex.getYPos()) < radiusHigh * radiusHigh) {
                    if ((xPos - vertex.getXPos()) * (xPos - vertex.getXPos()) +
                            (yPos - vertex.getYPos()) * (yPos - vertex.getYPos()) > radiusLow * radiusLow) {
                        if (neighbour.getEdgeBetween(vertex) == null) {


                            //get the probability
                            double probab = genRand(1, 10);

                            if (probab < probability * 10) {
                                continue; //if not succed
                            }

                            //create the edge
                            mapController.requestAddEdge(floorId, vertex.getId(), floorId, neighbour.getId());


                        }
                    }
                }

            }


            double radiusHighNew = rangeMax;
            while(vertex.getNeighbours().size() == 0) {
                System.out.println("-----isoliert");


                    for (int j: mapController.requestVertexList(floorId)) {

                          Vertex neighbour = mapController.requestSearchVertex(floorId, j);

                          double xPos = neighbour.getXPos();
                          double yPos = neighbour.getYPos();

                        //  (x - center_x)^2 + (y - center_y)^2 < radius^2


                         if ((xPos - vertex.getXPos()) * (xPos - vertex.getXPos()) +
                                (yPos - vertex.getYPos()) * (yPos - vertex.getYPos()) < radiusHighNew * radiusHighNew) {

                            if (neighbour.getEdgeBetween(vertex) == null) {


                                mapController.requestAddEdge(floorId, vertex.getId(), floorId, neighbour.getId());
                            }
                        }

                    }

                   radiusHighNew += rangeMax/2;
            }


        }
    }

    private void createVertices(int n) {


        double oldX = 0;
        double oldY = 0;
        double x;
        double y;
        int actualSize = 0; //number of vertices now


        Queue<Vertex> queue = new LinkedList<>();

        //create first vertex
        Vertex vertex = mapController.requestAddVertex(floorId, 10, 10);
        actualSize++;

        queue.offer(vertex);


       while(n !=actualSize) {

           //take the first vertex of the queue
           Vertex vertexOld = queue.remove();

           //save his position
           oldX = vertexOld.getXPos();
           oldY = vertexOld.getYPos();



        //   double angle = 0;



            //nagore
           x = oldX;        //get the position for a new vertex
           y = genRand(rangeMin, rangeMax)  + oldY;




           if(n != actualSize && createVertex(floorId, x, y, queue)){
                actualSize++;
            }







           //nagore diagonal
    //       angle = genRand(30, 60);
           x = (genRand(rangeMin, rangeMax) + oldX);  //get the position for a new vertex
           y = (genRand(rangeMin, rangeMax) + oldY);

           if(n != actualSize && createVertex(floorId, x, y, queue)){
               actualSize++;
           }




           //nagore nadqsno

  //         angle = (45 + (135 - 45) * rand.nextDouble());
           x = genRand(rangeMin, rangeMax)  + oldX; //get the position for a new vertex
           y = oldY;

           if(n != actualSize && createVertex(floorId, x, y, queue)){
               actualSize++;
           }



       }





    }

    private double genRand(double low, double high){
        rand = new Random();
        return  (low + (high - low) * rand.nextDouble());
    }




    private boolean createVertex(int floorId, double x, double y, Queue<Vertex> queue){
        Vertex vertex = mapController.requestAddVertex(floorId, x, y);

        if(vertex == null){
            return false;
        }


        //add the vertex to the queue
        queue.add(vertex);



        if(vertex.getXPos() >= biggestX && vertex.getYPos() >= biggestY){
            biggestId = vertex.getId();
            biggestX = vertex.getXPos();
            biggestY = vertex.getYPos();
        }

        return true;
    }

}



