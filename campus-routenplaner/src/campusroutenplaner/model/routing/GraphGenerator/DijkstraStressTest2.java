package campusroutenplaner.model.routing.GraphGenerator;

import campusroutenplaner.Logger;
import campusroutenplaner.controller.main.BuildingController;
import campusroutenplaner.controller.main.MapController;
import campusroutenplaner.controller.usercontroller.RoutingController;
import campusroutenplaner.model.database.Database;
import campusroutenplaner.model.map.*;
import campusroutenplaner.model.routing.GraphGenerator.GraphGeneratorTable;
import campusroutenplaner.model.routing.RoutingManager;
import org.apache.commons.io.FileUtils;
import org.junit.*;
import org.omg.CORBA.INTERNAL;
import org.omg.CORBA.SystemException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.LinkedList;

/**
 * Created by nmlad on 8/18/16.
 */
public class DijkstraStressTest2 {

    private static Database database ;
    private static CampusMap campus;
    private static MapManager mapManager ;
    private static MapController mapController ;
    private static BuildingController buildingController ;
    private static RoutingManager routingManager ;
    private static RoutingController routingController ;

    private static LinkedList<Integer> floors;
    int floorId = 25;

    @BeforeClass
    public static  void setUpBefore(){




        File origin = new File("res/test/model/Stress.db");
        File copy = new File("res/test/model/StressCopy2.db");

        System.out.println("Creating copy...");
        try {
            FileUtils.copyFile(origin, copy);
        } catch (IOException e) {
            System.out.println("Copy could not be created.");
            e.printStackTrace();
            System.exit(127);
        }


        database = new Database("test/model/StressCopy2.db");

        campus = new CampusMap(database);
        mapManager = new MapManager(campus, database);
        mapManager.loadDatabase();
        mapController = new MapController(mapManager);
        buildingController = new BuildingController(mapManager, campus);

        routingManager = new RoutingManager();
        routingController = new RoutingController(routingManager, buildingController);





    }




    @Test
    public void test200(){
        String buildingNumber = "22.99";
        int height = 10;
        int width = 20;


        genGraph(height, width, buildingNumber);
        routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");

    }

    @Test
    public void test300() {
        String buildingNumber = "22.98";
        int height = 10;
        int width = 30;


        LinkedList<Edge> route;

        do{
            genGraph(height, width, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route.isEmpty() || route == null);


    }

    @Test
    public void test400(){
        String buildingNumber = "22.97";
        int height = 10;
        int width = 40;


        LinkedList<Edge> route;

        do{
            genGraph(height, width, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route.isEmpty() || route == null);

    }

    @Test
    public void test500(){
        String buildingNumber = "22.96";
        int height = 10;
        int width = 50;


        LinkedList<Edge> route;

        do{
            genGraph(height, width, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route.isEmpty() || route == null);


    }
    @Test
    public void test600(){
        String buildingNumber = "22.95";
        int height = 10;
        int width = 60;

        LinkedList<Edge> route;

        do{
            genGraph(height, width, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route.isEmpty() || route == null);

    }





    @Test
    public void test700(){
        String buildingNumber = "22.01";
        int height = 10;
        int width = 70;


        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(height, width, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }

    @Test
    public void test800(){
        String buildingNumber = "22.02";
        int height = 10;
        int width = 80;

        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(height, width, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }

    @Test
    public void test900(){
        String buildingNumber = "22.03";
        int height = 10;
        int width = 90;


        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(height, width, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }

    @Test
    public void test1000(){
        String buildingNumber = "22.04";
        int height = 100;
        int width = 10;


        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(height, width, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }

    @Test
    public void test1100(){
        String buildingNumber = "22.05";
        int height = 10;
        int width = 110;


        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(height, width, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }
    @Test
    public void test1200(){
        String buildingNumber = "22.06";
        int height = 10;
        int width = 120;


        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(height, width, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }

    @Test
    public void test1300(){
        String buildingNumber = "22.07";
        int height = 10;
        int width = 130;


        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(height, width, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }
    @Test
    public void test1400(){
        String buildingNumber = "22.08";
        int height = 10;
        int width = 140;


        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(height, width, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }
    @Test
    public void test1500(){
        String buildingNumber = "22.09";
        int height = 10;
        int width = 150;


        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(height, width, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }
    @Test
    public void test1600(){
        String buildingNumber = "22.10";
        int height = 40;
        int width = 40;

        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(height, width, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }

    @Test
    public void test1700(){
        String buildingNumber = "22.11";
        int height = 100;
        int width = 17;

        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(height, width, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }

    @Test
    public void test1800(){
        String buildingNumber = "22.12";
        int height = 100;
        int width = 18;

        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(height, width, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }

    @Test
    public void test1900(){
        String buildingNumber = "22.13";
        int height = 100;
        int width = 19;

        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(height, width, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }

    @Test
    public void test2000(){
        String buildingNumber = "22.14";
        int height = 100;
        int width = 20;

        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(height, width, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }

    @Test
    public void test2100(){
        String buildingNumber = "22.15";
        int height = 21;
        int width = 100;

        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(height, width, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }

    @Test
    public void test2200(){
        String buildingNumber = "22.16";
        int height = 22;
        int width = 100;

        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(height, width, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }

    @Test
    public void test2300(){
        String buildingNumber = "22.17";
        int height = 23;
        int width = 100;

        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(height, width, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }

    @Test
    public void test2400(){
        String buildingNumber = "22.18";
        int height = 24;
        int width = 100;

        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(height, width, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }

    @Test
    public void test2500(){
        String buildingNumber = "22.19";
        int height = 25;
        int width = 00;

        LinkedList<Edge> route;

        do{
            buildingController.requestDeleteBuilding(buildingNumber);

            genGraph(height, width, buildingNumber);
            route = routingController.requestRouting(buildingNumber, "1", buildingNumber, "2", "walk");
        }while (route == null || route.isEmpty());

    }





    private void genGraph(int height, int width, String buildingNumber){


      //  buildingController.requestDeleteBuilding(buildingNumber);


        buildingController.requestAddBuilding(buildingNumber);
        mapController.requestChangeVertexType(0, 66, new EntranceType(buildingNumber));

        int floorNumber = buildingController.requestAddFloor(buildingNumber, true);
        floorId =  buildingController.requestMapIdOfFloor(buildingNumber, floorNumber);

        GraphGeneratorTable graphGeneratorTable = new GraphGeneratorTable(mapController, 10, height, width, 0.3, floorId);
        graphGeneratorTable.generateGraph();




    }






    @AfterClass()
    public static void cleanUpAfter(){

        System.out.println("Deleting copy...");
        File toDelete = new File("res/test/model/StressCopy2.db");
        if(!toDelete.delete()){
            System.out.println("Copy could not be deleted.");
            System.exit(126);
        }

    }
}
