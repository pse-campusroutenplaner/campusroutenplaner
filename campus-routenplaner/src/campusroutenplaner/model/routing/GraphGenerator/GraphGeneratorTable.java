package campusroutenplaner.model.routing.GraphGenerator;

import campusroutenplaner.controller.main.MapController;
import campusroutenplaner.model.map.RoomType;
import campusroutenplaner.model.map.Vertex;

import java.util.DoubleSummaryStatistics;
import java.util.Random;

/**
 * Created by nmlad on 8/18/16.
 */
public class GraphGeneratorTable {



    private Vertex[][] vertices;
    int floorId;
    int height;
    int width;
    double probability;
    double range;
    MapController mapController;


    public GraphGeneratorTable(MapController mapController, double range, int height, int width, double probability, int floorId){
        this.height = height;
        this.width = width;
        this.probability = probability;
        this.range = range;
        this.mapController = mapController;
        this.floorId = floorId;

        vertices = new Vertex[height][width];
    }




    public void generateGraph(){

        for(int i=0; i < height; i++){
            for(int j = 0 ; j < width; j++){

                //add a vertex
                Vertex vertex = mapController.requestAddVertex(floorId, i*range, j*range);


                vertices[i][j] = vertex;
                int x;
                int y;

                //connect with other vertices


               // Vertex neighbour;

                //nagore
                x = i+1;
                y = j;
                addEdgeFail(vertex, x,y);



                //lqv goren diagonal
                x = i+1;
                y = j-1;
                addEdgeFail(vertex, x,y);

                //lqvo
                x = i;
                y = j-1;
                addEdgeFail(vertex, x,y);



                //dolen lqv diagonal
                x = i-1;
                y = j-1;
                addEdgeFail(vertex, x,y);



                //nadolu
                x = i-1;
                y = j;
                addEdgeFail(vertex, x,y);

                //if the vertex is isoliert, to the same thing
                if(vertex.getNeighbours().size() == 0){
                    System.out.println("--isoliert");


                    //nagore
                    x = i+1;
                    y = j;
                    addEdge(vertex, x,y);



                    //lqv goren diagonal
                    x = i+1;
                    y = j-1;
                    addEdge(vertex, x,y);

                    //lqvo
                    x = i;
                    y = j-1;
                    addEdge(vertex, x,y);



                    //dolen lqv diagonal
                    x = i-1;
                    y = j-1;
                    addEdge(vertex, x,y);



                    //nadolu
                    x = i-1;
                    y = j;
                    addEdge(vertex, x,y);


                }


            }
        }



        mapController.requestChangeVertexType(floorId, vertices[0][0].getId(), new RoomType("1"));
        mapController.requestChangeVertexType(floorId, vertices[height-1][width-1].getId(), new RoomType("2"));


    }


    /**
     * create an edge with the chance of failure
     * @param vertex
     * @param x
     * @param y
     */
    private void addEdgeFail(Vertex vertex, int x, int y) {


        if(x >=0 && y >= 0 && x<height && y<width){

            Random rand = new Random();
            double probab = 1 + (10 - 1) * rand.nextDouble();

            if (probab < probability * 10) {
                return;
            }



            Vertex neighbour = vertices[x][y];
            if(neighbour != null && neighbour.getEdgeBetween(vertex) == null){
                mapController.requestAddEdge(floorId, vertex.getId(), floorId, neighbour.getId());
            }
        }
    }


    //create an edge without chance of fail
    private void addEdge(Vertex vertex, int x, int y) {


        if(x >=0 && y >= 0 && x<height && y<width){





            Vertex neighbour = vertices[x][y];
            if(neighbour != null && neighbour.getEdgeBetween(vertex) == null){
                mapController.requestAddEdge(floorId, vertex.getId(), floorId, neighbour.getId());
            }
        }
    }


}
