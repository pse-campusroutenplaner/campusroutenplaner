package campusroutenplaner.model.routing.GraphGenerator;

import campusroutenplaner.controller.main.MapController;
import campusroutenplaner.model.database.Database;
import campusroutenplaner.model.map.CampusMap;
import campusroutenplaner.model.map.MapManager;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.xml.crypto.Data;
import java.util.LinkedList;

/**
 * Created by nmlad on 8/17/16.
 */
public class GraphGeneratorTest {


    private static MapController mapController;
    private static GraphGeneratorReal graphGenerator;
    private static Database database;

    @BeforeClass
    public static void setUp(){
        database = new Database("test/model/graphGenerator.db");
        database.forgetDB();
        database = new Database("test/model/graphGenerator.db");
        CampusMap campusMap = new CampusMap(database);
        MapManager mapManager = new MapManager(campusMap, database);
        mapController = new MapController(mapManager);
        graphGenerator = new GraphGeneratorReal(mapController, 10 , 20, 0.5, 15, 0);
    }



    @Test
    public void Test1(){

        graphGenerator.generateGraph(30);
    }
}
