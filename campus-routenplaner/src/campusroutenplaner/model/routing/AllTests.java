package campusroutenplaner.model.routing;
 
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
 
@RunWith(value = Suite.class)
@Suite.SuiteClasses(value = { DijkstraElementTest.class, DijkstraTest.class,
RoutingManagerTest.class})

public class AllTests {
 
    public AllTests() {
    }
}