package campusroutenplaner.model.routing;

import campusroutenplaner.model.map.Vertex;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.nio.file.Files;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

/**
 * Created by nmlad on 8/5/16.
 */
public class DijkstraElementTest {


    DijkstraElement dijkstraElement;


    @Test
    public void constructorTest(){
        Vertex element = new Vertex(0, 0, 0, 0);
        Vertex prev = new Vertex(0, 0, 1, 0);

        dijkstraElement = new DijkstraElement(element,  prev);

        assertEquals(element, dijkstraElement.getElement());
        assertEquals(prev, dijkstraElement.getPrevious());
        assertEquals(Double.MAX_VALUE, dijkstraElement.currentWeight(), 1);
        assertEquals("DijkstraElement{elementId=0, previous= 1, weight=1.7976931348623157E308}", dijkstraElement.toString());
        }

    @Test
    public void extendedConstructorTest(){
        Vertex element = new Vertex(0, 0, 0, 0);
        Vertex prev = new Vertex(0, 0, 1, 0);

        dijkstraElement = new DijkstraElement(element,  prev, 5);

        assertEquals(element, dijkstraElement.getElement());
        assertEquals(prev, dijkstraElement.getPrevious());
        assertEquals( 5, dijkstraElement.currentWeight(), 1);
        assertEquals("DijkstraElement{elementId=0, previous= 1, weight=5.0}", dijkstraElement.toString());

    }



    @After
    public void cleanUp(){
        dijkstraElement = null;
    }

}


