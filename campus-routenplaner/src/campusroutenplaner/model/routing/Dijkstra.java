package campusroutenplaner.model.routing;


import campusroutenplaner.Logger;
import campusroutenplaner.model.map.Edge;
import campusroutenplaner.model.map.Vertex;
import org.junit.*;

import java.util.*;

import static org.junit.Assert.*;


public class Dijkstra {

    private PriorityQueue<DijkstraElement> pqueue;
    private HashMap<Vertex, DijkstraElement> elementHashMap;
    private HashMap<Vertex, DijkstraElement> visited;


    /**
     * on every routing, you have to delete this object and create it again
     */
    public Dijkstra(){

        elementHashMap = new HashMap<>();
        visited = new HashMap<>();

        pqueue = new PriorityQueue<>(new Comparator<DijkstraElement>() {
        @Override
        public int compare(DijkstraElement o1, DijkstraElement o2) {
            if(o1.currentWeight() < o2.currentWeight()){
                return -1;
            }else if(o1.currentWeight() == o2.currentWeight()){
                return 0;
            }else{
                return 1;
            }

        }
          });


    }


    /**
     *
     * @param startList
     * @param destList
     * @return list with path, from end to start
     */
    public LinkedList<Vertex> routing(LinkedList<Vertex> startList, LinkedList<Vertex> destList){
        if(startList == null || destList == null || startList.isEmpty() || destList.isEmpty()){
            System.out.println("startList or destList is null or empty");
            return new LinkedList<>();
        }




        pqueue = new PriorityQueue<>(new Comparator<DijkstraElement>() {
            @Override
            public int compare(DijkstraElement o1, DijkstraElement o2) {
                if(o1.currentWeight() < o2.currentWeight()){
                    return -1;
                }else if(o1.currentWeight() == o2.currentWeight()){
                    return 0;
                }else{
                    return 1;
                }

            }
        });
        elementHashMap =  new HashMap<>();
        visited = new HashMap<>();




        long startTime = System.currentTimeMillis();


        System.out.println("-------------ROUTING STARTS--------------");
        System.out.println("Starting to route from "+ startList + "to "+ destList);


    	HashSet<Vertex> start = new HashSet<Vertex>();
    	HashSet<Vertex> dest = new HashSet<Vertex>();

    	
        //init beginning Dijkstra elements for the start
        for(Vertex vertex : startList){
            if(vertex == null) continue;

        	start.add(vertex);
            DijkstraElement dij = new DijkstraElement(vertex, null, 0);
            elementHashMap.put(vertex, dij);
            pqueue.add(dij);
        }


        //init dest vertices
        for(Vertex vertex : destList){
            if(vertex == null) continue;
        	dest.add(vertex);
            DijkstraElement dij = new DijkstraElement(vertex, null);
            elementHashMap.put(vertex, dij);
        }



        //adding one dummy
        pqueue.add(new DijkstraElement(new Vertex(),null,Double.MAX_VALUE));
        pqueue.add(new DijkstraElement(new Vertex(), null, Double.MAX_VALUE));

       
        
        DijkstraElement end = new DijkstraElement(null, null);



        while(!pqueue.isEmpty()){
        	
        	
        //	System.out.println(printHashMap());
       // 	System.out.println(printVisited());


            //print visited
//            String stringStream = "Visited: ";

 //           for(DijkstraElement elem : visited.values()){
 //               stringStream = stringStream +", " + elem.getElement().getId();
//            }
//          System.out.println(stringStream);

        //    Logger.print(stringStream);
        //	System.out.println(printPqueue());

            //get the elements with smallest weight
            DijkstraElement u = pqueue.poll();
     //   System.out.println("Getting el. with smallest weight "+u);

            
            if(visited.containsKey(u.getElement())){
            	continue;
            }

            if(u.getElement().getId() == 0 ){
                System.out.println("Route not found");
                long stopTime = System.currentTimeMillis();

                Logger.print("Elapsed time was " + (stopTime - startTime) + " miliseconds.");
                System.out.println("all visited: " + visited.size());
                return new LinkedList<>();
            }
            
            
            //add him as visited
            visited.put(u.getElement(), u);
            
            
            
            
            //we have visited a destination vertex
            if(dest.contains(u.getElement())){
            	end = u;
            	break;	
            }
           


            //for every neighbour
            for(Edge edge : u.getElement().getNeighbours()){
            	
            	if(edge.isBlocked()){
            		continue;
            	}



                if (edge.getSource().equals(u.getElement())) {

            //    System.out.println("- Relaxing, source " + edge.getSource().getId() + ", with "+ edge.getDest().getId());

                    relax(edge.getSource(), edge.getDest(), edge.getLength());


                }else if(edge.getDest().equals(u.getElement())){

                //   System.out.println("- Relaxing, source " + edge.getDest().getId() + ", with "+ edge.getSource().getId());

                    relax(edge.getDest(), edge.getSource(), edge.getLength());


                }
                //else{
                 //   System.out.println("Dijkstra, routing: DEFINITELY NOT SUPPOSED TO HAPPEND!");
                //}
            }



        }
        
        
        
        
        //create path
        LinkedList<Vertex> path = getPath(end.getElement());
    	
  

    	//print it
       String paa="";
        for(Vertex v : path){
            paa = paa + "," + v.getId();
        }

        System.out.println("Route: "+ paa);
     //   Logger.print("Route: "+ paa);
        long stopTime = System.currentTimeMillis();

        Logger.print("Elapsed time was " + (stopTime - startTime) + " miliseconds.");
        System.out.println("all visited: " + visited.size());
        
        return path;

    }




    /**
     *
     * @param startList
     * @param destList
     * @return list with path, from end to start
     */
    public LinkedList<Vertex> routingAccesible(LinkedList<Vertex> startList, LinkedList<Vertex> destList){
        if(startList == null || destList == null || startList.isEmpty() || destList.isEmpty()){
            System.out.println("startList or destList is null or empty");
            return new LinkedList<>();
        }



        pqueue = new PriorityQueue<>(new Comparator<DijkstraElement>() {
            @Override
            public int compare(DijkstraElement o1, DijkstraElement o2) {
                if(o1.currentWeight() < o2.currentWeight()){
                    return -1;
                }else if(o1.currentWeight() == o2.currentWeight()){
                    return 0;
                }else{
                    return 1;
                }

            }
        });
        elementHashMap =  new HashMap<>();
        visited = new HashMap<>();


        long startTime = System.currentTimeMillis();
        System.out.println("-------------ACCESSIBLE ROUTING STARTS--------------");

        System.out.println("Starting to route from "+ startList + "to "+ destList);


        HashSet<Vertex> start = new HashSet<Vertex>();
        HashSet<Vertex> dest = new HashSet<Vertex>();


        //init beginning Dijkstra elements for the start
        for(Vertex vertex : startList){
            if(vertex == null) continue;
            start.add(vertex);
            DijkstraElement dij = new DijkstraElement(vertex, null, 0);
            elementHashMap.put(vertex, dij);
            pqueue.add(dij);
        }


        //init dest vertices
        for(Vertex vertex : destList){
            if(vertex == null) continue;
            dest.add(vertex);
            DijkstraElement dij = new DijkstraElement(vertex, null);
            elementHashMap.put(vertex, dij);
        }



        //adding one dummy
        pqueue.add(new DijkstraElement(new Vertex(), null, Double.MAX_VALUE));



        DijkstraElement end = new DijkstraElement(null, null);



        while(!pqueue.isEmpty()){


            
            
            //get the elements with smallest weight
            DijkstraElement u = pqueue.poll();
            //   System.out.println("Getting el. with smallest weight "+u);


            if(visited.containsKey(u.getElement())){
                continue;
            }



            if(u.getElement().getId() == 0 ){
                System.out.println("Route not found");
                long stopTime = System.currentTimeMillis();

                Logger.print("Elapsed time was " + (stopTime - startTime) + " miliseconds.");
                System.out.println("all visited: " + visited.size());
                return new LinkedList<>();
            }


            //add him as visited
            visited.put(u.getElement(), u);




            //we have visited a destination vertex
            if(dest.contains(u.getElement())){
                end = u;
                break;
            }



            //for every neighbour
            for(Edge edge : u.getElement().getNeighbours()){

            	
            	
                if(edge.isBlocked() ||(!edge.isAccessible())){
                	//Logger.print("Cannot create dijkstraElement for Edge: " + edge);
                    continue;
                }


                if (edge.getSource().equals(u.getElement())) {

                 //   System.out.println("- Relaxing, source " + edge.getSource().getId() + ", with "+ edge.getDest().getId());

                    relax(edge.getSource(), edge.getDest(), edge.getLength());


                }else if(edge.getDest().equals(u.getElement())){

               //     System.out.println("- Relaxing, source " + edge.getDest().getId() + ", with "+ edge.getSource().getId());

                    relax(edge.getDest(), edge.getSource(), edge.getLength());


                }//else{
                 //   System.out.println("Dijkstra, routing: DEFINITELY NOT SUPPOSED TO HAPPEND!");
                //}
            }



        }




        //create path
        LinkedList<Vertex> path = getPath(end.getElement());




		//print it
	    String paa="";
	    for(Vertex v : path){
	        paa = paa + "," + v.getId();
	    }

        System.out.println("Route: "+ paa);

        long stopTime = System.currentTimeMillis();

        Logger.print("Elapsed time was " + (stopTime - startTime) + " miliseconds.");
        System.out.println("all visited: " + visited.size());

        return path;

    }

    
    

    private void relax(Vertex element, Vertex toRelax, double lenght) {




        DijkstraElement source = elementHashMap.get(element);



        //if(source == null){
         //   System.out.println("Dijsktra, relax: not suppose to happend");
       //     return;
     //   }




        DijkstraElement dest = elementHashMap.get(toRelax);

        if(dest == null){
       //     System.out.println("--- No dijkstraElement for this vertex");
            
            dest = new DijkstraElement(toRelax, element, source.currentWeight() + lenght);
            elementHashMap.put(toRelax, dest);
            pqueue.add(dest);
            
            
          //  System.out.println("--- Create: "+dest);
            
            //in case of not accessible search, do not create dijsktra element
            return;
        }


        if(visited.containsKey(dest.getElement())){
         //   System.out.println("---"+ dest.getElement().getId()+" is visited");
            return;
        }
        
        
        


        //calculate new lenght
        double newLenght = lenght + source.currentWeight();


        if(newLenght < dest.currentWeight()){
        	
        	//relax him
        	DijkstraElement newDelement = new DijkstraElement(toRelax, element, newLenght);
        	pqueue.add(newDelement);
        	elementHashMap.put(toRelax, newDelement);
        	
        	
            
      //   System.out.println("---new length better "+ newLenght + " update: "+ newDelement);
        }else{
      //   System.out.println("---new lenght not better  "+ newLenght + " than "+ dest.currentWeight());
        }



    }

    
    
    

 /*   public String printHashMap(){
    	
    	String stringStream = "HashMap: ";
    	
    	for(DijkstraElement elem : elementHashMap.values()){
    		stringStream = stringStream +", " + elem.getElement().getId();
    	//	stringStream = stringStream +",, " + elem;
    	}
    	
    	return stringStream;
    }*/
    
    

    
 /*   public String printPqueue(){
    	
    	String stringStream = "Pqueue: ";
    	
    	Iterator<DijkstraElement> it = pqueue.iterator();
    	
    	while(it.hasNext()){
    		
    		DijkstraElement elem = (DijkstraElement) it.next();
    		
    		stringStream = stringStream +", " +  elem.getElement().getId();
 
    	}
    	
    	return stringStream;
    	
    }*/



    private LinkedList<Vertex> getPath(Vertex dest){

        LinkedList<Vertex> path = new LinkedList<>();

        path.add(dest);
        getPath(dest, path);

        return path;


    }
    
    
    


    private void getPath(Vertex dest, LinkedList<Vertex> path){


       // System.out.println("Path: dest " + dest);

        //find the dijkstraelemtn
        DijkstraElement destElem = visited.get(dest);




        Vertex prev = destElem.getPrevious();


        if(prev == null){
           
        	
        	//we are done
            return;
        }else{
            path.add(prev);
            getPath(prev, path);
        }



    }


    /**
     *
     * @param startList
     * @param destList
     * @return list with path, from end to start
     */
    public LinkedList<Vertex> routingBike(LinkedList<Vertex> startList, LinkedList<Vertex> destList){
        if(startList == null || destList == null || startList.isEmpty() || destList.isEmpty()){
            System.out.println("startList or destList is null or empty");
            return new LinkedList<>();
        }



        pqueue = new PriorityQueue<>(new Comparator<DijkstraElement>() {
            @Override
            public int compare(DijkstraElement o1, DijkstraElement o2) {
                if(o1.currentWeight() < o2.currentWeight()){
                    return -1;
                }else if(o1.currentWeight() == o2.currentWeight()){
                    return 0;
                }else{
                    return 1;
                }

            }
        });
        elementHashMap =  new HashMap<>();
        visited = new HashMap<>();


        long startTime = System.currentTimeMillis();
        System.out.println("-------------BIKE ROUTING STARTS--------------");

        System.out.println("Starting to route from "+ startList + "to "+ destList);


        HashSet<Vertex> start = new HashSet<Vertex>();
        HashSet<Vertex> dest = new HashSet<Vertex>();


        //init beginning Dijkstra elements for the start
        for(Vertex vertex : startList){
            if(vertex == null) continue;
            start.add(vertex);
            DijkstraElement dij = new DijkstraElement(vertex, null, 0);
            elementHashMap.put(vertex, dij);
            pqueue.add(dij);
        }


        //init dest vertices
        for(Vertex vertex : destList){
            if(vertex == null) continue;
            dest.add(vertex);
            DijkstraElement dij = new DijkstraElement(vertex, null);
            elementHashMap.put(vertex, dij);
        }



        //adding one dummy
        pqueue.add(new DijkstraElement(new Vertex(), null, Double.MAX_VALUE));



        DijkstraElement end = new DijkstraElement(null, null);



        while(!pqueue.isEmpty()){




            //get the elements with smallest weight
            DijkstraElement u = pqueue.poll();
            //   System.out.println("Getting el. with smallest weight "+u);


            if(visited.containsKey(u.getElement())){
                continue;
            }



            if(u.getElement().getId() == 0 ){
                System.out.println("Route not found");
                long stopTime = System.currentTimeMillis();

                Logger.print("Elapsed time was " + (stopTime - startTime) + " miliseconds.");
                System.out.println("all visited: " + visited.size());
                return new LinkedList<>();
            }


            //add him as visited
            visited.put(u.getElement(), u);




            //we have visited a destination vertex
            if(dest.contains(u.getElement())){
                end = u;
                break;
            }



            //for every neighbour
            for(Edge edge : u.getElement().getNeighbours()){



                if(edge.isBlocked() ||(!edge.isAccessible())){
                    //Logger.print("Cannot create dijkstraElement for Edge: " + edge);
                    continue;
                }


                if (edge.getSource().equals(u.getElement())) {

                    //   System.out.println("- Relaxing, source " + edge.getSource().getId() + ", with "+ edge.getDest().getId());

                    relaxBike(edge.getSource(), edge.getDest(), edge.getLength());


                }else if(edge.getDest().equals(u.getElement())){

                    //     System.out.println("- Relaxing, source " + edge.getDest().getId() + ", with "+ edge.getSource().getId());

                    relaxBike(edge.getDest(), edge.getSource(), edge.getLength());


                }//else{
                //   System.out.println("Dijkstra, routing: DEFINITELY NOT SUPPOSED TO HAPPEND!");
                //}
            }



        }




        //create path
        LinkedList<Vertex> path = getPath(end.getElement());




        //print it
        String paa="";
        for(Vertex v : path){
            paa = paa + "," + v.getId();
        }

        System.out.println("Route: "+ paa);

        long stopTime = System.currentTimeMillis();

        Logger.print("Elapsed time was " + (stopTime - startTime) + " miliseconds.");
        System.out.println("all visited: " + visited.size());

        return path;

    }



    private void relaxBike(Vertex element, Vertex toRelax, double lenght) {




        DijkstraElement source = elementHashMap.get(element);



        //if(source == null){
        //   System.out.println("Dijsktra, relax: not suppose to happend");
        //     return;
        //   }




        DijkstraElement dest = elementHashMap.get(toRelax);

        if(dest == null){
            //     System.out.println("--- No dijkstraElement for this vertex");
            if(toRelax.getMapId() != 0){
                return;
            }


            dest = new DijkstraElement(toRelax, element, source.currentWeight() + lenght);
            elementHashMap.put(toRelax, dest);
            pqueue.add(dest);


            //  System.out.println("--- Create: "+dest);

            //in case of not accessible search, do not create dijsktra element
            return;
        }


        if(visited.containsKey(dest.getElement())){
            //   System.out.println("---"+ dest.getElement().getId()+" is visited");
            return;
        }





        //calculate new lenght
        double newLenght = lenght + source.currentWeight();


        if(newLenght < dest.currentWeight()){

            //relax him
            DijkstraElement newDelement = new DijkstraElement(toRelax, element, newLenght);
            pqueue.add(newDelement);
            elementHashMap.put(toRelax, newDelement);



            //   System.out.println("---new length better "+ newLenght + " update: "+ newDelement);
        }else{
            //   System.out.println("---new lenght not better  "+ newLenght + " than "+ dest.currentWeight());
        }



    }

    
    








}
