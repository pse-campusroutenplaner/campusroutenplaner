package campusroutenplaner.model.routing;

import campusroutenplaner.model.map.Vertex;

/**
 * Created by nmladenov on 7/9/16.
 */
public class DijkstraElement {


    private Vertex previous;
    private Vertex element;
    private double weight;
   
    
    
    /**
     * 
     * @param element
     * @param previous
     * @param weight
     */
    public DijkstraElement(Vertex element, Vertex previous, double weight){
        this.element = element;
        this.previous = previous;
        this.weight = weight;
    }

    /**
     * 
     * @param element
     * @param previous
     */
    public DijkstraElement(Vertex element, Vertex previous){
        this.element = element;
        this.previous = previous;
        this.weight = Double.MAX_VALUE;
    }


    public Vertex getPrevious() {
        return previous;
    }

   // public void setPrevious(Vertex previous) {
    //    this.previous = previous;
   // }

    @Override
    public String toString() {
        return "DijkstraElement{" +
                "elementId=" + element.getId() +
                ", previous= " + (previous==null? "null": previous.getId()) +

                ", weight=" + weight +
                '}';
    }

    public Vertex getElement() {
        return element;
    }



    public double currentWeight() {
        return weight;
    }

  //  public void setWeight(double weight) {
       // this.weight = weight;
   // }



}
