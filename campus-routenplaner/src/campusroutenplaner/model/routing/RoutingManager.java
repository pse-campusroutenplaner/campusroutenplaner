package campusroutenplaner.model.routing;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

import campusroutenplaner.Logger;
import campusroutenplaner.model.map.Edge;
import campusroutenplaner.model.map.Vertex;
import sun.awt.image.ImageWatched.Link;

public class RoutingManager {

	private Dijkstra dijkstra;

	/**
	 * Constructor
	 */
	public RoutingManager() {

	}

	/**
	 * It will proof if entranceBuilding or sourcebuilding equals null.it means , it was no found such building(respectly buildings)
	 * Then we say directly, that no route found , because if a building is equals null,it doesn't matter, if there is correct input for room, because room must be assign to building,
	 * roomnumber is not unique.Otherweise there is four cases,as the case may be, which list are empty.
	 * Then for every case it will called up dijkstra.The result from dijkstra must be only converted . 
	 * @param entrancesSourceBuilding
	 * @param sourceRoom
	 * @param entrancesDestinationBuilding
	 * @param destinationRoom
	 * @return LinkedList
	 */
	public LinkedList<Edge> routing(LinkedList<Vertex> entrancesSourceBuilding, LinkedList<Vertex> sourceRoom, LinkedList<Vertex> entrancesDestinationBuilding, LinkedList<Vertex> destinationRoom) {


		LinkedList<Edge> defaultRoute = new LinkedList<Edge>();

		if (entrancesSourceBuilding.isEmpty() || entrancesDestinationBuilding.isEmpty()) {
			return defaultRoute;
		}

		System.out.println("entranceSourceB: "+ entrancesSourceBuilding);
		System.out.println("sourceRoom: "+ sourceRoom);
		System.out.println("entrancesDestinationBuilding: "+ entrancesDestinationBuilding);
		System.out.println("destinationRoom: "+ destinationRoom);

		LinkedList<Vertex> resultDijkstra = new LinkedList<Vertex>();
		dijkstra = new Dijkstra();


		if (!sourceRoom.isEmpty()) {


			if (!destinationRoom.isEmpty()) {
				resultDijkstra = dijkstra.routing(sourceRoom, destinationRoom);

			} else {
				resultDijkstra = dijkstra.routing(sourceRoom, entrancesDestinationBuilding);

			}


		} else {


			if (destinationRoom.isEmpty()) {
				resultDijkstra = dijkstra.routing(entrancesSourceBuilding, entrancesDestinationBuilding);

			} else {
				resultDijkstra = dijkstra.routing(entrancesSourceBuilding, destinationRoom);

			}

		}

		dijkstra = null;
		Collections.reverse(resultDijkstra);
		return convertDijkstraResult(resultDijkstra);
	}
	
	

	
	

	public LinkedList<Edge> routingBike(LinkedList<Vertex> entrancesSourceBuilding, LinkedList<Vertex> sourceRoom, LinkedList<Vertex> entrancesDestinationBuilding, LinkedList<Vertex> destinationRoom) {


		LinkedList<Edge> defaultRoute = new LinkedList<Edge>();

		if (entrancesSourceBuilding.isEmpty() || entrancesDestinationBuilding.isEmpty()) {
			return defaultRoute;
		}
		
		
		if ((sourceRoom.isEmpty()) && (destinationRoom.isEmpty()) ) {
			
			
			LinkedList<Vertex> resultDijkstra = new LinkedList<Vertex>();
			dijkstra = new Dijkstra();

			resultDijkstra = dijkstra.routingBike(entrancesSourceBuilding, entrancesDestinationBuilding);


			dijkstra = null;
			Collections.reverse(resultDijkstra);
			return convertDijkstraResult(resultDijkstra);
			
			
		}

		System.out.println("entranceSourceB: "+ entrancesSourceBuilding);
		System.out.println("sourceRoom: "+ sourceRoom);
		System.out.println("entrancesDestinationBuilding: "+ entrancesDestinationBuilding);
		System.out.println("destinationRoom: "+ destinationRoom);

		
		return null;

	}
	
	

	
	
	/**
	 * This method is the same, like routing, the only difference is, that it will be called up dijkstra for accessible routes
	 * @param entrancesSourceBuilding
	 * @param sourceRoom
	 * @param entrancesDestinationBuilding
	 * @param destinationRoom
	 * @return LinkedList
	 */
	public LinkedList<Edge> routingAccessilbe(LinkedList<Vertex> entrancesSourceBuilding, LinkedList<Vertex> sourceRoom, LinkedList<Vertex> entrancesDestinationBuilding, LinkedList<Vertex> destinationRoom) {

		
		
		


		LinkedList<Edge> defaultRoute = new LinkedList<Edge>();


		if (entrancesSourceBuilding.isEmpty() || entrancesDestinationBuilding.isEmpty()) {
			return defaultRoute;
		}


		LinkedList<Vertex> resultDijkstra = new LinkedList<Vertex>();
		dijkstra = new Dijkstra();


		if (!sourceRoom.isEmpty()) {

			if (!destinationRoom.isEmpty()) {
				resultDijkstra = dijkstra.routingAccesible(sourceRoom, destinationRoom);

			} else {
				resultDijkstra = dijkstra.routingAccesible(sourceRoom, entrancesDestinationBuilding);

			}


		} else {


			if (destinationRoom.isEmpty()) {
				resultDijkstra = dijkstra.routingAccesible(entrancesSourceBuilding, entrancesDestinationBuilding);

			} else {
				resultDijkstra = dijkstra.routingAccesible(entrancesSourceBuilding, destinationRoom);

			}

		}

		dijkstra = null;
		Collections.reverse(resultDijkstra);
		return convertDijkstraResult(resultDijkstra);
	}


	/**
	 * It will proof if entranceBuilding ,stopOverBuilding or sourcebuilding equals null.it means , it was no found such building(respectly buildings)
	 * Then we say directly, that no route found , because if a building is equals null,it doesn't matter, if there is correct input for room, because room must be assign to building,
	 * roomnumber is not unique.Otherweise there is four cases,as the case may be, which list are empty.
	 * Then for every case it will called up dijkstra two times. one time from start (building or room) to stopOver(building or room) and then from stopOver to end(building or room).
	 * Then the result must be converted.
	 * @param entrancesSourceBuilding
	 * @param sourceRoom
	 * @param entrancesStopOverBuilding
	 * @param stopOverRoom
	 * @param entrancesDestinationBuilding
	 * @param destinationRoom
	 * @return LinkedList
	 */
	public LinkedList<Edge> routingWithStop(LinkedList<Vertex> entrancesSourceBuilding, LinkedList<Vertex> sourceRoom, LinkedList<Vertex> entrancesStopOverBuilding, LinkedList<Vertex> stopOverRoom, LinkedList<Vertex> entrancesDestinationBuilding, LinkedList<Vertex> destinationRoom) {

		if (entrancesSourceBuilding.isEmpty() || entrancesStopOverBuilding.isEmpty() || entrancesDestinationBuilding.isEmpty()) {
			return new LinkedList<Edge>();
		}
		
		dijkstra = new Dijkstra();
		LinkedList<Vertex> shortestRouteBetweenStartAndStopOVer = dijkstra.routing(entrancesSourceBuilding, entrancesStopOverBuilding);
		dijkstra = null;
		
		
		dijkstra = new Dijkstra();
		LinkedList<Vertex> shortestRouteBetweenStopOverAndDestination = dijkstra.routing(entrancesStopOverBuilding, entrancesDestinationBuilding);
		dijkstra = null;
		
		


//		if (!sourceRoom.isEmpty()) {
//			Logger.print("AUF JEDEN FALLLL");
//			if (!stopOverRoom.isEmpty()) {
//
//				if (!destinationRoom.isEmpty()) {
//					dijkstra =new Dijkstra();
//					shortestRouteBetweenStartAndStopOVer = dijkstra.routing(sourceRoom, stopOverRoom);
//                    if (shortestRouteBetweenStartAndStopOVer.isEmpty()) {
//                        return new LinkedList<>();
//                    }
//					dijkstra = null;
//					dijkstra = new Dijkstra();
//					shortestRouteBetweenStopOverAndDestination = dijkstra.routing(stopOverRoom, destinationRoom);
//
//                    if (shortestRouteBetweenStopOverAndDestination.isEmpty()) {
//                        return new LinkedList<>();
//                    }
//					dijkstra = null;
//				} else {
//					dijkstra =new Dijkstra();
//					shortestRouteBetweenStartAndStopOVer = dijkstra.routing(sourceRoom, stopOverRoom);
//
//                    if (shortestRouteBetweenStartAndStopOVer.isEmpty()) {
//                        return new LinkedList<>();
//                    }
//					dijkstra = null;
//					dijkstra =new Dijkstra();
//					shortestRouteBetweenStopOverAndDestination = dijkstra.routing(stopOverRoom, entrancesDestinationBuilding);
//                    if (shortestRouteBetweenStopOverAndDestination.isEmpty()) {
//                        return new LinkedList<>();
//                    }
//					dijkstra = null;
//				}
//
//			} else {
//				Logger.print("AUF JEDEN FALL 22222");
//				
//
//				
//				
//				if (destinationRoom.isEmpty()) {
//					
//					Logger.print("DEIN PLATZ");
//					dijkstra =new Dijkstra();
//					Logger.print("DU MUSSSSST  HIER ODERRRRRRRRRRRRRR R R R R R RR RR R  R R R R");
//					shortestRouteBetweenStartAndStopOVer = dijkstra.routing(entrancesSourceBuilding, entrancesStopOverBuilding);
//                    if (shortestRouteBetweenStartAndStopOVer.isEmpty()) {
//                        return new LinkedList<>();
//                    }
//					
//					dijkstra = null;
//					dijkstra =new Dijkstra();
//					shortestRouteBetweenStopOverAndDestination = dijkstra.routing(entrancesStopOverBuilding, entrancesDestinationBuilding);
//                    if (shortestRouteBetweenStopOverAndDestination.isEmpty()) {
//                        return new LinkedList<>();
//                    }
//					dijkstra = null;
//				} else {
//					dijkstra =new Dijkstra();
//					shortestRouteBetweenStartAndStopOVer = dijkstra.routing(entrancesSourceBuilding, entrancesStopOverBuilding);
//                    if (shortestRouteBetweenStartAndStopOVer.isEmpty()) {
//                        return new LinkedList<>();
//                    }
//
//                    dijkstra = null;
//					dijkstra =new Dijkstra();
//					shortestRouteBetweenStopOverAndDestination = dijkstra.routing(entrancesStopOverBuilding, destinationRoom);
//                    if (shortestRouteBetweenStopOverAndDestination.isEmpty()) {
//                        return new LinkedList<>();
//                    }
//					dijkstra = null;
//				}
//
//			}
//		}
//
//
//		if (stopOverRoom.isEmpty()) {
//
//			if (destinationRoom.isEmpty()) {
//				dijkstra =new Dijkstra();
//				shortestRouteBetweenStartAndStopOVer = dijkstra.routing(entrancesSourceBuilding, entrancesStopOverBuilding);
//                if (shortestRouteBetweenStartAndStopOVer.isEmpty()) {
//                    return new LinkedList<>();
//                }
//				dijkstra = null;
//				dijkstra =new Dijkstra();
//				shortestRouteBetweenStopOverAndDestination = dijkstra.routing(entrancesStopOverBuilding, entrancesDestinationBuilding);
//                if (shortestRouteBetweenStopOverAndDestination.isEmpty()) {
//                    return new LinkedList<>();
//                }
//				dijkstra = null;
//			} else {
//				dijkstra =new Dijkstra();
//				shortestRouteBetweenStartAndStopOVer = dijkstra.routing(entrancesSourceBuilding, entrancesStopOverBuilding);
//                if (shortestRouteBetweenStartAndStopOVer.isEmpty()) {
//                    return new LinkedList<>();
//                }
//		
//				dijkstra = null;
//				dijkstra =new Dijkstra();
//				shortestRouteBetweenStopOverAndDestination = dijkstra.routing(entrancesStopOverBuilding, destinationRoom);
//                if (shortestRouteBetweenStopOverAndDestination.isEmpty()) {
//                    return new LinkedList<>();
//                }
//				dijkstra = null;
//			}
//
//		} else {
//			if (destinationRoom.isEmpty()) {
//				dijkstra =new Dijkstra();
//				shortestRouteBetweenStartAndStopOVer = dijkstra.routing(entrancesSourceBuilding, stopOverRoom);
//                if (shortestRouteBetweenStartAndStopOVer.isEmpty()) {
//                    return new LinkedList<>();
//                }
//				dijkstra = null;
//				dijkstra =new Dijkstra();
//				shortestRouteBetweenStopOverAndDestination = dijkstra.routing(stopOverRoom, entrancesDestinationBuilding);
//                if (shortestRouteBetweenStopOverAndDestination.isEmpty()) {
//                    return new LinkedList<>();
//                }
//				dijkstra = null;
//			} else {
//				dijkstra =new Dijkstra();
//				shortestRouteBetweenStartAndStopOVer = dijkstra.routing(entrancesSourceBuilding, stopOverRoom);
//                if (shortestRouteBetweenStartAndStopOVer.isEmpty()) {
//                    return new LinkedList<>();
//                }
//				
//				dijkstra = null;
//				dijkstra =new Dijkstra();
//				shortestRouteBetweenStopOverAndDestination = dijkstra.routing(stopOverRoom, destinationRoom);
//                if (shortestRouteBetweenStopOverAndDestination.isEmpty()) {
//                    return new LinkedList<>();
//                }
//				dijkstra = null;
//			}
//		}
		
		
		LinkedList<Vertex> pathBetweenSourceBuildingAndSourceRoom = new LinkedList<Vertex>();
		
		LinkedList<Vertex> pathBetweenStopOVerRoomAndStopOVerBuilding = new LinkedList<Vertex>();
		LinkedList<Vertex> pathBetweenStopOverBuildingAndStopOverRoom = new LinkedList<Vertex>();

		LinkedList<Vertex> pathBetweenDestinationRoomAndDestBuilding  =  new LinkedList<Vertex>();
		
		
		LinkedList<Vertex> result = new LinkedList<Vertex>();
		
		
		
		if ((!entrancesSourceBuilding.isEmpty()) && (!sourceRoom.isEmpty()) ) {
			dijkstra = new Dijkstra();
		
		    pathBetweenSourceBuildingAndSourceRoom = dijkstra.routing(sourceRoom, entrancesSourceBuilding);
		    
            if (pathBetweenSourceBuildingAndSourceRoom.isEmpty()) {
    			dijkstra = null;
                return new LinkedList<Edge>();
            }
			dijkstra = null;

		}
		
		
		if (  (!entrancesStopOverBuilding.isEmpty()) && (!stopOverRoom.isEmpty()) ) {
			dijkstra = new Dijkstra();
			pathBetweenStopOverBuildingAndStopOverRoom = dijkstra.routing(entrancesStopOverBuilding, stopOverRoom);
			dijkstra = null;
			
			dijkstra = new Dijkstra();
			pathBetweenStopOVerRoomAndStopOVerBuilding = dijkstra.routing( stopOverRoom, entrancesStopOverBuilding);
            if (pathBetweenStopOVerRoomAndStopOVerBuilding.isEmpty()) {
                return new LinkedList<Edge>();
            }
			dijkstra = null;
		}
		
		
		
		if ( (!entrancesDestinationBuilding.isEmpty()) && (!destinationRoom.isEmpty()) ) {
			dijkstra = new Dijkstra();
			pathBetweenDestinationRoomAndDestBuilding = dijkstra.routing(destinationRoom, entrancesDestinationBuilding);
			
            if (pathBetweenDestinationRoomAndDestBuilding.isEmpty()) {
                return new LinkedList<Edge>();
            }
			dijkstra = null;
		}
		
		
		LinkedList<Edge> finalResult = new LinkedList<Edge>();

		
		if ( (!pathBetweenSourceBuildingAndSourceRoom.isEmpty())  ) {
			
	
			if (!pathBetweenStopOVerRoomAndStopOVerBuilding.isEmpty()) {
				
				if ( !pathBetweenDestinationRoomAndDestBuilding.isEmpty() ) {
					
					

					
					Collections.reverse(pathBetweenSourceBuildingAndSourceRoom);
					finalResult.addAll(convertDijkstraResult(pathBetweenSourceBuildingAndSourceRoom));
					

					Collections.reverse(shortestRouteBetweenStartAndStopOVer);
					finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStartAndStopOVer));
					

					
					Collections.reverse(pathBetweenStopOVerRoomAndStopOVerBuilding);
					finalResult.addAll(convertDijkstraResult(pathBetweenStopOVerRoomAndStopOVerBuilding));

					
					Collections.reverse(shortestRouteBetweenStopOverAndDestination);
					finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStopOverAndDestination));
					//Collections.reverse(pathBetweenDestinationRoomAndDestBuilding);

					finalResult.addAll(convertDijkstraResult(pathBetweenDestinationRoomAndDestBuilding));					

					
//					for (Edge edge : finalResult) {
//						
//						System.out.println("finalResult     " + edge);
//
//					}
					
					// CHECKED
					return finalResult;

				} else {
					
					
					
//					
//					result.addAll(shortestRouteBetweenStopOverAndDestination);
//					
//					result.addAll(pathBetweenStopOVerRoomAndStopOVerBuilding);
//					
//					result.addAll(pathBetweenStopOverBuildingAndStopOverRoom);
//					
//					//shortestRouteBetweenStartAndStopOVer.removeFirst();
//					
//					result.addAll(shortestRouteBetweenStartAndStopOVer);
//					//pathBetweenSourceBuildingAndSourceRoom.removeFirst();
//					result.addAll(pathBetweenSourceBuildingAndSourceRoom);
					
					Collections.reverse(pathBetweenSourceBuildingAndSourceRoom);
					finalResult.addAll(convertDijkstraResult(pathBetweenSourceBuildingAndSourceRoom));
					
					
					Collections.reverse(shortestRouteBetweenStartAndStopOVer);
					finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStartAndStopOVer));
					
					Collections.reverse(pathBetweenStopOverBuildingAndStopOverRoom);
					
					finalResult.addAll(convertDijkstraResult(pathBetweenStopOverBuildingAndStopOverRoom));

					Collections.reverse(pathBetweenStopOVerRoomAndStopOVerBuilding);
					
					finalResult.addAll(convertDijkstraResult(pathBetweenStopOVerRoomAndStopOVerBuilding));

					Collections.reverse(shortestRouteBetweenStopOverAndDestination);

					
					finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStopOverAndDestination));
			
					//FUNKTIONIERT PERFEKT
					return finalResult;
				}
	
			} else {
				
				if (!pathBetweenDestinationRoomAndDestBuilding.isEmpty()) {
					
										
				
					
//					result.addAll(pathBetweenDestinationRoomAndDestBuilding);
//					
//					result.addAll(shortestRouteBetweenStopOverAndDestination);
//					
//					result.addAll(shortestRouteBetweenStartAndStopOVer);
//
//					result.addAll(pathBetweenSourceBuildingAndSourceRoom);
					
					
					
					
					
					
					Collections.reverse(pathBetweenSourceBuildingAndSourceRoom);
					finalResult.addAll(convertDijkstraResult(pathBetweenSourceBuildingAndSourceRoom));
					

					
					
					Collections.reverse(shortestRouteBetweenStartAndStopOVer);
					finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStartAndStopOVer));


					Collections.reverse(shortestRouteBetweenStopOverAndDestination);
					finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStopOverAndDestination));


					//Collections.reverse(pathBetweenDestinationRoomAndDestBuilding);
					finalResult.addAll(convertDijkstraResult(pathBetweenDestinationRoomAndDestBuilding));

	
					

//					for (Edge edge : finalResult) {
//						
//						System.out.println("new Edge     " + edge);
//
//					}
					
					
					//CHECKED 
					return finalResult;
					
				} else {
					
					
					
					
//					
//					result.addAll(shortestRouteBetweenStopOverAndDestination);
//					//shortestRouteBetweenStartAndStopOVer.removeFirst();
//					
//					result.addAll(shortestRouteBetweenStartAndStopOVer);
//					//pathBetweenSourceRoomAndSourceBuilding.removeFirst();
//					
//					//pathBetweenSourceBuildingAndSourceRoom.removeFirst();
//					result.addAll(pathBetweenSourceBuildingAndSourceRoom);
//					
			
					Collections.reverse(pathBetweenSourceBuildingAndSourceRoom);
					finalResult.addAll(convertDijkstraResult(pathBetweenSourceBuildingAndSourceRoom));
					
					
					Collections.reverse(shortestRouteBetweenStartAndStopOVer);
					finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStartAndStopOVer));

					Collections.reverse(shortestRouteBetweenStopOverAndDestination);
					finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStopOverAndDestination));

					
					
//					
//					for (Edge edge : finalResult) {
//						
//						System.out.println("new hallo     " + edge);
//
//					}
					
					//CHECKED
					return finalResult;

					
				}
				
			}
			
		}
		
		
		if ( !pathBetweenStopOVerRoomAndStopOVerBuilding.isEmpty() ) {
			
			if (!pathBetweenDestinationRoomAndDestBuilding.isEmpty()) {
				
				
							
//				
//				result.addAll(pathBetweenDestinationRoomAndDestBuilding);
//
//				
//				
//				result.addAll(pathBetweenDestinationRoomAndDestBuilding);
//				
//	
//				
//				
//				result.addAll(shortestRouteBetweenStopOverAndDestination);
//				
//
//				
//				
//				
//				//pathBetweenStopOverBuildingAndStopOverRoom.removeFirst();			
//				
//				result.addAll(pathBetweenStopOVerRoomAndStopOVerBuilding);
//		
//
//				result.addAll(pathBetweenStopOverBuildingAndStopOverRoom);
//				
//
//				
//				
//			//	shortestRouteBetweenStartAndStopOVer.removeFirst();
//				
//				result.addAll(shortestRouteBetweenStartAndStopOVer);
//				
				
				
				
				
				Collections.reverse(shortestRouteBetweenStartAndStopOVer);
				finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStartAndStopOVer));
				
			
				
				Collections.reverse(pathBetweenStopOverBuildingAndStopOverRoom);
				finalResult.addAll(convertDijkstraResult(pathBetweenStopOverBuildingAndStopOverRoom));

	
				
				Collections.reverse(shortestRouteBetweenStopOverAndDestination);
				finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStopOverAndDestination));

				
		
				
//				Collections.reverse(pathBetweenDestinationRoomAndDestBuilding);
				finalResult.addAll(convertDijkstraResult(pathBetweenDestinationRoomAndDestBuilding));

				

				
//				for (Edge edge : finalResult) {
//					
//					System.out.println("new Edge     " + edge);
//
//				}
				
				
				//CHECKED
				return finalResult;
				
			} else {
				
								
				
				
//				result.addAll(shortestRouteBetweenStopOverAndDestination);
////				Collections.reverse(shortestRouteBetweenStopOverAndDestination);
////				result.addAll(shortestRouteBetweenStopOverAndDestination);
//				
//				
//				result.addAll(pathBetweenStopOVerRoomAndStopOVerBuilding);
//				
//				
//				result.addAll(pathBetweenStopOverBuildingAndStopOverRoom);
//				
//				
//				result.addAll(shortestRouteBetweenStartAndStopOVer);
				
				
				
				
				Collections.reverse(shortestRouteBetweenStartAndStopOVer);
				finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStartAndStopOVer));
				
				Collections.reverse(pathBetweenStopOverBuildingAndStopOverRoom);
				finalResult.addAll(convertDijkstraResult(pathBetweenStopOverBuildingAndStopOverRoom));
				
				
				Collections.reverse(pathBetweenStopOVerRoomAndStopOVerBuilding);
				finalResult.addAll(convertDijkstraResult(pathBetweenStopOVerRoomAndStopOVerBuilding));
				
				
				Collections.reverse(shortestRouteBetweenStopOverAndDestination);
				finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStopOverAndDestination));

				
//				for (Edge edge : finalResult) {
//					
//					System.out.println(" Edge     " + edge);
//
//				}
				
				//FUNKTIONIERT PERFEKT
				return finalResult;
				
			}
			
			
		} else {
			
			
			
						
			if (!pathBetweenDestinationRoomAndDestBuilding.isEmpty()) {
				
		
//				result.addAll(pathBetweenDestinationRoomAndDestBuilding);
//				//shortestRouteBetweenStopOverAndDestination.removeFirst();
//				Collections.reverse(pathBetweenDestinationRoomAndDestBuilding);
//
//				result.addAll(pathBetweenDestinationRoomAndDestBuilding);
//				
//				result.addAll(shortestRouteBetweenStopOverAndDestination);
//			//	shortestRouteBetweenStartAndStopOVer.removeFirst();
//				
//				result.addAll(shortestRouteBetweenStartAndStopOVer);
//				
//				
				
				Collections.reverse(shortestRouteBetweenStartAndStopOVer);
				finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStartAndStopOVer));
				
				Collections.reverse(shortestRouteBetweenStopOverAndDestination);
				finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStopOverAndDestination));

				
				finalResult.addAll(convertDijkstraResult(pathBetweenDestinationRoomAndDestBuilding));


//				for (Edge edge : finalResult) {
//					
//					System.out.println("new Edge     " + edge);
//
//				}
				
				
				//CHECKED
				
				return finalResult;

			} else { 

				
				Collections.reverse(shortestRouteBetweenStartAndStopOVer);
				finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStartAndStopOVer));
				
				Collections.reverse(shortestRouteBetweenStopOverAndDestination);
				finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStopOverAndDestination));

//				for (Edge edge : finalResult) {
//					
//					System.out.println("new Edge     " + edge);
//
//				}
				
				//FUNKTIONIERT PERFEKT
				return finalResult;
				
			}
		}
	
	}


	/**
	 * This method is the same, like routingWithStop, the only difference is, that it will be called up dijkstra for accessible routes
	 * @param entrancesSourceBuilding
	 * @param sourceRoom
	 * @param entrancesStopOverBuilding
	 * @param stopOverRoom
	 * @param entrancesDestinationBuilding
	 * @param destinationRoom
	 * @return LinkedList
	 */
	public LinkedList<Edge> routingWithStopAccessible(LinkedList<Vertex> entrancesSourceBuilding, LinkedList<Vertex> sourceRoom, LinkedList<Vertex> entrancesStopOverBuilding, LinkedList<Vertex> stopOverRoom, LinkedList<Vertex> entrancesDestinationBuilding, LinkedList<Vertex> destinationRoom) {


		if (entrancesSourceBuilding.isEmpty() || entrancesStopOverBuilding.isEmpty() || entrancesDestinationBuilding.isEmpty()) {
			return new LinkedList<Edge>();
		}

		dijkstra = new Dijkstra();
		LinkedList<Vertex> shortestRouteBetweenStartAndStopOVer = dijkstra.routingAccesible(entrancesSourceBuilding, entrancesStopOverBuilding);
		dijkstra = null;
		
		dijkstra = new Dijkstra();
		LinkedList<Vertex> shortestRouteBetweenStopOverAndDestination = dijkstra.routingAccesible(entrancesStopOverBuilding, entrancesDestinationBuilding);
		dijkstra = null;



		
		
		
		LinkedList<Vertex> pathBetweenSourceBuildingAndSourceRoom = new LinkedList<Vertex>();
		
		LinkedList<Vertex> pathBetweenStopOVerRoomAndStopOVerBuilding = new LinkedList<Vertex>();
		LinkedList<Vertex> pathBetweenStopOverBuildingAndStopOverRoom = new LinkedList<Vertex>();

		LinkedList<Vertex> pathBetweenDestinationRoomAndDestBuilding  =  new LinkedList<Vertex>();
		
		
		LinkedList<Vertex> result = new LinkedList<Vertex>();
		
		
		
		if ((!entrancesSourceBuilding.isEmpty()) && (!sourceRoom.isEmpty()) ) {
			dijkstra = new Dijkstra();
		    pathBetweenSourceBuildingAndSourceRoom = dijkstra.routingAccesible(sourceRoom, entrancesSourceBuilding);
		  
		    
            if (pathBetweenSourceBuildingAndSourceRoom.isEmpty()) {
                return new LinkedList<>();
            }
			dijkstra = null;
		}
		
		
		if (  (!entrancesStopOverBuilding.isEmpty()) && (!stopOverRoom.isEmpty()) ) {
			dijkstra = new Dijkstra();
			pathBetweenStopOverBuildingAndStopOverRoom = dijkstra.routingAccesible(entrancesStopOverBuilding, stopOverRoom);
			dijkstra = null;
			
			dijkstra = new Dijkstra();
			pathBetweenStopOVerRoomAndStopOVerBuilding = dijkstra.routingAccesible( stopOverRoom, entrancesStopOverBuilding);
            if (pathBetweenStopOVerRoomAndStopOVerBuilding.isEmpty()) {
                return new LinkedList<>();
            }
			dijkstra = null;
		}
		
		if ( (!entrancesDestinationBuilding.isEmpty()) && (!destinationRoom.isEmpty()) ) {
			dijkstra = new Dijkstra();
			pathBetweenDestinationRoomAndDestBuilding = dijkstra.routingAccesible(destinationRoom, entrancesDestinationBuilding);
            if (pathBetweenDestinationRoomAndDestBuilding.isEmpty()) {
                return new LinkedList<>();
            }
			dijkstra = null;
		}
		
		
		LinkedList<Edge> finalResult = new LinkedList<Edge>();
		
		if ( (!pathBetweenSourceBuildingAndSourceRoom.isEmpty())  ) {
			
			
			if (!pathBetweenStopOVerRoomAndStopOVerBuilding.isEmpty()) {
				
				if ( !pathBetweenDestinationRoomAndDestBuilding.isEmpty() ) {
					
					

					
					Collections.reverse(pathBetweenSourceBuildingAndSourceRoom);
					finalResult.addAll(convertDijkstraResult(pathBetweenSourceBuildingAndSourceRoom));
					

					Collections.reverse(shortestRouteBetweenStartAndStopOVer);
					finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStartAndStopOVer));
					

					
					Collections.reverse(pathBetweenStopOVerRoomAndStopOVerBuilding);
					finalResult.addAll(convertDijkstraResult(pathBetweenStopOVerRoomAndStopOVerBuilding));

					
					Collections.reverse(shortestRouteBetweenStopOverAndDestination);
					finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStopOverAndDestination));
					//Collections.reverse(pathBetweenDestinationRoomAndDestBuilding);

					finalResult.addAll(convertDijkstraResult(pathBetweenDestinationRoomAndDestBuilding));					

					
//					for (Edge edge : finalResult) {
//						
//						System.out.println("finalResult     " + edge);
//
//					}
					
					// CHECKED
					return finalResult;

				} else {
					
					
					
//					
//					result.addAll(shortestRouteBetweenStopOverAndDestination);
//					
//					result.addAll(pathBetweenStopOVerRoomAndStopOVerBuilding);
//					
//					result.addAll(pathBetweenStopOverBuildingAndStopOverRoom);
//					
//					//shortestRouteBetweenStartAndStopOVer.removeFirst();
//					
//					result.addAll(shortestRouteBetweenStartAndStopOVer);
//					//pathBetweenSourceBuildingAndSourceRoom.removeFirst();
//					result.addAll(pathBetweenSourceBuildingAndSourceRoom);
					
					Collections.reverse(pathBetweenSourceBuildingAndSourceRoom);
					finalResult.addAll(convertDijkstraResult(pathBetweenSourceBuildingAndSourceRoom));
					
					
					Collections.reverse(shortestRouteBetweenStartAndStopOVer);
					finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStartAndStopOVer));
					
					Collections.reverse(pathBetweenStopOverBuildingAndStopOverRoom);
					
					finalResult.addAll(convertDijkstraResult(pathBetweenStopOverBuildingAndStopOverRoom));

					Collections.reverse(pathBetweenStopOVerRoomAndStopOVerBuilding);
					
					finalResult.addAll(convertDijkstraResult(pathBetweenStopOVerRoomAndStopOVerBuilding));

					Collections.reverse(shortestRouteBetweenStopOverAndDestination);

					
					finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStopOverAndDestination));
			
					//FUNKTIONIERT PERFEKT
					return finalResult;
				}
	
			} else {
				
				if (!pathBetweenDestinationRoomAndDestBuilding.isEmpty()) {
					
										
				
					
//					result.addAll(pathBetweenDestinationRoomAndDestBuilding);
//					
//					result.addAll(shortestRouteBetweenStopOverAndDestination);
//					
//					result.addAll(shortestRouteBetweenStartAndStopOVer);
//
//					result.addAll(pathBetweenSourceBuildingAndSourceRoom);
					
					
					
					
					
					
					Collections.reverse(pathBetweenSourceBuildingAndSourceRoom);
					finalResult.addAll(convertDijkstraResult(pathBetweenSourceBuildingAndSourceRoom));
					

					
					
					Collections.reverse(shortestRouteBetweenStartAndStopOVer);
					finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStartAndStopOVer));


					Collections.reverse(shortestRouteBetweenStopOverAndDestination);
					finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStopOverAndDestination));


					//Collections.reverse(pathBetweenDestinationRoomAndDestBuilding);
					finalResult.addAll(convertDijkstraResult(pathBetweenDestinationRoomAndDestBuilding));

	
					

//					for (Edge edge : finalResult) {
//						
//						System.out.println("new Edge     " + edge);
//
//					}
					
					
					//CHECKED 
					return finalResult;
					
				} else {
					
					
					
					
//					
//					result.addAll(shortestRouteBetweenStopOverAndDestination);
//					//shortestRouteBetweenStartAndStopOVer.removeFirst();
//					
//					result.addAll(shortestRouteBetweenStartAndStopOVer);
//					//pathBetweenSourceRoomAndSourceBuilding.removeFirst();
//					
//					//pathBetweenSourceBuildingAndSourceRoom.removeFirst();
//					result.addAll(pathBetweenSourceBuildingAndSourceRoom);
//					
			
					Collections.reverse(pathBetweenSourceBuildingAndSourceRoom);
					finalResult.addAll(convertDijkstraResult(pathBetweenSourceBuildingAndSourceRoom));
					
					
					Collections.reverse(shortestRouteBetweenStartAndStopOVer);
					finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStartAndStopOVer));

					Collections.reverse(shortestRouteBetweenStopOverAndDestination);
					finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStopOverAndDestination));

					
					
//					
//					for (Edge edge : finalResult) {
//						
//						System.out.println("new hallo     " + edge);
//
//					}
					
					//CHECKED
					return finalResult;

					
				}
				
			}
			
		}
		
		
		if ( !pathBetweenStopOVerRoomAndStopOVerBuilding.isEmpty() ) {
			
			if (!pathBetweenDestinationRoomAndDestBuilding.isEmpty()) {
				
				
							
//				
//				result.addAll(pathBetweenDestinationRoomAndDestBuilding);
//
//				
//				
//				result.addAll(pathBetweenDestinationRoomAndDestBuilding);
//				
//	
//				
//				
//				result.addAll(shortestRouteBetweenStopOverAndDestination);
//				
//
//				
//				
//				
//				//pathBetweenStopOverBuildingAndStopOverRoom.removeFirst();			
//				
//				result.addAll(pathBetweenStopOVerRoomAndStopOVerBuilding);
//		
//
//				result.addAll(pathBetweenStopOverBuildingAndStopOverRoom);
//				
//
//				
//				
//			//	shortestRouteBetweenStartAndStopOVer.removeFirst();
//				
//				result.addAll(shortestRouteBetweenStartAndStopOVer);
//				
				
				
				
				
				Collections.reverse(shortestRouteBetweenStartAndStopOVer);
				finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStartAndStopOVer));
				
			
				
				Collections.reverse(pathBetweenStopOverBuildingAndStopOverRoom);
				finalResult.addAll(convertDijkstraResult(pathBetweenStopOverBuildingAndStopOverRoom));

	
				
				Collections.reverse(shortestRouteBetweenStopOverAndDestination);
				finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStopOverAndDestination));

				
		
				
//				Collections.reverse(pathBetweenDestinationRoomAndDestBuilding);
				finalResult.addAll(convertDijkstraResult(pathBetweenDestinationRoomAndDestBuilding));

				

				
//				for (Edge edge : finalResult) {
//					
//					System.out.println("new Edge     " + edge);
//
//				}
				
				
				//CHECKED
				return finalResult;
				
			} else {
				
								
				
				
//				result.addAll(shortestRouteBetweenStopOverAndDestination);
////				Collections.reverse(shortestRouteBetweenStopOverAndDestination);
////				result.addAll(shortestRouteBetweenStopOverAndDestination);
//				
//				
//				result.addAll(pathBetweenStopOVerRoomAndStopOVerBuilding);
//				
//				
//				result.addAll(pathBetweenStopOverBuildingAndStopOverRoom);
//				
//				
//				result.addAll(shortestRouteBetweenStartAndStopOVer);
				
				
				
				
				Collections.reverse(shortestRouteBetweenStartAndStopOVer);
				finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStartAndStopOVer));
				
				Collections.reverse(pathBetweenStopOverBuildingAndStopOverRoom);
				finalResult.addAll(convertDijkstraResult(pathBetweenStopOverBuildingAndStopOverRoom));
				
				
				Collections.reverse(pathBetweenStopOVerRoomAndStopOVerBuilding);
				finalResult.addAll(convertDijkstraResult(pathBetweenStopOVerRoomAndStopOVerBuilding));
				
				
				Collections.reverse(shortestRouteBetweenStopOverAndDestination);
				finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStopOverAndDestination));

				
//				for (Edge edge : finalResult) {
//					
//					System.out.println(" Edge     " + edge);
//
//				}
				
				//FUNKTIONIERT PERFEKT
				return finalResult;
				
			}
			
			
		} else {
			
			
			
						
			if (!pathBetweenDestinationRoomAndDestBuilding.isEmpty()) {
				
		
//				result.addAll(pathBetweenDestinationRoomAndDestBuilding);
//				//shortestRouteBetweenStopOverAndDestination.removeFirst();
//				Collections.reverse(pathBetweenDestinationRoomAndDestBuilding);
//
//				result.addAll(pathBetweenDestinationRoomAndDestBuilding);
//				
//				result.addAll(shortestRouteBetweenStopOverAndDestination);
//			//	shortestRouteBetweenStartAndStopOVer.removeFirst();
//				
//				result.addAll(shortestRouteBetweenStartAndStopOVer);
//				
//				
				
				Collections.reverse(shortestRouteBetweenStartAndStopOVer);
				finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStartAndStopOVer));
				
				Collections.reverse(shortestRouteBetweenStopOverAndDestination);
				finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStopOverAndDestination));

				
				finalResult.addAll(convertDijkstraResult(pathBetweenDestinationRoomAndDestBuilding));


//				for (Edge edge : finalResult) {
//					
//					System.out.println("new Edge     " + edge);
//
//				}
				
				
				//CHECKED
				
				return finalResult;

			} else { 

				
				Collections.reverse(shortestRouteBetweenStartAndStopOVer);
				finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStartAndStopOVer));
				
				Collections.reverse(shortestRouteBetweenStopOverAndDestination);
				finalResult.addAll(convertDijkstraResult(shortestRouteBetweenStopOverAndDestination));

//				for (Edge edge : finalResult) {
//					
//					System.out.println("new Edge     " + edge);
//
//				}
				
				//FUNKTIONIERT PERFEKT
				return finalResult;
				
			}
		}
		
		
		
		
	}
	
	
	
	public LinkedList<Edge> simpleRouting(LinkedList<Vertex> sourceBuildingEntrances, LinkedList<Vertex> sourceRoom) {
		
		
		if (sourceBuildingEntrances.isEmpty()) {
			return null;
		}
		
		if (sourceRoom == null) {
			return null;
		}
		Dijkstra dijkstra = new Dijkstra();
		LinkedList<Vertex> dijkstraResult = new LinkedList<Vertex>();
		dijkstraResult = dijkstra.routing(sourceBuildingEntrances, sourceRoom);
		dijkstra = null;
		Collections.reverse(dijkstraResult);
		return convertDijkstraResult(dijkstraResult);
			
	}
	
	
	
	
public LinkedList<Edge> simpleRoutingAccessible(LinkedList<Vertex> sourceBuildingEntrances, LinkedList<Vertex> sourceRoom) {
		
		if (sourceBuildingEntrances.isEmpty()) {
			return null;
		}
		
		if (sourceRoom == null) {
			return null;
		}
		Dijkstra dijkstra = new Dijkstra();
		LinkedList<Vertex> dijkstraResult = new LinkedList<Vertex>();
		dijkstraResult = dijkstra.routingAccesible(sourceBuildingEntrances, sourceRoom);
		dijkstra = null;
		Collections.reverse(dijkstraResult);
		return convertDijkstraResult(dijkstraResult);
			
	}
	
	




	
	
	/**
	 * this method receives the result list from dijkstra. This list contains only vertices and this vertices are in reverse order.
	 * Therefore reverse the result list from dijkstra and then for every vertex and his next(if next unequals null) take the edge and add in list, which returned.
	 * @param dijkstraResult
	 * @return LinkedList
	 */
	public LinkedList<Edge> convertDijkstraResult(LinkedList<Vertex> dijkstraResult) {

		if(dijkstraResult.isEmpty()){
			return  new LinkedList<Edge>();
		}


		LinkedList<Edge> toReturn = new LinkedList<Edge>();

		Iterator<Vertex> it = dijkstraResult.iterator();

		Vertex prev;
		Vertex curr = it.next();


		while ( it.hasNext() ) {
			
			prev = curr;
			curr = it.next();

            Edge theEdge = prev.getEdgeBetween(curr);
            toReturn.add(theEdge);

		}

		return toReturn;
	}
	
	
	





}
