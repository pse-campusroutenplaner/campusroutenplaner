package campusroutenplaner.model.routing;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import campusroutenplaner.controller.main.MapController;
import campusroutenplaner.model.database.Database;
import campusroutenplaner.model.map.Building;
import campusroutenplaner.model.map.CampusMap;
import campusroutenplaner.model.map.Edge;
import campusroutenplaner.model.map.MapManager;
import campusroutenplaner.model.map.Vertex;

public class RoutingManagerTest {

	private RoutingManager routingMan;
	private LinkedList<Vertex> defaultList, defaultListTwo;
	private LinkedList<Vertex> legitimList, legitimList2;
	LinkedList<Edge> resultNull;
	private LinkedList<Edge> defaultResult, defaultResultTwo;
	private Vertex vertex, vertexOne, vertexTwo, defaultVertex;
	private Edge edge;
	private Database data;
	
	
	
	
	
	private MapController mapController;
    private MapManager mapManager;
    private CampusMap campus;
    private Dijkstra dijkstra;

	
	
	
	
	@Before
	public void before () {
		
		data = new Database("test/model/KIT(testCopy).db");
		
		campus = new CampusMap(data);
		
		mapManager = new MapManager(campus, data);
		
		mapController = new MapController(mapManager);
		
		dijkstra  = new Dijkstra();
		
		
		
		resultNull = new LinkedList<Edge>();
		resultNull.add(edge);
		
		
		

		routingMan = new RoutingManager();
		defaultList = new LinkedList<Vertex>();
		defaultListTwo = new LinkedList<Vertex>();
		defaultResult = new LinkedList<Edge>();
		defaultResultTwo = new LinkedList<Edge>();
		defaultResultTwo.add(edge);
		vertex = new Vertex(0, 0, 1, 0);
		defaultListTwo.add(vertex);
		
		legitimList = new LinkedList<Vertex>();
		legitimList2 = new LinkedList<Vertex>();

	}
	
	
	@Test
	public void routing() {

	
		assertEquals(defaultResult, routingMan.routing(defaultList, defaultList, defaultList, defaultList));
		assertEquals(defaultResult, routingMan.routing(defaultList, defaultList, defaultListTwo, defaultList));

		assertEquals(defaultResult, routingMan.routing(defaultListTwo, defaultList, defaultListTwo, defaultList));

		assertEquals(defaultResult, routingMan.routing(defaultListTwo, defaultList, defaultList, defaultList));
		assertEquals(defaultResult, routingMan.routing(defaultListTwo, defaultListTwo, defaultListTwo, defaultListTwo));
		assertEquals(defaultResult, routingMan.routing(defaultListTwo, defaultListTwo, defaultListTwo, defaultList));
		assertEquals(defaultResult, routingMan.routing(defaultListTwo, defaultList, defaultListTwo, defaultListTwo));
		

		
		
		
		
	}
	
	
	@Test
	public void routingAccessible() {
		assertEquals(defaultResult, routingMan.routingAccessilbe(defaultList, defaultList, defaultList, defaultList));
		assertEquals(defaultResult, routingMan.routingAccessilbe(defaultList, defaultList, defaultListTwo, defaultList));

		assertEquals(defaultResult, routingMan.routingAccessilbe(defaultListTwo, defaultList, defaultListTwo, defaultList));

		assertEquals(defaultResult, routingMan.routingAccessilbe(defaultListTwo, defaultList, defaultList, defaultList));
		assertEquals(defaultResult, routingMan.routingAccessilbe(defaultListTwo, defaultListTwo, defaultListTwo, defaultListTwo));
		assertEquals(defaultResult, routingMan.routingAccessilbe(defaultListTwo, defaultListTwo, defaultListTwo, defaultList));
		assertEquals(defaultResult, routingMan.routingAccessilbe(defaultListTwo, defaultList, defaultListTwo, defaultListTwo));
		
	}
	
	
	@Test
	public void routingWithStop() {
		
		mapManager.loadDatabase();
		
		campus.addBuilding("50.50");
		
		Building building = new Building("50.50");
		
		
		Vertex entranceOne = mapManager.addVertex(0, 0, 0);
		Vertex entranceTwo = mapManager.addVertex(0, 1, 1);
		Vertex room = mapManager.addVertex(0, 2, 2);
		Vertex roomTwo = mapManager.addVertex(0, 3, 3);

		
		
		
		
		
		Edge edgeTwo = mapManager.addEdge(entranceOne.getMapId(), entranceOne.getId(), entranceTwo.getMapId(), entranceTwo.getId());
		Edge edgeThree = mapManager.addEdge(entranceTwo.getMapId(), entranceTwo.getId(), room.getMapId(), room.getId());
		Edge edge4 = mapManager.addEdge(entranceOne.getMapId(), entranceOne.getId(), room.getMapId(), room.getId());


		campus.assignVertexToBuildingEntrances(entranceOne, building);
		campus.assignVertexToBuildingEntrances(entranceTwo, building);

		

		
		LinkedList<Vertex> entrance = new LinkedList<Vertex>();
		
		entrance.add(entranceOne);
	    entrance.add(entranceTwo);
		
		
	    LinkedList<Vertex> roomList = new LinkedList<Vertex>();
	    roomList.add(room);
	    roomList.add(roomTwo);
	    
	    
	    
		 
			LinkedList<Vertex> deflist3 = new LinkedList<Vertex>();
			deflist3.add(defaultVertex);
		 LinkedList<Vertex> result = new LinkedList<Vertex>();
		 
		 assertEquals(result, routingMan.routingWithStop(deflist3, deflist3, defaultListTwo, defaultList, defaultListTwo, defaultList));
		
		
		
		assertEquals(defaultResult, routingMan.routingWithStop(defaultList, defaultList, defaultList, defaultList, defaultList, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStop(defaultListTwo, defaultList, defaultList, defaultList, defaultList, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStop(defaultList, defaultList, defaultListTwo, defaultList, defaultList, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStop(defaultList, defaultList, defaultList, defaultList, defaultListTwo, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStop(defaultListTwo, defaultList, defaultList, defaultList, defaultListTwo, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStop(defaultListTwo, defaultList, defaultListTwo, defaultList, defaultList, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStop(defaultList, defaultList, defaultListTwo, defaultList, defaultListTwo, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStop(defaultListTwo, defaultList, defaultListTwo, defaultList, defaultListTwo, defaultList));
	
		assertEquals(defaultResultTwo, routingMan.routingWithStop(defaultListTwo, defaultListTwo, defaultListTwo, defaultListTwo, defaultListTwo, defaultListTwo));
		assertEquals(defaultResult, routingMan.routingWithStop(defaultListTwo, defaultList, defaultListTwo, defaultList, defaultListTwo, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStop(defaultList, defaultListTwo, defaultListTwo, defaultList, defaultListTwo, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStop(defaultList, defaultList, defaultListTwo, defaultList, defaultListTwo, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStop(defaultList, defaultList, defaultListTwo, defaultListTwo, defaultListTwo, defaultListTwo));
		assertEquals(defaultResult, routingMan.routingWithStop(defaultList, defaultList, defaultListTwo, defaultListTwo, defaultListTwo, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStop(defaultList, defaultList, defaultListTwo, defaultList, defaultListTwo, defaultListTwo));

		assertEquals(result, routingMan.routingWithStop(defaultListTwo, defaultList, defaultListTwo, defaultList, deflist3, deflist3));


		assertEquals(resultNull, routingMan.routingWithStop(defaultListTwo, defaultListTwo, defaultListTwo, defaultListTwo, defaultListTwo, defaultListTwo));
		
		 assertEquals(result, routingMan.routingWithStop(defaultListTwo, defaultList, defaultListTwo, defaultList, defaultListTwo, defaultList));

 	// LinkedList<Edge> resultEdge = routingMan.routingWithStop(entrance, roomList, entrance, roomList, defaultListTwo, defaultList);
		 
		// assertEquals(resultEdge, routingMan.routingWithStop(entrance, roomList, entrance, roomList, defaultListTwo, defaultList));

		
		 
		 
		 assertEquals(result, routingMan.routingWithStop(entrance, roomList, deflist3, deflist3, entrance, roomList));
		 
		 

		 
		 
		 
		 LinkedList<Edge> res = new LinkedList<Edge>();
		 res.add(mapManager.getEdge(0, 2));
		 res.add(mapManager.getEdge(0, 1));

		 res.add(mapManager.getEdge(0, 3));
		 
		 assertEquals(res, routingMan.routingWithStop(entrance, roomList, entrance, defaultList, entrance, roomList));

		 
		 LinkedList<Edge> ress = new LinkedList<Edge>();
		 ress.add(mapManager.getEdge(0, 2));
		 ress.add(mapManager.getEdge(0, 1));
		 
		 
		 
		 LinkedList<Edge> ressOne = new LinkedList<Edge>();
		 ressOne.add(mapManager.getEdge(0, 2));
		 
		 
		 assertEquals(ressOne, routingMan.routingWithStop(entrance, roomList, roomList, defaultList, defaultListTwo, defaultList));

		 
		 
		 
		 
		 LinkedList<Edge> resds = new LinkedList<Edge>();
		 resds.add(mapManager.getEdge(0, 2));
		 resds.add(mapManager.getEdge(0, 2));
		 
		 
		 
		 assertEquals(resds, routingMan.routingWithStop(defaultListTwo, defaultList, entrance, roomList, roomList, roomList));
		 
		 
		 LinkedList<Edge> resss = new LinkedList<Edge>();
		 resss.add(mapManager.getEdge(0, 2));
		 resss.add(mapManager.getEdge(0, 2));
		 resss.add(mapManager.getEdge(0, 2));
		 resss.add(mapManager.getEdge(0, 2));


		 
		 assertEquals(resss, routingMan.routingWithStop(roomList, defaultList, entrance, roomList, roomList, defaultList));


		 assertEquals(result, routingMan.routingWithStop(deflist3, deflist3, defaultListTwo, defaultList, entrance, roomList));

		 
		 

		Vertex def = mapManager.getVertex(0, 0);
		LinkedList<Vertex> deflist = new LinkedList<Vertex>();
		deflist.add(def);
		
		
		Vertex defTwo = mapManager.getVertex(1, 0);
		LinkedList<Vertex> deflistTwo = new LinkedList<Vertex>();
		deflistTwo.add(defTwo);
		
		
		ressOne.clear();
		resss.clear();
		res.clear();
		ress.clear();

	}
	
	
	
	
	
	@Test
	public void routingTest() {
		
//		LinkedList<Vertex> result = dijkstra.routing(deflist,deflistTwo );
//		 assertEquals(result, dijkstra.routing(deflist,deflistTwo ));
		
		 

		
		
	}
	
	
	@Test
	public void routingWithStopAccessible() {
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultList, defaultList, defaultList, defaultList, defaultList, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultListTwo, defaultList, defaultList, defaultList, defaultList, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultList, defaultList, defaultListTwo, defaultList, defaultList, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultList, defaultList, defaultList, defaultList, defaultListTwo, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultListTwo, defaultList, defaultList, defaultList, defaultListTwo, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultListTwo, defaultList, defaultListTwo, defaultList, defaultList, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultList, defaultList, defaultListTwo, defaultList, defaultListTwo, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultListTwo, defaultList, defaultListTwo, defaultList, defaultListTwo, defaultList));

		
		
		assertEquals(defaultResultTwo, routingMan.routingWithStopAccessible(defaultListTwo, defaultListTwo, defaultListTwo, defaultListTwo, defaultListTwo, defaultListTwo));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultListTwo, defaultList, defaultListTwo, defaultList, defaultListTwo, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultList, defaultListTwo, defaultListTwo, defaultList, defaultListTwo, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultList, defaultList, defaultListTwo, defaultList, defaultListTwo, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultList, defaultList, defaultListTwo, defaultListTwo, defaultListTwo, defaultListTwo));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultList, defaultList, defaultListTwo, defaultListTwo, defaultListTwo, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultList, defaultList, defaultListTwo, defaultList, defaultListTwo, defaultListTwo));

		
		
		
		
		
		
	mapManager.loadDatabase();
		
		campus.addBuilding("50.50");
		
		Building building = new Building("50.50");
		
		
		Vertex entranceOne = mapManager.addVertex(0, 0, 0);
		Vertex entranceTwo = mapManager.addVertex(0, 1, 1);
		Vertex room = mapManager.addVertex(0, 2, 2);
		Vertex roomTwo = mapManager.addVertex(0, 3, 3);

		
		
		
		
		
		Edge edgeTwo = mapManager.addEdge(entranceOne.getMapId(), entranceOne.getId(), entranceTwo.getMapId(), entranceTwo.getId());
		Edge edgeThree = mapManager.addEdge(entranceTwo.getMapId(), entranceTwo.getId(), room.getMapId(), room.getId());
		Edge edge4 = mapManager.addEdge(entranceOne.getMapId(), entranceOne.getId(), room.getMapId(), room.getId());


		campus.assignVertexToBuildingEntrances(entranceOne, building);
		campus.assignVertexToBuildingEntrances(entranceTwo, building);

		

		
		LinkedList<Vertex> entrance = new LinkedList<Vertex>();
		
		entrance.add(entranceOne);
	    entrance.add(entranceTwo);
		
		
	    LinkedList<Vertex> roomList = new LinkedList<Vertex>();
	    roomList.add(room);
	    roomList.add(roomTwo);
	    
	    
	    
		 
			LinkedList<Vertex> deflist3 = new LinkedList<Vertex>();
			deflist3.add(defaultVertex);
		 LinkedList<Vertex> result = new LinkedList<Vertex>();
		 
		 assertEquals(result, routingMan.routingWithStop(deflist3, deflist3, defaultListTwo, defaultList, defaultListTwo, defaultList));
		
		
		
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultList, defaultList, defaultList, defaultList, defaultList, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultListTwo, defaultList, defaultList, defaultList, defaultList, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultList, defaultList, defaultListTwo, defaultList, defaultList, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultList, defaultList, defaultList, defaultList, defaultListTwo, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultListTwo, defaultList, defaultList, defaultList, defaultListTwo, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultListTwo, defaultList, defaultListTwo, defaultList, defaultList, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultList, defaultList, defaultListTwo, defaultList, defaultListTwo, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultListTwo, defaultList, defaultListTwo, defaultList, defaultListTwo, defaultList));
	
		assertEquals(defaultResultTwo, routingMan.routingWithStopAccessible(defaultListTwo, defaultListTwo, defaultListTwo, defaultListTwo, defaultListTwo, defaultListTwo));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultListTwo, defaultList, defaultListTwo, defaultList, defaultListTwo, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultList, defaultListTwo, defaultListTwo, defaultList, defaultListTwo, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultList, defaultList, defaultListTwo, defaultList, defaultListTwo, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultList, defaultList, defaultListTwo, defaultListTwo, defaultListTwo, defaultListTwo));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultList, defaultList, defaultListTwo, defaultListTwo, defaultListTwo, defaultList));
		assertEquals(defaultResult, routingMan.routingWithStopAccessible(defaultList, defaultList, defaultListTwo, defaultList, defaultListTwo, defaultListTwo));

		assertEquals(result, routingMan.routingWithStopAccessible(defaultListTwo, defaultList, defaultListTwo, defaultList, deflist3, deflist3));


		assertEquals(resultNull, routingMan.routingWithStopAccessible(defaultListTwo, defaultListTwo, defaultListTwo, defaultListTwo, defaultListTwo, defaultListTwo));
		
		 assertEquals(result, routingMan.routingWithStopAccessible(defaultListTwo, defaultList, defaultListTwo, defaultList, defaultListTwo, defaultList));

 	 LinkedList<Edge> resultEdge = routingMan.routingWithStopAccessible(entrance, roomList, entrance, roomList, defaultListTwo, defaultList);
		 
		 assertEquals(resultEdge, routingMan.routingWithStopAccessible(entrance, roomList, entrance, roomList, defaultListTwo, defaultList));

		
		 
		 
		 assertEquals(result, routingMan.routingWithStopAccessible(entrance, roomList, deflist3, deflist3, entrance, roomList));
		 
		 

		 
		 
		 
		 LinkedList<Edge> res = new LinkedList<Edge>();
		 res.add(mapManager.getEdge(0, 2));
		 res.add(mapManager.getEdge(0, 1));

		 res.add(mapManager.getEdge(0, 3));
		 
		 assertEquals(res, routingMan.routingWithStopAccessible(entrance, roomList, entrance, defaultList, entrance, roomList));

		 
		 LinkedList<Edge> ress = new LinkedList<Edge>();
		 ress.add(mapManager.getEdge(0, 2));
		 ress.add(mapManager.getEdge(0, 1));
		 
		 
		 
		 LinkedList<Edge> ressOne = new LinkedList<Edge>();
		 ressOne.add(mapManager.getEdge(0, 2));
		 ressOne.add(mapManager.getEdge(0, 1));

		 
		 

		 
		 assertEquals(ressOne, routingMan.routingWithStopAccessible(entrance, roomList, roomList, defaultList, defaultListTwo, defaultList));

		 LinkedList<Edge> resds = new LinkedList<Edge>();
		 resds.add(mapManager.getEdge(0, 2));
		 resds.add(mapManager.getEdge(0, 2));
		 
		 
		 
		 assertEquals(resds, routingMan.routingWithStopAccessible(defaultListTwo, defaultList, entrance, roomList, roomList, roomList));

		 LinkedList<Edge> resss = new LinkedList<Edge>();
		 resss.add(mapManager.getEdge(0, 2));
		 resss.add(mapManager.getEdge(0, 2));
		 resss.add(mapManager.getEdge(0, 2));
		 resss.add(mapManager.getEdge(0, 2));


		 
		 assertEquals(resss, routingMan.routingWithStopAccessible(roomList, defaultList, entrance, roomList, roomList, defaultList));


		 assertEquals(result, routingMan.routingWithStopAccessible(deflist3, deflist3, defaultListTwo, defaultList, entrance, roomList));

		 
		 

		Vertex def = mapManager.getVertex(0, 0);
		LinkedList<Vertex> deflist = new LinkedList<Vertex>();
		deflist.add(def);
		
		
		Vertex defTwo = mapManager.getVertex(1, 0);
		LinkedList<Vertex> deflistTwo = new LinkedList<Vertex>();
		deflistTwo.add(defTwo);
		
		
		ressOne.clear();
		resss.clear();
		res.clear();
		ress.clear();
		
		
	}
	
	
	
	@Test
	public void simpleRouting() {
		LinkedList<Vertex> deflist3 = new LinkedList<Vertex>();
		deflist3.add(defaultVertex);

		assertEquals(defaultList, routingMan.simpleRouting(defaultListTwo, defaultListTwo));
		assertEquals(null, routingMan.simpleRouting(defaultListTwo, null));

		assertEquals(null, routingMan.simpleRouting(defaultList, defaultList));

	}
	
	
	
	
	
	@Test
	public void simpleRoutingAccessible() {
		LinkedList<Vertex> deflist3 = new LinkedList<Vertex>();
		deflist3.add(defaultVertex);

		assertEquals(defaultList, routingMan.simpleRoutingAccessible(defaultListTwo, defaultListTwo));
		assertEquals(null, routingMan.simpleRoutingAccessible(defaultListTwo, null));

		assertEquals(null, routingMan.simpleRoutingAccessible(defaultList, defaultList));

	}
	
	@After
	public void after() {
		data.forgetDB();
	}
	
	
	

}
