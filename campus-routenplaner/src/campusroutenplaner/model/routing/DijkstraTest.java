package campusroutenplaner.model.routing;


import static org.junit.Assert.*;

import campusroutenplaner.controller.main.MapController;
import campusroutenplaner.model.database.Database;
import campusroutenplaner.model.map.CampusMap;
import campusroutenplaner.model.map.Edge;
import campusroutenplaner.model.map.MapManager;
import campusroutenplaner.model.map.Vertex;
import org.junit.*;

import java.util.LinkedList;

public class DijkstraTest {

	
    private Dijkstra dijkstra;
    private static MapManager mapManager ;
    private static MapController mapController;
	private static Database database;

    LinkedList<Vertex> start;
    LinkedList<Vertex> dest;


    @BeforeClass
    public static void loadDat(){
        
    	database = new Database("test/model/dijkstraTest.db");
        CampusMap campus = new CampusMap(database);
        mapManager = new MapManager(campus, database);
        mapController = new MapController(mapManager);
        mapManager.loadDatabase();
    }


    @Before
    public void setUp(){
        dijkstra = new Dijkstra();
        //dijkstra.printHashMap();

        start = new LinkedList<>();
        dest = new LinkedList<>();

    }

    @Test
    public void noStartNoDest(){
        LinkedList<Vertex> path = dijkstra.routing(start, dest);
        assertEquals(new LinkedList<Vertex>(), path);
    }


    @Test
    public void nullLists(){
        assertEquals(new LinkedList<Vertex>(), dijkstra.routing(null,null));
    }

    @Test
    public void listsWithNull(){
        LinkedList<Vertex> start = new LinkedList<>();
        start.add(null);

        LinkedList<Vertex> dest = new LinkedList<>();
        dest.add(null);


        assertEquals(new LinkedList<Vertex>(), dijkstra.routing(start,dest));
    }






    @Test
    public void noStartNoDestAccess(){
        LinkedList<Vertex> path = dijkstra.routingAccesible(start, dest);
        assertEquals(new LinkedList<Vertex>(), path);
    }


    @Test
    public void nullListsAccess(){
        assertEquals(new LinkedList<Vertex>(), dijkstra.routingAccesible(null,null));
    }

    @Test
    public void listsWithNullAccess(){
        LinkedList<Vertex> start = new LinkedList<>();
        start.add(null);

        LinkedList<Vertex> dest = new LinkedList<>();
        dest.add(null);


        assertEquals(new LinkedList<Vertex>(), dijkstra.routingAccesible(start,dest));
    }

    @Test
    public void simpleTest(){
    	

        start.add(mapController.requestSearchVertex(0, 5));
        dest.add(mapController.requestSearchVertex(0, 9));
        //dest.add(mapController.requestSearchVertex(0, 1));
        LinkedList<Vertex> actual = dijkstra.routing(start, dest);


        LinkedList<Vertex> expected = new LinkedList<>();
        expected.add(mapController.requestSearchVertex(0, 9));
        expected.add(mapController.requestSearchVertex(0, 6));
        expected.add(mapController.requestSearchVertex(0, 4));
        expected.add(mapController.requestSearchVertex(0, 5));

        assertEquals(expected, actual);


    }


    @Test
    public void simpleTestBlocked(){


        start.add(mapController.requestSearchVertex(0, 5));
        dest.add(mapController.requestSearchVertex(0, 9));
        Edge edge = mapController.requestSearchVertex(0, 6).getEdgeBetween(mapController.requestSearchVertex(0, 4));
        mapController.requestChangeEdgeBlocked(edge.getSource().getMapId(), edge.getId(), true);
        //dest.add(mapController.requestSearchVertex(0, 1));
        LinkedList<Vertex> actual = dijkstra.routing(start, dest);


        LinkedList<Vertex> expected = new LinkedList<>();
        expected.add(mapController.requestSearchVertex(0, 9));
        expected.add(mapController.requestSearchVertex(0, 7));
     //   expected.add(mapController.requestSearchVertex(0, 4));
        expected.add(mapController.requestSearchVertex(0, 5));

        assertEquals(expected, actual);

        mapController.requestChangeEdgeBlocked(edge.getSource().getMapId(), edge.getId(), false);

    }



    @Test
    public void simpleTestAccess(){


        start.add(mapController.requestSearchVertex(0, 5));
        dest.add(mapController.requestSearchVertex(0, 9));
        //dest.add(mapController.requestSearchVertex(0, 1));
        LinkedList<Vertex> actual = dijkstra.routingAccesible(start, dest);


        LinkedList<Vertex> expected = new LinkedList<>();
        expected.add(mapController.requestSearchVertex(0, 9));
        expected.add(mapController.requestSearchVertex(0, 7));
        expected.add(mapController.requestSearchVertex(0, 5));

        assertEquals(expected, actual);


    }

    @Test
    public void simpleTestBike(){


        start.add(mapController.requestSearchVertex(0, 5));
        dest.add(mapController.requestSearchVertex(0, 9));
        //dest.add(mapController.requestSearchVertex(0, 1));
        LinkedList<Vertex> actual = dijkstra.routingBike(start, dest);


        LinkedList<Vertex> expected = new LinkedList<>();
        expected.add(mapController.requestSearchVertex(0, 9));
        expected.add(mapController.requestSearchVertex(0, 7));
        expected.add(mapController.requestSearchVertex(0, 5));

        assertEquals(expected, actual);
    }


    @Test
    public void doubleStart(){
    	

    	
        start.add(mapController.requestSearchVertex(0, 2));
        start.add(mapController.requestSearchVertex(0, 7));

        dest.add(mapController.requestSearchVertex(0, 8));


        LinkedList<Vertex> actual = dijkstra.routing(start, dest);


        LinkedList<Vertex> expected = new LinkedList<>();
        expected.add(mapController.requestSearchVertex(0, 8));
        expected.add(mapController.requestSearchVertex(0, 9));
        expected.add(mapController.requestSearchVertex(0, 7));

        assertEquals(expected, actual);

    }

    @Test
    public void doubleEnd(){

    	

        start.add(mapController.requestSearchVertex(0, 5));


        dest.add(mapController.requestSearchVertex(0, 8));
        dest.add(mapController.requestSearchVertex(0, 9));


        LinkedList<Vertex> actual = dijkstra.routing(start, dest);


        LinkedList<Vertex> expected = new LinkedList<>();
        expected.add(mapController.requestSearchVertex(0, 8));
        expected.add(mapController.requestSearchVertex(0, 6));
        expected.add(mapController.requestSearchVertex(0, 4));
        expected.add(mapController.requestSearchVertex(0, 5));

        assertEquals(expected, actual);

    }

    @Test
    public void doubleStartDoubleEnd(){

        start.add(mapController.requestSearchVertex(0, 6));
        start.add(mapController.requestSearchVertex(0, 5));


        dest.add(mapController.requestSearchVertex(0, 1));
        dest.add(mapController.requestSearchVertex(0, 2));


        LinkedList<Vertex> actual = dijkstra.routing(start, dest);


        LinkedList<Vertex> expected = new LinkedList<>();

        expected.add(mapController.requestSearchVertex(0, 2));
        expected.add(mapController.requestSearchVertex(0, 4));
        expected.add(mapController.requestSearchVertex(0, 5));

        assertEquals(expected, actual);

    }











    @After
    public void tearDown(){
    	 dijkstra = null;
         start = null;
         dest = null;
    }


    @AfterClass
    public static void cleanUp(){
   
        mapManager = null;
        database = null;
        MapController mapController = null ;
        
    }
}
