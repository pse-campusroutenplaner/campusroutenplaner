package campusroutenplaner.model.constructionsites;
 
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
 
@RunWith(value = Suite.class)
@Suite.SuiteClasses(value = { ConstructionSiteTest.class, ConstructionSiteManagerTest.class})

public class AllTestss {
 
    public AllTestss() {
    }
}