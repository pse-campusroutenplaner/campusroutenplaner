package campusroutenplaner.model.constructionsites;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;


import campusroutenplaner.model.database.Database;
import campusroutenplaner.model.map.OnError;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;



public class ConstructionSiteManager {

//	private ConstructionSite construct;
	private Database database;
	private ArrayList<ConstructionSite> constructionSites;
	
	/**
	 * Constructor manager
	 * @param database
	 */
	public ConstructionSiteManager(Database database) {
	//	this.construct = construct;
		this.database = database;
		constructionSites = new ArrayList<ConstructionSite>();
		loadConstructionSitesFromDatabase();
	}
	
	
	/**
	 * load all saved consstructionsites from the database.
	 * before return, convert every constructionsite from String to SimpleStringProperty and then add in the list that will be returned.
	 * @return LinkedList
	 */
	public void loadConstructionSitesFromDatabase() {
		
		ArrayList<String[]> constrSites = database.getConstructionSites();
		
		for ( int i = 0; i < constrSites.size(); i++) {
				int id = Integer.parseInt(constrSites.get(i)[0]);
				int edgeId = Integer.parseInt(constrSites.get(i)[3]);
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			    LocalDate beginDate = LocalDate.parse(constrSites.get(i)[1], dtf);
			    LocalDate expDate = LocalDate.parse(constrSites.get(i)[2], dtf);
				constructionSites.add(new ConstructionSite(id, beginDate, expDate, edgeId));
		}
		
	}
	
	public ArrayList<ConstructionSite> getConstructionSite() { 
		return constructionSites;
	}
	

	/**
	 * If startdate and expDate are not equals null and edge is not blocked, then add constructionsite in database .
	 * it will be returned the id from the new constructionsite. if unequal 0 then add new constructionsite in the local 
	 * list with the constructionsites and return the new COnstructionSite.
	 * Otherweise return null
	 * @param startDate
	 * @param expDate
	 * @param edgeId
	 * @return Enum
	 */
	public ConstructionSite addConstructionSite(LocalDate startDate, LocalDate expDate, int edgeId) {
		
		int id = 0;
		
		if ( (!database.checkEdgeBlocked(edgeId)) ) {
			
			if (expDate == null) {
				String defaultExpDate = "2030-01-01";
				id = database.addConstrSite(startDate.toString(), defaultExpDate, edgeId);
			} else {
				id = database.addConstrSite(startDate.toString(), expDate.toString(), edgeId);

			}
	    }
		
		if ( id != 0) {
			
			ConstructionSite constriSite = new ConstructionSite(id, startDate, expDate, edgeId);
					constructionSites.add(constriSite);
					return constriSite;
		}
			
				
		return null;
		
	}
	
	
	/**
	 * change startdate, expdate or edge to constructionsite.
	 * it will be searched for the constructionsite with the commited id,if found then it will be proofed ,if the new parameters are equals the defaultparameters or null.
	 * In both cases nothing to change.Otherweise override old pamaters. Changes must be saved both in the database and in the local list.
	 * if no such constructionsite is found ,return null.
	 * @param toChangeConstructionSiteId
	 * @param startDate
	 * @param expDate
	 * @param edgeId
	 * @return ConstructionSite
	 */
	public ConstructionSite changeConstructionSite(int toChangeConstructionSiteId, LocalDate startDate, LocalDate expDate, int edgeId) {
		

		 ConstructionSite toChangeConstructionSite = getConstructionSite(toChangeConstructionSiteId);
		 
		 
		if(toChangeConstructionSite != null) {
		
			if ( (!toChangeConstructionSite.getBeginDate().equals(startDate)) && (!startDate.equals(null)) ) {
				if (database.changeConstructionSiteStartDate(startDate.toString(), toChangeConstructionSite.getId())) {
					toChangeConstructionSite.setBeginDate(startDate);
				}
			}
		
			if ( (!toChangeConstructionSite.getEndDate().equals(expDate)) && (!expDate.equals(null))  ) {
				if (database.changeConstructionSiteExpDate(expDate.toString(), toChangeConstructionSite.getId()) ) {
					toChangeConstructionSite.setEndDate(expDate);
				}
			}
			
			return toChangeConstructionSite;		
		}
		
		return null;
	}
	
	/**
	 * search in the list with constructionSites . if no constructionsite with such id found,return null.
	 * @param constructionsiteId
	 * @return ConstructionSite
	 */
	public ConstructionSite getConstructionSite(int constructionsiteId) {
		
		for (ConstructionSite temp :constructionSites) {
			if (temp.getId() == constructionsiteId) {
				return temp;
			}
		}
		return null;
		
	}
	
	/**
	 * if constructionsite removed from database then remove from the list with constructionSite and the return SuccessfulRemoved
	 * Otherweise return UnSuccessfulremoved.
	 * @param constructionsiteId
	 * @return Enum
	 */
	public OnError removeConstructionSite(int constructionsiteId) {
		
		
		if (database.deleteConstrSite(constructionsiteId)) {
			for (ConstructionSite temp : constructionSites) {
				if (temp.getId() == constructionsiteId) {
					constructionSites.remove(temp);
					return OnError.SuccessfulRemoved;
				}
			}
		}
		
		return OnError.UnSuccessfulRemoved;

	}
	
	/**
	 * Get construtionSiteId.in case that constructionsite with such edge
	 * already exist, it will be returned the id from constructionsite.
	 * Otherweise it will be returned -1.
	 *
	 * 
	 * @return int
	 * 
	 */

	public int getConstructionSiteId(int edgeId) {
		
		for (ConstructionSite temp : constructionSites) {
			if (temp.getEdgeId() == edgeId) {
				return temp.getId();
			}
		}
		return -1;

	}	
	
	public boolean checkEdgeBlocked(int edgeID) {
		return database.checkEdgeBlocked(edgeID);
	}




	

	
	
}
