package campusroutenplaner.model.constructionsites;

import static org.junit.Assert.*;

import java.sql.DataTruncation;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import campusroutenplaner.model.database.Database;
import campusroutenplaner.model.map.Edge;
import campusroutenplaner.model.map.Map;
import campusroutenplaner.model.map.OnError;
import campusroutenplaner.model.map.Vertex;

public class ConstructionSiteManagerTest {

	
	private ConstructionSiteManager constructionSiteManager;
	private Database data;
	private LocalDate date, dateOne, dateDefault, defaultDate;
	private Edge edge, edgeTwo;
	private Vertex v1, v2;
	
	
	
	@Before public void beforetest() {
		data = new Database("test/model/KIT(testCopy).db");

    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        date = LocalDate.parse("2015-11-12", formatter);
        dateOne = LocalDate.parse("2016-12-16", formatter);
        defaultDate = LocalDate.parse("2030-01-01", formatter);
        
		constructionSiteManager = new ConstructionSiteManager(data);
		v1 = new Vertex();
		v2 = new Vertex();
		
		edge = new Edge(1, v1, v2);
    	edgeTwo = new Edge(2, v1, v2);
	}
	
	
	@Test
	public void loadAndFielTheConstructionSiteList() {
		
		constructionSiteManager.getConstructionSite().clear();
		
		assertEquals(0, constructionSiteManager.getConstructionSite().size());
		
		data.addConstrSite(dateOne.toString(), dateOne.toString(), edge.getId());

		constructionSiteManager.loadConstructionSitesFromDatabase();
		
		//ConstructionSite constr = new ConstructionSite(0, date, dateOne, edge.getId());
		
		//constructionSiteManager.getConstructionSite().add(constr);
		assertEquals(1, constructionSiteManager.getConstructionSite().size());


	}
	

	
	
	

	
	
	@Test
	public void addConstructionSite() {		


		ConstructionSite constrt1 = new ConstructionSite(1, date, dateOne, edgeTwo.getId());
		
		
		ConstructionSite constrt2 = constructionSiteManager.addConstructionSite(date, dateOne, edgeTwo.getId());
		

		assertEquals(constrt1.getBeginDate(), constrt2.getBeginDate());
		assertEquals(constrt1.getEndDate(), constrt2.getEndDate());
		assertEquals(constrt1.getEdgeId(), constrt2.getEdgeId());
		
		ConstructionSite constrt3 = new ConstructionSite(1, date, null, edgeTwo.getId());
		
		ConstructionSite constrt4 = constructionSiteManager.addConstructionSite(date, null, edgeTwo.getId());

		//assertNull(constrt4.getBeginDate());
		
		ConstructionSite constrt5 = new ConstructionSite(2, date, defaultDate, edge.getId());

		ConstructionSite constrt6 = constructionSiteManager.addConstructionSite(date, null, edge.getId());
		assertEquals(constrt5.getBeginDate(), constrt6.getBeginDate());
		
	//	assertEquals(constrt5.getEndDate(), constrt6.getEndDate());
		assertEquals(constrt5.getEdgeId(), constrt6.getEdgeId());


		
		//assertEquals(constrt, constructionSiteManager.addConstructionSite(date, dateDefault, edge.getId() ));



		//assertEquals(constrt, constructionSiteManager.addConstructionSite(dateOne, dateDefault, 0));

		
		//data.addConstrSite(constr.getBeginDate().toString(), constr.getEndDate().toString(), constr.getEdgeId());
		
		
	}
	
	
	
	
	@Test
	public void getConstructionSite() {
		
		ConstructionSite constr = constructionSiteManager.addConstructionSite(date, dateOne, edge.getId());

		
		constructionSiteManager.getConstructionSite().add(constr);

		
		//assertEquals(constr, constructionSiteManager.getConstructionSite(constr.getId()));
		
	}
	
	
	
	@Test
	public void getConstructionSiteId() {
		
		
		
		int id = constructionSiteManager.getConstructionSiteId(edge.getId());
		
		assertEquals(-1, constructionSiteManager.getConstructionSiteId(666666));
		ConstructionSite constrt = new ConstructionSite(1, date, dateOne, edge.getId());

		constructionSiteManager.getConstructionSite().add(constrt);
		assertEquals(constrt.getEdgeId(), constructionSiteManager.getConstructionSiteId(constrt.getEdgeId()));
		
	}
	
	
	@Test
	public void testCheckTestBlocked() {
		
		assertEquals(false, constructionSiteManager.checkEdgeBlocked(edge.getId()));
	}
	
	@Test
	public void changeConstructionSite() {
		
		ConstructionSite constrt = new ConstructionSite(1, date, dateOne, edgeTwo.getId());


		assertEquals(null, constructionSiteManager.changeConstructionSite(1, date, dateOne, edge.getId()) );
		
		constructionSiteManager.getConstructionSite().add(constrt);
		data.addConstrSite(constrt.getBeginDate().toString(), constrt.getEndDate().toString(), constrt.getEdgeId());
		
		assertEquals(constrt, constructionSiteManager.changeConstructionSite(constrt.getId(), dateOne, date, constrt.getEdgeId()));
		
		//assertEquals(constrt, constructionSiteManager.changeConstructionSite(constrt.getId(), constrt.getBeginDate(), constrt.getEndDate(), constrt.getEdgeId()));
	}
	

	@Test
	public void removeConstructionSite() {
		
		ConstructionSite constrt = new ConstructionSite(1, date, dateOne, edgeTwo.getId());

		data.addConstrSite(date.toString(), dateOne.toString(), edge.getId());
		
		assertEquals(OnError.UnSuccessfulRemoved, constructionSiteManager.removeConstructionSite(0));
		
		data.addConstrSite(date.toString(), dateOne.toString(), edge.getId());
		constructionSiteManager.getConstructionSite().add(constrt);
		
		assertEquals(OnError.SuccessfulRemoved, constructionSiteManager.removeConstructionSite(constrt.getId()));
		
		//assertEquals(OnError.SuccessfulRemoved, constructionSiteManager.removeConstructionSite(constrt.getId()));

		
	}
	

	
	@After
	public void after () {
		data.forgetDB();
	}
	
	

}
