package campusroutenplaner.model.constructionsites;

import java.time.LocalDate;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

public class ConstructionSite {
	





	private SimpleStringProperty edgeId;
	private SimpleObjectProperty<LocalDate> beginDate;
	private SimpleObjectProperty<LocalDate> endDate;
	private int id;

	/**
	 * Constructor for ConstructionSite
	 * @param id
	 * @param beginDate
	 * @param endDate
	 * @param edgeId
	 */
	public ConstructionSite(int id, LocalDate beginDate, LocalDate endDate, int edgeId) {
		this.id = id;
		this.beginDate = new SimpleObjectProperty<LocalDate>(beginDate);
		this.endDate = new SimpleObjectProperty<LocalDate>(endDate);
		this.edgeId = new SimpleStringProperty("" + edgeId);

	}

	/**
	 * getplace
	 * @return String
	 */
	public String getPlace() {
		return edgeId.getValue();
	}

	/**
	 * get begindate
	 * @return LocalDate
	 */
	public LocalDate getBeginDate() {
		return beginDate.get();
	}

	/**
	 * get enddate
	 * @return LocalDate
	 */
	public LocalDate getEndDate() {
		return endDate.get();
	}
	
	/**
	 * setbeginDate
	 * @param beginDate
	 */
	protected void setBeginDate(LocalDate beginDate) {
		this.beginDate.set(beginDate);
		System.out.println("beginDate " + beginDate);
	}

	/**
	 * set enddate
	 * @param endDate
	 */
	protected void setEndDate(LocalDate endDate) {
		this.endDate.set(endDate);
		System.out.println("endDate " + endDate);
	}
	
	/**
	 * getConstructionSite Id
	 * @return int
	 */
	public int getId() {
		return id;
	}
	
	
	/**
	 * get ConstructionsiteEdge id
	 * @return int
	 */
	public int getEdgeId() {
		return Integer.parseInt(edgeId.getValue());
	}



	
	
	
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		ConstructionSite other = (ConstructionSite) obj;
//		if (beginDate == null) {
//			if (other.beginDate != null)
//				return false;
//		} else if (!beginDate.equals(other.beginDate))
//			return false;
//		if (edgeId == null) {
//			if (other.edgeId != null)
//				return false;
//		} else if (!edgeId.equals(other.edgeId))
//			return false;
//		if (endDate == null) {
//			if (other.endDate != null)
//				return false;
//		} else if (!endDate.equals(other.endDate))
//			return false;
//		if (id != other.id)
//			return false;
//		return true;
//	}
	
	
	
}