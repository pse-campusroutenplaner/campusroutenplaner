package campusroutenplaner.model.constructionsites;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.Before;
import org.junit.Test;

public class ConstructionSiteTest {

	
	
	
	
    private ConstructionSite constructionSite;
    private LocalDate localone, localTwo;



    @Before
    public void setUp(){
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    	LocalDate date = LocalDate.parse("2005-11-12", formatter);
    	LocalDate dateOne = LocalDate.parse("2005-11-12", formatter);
    	
    	
    	
        constructionSite = new ConstructionSite(0, date, dateOne, 6);
    }
	@Test
	public void test() {
		assertEquals(constructionSite.getPlace(), constructionSite.getPlace());
		assertEquals(constructionSite.getBeginDate(), constructionSite.getBeginDate());
		assertEquals(constructionSite.getEndDate(), constructionSite.getEndDate());
		constructionSite.setBeginDate(localTwo);
		assertEquals(localTwo, constructionSite.getBeginDate());
		constructionSite.setEndDate(localTwo);
		assertEquals(localTwo, constructionSite.getEndDate());
		assertEquals(constructionSite.getId(), constructionSite.getId());
		assertEquals(constructionSite.getEdgeId(), constructionSite.getEdgeId());
	}

}
