package campusroutenplaner.model.logIn;

import campusroutenplaner.model.database.Database;
import campusroutenplaner.model.map.OnError;

public class LogInManager {
	
	Database database;
	
	/**
	 * Constructor 
	 * @param database
	 */
	public LogInManager(Database database) {
		this.database = database;
	}

	
	/**
	 * it will be checked the validity of the name and password for logIn in admin-area
	 * @return SuccessfulLogin or UnSuccessfulLogin
	 */
	public OnError checkLogIn(String name, String password) {
		
		if (database.login(name, password)) {
			return OnError.SuccessfulLogin;
		}
		return OnError.UnSuccessfulLogin;
	}
	
	
	/**
	 * it will be changed the paswword  for logIn in admin-area
	 * @return SuccessfulChangePassword or UnSuccessfulChangePassword
	 */
	public OnError changePassword(String oldPassword, String newPassword) {
		
		if (database.changePassword(oldPassword, newPassword)) {
			return OnError.SuccessfulChangePassword;
		}
		
		return OnError.UnSuccessfulChangePassword;
	}
	
	
	/**
	 * it will be changed the name  for logIn in admin-area
	 * @return Enum
	 */
	//public OnError changeName(String oldName, String newName) {
		
//		if(database.changeLogin(oldName, newName)) {
//			return OnError.SuccessfulChangeName;
//		}
//		return OnError.UnSuccessfulChangeName;
//	}
	
}
