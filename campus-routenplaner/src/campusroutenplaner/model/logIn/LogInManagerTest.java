package campusroutenplaner.model.logIn;

import campusroutenplaner.model.database.Database;
import campusroutenplaner.model.map.OnError;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


import static org.junit.Assert.assertEquals;

/**
 * Created by nmlad on 8/13/16.
 */
public class LogInManagerTest {

    private static LogInManager logInManager;
    private static Database database;

    @BeforeClass
    public static void setUpBeforeClass(){
        database = new Database("test/model/mapManagerTestKit.db");
    }

    @Before
    public void setUp(){
        logInManager = new LogInManager(database);
    }

    @Test
    public void succLogin(){
        assertEquals(OnError.SuccessfulLogin, logInManager.checkLogIn("admin", "1"));
    }

    @Test
    public void unsuccLogin(){
        assertEquals(OnError.UnSuccessfulLogin, logInManager.checkLogIn("admin", "2"));
    }

    @Test
    public void succChangePassword(){
        assertEquals(OnError.SuccessfulChangePassword, logInManager.changePassword("1", "2") );
        logInManager.changePassword("2", "1");
    }


    @Test
    public void unsuccChangePassword(){
        assertEquals(OnError.UnSuccessfulChangePassword, logInManager.changePassword("3", "2") );

    }


    @After
    public void tearDown(){

    }
}
