package campusroutenplaner.model.logIn;
 
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
 
@RunWith(value = Suite.class)
@Suite.SuiteClasses(value = { LogInManagerTest.class})

public class Alltest {
 
    public Alltest() {
    }
}