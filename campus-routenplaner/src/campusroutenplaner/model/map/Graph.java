package campusroutenplaner.model.map;

import campusroutenplaner.Logger;

import java.util.HashMap;
import java.util.LinkedList;

public class Graph {
	
	
	private HashMap<Integer, Vertex> vertices;
	private HashMap<Integer, Edge> edges;

	
	/**
	 * empty graph
	 */
	public Graph(){
		vertices = new HashMap<>();
		edges = new HashMap<>();
	}


	
	/**
	 * adds the 
	 * @param vertex to the HashMap
	 */
	protected void addVertex(Vertex vertex){

	    if(vertex == null){
	        Logger.print("Error: Trying to add null vertex");
	        return;
        }

        Logger.print("Created: " + vertex);

        vertices.put(vertex.getId(), vertex);
	}


    /**
     *
     * @param edge
     */
	protected void addEdge(Edge edge){
      //  System.out.println("Created: "+edge);

        if(edge == null){
            Logger.print("Error: Trying to add null edge");
            return;
        }

        Logger.print("Created: " + edge);

		edges.put(edge.getId(), edge);
	}
	
	
	/**
	 * removes vertex with 
	 * @param vertexId from the HashMap
	 */
	protected void deleteVertex(int vertexId){


        Vertex vert = vertices.get(vertexId);

        if(vert == null){
            return;
        }

        Logger.print("Deleted: "+vert);
        vertices.remove(vertexId);
        vert = null;

    }
	
	
	protected void deleteEdge(int edgeId){
		Edge ed = edges.get(edgeId);

        if(ed == null){
            return;
        }

        Logger.print("Deleted: "+ed);
        edges.remove(edgeId);
        ed = null;
	}
	
	
	
	/**
	 * getter for a vertex with
	 * @param vertexId
	 * @return
	 */
	public Vertex getVertex(int vertexId){
		Vertex vert = vertices.get(vertexId);
		return vert;
	}
	
	
	
	/**
	 * getter for an edge with
	 * @param edgeId
	 * @return
	 */
	public Edge getEdge(int edgeId){
		Edge edge = edges.get(edgeId);
		return edge;
	}
	

	

	
	/**
	 * getter for
	 * @return vertices
	 */
	public LinkedList<Vertex> getVertices() {
		LinkedList<Vertex> vertexList = new LinkedList<>(vertices.values());
		return vertexList;
	}


	public LinkedList<Integer> getVerticesIds(){
		LinkedList<Integer> vertexIds = new LinkedList<>();
		
		for(Vertex v : getVertices()){
			vertexIds.add(v.getId());
		}
		
		
		return vertexIds;
	}


	protected void setVertices(LinkedList<Vertex> verticesList){
	    for(Vertex vert : verticesList){
	        vertices.put(vert.getId(), vert);
        }
    }

    protected void setEdges(LinkedList<Edge> edgesList){
        for(Edge edge : edgesList){
            edges.put(edge.getId(), edge);
        }
    }




	/**
	 * getter for
	 * @return edges
	 */
	public LinkedList<Edge> getEdges() {
		LinkedList<Edge> edgesList = new LinkedList<>(edges.values());
		return edgesList;
	}

	public LinkedList<Integer> getEdgesIds() {
		LinkedList<Integer> edgesId = new LinkedList<>();

		for(Edge e : edges.values()){
			edgesId.add(e.getId());
		}


		return edgesId;
	}


}
