
package campusroutenplaner.model.map;

public class FloorMap extends Map {

	private String buildingNumber;
	private int flooNumber;

	public int getFloorNumber() {
		return flooNumber;
	}

	protected void setFlooNumber(int flooNumber) {
		this.flooNumber = flooNumber;
	}



	public FloorMap(Graph graph, Background backgroundImage, int mapId, String buildingNumber, int floorNumber) {

		super(graph, backgroundImage, mapId);
		this.buildingNumber = buildingNumber;
		this.flooNumber = floorNumber;

	}
	
	
	public FloorMap(int mapId, String buildingNumber, int floorNumber){
		
		super(new Graph(), new Background(), mapId);
		this.buildingNumber = buildingNumber;
		this.flooNumber = floorNumber;
	}


	public String getBuildingNumber(){
	    return buildingNumber;
    }

    @Override
    public String toString() {
        return "FloorMap{" +
                "buildingNumber='" + buildingNumber + '\'' +
                ", flooNumber=" + flooNumber +
                '}';
    }
}
