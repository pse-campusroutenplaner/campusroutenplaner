package campusroutenplaner.model.map;

public class Edge {
	
	
	
	//unique for this mapId
	private int id;

	
	//vertices
	private Vertex source;
	private Vertex dest;
	
	
	//in meters
	private double length;
	
	//barrierenfreiheit
	private boolean accessible;
	
	//gesperrt
	private boolean blocked;

	
	
	/**
	 * default Edge with id = 0;
	 */
	public Edge(){
		source = new Vertex();
		dest = new Vertex();
		id = 0;
	}
	
	
	/**
	 * 
	 * @param id
	 * @param source
	 * @param dest
	 * 
	 * <p>
	 * accessible is false
	 * <p>
	 * blocked is false
	 * 
	 * <p>
	 * if id invalid, then it is 0
	 */
	public Edge(int id, Vertex source, Vertex dest){
		if(id<0){
			this.id = 0;
		}else{
			this.id = id;
		}

		if(source != null){
			this.source=source;
		}else{
			source = new Vertex();
			System.out.println("Error, trying to create an edge with null as dest");
		}


		if(dest != null){
			this.dest = dest;
		}else{
			dest = new Vertex();
			System.out.println("Error, trying to create an edge with null as source");
		}
		

	}
	
	/**
	 * @param id
	 * @param source
	 * @param dest
	 * @param length
	 * @param accessible
	 * @param blocked
	 * 
	 * <p>
	 * if id invalid, then it is -1
	 */
	public Edge(int id, Vertex source, Vertex dest, double length, boolean accessible, boolean blocked) {
	
		if(id<0){
			this.id = 0;
		}else{
			this.id = id;
		}


		if(source != null){
			this.source=source;
		}else{
			source = new Vertex();
			System.out.println("Error, trying to create an edge with null as dest");
		}


		if(dest != null){
			this.dest = dest;
		}else{
			dest = new Vertex();
			System.out.println("Error, trying to create an edge with null as source");
		}
		
		
		this.length = length;
		this.accessible = accessible;
		this.blocked = blocked;
	}



	/**
	 * getter id of Edge
	 * @return
	 */
	public int getId() {
		return id;
	}







	/**
	 * getter for Vertex
	 * @return
	 */
	public Vertex getSource() {
		return source;
	}



	/**
	 * setter for 
	 * @param source
	 */
	protected void setSource(Vertex source) {
		this.source = source;
	}



	/**
	 * getter for
	 * @return dest
	 */
	public Vertex getDest() {
		return dest;
	}



	/**
	 * setter for
	 * @param dest
	 */
	protected void setDest(Vertex dest) {
		this.dest = dest;
	}



	/**
	 * getter for
	 * @return length
	 */
	public double getLength() {
		return length;
	}



	/**
	 * setter for
	 * @param length
	 */
	protected void setLength(double length) {
		this.length = length;
	}



	/**
	 * is it barrierenfrei
	 * @return
	 */
	public boolean isAccessible() {
		return accessible;
	}



	/**
	 * set for
	 * @param accessible
	 */
	protected void setAccessible(boolean accessible) {
		this.accessible = accessible;
	}



	/**
	 * is it gesperrt
	 * @return
	 */
	public boolean isBlocked() {
		if ( blocked == true) {
			return true;
		} 
		return false;
	}



	/**
	 * setter for
	 * @param blocked
	 */
	protected void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	@Override
	public String toString() {
		return "Edge{" +
				"id=" + id +
				", source=" + source.getId() +
				", sourceMapId=" + source.getMapId() +
				", dest=" + dest.getId() +
				", destMapId=" + dest.getMapId() +
				", length=" + length +
				", accessible=" + accessible +
				", blocked=" + blocked +
				'}';
	}


}
