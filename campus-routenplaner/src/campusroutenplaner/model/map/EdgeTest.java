package campusroutenplaner.model.map;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class EdgeTest {

	
	
	private Edge edge;
	private int id, idTwo;
	private double gg;
	private Vertex source, dest;
	private Vertex sourceOne, destOne;
	
	@Before public void beforeTest() {
		id = 0;
		idTwo = -1;
		gg = 0.1;
		sourceOne = new Vertex();
		destOne = new Vertex();
		edge = new Edge();
		edge = new Edge(id, source, dest);
		edge = new Edge(idTwo, source, dest);
		edge = new Edge(id, sourceOne, destOne);
		edge = new Edge(id, source, dest, 0.1, false, true);
		edge = new Edge(idTwo, source, dest, 0.1, false, true);
		edge = new Edge(id, sourceOne, destOne, 0.1, false, true);
		
	}

	
	@Test
	public void getId() {
		assertEquals(id, edge.getId());
	}
	
	@Test public void getSource() {
		assertEquals(sourceOne, edge.getSource());
	}
	
	@Test public void setSource() {
		edge.setSource(sourceOne);
		assertEquals(sourceOne, edge.getSource());
	}
	
	@Test public void getDest() {
		assertEquals(destOne, edge.getDest());
	}
	
	@Test public void setDest() {
		edge.setDest(destOne);
		assertEquals(destOne, edge.getDest());
	}
	
	@Test public void getLength() {
		assertEquals(gg, edge.getLength(), gg);
	}
	
	@Test public void setLength() {
		edge.setLength(gg);
		assertEquals(gg, edge.getLength(), gg);
	}
	
	@Test public void isAccessible() {
		assertEquals(false, edge.isAccessible());
	}

	@Test public void setIsAccessible() {
		edge.setAccessible(true);
		assertEquals(true, edge.isAccessible());
	}
	
	@Test public void isBlocked() {
		assertEquals(false, edge.isAccessible());
	}
	
	@Test public void isBlockedTwo() {
		edge.setAccessible(true);
		assertEquals(true, edge.isBlocked());
	}
	

	@Test public void setIsBlocked() {
		edge.setBlocked(false);
		assertEquals(false, edge.isBlocked());
	}
	
	
	@Test public void toStRIng() {
		String expectedPrint = "Edge{" +
				"id=" + id +
				", source=" + sourceOne.getId() +
				", sourceMapId=" + sourceOne.getMapId() +
				", dest=" + destOne.getId() +
				", destMapId=" + destOne.getMapId() +
				", length=" + edge.getLength() +
				", accessible=" + edge.isAccessible() +
				", blocked=" + edge.isBlocked() +
				'}';
		assertEquals(expectedPrint, edge.toString());
		
	}

}
