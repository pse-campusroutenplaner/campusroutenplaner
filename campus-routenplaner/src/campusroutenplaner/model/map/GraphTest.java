package campusroutenplaner.model.map;


import campusroutenplaner.model.map.Edge;
import campusroutenplaner.model.map.Graph;
import campusroutenplaner.model.map.Vertex;
import org.junit.*;

import java.util.LinkedList;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * Created by nmladenov on 7/5/16.
 */
public class GraphTest{
    private Graph graph;






    @Before
    public void createEmptyGraph(){
        graph = new Graph();
    }




    @Test
    public void constructorTest(){
        assertTrue( graph.getVertices().size() == 0 && graph.getEdges().size() == 0);
    }


    @Test
    public void addValidVertex(){

        Vertex vert = new Vertex();
        graph.addVertex(vert);


        assertEquals(vert, graph.getVertex(vert.getId()));
        assertTrue(graph.getVertices().size() == 1);
    }


    @Test
    public void addNullVertex(){

        graph.addVertex(null);
        assertTrue(graph.getVertices().size() == 0);

    }



    @Test
    public void addValidEdge(){
        Edge edge = new Edge();

        graph.addEdge(edge);

        assertEquals(graph.getEdge(edge.getId()), edge);
        assertTrue(graph.getEdges().size() == 1);
    }

    @Test
    public void addNullEdge(){
        graph.addEdge(null);
        assertTrue(graph.getEdges().size() == 0);
    }





    @Test
    public void getExistingVertexTest(){
        Vertex vert = new Vertex();
        graph.addVertex(vert);

        assertEquals(vert, graph.getVertex(vert.getId()));

    }


    @Test
    public void getNonExistingVertex(){

        Vertex vert = new Vertex();
        graph.addVertex(vert);

        graph.deleteVertex(vert.getId());

        assertEquals(graph.getVertex(vert.getId()), null);
    }

    @Test
    public void getNonExistingEdge(){
        Edge edge = new Edge();
        graph.addEdge(edge);

        graph.deleteEdge(edge.getId());

        assertEquals(graph.getEdge(edge.getId()), null);
    }


    @Test
    public void getExistingEdgeTest(){
       Edge edge = new Edge();
        graph.addEdge(edge);

        assertEquals(edge, graph.getEdge(edge.getId()));

    }





    @Test
    public void deleteExistingVertex(){

        //add the vertex
        Vertex vert = new Vertex();
        graph.addVertex(vert);

        int size = graph.getVertices().size();

        graph.deleteVertex(vert.getId());
        assertNull( graph.getVertex(vert.getId()));


        size--;
        assertEquals(size, graph.getVertices().size());

    }


    @Test
    public void deleteExistingEdge(){



        //add the vertex
        Edge edge = new Edge();
        graph.addEdge(edge);

        int size = graph.getEdges().size();

        graph.deleteEdge(edge.getId());
        assertNull( graph.getEdge(edge.getId()) );

        size--;
        assertEquals(size, graph.getEdges().size());

    }


    @Test
    public void deleteNonExistingVertex(){
        int size = graph.getVertices().size();


        graph.deleteVertex(2);

        assertEquals(size, graph.getVertices().size());
    }


    @Test
    public void deleteNonExistingEdge(){
        int size = graph.getEdgesIds().size();

        graph.deleteEdge(2);

        assertEquals(size, graph.getEdges().size());
    }




    @Test
    public void getEdgesIds(){
        graph.addEdge(new Edge(0, new Vertex(), new Vertex()));
        graph.addEdge(new Edge(1, new Vertex(), new Vertex()));
        graph.addEdge(new Edge(2, new Vertex(), new Vertex()));


        LinkedList<Integer> actualEdgeIdsList = graph.getEdgesIds();
        LinkedList<Integer> expectedEdgeIds = new LinkedList<>();

        expectedEdgeIds.add(0);
        expectedEdgeIds.add(1);
        expectedEdgeIds.add(2);


        assertEquals(actualEdgeIdsList, expectedEdgeIds);


    }



    @Test
    public void getVertexIds(){
        graph.addVertex(new Vertex(0,0,0,0));
        graph.addVertex(new Vertex(0,0,1,0));
        graph.addVertex(new Vertex(0,0,2,0));



        LinkedList<Integer> actualVertexIds = graph.getVerticesIds();
        LinkedList<Integer> expectedVertexIds = new LinkedList<>();

        expectedVertexIds.add(0);
        expectedVertexIds.add(1);
        expectedVertexIds.add(2);


        assertEquals(actualVertexIds, expectedVertexIds);


    }


    @Test
    public void setVerticesTest(){
        LinkedList<Vertex> verticesList = new LinkedList<>();

        verticesList.add(new Vertex(0,0, 0, 0));
        verticesList.add(new Vertex(0,0, 1, 0));
        verticesList.add(new Vertex(0,0, 2, 0));

        graph.setVertices(verticesList);


        assertEquals(verticesList, graph.getVertices());
    }

    @Test
    public void setEdgesTest(){
        LinkedList<Edge> edgesList = new LinkedList<>();

        edgesList.add(new Edge(0, new Vertex(), new Vertex()));
        edgesList.add(new Edge(1, new Vertex(), new Vertex()));

        graph.setEdges(edgesList);

        assertEquals(edgesList, graph.getEdges());

     }



    @After
    public void cleanUp(){


        for(Edge e : graph.getEdges()){
            graph.deleteEdge(e.getId());
            e = null;
        }
        graph.getEdges().clear();


        for(Vertex v : graph.getVertices()){
            graph.deleteVertex(v.getId());
            v = null;
        }
        graph.getVertices().clear();

        graph = null;


    }










}
