package campusroutenplaner.model.map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;

import static org.junit.Assert.*;

/**
 * Created by nmlad on 8/1/16.
 */
public class RoomTypeTest {

    RoomType roomType;


    @Before
    public void setUp(){

    }

    @Test
    public void emptyConstructorTest(){
        roomType = new RoomType();

        RoomType roomTypeExpected = new RoomType();
        roomTypeExpected.setName("");

        assertEquals(roomTypeExpected, roomType);
        assertNotNull(roomType.getAliases());
    }



    @Test
    public void constructorTest(){
        roomType = new RoomType("test");

        RoomType roomTypeExpected = new RoomType();
        roomTypeExpected.setName("test");

        assertEquals(roomTypeExpected, roomType);
        assertNotNull(roomType.getAliases());
    }


    @Test
    public void setRoomTypeTest(){
        roomType = new RoomType();

        roomType.setName("test");

        RoomType roomTypeExpected = new RoomType("test");

        assertEquals(roomTypeExpected, roomType);

    }

    @Test
    public void setNullRoomType(){
        roomType = new RoomType(null);

        assertEquals(roomType.getName(), "");
    }


    @Test
    public void getNameTest(){
        roomType = new RoomType("test");

        assertEquals("test", roomType.getName());
    }

    @Test
    public void getTypeTest(){
        roomType = new RoomType("test");

        assertEquals("Room", roomType.getType());
    }

    @Test
    public void toStringTest(){
        roomType = new RoomType("test");

        assertEquals("room: test, Aliases: []", roomType.toString());
    }




    @Test
    public void equalsOtherTypeTest(){
        roomType = new RoomType("test");

        StairsType stairsType = new StairsType();

        assertFalse(roomType.equals(stairsType));
    }


    @Test
    public void addValidAliasWithNumber(){
        roomType = new RoomType("331");

        roomType.addAlias("Wagner");

        assertEquals("Wagner", roomType.getAliases().get(0));
    }

    @Test
    public void addNonValidAliasWithNumber(){
        roomType = new RoomType("331");

        roomType.addAlias(null);

        assertEquals(0, roomType.getAliases().size());
    }

    @Test
    public void voidNonValidWithoutNumber(){
        roomType = new RoomType();

        roomType.addAlias(null);

        assertEquals(0, roomType.getAliases().size());
    }

    @Test
    public void AddValidWithoutNumber(){
        roomType = new RoomType();

        roomType.addAlias("Wagner");
        assertEquals(0, roomType.getAliases().size());

    }


    @Test
    public void setAliasWithoutNumber(){
        roomType = new RoomType();

        LinkedList<String> list = new LinkedList<>();
        list.add("asd");
        roomType.setAliases(list);


        assertEquals(0, roomType.getAliases().size());
    }

    @Test
    public void setNullAliases(){
        roomType = new RoomType("331");

        roomType.setAliases(null);

        assertEquals(0, roomType.getAliases().size());
    }

    @Test
    public void setValidAliases(){
        roomType = new RoomType("331");


        LinkedList<String> list = new  LinkedList<>();
        list.add("Wagner");
        roomType.setAliases(list);


        assertEquals("Wagner", roomType.getAliases().get(0));

    }


    @Test
    public void equalsTest(){
        roomType = new RoomType("test");

        RoomType roomTypeActual = new RoomType("test");

        assertTrue(roomType.equals(roomTypeActual));
    }

    @Test
    public void equalsFalseTest(){
        roomType = new RoomType("test");

        RoomType roomTypeActual = new RoomType("kur");

        assertFalse(roomType.equals(roomTypeActual));
    }


    @After
    public void cleanUp(){
        roomType = null;
    }
}
