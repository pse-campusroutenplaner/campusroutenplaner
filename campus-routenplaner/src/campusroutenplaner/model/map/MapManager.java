package campusroutenplaner.model.map;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;

import campusroutenplaner.Logger;
import campusroutenplaner.model.database.Database;
import org.junit.Assert;

/**
 * The type Map manager.
 */
public class MapManager {


    private Database database;

    private HashMap <Integer, Map> maps;
    private CampusMap campus;



    /**
     * Instantiates a new Map manager.
     */
    public MapManager(CampusMap campus, Database database){
        maps = new HashMap<>();

        this.campus = campus;

        maps.put(0, campus);

        this.database = database;


    }
    /**
     * Load 
     * @throws IOException 
     * 
     */
    
    public void loadDatabase() {
        ArrayList<String[]> currObject = database.getBuildings();
    	for (int i=0;i<currObject.size();i++) {
    	    String[] building = currObject.get(i);
    	    if (building[1].length() < 5) {
    	    	if (building[1].length() == 3) {
    	    		building[1] = "0"+building[1]+"0";
    	    	}
    	    	else if(building[1].split("\\.")[0].length() == 1) {
    	    		building[1] = "0"+building[1];
    	    	}
    	    	else {
    	    		building[1] = building[1] + "0";
    	    	}
    	    }
    	    campus.loadBuilding(building[1], building[2], building[0]);
    	    if (database.checkBluePrinted(building[1])) {
    	    	LinkedList<Building> blist = campus.getBuildings();
    	    	for (int j = 0; j < blist.size(); j++) {
    	    		if (blist.get(j).getNumber().equals(building[1])) {
    	    			blist.get(j).setBlueprinted(true);
    	    		}
    	    	}
    	    }
    	}
    	currObject = database.getFloors();
        for (int i=0; i<currObject.size(); i++) {
            String[] floor = currObject.get(i);
            if (floor[2] != null && floor[2].length() < 5) {
                if (floor[2].length() == 3) {
                    floor[2] = "0"+floor[2]+"0";
                }
                else if(floor[2].split("\\.")[0].length() == 1) {
                    floor[2] = "0"+floor[2];
                }
                else if(floor[2].split("\\.")[1].length() == 1) {
                    floor[2] = floor[2] + "0";
                }
            }
            FloorMap newFloor = new FloorMap(Integer.parseInt(floor[1]), floor[2], Integer.parseInt(floor[0]));
            try {
                 campus.searchBuildingByNumber(floor[2]).addFloor(newFloor);
                 maps.put(Integer.parseInt(floor[1]), newFloor);
                System.out.println("Created: "+ newFloor + " id: "+ Integer.parseInt(floor[1]));

            } catch (NullPointerException e) {
            	System.out.println("Couldnt add floor, because building "+floor[2]+" doesnt exist");
            }
        }
        ArrayList<String[]> bgs = database.getBackgrounds();
        for (int i=0; i<bgs.size(); i++) {
            String[] bgArray = bgs.get(i);
            BufferedImage background = database.getBackground(Integer.parseInt(bgArray[3]));
            Background bg = new Background(Double.parseDouble(bgArray[2]),Double.parseDouble(bgArray[1]),background,Double.parseDouble(bgArray[4]));
            try {
                Map map = maps.get(Integer.parseInt(bgArray[3]));
            	map.setBackground(bg);
            } catch (Exception e) {
            	System.out.println("Error, "+bgArray[0]+" couldn't be loaded.");
            	e.printStackTrace();
            }
        }
        currObject = database.getVertices();
        for (int i=0; i<currObject.size(); i++) {
            String[] vertex = currObject.get(i);
    	    if (vertex[5] != null && vertex[5].length() < 5) {
    	    	if (vertex[5].length() == 3) {
    	    		vertex[5] = "0"+vertex[5]+"0";
    	    	}
    	    	else if(vertex[5].split("\\.")[0].length() == 1) {
    	    		vertex[5] = "0"+vertex[5];
    	    	}
    	    	else if(vertex[5].split("\\.")[1].length() == 1) {
    	    		vertex[5] = vertex[5] + "0";
    	    	}
    	    }
            VertexType type;
            if (vertex[3].equalsIgnoreCase("stairs")) {
            	type = new StairsType();
            }
            else if (vertex[3].equalsIgnoreCase("elevator")) {
            	type = new ElevatorType();
            }
            else if (vertex[3].equalsIgnoreCase("entrance")) {
            	type = new EntranceType(vertex[5]);
            }
            else if (vertex[3].equalsIgnoreCase("room")) {
            	type = new RoomType(vertex[6]);
            }
            else {
            	type = new DefaultType();
            }
            Vertex vertexObject = new Vertex(Double.parseDouble(vertex[1]), Double.parseDouble(vertex[2]), Integer.parseInt(vertex[0]), type, Integer.parseInt(vertex[4]));

            Map map = maps.get(Integer.parseInt(vertex[4])); 
            if(map == null){
            	System.out.println("loadDatabse: mapId not existing for vertexID: " + Integer.parseInt(vertex[0]));
            	continue;
            	
            }
            map.getGraph().addVertex(vertexObject);
			if (vertex[3].equalsIgnoreCase("Entrance")) {
				try {

				    //if the vertex is in the building
				    if(Integer.parseInt(vertex[4]) == 0){
                        campus.searchBuildingByNumber(vertex[5]).addEntrance(vertexObject);
                    }

					int reprID = database.getReprID(Integer.parseInt(vertex[0]));
					Vertex repr = getVertex(Integer.parseInt(database.getVertex(reprID)[4]), reprID);
					if (repr != null) {
						((EntranceType) type).setRepresentant(repr);
						((EntranceType) repr.getType()).setRepresentant(map.getGraph().getVertex(Integer.parseInt(vertex[0])));

					}
				} catch (Exception e) {
					System.out.println("A vertex points to a not existing vertex in building: "+vertex[5]);
                    System.out.println("Vertex representant needs to be loaded first");
                }
			}
        	}
        
        	currObject = database.getEdges();
        	for (int i=0; i < currObject.size(); i++) {
        		String[] edge = currObject.get(i);
        		String[] v1 = database.getVertex(Integer.parseInt(edge[3]));//Fehleranf?lligkeit: Wenn die KnotenIDs nicht existieren gibtsn Problem
        		String[] v2 = database.getVertex(Integer.parseInt(edge[4]));
        		Vertex vertex1 = getVertex(Integer.parseInt(v1[4]), Integer.parseInt(v1[0]));
        		Vertex vertex2 = getVertex(Integer.parseInt(v2[4]), Integer.parseInt(v2[0]));
        		boolean access = true;
        		if (edge[2].equals("0")) {
        			access = false;
        		}
        		Edge currEdge = new Edge(Integer.parseInt(edge[0]),vertex1,vertex2, Double.parseDouble(edge[1]) ,access,false);
        		Map map = maps.get(Integer.parseInt(v1[4]));
        		
        		if(map == null){
        			System.out.println("loadDatabase: cannot add edge to non existing floor");
        			continue;
        		}
        		
       
        		map.getGraph().addEdge(currEdge);
                vertex1.getNeighbours().add(currEdge);
                vertex2.getNeighbours().add(currEdge);
        		if (!v1[4].equals(v2[4])) {
        			maps.get(Integer.parseInt(v2[4])).getGraph().addEdge(currEdge);
        		}
        	}
        	
        	currObject = database.getConstructionSites();
            for (int i=0; i < currObject.size(); i++) {
                String[] constrSite = currObject.get(i);
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                Date currDate = new Date();
                try {
					if (currDate.before(df.parse(constrSite[1]))) {
						continue;
					}
				} catch (Exception e) {
					System.out.println("Error, DateFormat in ConstructionSite wrong: "+constrSite[1]);
				}

                int edgeId = Integer.parseInt(constrSite[3]);
                
                for(Map map : maps.values()){
                	Edge edge = map.getGraph().getEdge(edgeId);
                	
                	if(edge == null){
                		continue;
                	}
                	
                	edge.setBlocked(true);
                	
                	
                	
                }
            }
            currObject = database.getAliases();
            for (int i=0; i < currObject.size(); i++) {
			String[] alias = currObject.get(i);
			try {
				int floorID = database.getVertexFloor(Integer.parseInt(alias[0]));
				VertexType v = getVertex(floorID, Integer.parseInt(alias[0])).getType();
				((RoomType) v).addAlias(alias[1]);
				v.toString();
			} catch (Exception e) {
				System.out.println("Can't load alias to VertexID: " + alias[0]);
			}
            }
               
            
    }






   /**
    * adds a vertex with
    * @param mapId
    * @param xPos
    * @param yPos
    * @return null, when this mapId is not existing
    *
    *<p>
    *campusMap has id 0
    */
   public Vertex addVertex(int mapId, double xPos, double yPos){

	   Map map;



       int id = database.addVertex(xPos, yPos, new DefaultType().getType()  ,mapId);



	   Vertex vert = new Vertex(xPos, yPos, id, new DefaultType(), mapId);


	   if(mapId == 0){
		  map = campus;
	   }else{
		  map = maps.get(mapId);
	   }


	   if(map == null){
           System.out.println("Map with such id is not existing");
           return null;
	   }


	   map.getGraph().addVertex(vert);




	   return vert;


   }


   /**
    * deletes vertex with mapId and id;
    * @return the id of the deleted vertex; -1 if not exisitng
    */
   public int deleteVertex(int mapId, int id){

       Map map = maps.get(mapId);



       Vertex vert = map.getGraph().getVertex(id);

       if(vert == null){
           System.out.println("Cannot delete unexisting vertex");
           return -1;
       }



       if(vert.getNeighbours().size() != 0) {
           System.out.println("Cannot delete vertex with edges");
           return -1;
       }


       VertexType type = vert.getType();
       if(type instanceof RoomType){

           for(String alias: ((RoomType) type).getAliases()){

               database.deleteAlias(alias);
           }
       }

	   map.getGraph().deleteVertex(id);
       vert = null;




       database.deleteVertex(id);



	   return id;
   }


    /**
     * changes the vertex
     * @param mapId
     * @param id
     * @param xPos
     * @param yPos
     * @param type
     * @return null if map or vertex not existing
     */
    public Vertex changeVertex(int mapId, int id, double xPos, double yPos, VertexType type ){
        Map map = maps.get(mapId);

        if(map == null){
            return null;
        }

        Vertex vert = map.getGraph().getVertex(id);


        if(vert == null){
            return null;
        }
        
        
        if(type instanceof RoomType && vert.getMapId() == 0){
        	System.out.println("Cannot make room vertex on campus");
        	return null;
        }


        double old = vert.getXPos();

        vert.setXPos(xPos);
        vert.setYPos(yPos);



        if(!type.equals(vert.getType()) && (vert.getType() instanceof  EntranceType) ){
        	System.out.println("Changing type from "+vert.getType() + " to  " + type);
        	
        	String buildingNumber = ((EntranceType) vert.getType()).getBuildingNumber();
        	//remove from building entrances
        	Building build = campus.searchBuildingByNumber(buildingNumber);
             if(build != null) {       	 
            	
                 boolean removed = build.getEntrances().remove(vert);
          
                 if(removed) System.out.println(" Removed from entrances: " + vert.getId());
               }
             
             
             //represenator change
             if(((EntranceType) vert.getType()).getRepresentant()!=null){ 
            	 
                 Vertex repres = ((EntranceType) vert.getType()).getRepresentant();
                 EntranceType represType = (EntranceType) repres.getType();

             
                 System.out.println("	Representator hat id: " + repres.getId());
                 Edge nullEdge = vert.getEdgeBetween(repres);
                                  
                 System.out.println("	Null Edge to be deleted: "+ nullEdge);

                 //0 edge delete, if it exists
                 if(nullEdge != null){
                     deleteEdge(vert.getMapId(), nullEdge.getId());
                 }
                 
                 
                
                 
                 //representator is null
                 represType.setRepresentant(null);
                 System.out.println("	Representator changed: " + repres);



                 //representator ist null
                 ((EntranceType) vert.getType()).setRepresentant(null);
                               
             }
             
        }



        if((!type.equals(vert.getType())) &&     (vert.getType() instanceof StairsType || vert.getType() instanceof ElevatorType)){


            for(Edge edge : vert.getNeighbours()){

                if(edge.getDest().getMapId() != edge.getSource().getMapId()){
                    System.out.println("deleteing edge between floors: "+ edge);
                    deleteEdge(edge.getSource().getMapId(), edge.getId());
                }


            }
        
        }




        //deleting all aliases
        if(vert.getType() instanceof RoomType){
        	
            for(String alias : ((RoomType) vert.getType()).getAliases()){
            	database.deleteAlias(alias);
            }
        }


        //setting the type
        vert.setType(type);


      


        //calculating new edge lenghts
        if(old != vert.getXPos()){
            for(Edge e : vert.getNeighbours()){
                double lenght;


                Vertex vertexSource = e.getSource();
                Vertex vertexDest = e.getDest();




                double x = Math.abs(vertexSource.getXPos() - vertexDest.getXPos()) * maps.get(vertexSource.getMapId()).getBackground().getScale();
                double y = Math.abs(vertexSource.getYPos() - vertexDest.getYPos()) * maps.get(vertexSource.getMapId()).getBackground().getScale();
                lenght = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));

                e.setLength(lenght);
                System.out.println("Changed: "+ e);
                database.changeEdge(e.getSource().getId(), e.getDest().getId(), lenght, e.isAccessible(), e.getId());
            }
        }


        if(type instanceof RoomType){
        	 
        	//adding all aliases
       // 	if(aliases != null){
    //    		((RoomType) vert.getType()).setAliases(aliases);
     //        }
        	
        	//adding aliases to database
        	for(String aliasNew : ((RoomType) type).getAliases()){
        		 database.addAlias(id, aliasNew);
        	}
        	
        	
   
            database.changeVertex(xPos, yPos, type.getType(), mapId, id, ((RoomType) type ).getName() , -1);
        }else if(type instanceof EntranceType){


        
            database.changeVertex(xPos, yPos, type.getType(), mapId, id, null, Double.parseDouble((((EntranceType)getVertex(mapId,id).getType()).getBuildingNumber())));
            Building build = campus.searchBuildingByNumber(((EntranceType) type).getBuildingNumber());
            if(!build.getEntrances().contains(vert) && vert.getMapId() == 0){
            	build.addEntrance(vert);	
            }
            
        }else{
            database.changeVertex(xPos, yPos, type.getType(), mapId, id, null, -1);
        }
        
        System.out.println("Changed: " + vert);
        return vert;
    }


    
    

    /**
     * get vertex with
     * @param mapId
     * @param vertexId
     * @return null if vertex does not exist
     */
	public Vertex getVertex(int mapId, int vertexId){
		 Map map = maps.get(mapId);
	        
	        if(map == null){
	        	return null;
	        }
	        
	        
	       return map.getGraph().getVertex(vertexId);
	}
	
	public Background saveBackgroundImage(int mapID, BufferedImage img) {
		database.addBackground(0, 0, 0, img, mapID);
		Background bg = new Background(0, 0, img);
		maps.get(mapID).setBackground(new Background(0.0,0.0,img));
		return bg;
	}


	/** adds an edge
	 * @param sourceMapId
	 * @param sourceVertexId
	 * @param destMapId
	 * @param destVertexId
	 * @return null if mapIds are not existing, or if there are no vertices with those Id s; null if these edge is already there
	null if you try to connect to vertices with different mapIds, and not Stairs or elevator
	 */
   public Edge addEdge(int sourceMapId, int sourceVertexId, int destMapId, int destVertexId){

	  
	   if(!maps.containsKey(sourceMapId) || !maps.containsKey(destMapId)){
           System.out.println("addEdge: no such source and destMapIds");
           return null;
	   }

	   

	   Vertex vertexSource = maps.get(sourceMapId).getGraph().getVertex(sourceVertexId);
	   Vertex vertexDest = maps.get(destMapId).getGraph().getVertex(destVertexId);

	   if(vertexDest == null || vertexSource == null) {
           System.out.println("Trying to create vertex, but vertexDest or verteSource is null");
           return null;
	   }


       //check if edge is existing
       if(vertexSource.getEdgeBetween(vertexDest) != null){
           System.out.println("Edge exists");
           return null;
       }



       double lenght;
       boolean accessible = true;


       double x = Math.abs(vertexSource.getXPos() - vertexDest.getXPos()) * maps.get(sourceMapId).getBackground().getScale();
       double y = Math.abs(vertexSource.getYPos() - vertexDest.getYPos()) * maps.get(sourceMapId).getBackground().getScale();
       lenght = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));




	   //vertices between two maps can one only connect, when they are both either Elevetar, or Stairs
	   if(destMapId != sourceMapId){

           System.out.println("Trying to create edge between " + sourceVertexId + " and "+ destVertexId);
           System.out.println(vertexSource);
           System.out.println(vertexDest);



           if (vertexSource.getType() instanceof  ElevatorType){

               if (!(vertexDest.getType() instanceof ElevatorType)){
                   System.out.println("second is not Elevator");
                   return null;
			   }

               //1 minute
               lenght = 10;
               

		   }else if(vertexSource.getType() instanceof StairsType) {


               if (!(vertexDest.getType() instanceof StairsType) ) {
                   System.out.println("second is not stairs");
                   return null;
               }

               //shano
               double temp = 9 + lenght*lenght;
               lenght = Math.sqrt(temp);
               accessible = false;

		   }else if(vertexSource.getType() instanceof EntranceType && vertexDest.getType() instanceof EntranceType){






                if(vertexSource.getMapId() == 0){

                    System.out.println("source.mapId is 0");

                    Map theFloorOfTheVertex = maps.get(vertexDest.getMapId());

                    if(((FloorMap) theFloorOfTheVertex).getFloorNumber() != 0){
                        System.out.println("floorNumber von dest ist not 0");
                        return null;
                    }


                    //set the representatnt for the source vertex
                    ((EntranceType) vertexSource.getType()).setRepresentant(vertexDest);



                    ((EntranceType) vertexDest.getType()).setRepresentant(vertexSource);
                    lenght = 0;
   
                }else if(vertexDest.getMapId() == 0){





                    System.out.println("dest.mapId is 0");

                    Map theFloorOfTheVertex = maps.get(vertexSource.getMapId());

                    if(((FloorMap) theFloorOfTheVertex).getFloorNumber() != 0){
                        System.out.println("floorNumber von dest ist not 0");
                        return null;
                    }


                    //set the representatnt for the source vertex
                    ((EntranceType) vertexSource.getType()).setRepresentant(vertexDest);



                    ((EntranceType) vertexDest.getType()).setRepresentant(vertexSource);
                    lenght = 0;
                    
                }else{
                	System.out.println("not possible");
                    return null;
                }



           }
           else{
               System.out.println("DEFAULT");
               return null;
           }


	   }else{

	       if(vertexDest.getType() instanceof EntranceType && vertexSource.getType() instanceof  EntranceType){
               System.out.println("Cannot create both Entrances on campus");
               return null;
           }

       }
	   
	   
	   
	  



	   Map map = maps.get(destMapId);



       int id = database.addEdge(sourceVertexId, destVertexId, lenght, true);


	   Edge edge = new Edge(id, vertexSource, vertexDest);
	   
       edge.setLength(lenght);

       edge.setAccessible(accessible);

	   map.getGraph().addEdge(edge);



	   if(destMapId != sourceMapId){
		   Map map2 = maps.get(sourceMapId);
		   map2.getGraph().addEdge(edge);
	   }


	   //adding him to neighbours
	   vertexSource.getNeighbours().add(edge);
	   vertexDest.getNeighbours().add(edge);



       return edge;
   }


    /**
     *
     * @param mapId
     * @param edgeId
     * @return edgeId, or -1 on error
     */
   public int deleteEdge(int mapId, int edgeId){
	   Map map = maps.get(mapId);

       if(map == null){
           return -1;
       }

	   Edge edge = map.getGraph().getEdge(edgeId);

       if(edge == null){
           return -1;
       }


	   Vertex vertSource = edge.getSource();
	   Vertex vertDest = edge.getDest();


	//   if(vertSource == null || vertDest == null){
	//	   return -1;
	//   }


	   vertSource.getNeighbours().remove(edge);
	   vertDest.getNeighbours().remove(edge);

	   map = maps.get(vertSource.getMapId());
	   map.getGraph().deleteEdge(edge.getId());

       database.deleteEdge(edgeId);


	   if(vertSource.getMapId() != vertDest.getMapId()){
		   map = maps.get(vertDest.getMapId());
		   map.getGraph().deleteEdge(edge.getId());



           if(vertDest.getType() instanceof EntranceType && vertSource.getType() instanceof EntranceType){
                ((EntranceType) vertDest.getType()).setRepresentant(null);
                 ((EntranceType) vertSource.getType()).setRepresentant(null);
           }
	   }


       edge = null;
	   return edgeId;
   }


    /**
     * get the Edge with
     * @param mapId and
     * @param edgeId
     * @return the edge;
     *
     * <p>
     *     if this edge is between the mapIds, null will be returned
     * </p>
     */
    public Edge getEdge(int mapId, int edgeId){
        Map map = maps.get(mapId);

        if(map == null){
            System.out.println("map is null");
            return null;
        }

        Edge edge = map.getGraph().getEdge(edgeId);


        return edge;
    }


    /**
     * change Edge with
     * @param mapId and
     * @param edgeId
     * @param lenght
     * @param accessible
     * @param blocked
     * @return null if edge not found
     *

     */
    public Edge changeEdge(int mapId, int edgeId, double lenght, boolean accessible, boolean blocked){
        Map map = maps.get(mapId);

        if(map == null){
            System.out.println("Map not found. id: "+mapId);
            return null;
        }

        Edge edge = map.getGraph().getEdge(edgeId);

        if(edge == null){
            System.out.println("edge with mapId:" + mapId + " and id: "+ edgeId + " is null");
            return null;
        }


        edge.setAccessible(accessible);
        edge.setBlocked(blocked);
        edge.setLength(lenght);

        System.out.println("Changed: "+ edge);

        database.changeEdge(edge.getSource().getId(), edge.getDest().getId(), lenght, accessible, edgeId);

        return edge;
    }












   /**
    * get vertexIds for this map with
    * @param mapId
    * @return LinkedList<Integer>; returns null, when mapId not existing
    */
   public LinkedList<Integer> getVertices(int mapId){
       if(maps.containsKey(mapId)){
           return maps.get(mapId).getGraph().getVerticesIds();
       }else{
           return null;
       }


   }

  //  public LinkedList<Integer> getEdgeList(int mapId){
  //      return maps.get(mapId).getGraph().getEdgesIds();
  //  }


    /**
     *
     * @param mapId
     * @return returns null, when mapId not existing
     */
    public LinkedList<Integer> getEdgeList(int mapId){
        if(maps.containsKey(mapId)){
            return maps.get(mapId).getGraph().getEdgesIds();
        }else{
            return null;
        }

    }

    /**
     * creates a floor and puts it in the map list
     * @param numberBuilding
     * @return the floor (mapId);
     */
	public int addFloor(String numberBuilding, int floorNumber){
	

        int mapId = database.addFloor(floorNumber, numberBuilding);

        FloorMap floor = new FloorMap(mapId, numberBuilding, floorNumber);


        maps.put(mapId, floor);



        System.out.println("Created: "+floor + " mapId:"+ mapId);

        return mapId;
	}


	/**
	 * 
	 * @param floorId
	 * @return -1 on error
	 */
    public int  removeFloor(int floorId){
        if(floorId == 0){
            Logger.print("Cannot delete campus map");
            return -1;
        }



        //every edge
        LinkedList<Integer> edgesId = getEdgeList(floorId);

        for(int edgeId : edgesId){

            deleteEdge(floorId, edgeId);
        }


        //every vertex
        LinkedList<Integer> verticesId =  getVertices(floorId);
        for(int vertexId : verticesId){
            deleteVertex(floorId, vertexId);
        }


        deleteBackground(floorId);
        database.deleteFloor(floorId);

        Map map = maps.get(floorId);
        System.out.println("Deleted:"+(FloorMap)map + " mapId: "+ floorId);
        maps.remove(floorId);
        map = null;

        return 0;
    }
    
  


    
    
 



    public Map getMap(int mapId){
        Map map = maps.get(mapId);

        if(map == null){
            return null;
        }else{
            return map;
        }
    }

    


    public Background getBackground(int mapId) {
        Map map = maps.get(mapId);

        if (map == null) {
            System.out.println("Map not existing");
            return null;
        }

        return map.getBackground();
    }
   
    
    


    public Background changeBackground(int mapId, BufferedImage picture, double hight, double width, double scale){
        
    	Background bg = getBackground(mapId);

        if(bg == null){
           return null;
        }


        
        
       bg.setHight(hight);
       bg.setPicture(picture);
       bg.setWidth(width);
       
       if(bg.getScale() != scale ){
           bg.setScale(scale);
           changeEdgeLenghts(mapId);
       }

       
       database.changeBackground(width, hight, scale, picture, mapId);
        return bg;
    }



   
	public void deleteBackground(int mapId){

        Background bg = getBackground(mapId);

        if(bg == null){
            System.out.println("Cannot delete unexisting background");
        }

        database.deleteBackGround(mapId);

        Map map = getMap(mapId);
        if(map == null){
            return;
        }
        map.setBackground(null);
    }
	
	
	 private void changeEdgeLenghts(int mapId) {
			//map is not null

        Assert.assertNotEquals(0, mapId);

		 Map map = maps.get(mapId);

         Assert.assertTrue(map!=null);
		 
		 for(Edge edge : map.getGraph().getEdges()){
			 
			 if(edge.getSource().getMapId() != edge.getDest().getMapId()){
				 continue;
			 }
			 
			 Vertex vertexSource = edge.getSource();
			 Vertex vertexDest = edge.getDest();
			 
			 double x = Math.abs(vertexSource.getXPos() - vertexDest.getXPos()) * maps.get(mapId).getBackground().getScale();
		     double y = Math.abs(vertexSource.getYPos() - vertexDest.getYPos()) * maps.get(mapId).getBackground().getScale();
		     double lenght = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));


             changeEdge(vertexSource.getMapId(), edge.getId(), lenght, edge.isAccessible(), edge.isBlocked());


			 
			 
		 }
			
	}



	public int deleteVerticesAndEdges(int floorId){
        if(floorId == 0){
            Logger.print("Cannot delete campus map");
            return -1;
        }



        //every edge
        LinkedList<Integer> edgesId = getEdgeList(floorId);

        for(int edgeId : edgesId){

            deleteEdge(floorId, edgeId);
        }


        //every vertex
        LinkedList<Integer> verticesId =  getVertices(floorId);
        for(int vertexId : verticesId){
            deleteVertex(floorId, vertexId);
        }
        return 1;
    }
    




}