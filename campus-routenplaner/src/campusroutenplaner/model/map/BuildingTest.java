package campusroutenplaner.model.map;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.LinkedList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;

import campusroutenplaner.model.database.Database;

public class BuildingTest {

	private CampusMap campMap;
	private Building testBuilding;
	private Building testBuildingTwo;
	private FloorMap testFloor;
	private LinkedList<ArrayList<Integer>> testFlooor;
	private LinkedList<ArrayList<Integer>> test;
	private LinkedList<Vertex> entrances;
	private ArrayList<Integer> testarray, testArrayTwo, testArrayThree, testArrayFour;
	private LinkedList<Vertex> expectedentrances;
	private Vertex vertex;
	private String expectedAddress, expectedAddressTwo;
	private int i, j;
	private Building  testBuildingThree;
	private Building defaultBuilding;
	private Building defaultBuildingTwo;
	private Building defaut;
	private Database data;
	private String adress;


	
	
	
	/**
	 * all temporary variables initialize 
	 */
	@Before public void beforeTest() {
		
		data = new Database("test/model/KIT(testCopy).db");

		campMap = new CampusMap(data);
		
		
		
		testBuilding = new Building("50.34", "Informatikgebäude", "am Fasanengarten 5");
		testBuilding.setBlueprinted(true);
		testBuildingTwo = new Building("50.35", "Informatik", "Fasanengarten");
		testFloor = new FloorMap(4, "50.34", 3);
		  testFlooor = new LinkedList<ArrayList<Integer>>();
		  testarray = new ArrayList<Integer>();
		  testArrayTwo = new ArrayList<Integer>();
		  testArrayThree = new ArrayList<Integer>();
		  testArrayFour = new ArrayList<Integer>();
		  test = new LinkedList<ArrayList<Integer>>();
		  entrances = new LinkedList<Vertex>();
		  expectedAddress = "Am Fasanengarten 5.0";
		  expectedAddressTwo = "am Fasanengarten 5";
		  testarray.add(4);
		  testarray.add(3);
		  testArrayTwo.add(4);
		  testArrayTwo.add(3);
		  testArrayThree.add(4);
		  testArrayThree.add(3);
		  testArrayFour.add(4);
		  testArrayFour.add(3);
		  testFlooor.add(testarray);
		  testFlooor.add(testArrayTwo);
		  testFlooor.add(testArrayThree);
		  testFlooor.add(testArrayFour);
		  expectedentrances = new LinkedList<Vertex>();
		  vertex = new Vertex(12.3, 13.2, 2, 0);
		  expectedentrances.add(vertex);
		  testBuildingTwo = new Building("50.50", "Telematikgebäude", "am Fasanengarten 6");
		  i = 0;
		  j = 0;
		   testBuildingThree = new Building("50.50"); 
		   defaultBuilding = new  Building("55.55", "name");
		   defaultBuildingTwo = new  Building("55.75", "nam44e");
		          
	}
	
	@Test public void addFloor() {
		
		while (i < 4) {
			testBuilding.addFloor(testFloor);
			i++;
		}


		
		LinkedList<ArrayList<Integer>> expectedFloors = new LinkedList<ArrayList<Integer>>();
		expectedFloors.add(testarray);
		expectedFloors.add(testArrayTwo);
		expectedFloors.add(testArrayThree);
		expectedFloors.add(testArrayFour);
		assertEquals(expectedFloors, testBuilding.getFloors());
		expectedFloors = null;

	}
	
	@Test public void getNumber() {
		String expectedNumber = "50.34";
		assertEquals(expectedNumber, testBuilding.getNumber());
		expectedNumber = null;
	}
	
	@Test public void setNumber() {
		String expectedNumber = "44.44";
		testBuilding.setNumber("44.44");
		assertEquals(expectedNumber, testBuilding.getNumber());
		expectedNumber = null;
	
	}
	
	@Test public void addEntrance() {
		LinkedList<Vertex> expectedListOfEntrances = new LinkedList<Vertex>();
		expectedListOfEntrances.add(vertex);
	    testBuilding.addEntrance(vertex);
	    assertEquals(expectedListOfEntrances, testBuilding.getEntrances());
	    expectedListOfEntrances = null;

	}
	
	@Test public void getName() {
		String expectedName = "Informatikgebäude";
		assertEquals(expectedName, testBuilding.getName());
		expectedName = null;
	}
	
	@Test public void setName() {
		String expectedName = "Infogebäude";
		testBuilding.setName("Infogebäude");
		assertEquals(expectedName, testBuilding.getName());	
		expectedName = null;
	}
	
	
	
	@Test public void getAddress() {
		assertEquals(expectedAddressTwo, testBuilding.getAddress());
	}
	
	@Test public void setAddress() {

		testBuilding.setAddress("Am Fasanengarten 5.0");
		assertEquals(expectedAddress, testBuilding.getAddress());
	}
	
	@Test public void setFloors() {	
		ArrayList<Integer> floor  = new ArrayList<>();

		floor.add(4);
		floor.add(3);
		testFlooor.add(floor);
		testBuilding.setFloors(testFlooor);
		assertEquals(testFlooor, testBuilding.getFloors());
		

	}
	
	@Test public void getEntrances() {
		testBuilding.addEntrance(vertex);
		assertEquals(expectedentrances, testBuilding.getEntrances());
	}
	
	@Test public void setEntrance() {
		testBuildingTwo.setEntrances(expectedentrances);
		assertEquals(expectedentrances, testBuildingTwo.getEntrances());
	}
	
	
	@Test public void isBlueprinted() {
		testBuilding.setBlueprinted(true);
		assertEquals(true, testBuilding.isBlueprinted());
	}
	
	@Test public void toTOString() {
		String expectedString = "Building [number = " + "50.34" + ", name = " + "Informatikgebäude" + ", address = " + "am Fasanengarten 5" + ", floors = " + test
				+ ", entrances = " + entrances + ", blueprinted = " + true + "]";
		assertEquals(expectedString, testBuilding.toString());
		expectedString = null;
	}
	
	@Test public void numberEquals() {
		
		assertEquals(false, testBuilding.equals(testBuildingTwo));
	}
	
	
	@Test
	public void checkEquals() {
		Building testBuilding = campMap.addBuilding("50.05");
				
		Building building = new Building("50.05", adress, adress);

		
	
		assertFalse(building.equals(testBuilding));
	
	}
	
	
	
	

	/**
	 * delete all temporary variables
	 */
	@After public void deleteALlTestObjects() {
		testBuilding = null;
		testFloor = null;
		testBuildingTwo = null;
		testFlooor = null;
		test = null;
		testarray = null;
		expectedentrances = null;
		entrances = null;
		expectedAddress = null;
		expectedAddressTwo = null;
		testBuildingThree = null;
		data.forgetDB();
	}
	

}
