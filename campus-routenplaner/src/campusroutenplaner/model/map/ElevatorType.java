/**
 * 
 */
package campusroutenplaner.model.map;


public class ElevatorType extends VertexType{

	@Override
	public String toString() {
		return "elevator";
	}

	public String getType(){
        return "Elevator";
    }

	@Override
	public boolean equals(Object o) {
		return o instanceof ElevatorType;
	}




}
