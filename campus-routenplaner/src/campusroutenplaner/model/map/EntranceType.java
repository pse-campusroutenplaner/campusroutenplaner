package campusroutenplaner.model.map;

public class EntranceType extends VertexType {

	private String buildingNumber;
	private Vertex representant;
	
	
	public EntranceType(String buildingNumber) {
		if(buildingNumber == null){
			this.buildingNumber = "00.00";
		}else{
			this.buildingNumber = buildingNumber;
		}

	}

	
	public EntranceType(String buildingNumber, Vertex representant){

	    if(buildingNumber == null){
			this.buildingNumber = "00.00";
		}else{
			this.buildingNumber = buildingNumber;
		}


		this.representant = representant;

	}
	
	
	
	@Override
	public String toString() {
		return "entrance to "+ buildingNumber + " and representantId: " + ((representant == null ) ? "n/a" : representant.getId());
	}

	public String getType(){
		return "Entrance";
	}

	public String getBuildingNumber() {

        return buildingNumber;

	}

	public void setBuildingNumber(String buildingNumber) {
        if(buildingNumber == null){

        }else{
            this.buildingNumber = buildingNumber;
        }

    }

	public Vertex getRepresentant() {
		return representant;
	}

	protected void setRepresentant(Vertex representant) {

            this.representant = representant;

	}


	@Override
	public boolean equals(Object o) {


		if(!(o instanceof EntranceType)){
			return false;
		}


		EntranceType type = (EntranceType) o;

		if(!type.getBuildingNumber().equals(this.buildingNumber)){
			return false;
		}


        if(type.getRepresentant() == null){
            return this.representant == null;
        }else{
            return type.getRepresentant() .equals(this.representant);
        }




	}

}
