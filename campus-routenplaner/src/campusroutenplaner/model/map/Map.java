package campusroutenplaner.model.map;

public abstract class Map {

	private Graph graph;
	private Background backgroundImage;
	private int mapId;
	
	public Map(Graph graph, Background backgroundImage, int mapId) {
		this.graph = graph;
		this.backgroundImage = backgroundImage;
		this.mapId = mapId;

	}

	public Graph getGraph() {
		return graph;
	}

	public Background getBackground() {
		return backgroundImage;
	}

	public void setBackground(Background backgroundImage) {
		this.backgroundImage = backgroundImage;
		
	}

	public int getMapId() {
		return mapId;
	}

	public void setMapId(int mapId) {
		this.mapId = mapId;
	}


    public void setGraph(Graph graph) {
        this.graph = graph;
    }
}
