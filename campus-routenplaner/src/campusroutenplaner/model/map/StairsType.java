package campusroutenplaner.model.map;

public class StairsType extends VertexType {

	@Override
	public String toString() {
		return "stairs";
	}

	public String getType(){
		return "Stairs";
	}


	@Override
	public boolean equals(Object o) {
		return o instanceof StairsType;
	}

}
