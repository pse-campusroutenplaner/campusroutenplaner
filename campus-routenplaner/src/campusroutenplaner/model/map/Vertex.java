/**
 * 
 */
package campusroutenplaner.model.map;

import org.hamcrest.core.StringStartsWith;

import java.util.LinkedList;

/**
 * @author nmladenov
 *
 */
public class Vertex {
	
	//unique for this mapId
	private int id;
	
	
	//x and y position for the Vertex
	private double xPos;
	private double yPos;
	
	
	
	//neighbours vertices
	private LinkedList<Edge> neighbours;
	private VertexType type;
	private int mapId;
	

	/**
	 * default Vertex with id,xPos,yPos, mapId = 0
	 */
	public Vertex(){
		
		//neighbours
		neighbours = new LinkedList<>();
		
		
		//default type
		this.type = new DefaultType();
		

	
	}
	/**
	 * creates a new Vertex with default type and 
	 * @param xPos
	 * @param yPos
	 * @param id
	 * @param mapId
	 * 
	 * <p>
	 * xPos, yPos and id, mapId have to be >= 0; if an unvalid value is given, then automatically 0 (default Vertex)
	 */
	public Vertex(double xPos, double yPos, int id, int mapId){
		
		
		if (xPos < 0) {
			this.xPos = 0;
		} else{
			this.xPos = xPos;
		}
		
		
		if(yPos < 0) {
			this.yPos = 0;
		} else{
			this.yPos = yPos;
		}
		
		
		if(id < 0) {
			this.id = 0;
		} else{
			this.id = id;
		}
		
		
		if(mapId < 0) {
			this.mapId = 0;
		} else{
			this.mapId = mapId;
		}
		//neighbours
		neighbours = new LinkedList<>();
		//default type
		this.type = new DefaultType();
	}
	
	
	/**
	 * creates a new Vertex with
	 * @param xPos
	 * @param yPos
	 * @param id
	 * @param type
	 * @param mapId
	 * 
	 * <p>
	 * xPos, yPos and id, mapId have to be >= 0; if an unvalid value is given, then automatically -1;
	 * <p>
	 * if type is null, then default type is chosen
	 */
	public Vertex(double xPos, double yPos, int id, VertexType type, int mapId){

		if (xPos < 0){
			xPos = 0;
		}else{
			this.xPos = xPos;
		}
		
		
		if(yPos < 0){
			yPos = 0;
		}else{
			this.yPos = yPos;
		}
		
		
		if(id < 0){
			id = 0;
		}else{
			this.id = id;
		}


		if(mapId < 0){
			this.mapId = 0;
		}else{
			this.mapId = mapId;
		}



		//neighbours
		neighbours = new LinkedList<>();
		
		
		//set the type
		if(type != null){
				this.type = type;
		}else{
				this.type = new DefaultType();
		}
	}
	

	

	
	
	
	
	
	
	/**
	 * sets the xPos for this Vertex
	 * @param xPos has to be >=0 to be valid;
	 *  <p>
	 * if xPos<0, nothing is done
	 */
	protected void setXPos(double xPos){
		
		if(xPos >= 0.0){
			this.xPos = xPos;
		}

	}
	
	
	/**
	 * sets the xPos for this Vertex
	 * @param yPos has to be >=0 to be valid;
	 *<p>
	 * if yPos<0, nothing is done
	 */
	protected void setYPos(double yPos){
		
		if(yPos  >= 0.0){
			this.yPos = yPos;
		}
	}
	
	

	
	

	
	/**
	 * sets the type of this Vertex
	 * @param type
	 * 
	 * <p>
	 * 
	 *
	 * 
	 */
	protected void setType(VertexType type){
		if(type != null){
			this.type = type;
		}
	}
	
	

	
	
	
	/**
	 * getter 
	 * @return xPos for this vertex
	 */
	public double getXPos(){
		return xPos;
	}
	
	
	/**
	 * getter
	 * @return yPos for this vertex
	 */
	public double getYPos(){
		return yPos;
	}
	
	/**
	 * getter
	 * @return id for this vertex
	 */
	public int getId(){
		return id;
	}

	
	/**
	 * getter
	 * @return mapId
	 */
	public int getMapId(){
		return mapId;
	}
	
	
	/**
	 * getter
	 * @return 
	 */
	public LinkedList <Edge> getNeighbours(){
		return neighbours;
	}
	
	
	/**
	 * getter
	 * @return vertexType
	 */
	public VertexType getType(){
		return type;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Vertex)) return false;

		Vertex vertex = (Vertex) o;

		if (id != vertex.id) return false;
		if (xPos != vertex.xPos) return false;
		if (yPos != vertex.yPos) return false;
		if (mapId != vertex.mapId) return false;
		if (neighbours != null ? !neighbours.equals(vertex.neighbours) : vertex.neighbours != null) return false;
        return type != null ? type.equals(vertex.type) : vertex.type == null;

	}





	@Override
	public String toString() {
		String stringStream = "";
		stringStream = "Vertex [id=" + id + ", xPos=" + xPos + ", yPos=" + yPos + ", type="
				+ type + ", mapId=" + mapId + ", neighbourVert=[";
		for (Edge e : neighbours) {
			if(e.getSource().equals(this)){
				stringStream += e.getDest().getId() + ",";
			}else{
				stringStream += e.getSource().getId() + ",";
			}
			
		}
		
		stringStream += "], neighEdges=[";
		
		for (Edge e : neighbours) {
			
				stringStream += e.getId() + ",";
			
			
		}
		return stringStream+"]";
	}

	
	
	
	/**
	 *
	 * @param dest
	 * @return the edge, between this vertex and dest; null if doesnt exist
	 */
	public Edge getEdgeBetween(Vertex dest){

	    if(dest == null){
	        return null;
        }
		
		for(Edge e : neighbours){
			if(dest.neighbours.contains(e)){
				return e;
			}
		}
		
		
		return null;
	}
	
	
	
}
