package campusroutenplaner.model.map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Created by nmlad on 8/4/16.
 */
public class EntranceTypeTest {

    VertexType entrance;

    @Before
    public void setUp(){

    }

    @Test
    public void constructorTestValid(){
        entrance = new EntranceType("50.34");


        assertEquals("Entrance", ((EntranceType) entrance).getType());
        assertEquals("entrance to 50.34 and representantId: n/a", ((EntranceType) entrance).toString());
        assertEquals("50.34", ((EntranceType) entrance).getBuildingNumber() );
        assertEquals(null, ((EntranceType) entrance).getRepresentant() );
    }

    @Test
    public void costructorTestInvalid(){
        entrance = new EntranceType(null);

        assertEquals("entrance to 00.00 and representantId: n/a", ((EntranceType) entrance).toString());
        assertEquals("Entrance", ((EntranceType) entrance).getType());
        assertEquals("00.00", ((EntranceType) entrance).getBuildingNumber() );
        assertEquals(null, ((EntranceType) entrance).getRepresentant() );

    }


    @Test
    public void extendetConstructorValidTest(){
        entrance = new EntranceType("50.34", new Vertex());


        assertEquals("entrance to 50.34 and representantId: 0", ((EntranceType) entrance).toString());
        assertEquals("Entrance", ((EntranceType) entrance).getType());
        assertEquals("50.34", ((EntranceType) entrance).getBuildingNumber() );
        assertEquals(new Vertex(), ((EntranceType) entrance).getRepresentant() );

    }

    @Test
    public void extendetConstructorInvalidTest(){
        entrance = new EntranceType(null, null);



        assertEquals("entrance to 00.00 and representantId: n/a", ((EntranceType) entrance).toString());
        assertEquals("Entrance", ((EntranceType) entrance).getType());
        assertEquals("00.00", ((EntranceType) entrance).getBuildingNumber() );
        assertEquals(null, ((EntranceType) entrance).getRepresentant() );

    }



    @Test
    public void setsValid(){
        entrance = new EntranceType(null);


        ((EntranceType) entrance).setRepresentant(new Vertex());
        ((EntranceType) entrance).setBuildingNumber("50.34");

        assertEquals(new EntranceType("50.34", new Vertex()), entrance);

    }

    @Test
    public void setsInvalid(){
        entrance = new EntranceType("50.34", new Vertex());



        ((EntranceType) entrance).setRepresentant(null);
        ((EntranceType) entrance).setBuildingNumber(null);


        assertEquals(new EntranceType("50.34", null), entrance);

    }



    @Test
    public void equalsOtherTest(){
        entrance = new EntranceType("50.34");

        VertexType temp = new DefaultType();

        assertFalse(entrance.equals(temp));
    }


    @Test
    public void equalsOtherNumberTest(){
        entrance = new EntranceType("50.34");

        VertexType temp = new EntranceType("00.00");


        assertFalse(entrance.equals(temp));

    }


    @After
    public void tearDown(){
        entrance = null;
    }


}
