package campusroutenplaner.model.map;



public abstract class VertexType {
	
	/**
	 * retrun the type of Vertex as String
	 */
	public abstract String toString();


	public abstract String getType();

	public abstract boolean equals(Object o);

}
