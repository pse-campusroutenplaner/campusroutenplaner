package campusroutenplaner.model.map;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Pattern;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import campusroutenplaner.model.database.Database;

public class CampusMapTest {

	private Database data;
	private CampusMap map;
	private Building testBuilding;
	private Building testBuildingTwo;
	private Building searchedBuilding;
	
	
	
	@Before public void beforeTest() {
		data = new Database("test/model/KIT(testCopy).db");
		map = new CampusMap(data);
		testBuilding = map.loadBuilding("50.34", null, "Am Fasanengarten 5a/b, 76131 Karlsruhe");
		testBuildingTwo = new Building("50.34", "Infomatikgebäude", "Am Fasanengarten 5");
		searchedBuilding = new Building("50.90", "", "");
		
		

		//testBuilding.setName("Informatikgebäude");
		//testBuilding.setAddress("Am Fasanengarten 5");


	}
	
	
	
	@Test
	public void addBuilding() {

		Building build = map.addBuilding("50.70");
		
		data.addBuilding("50.70", "", "");
		
		assertEquals(build, build);
		assertEquals(null, map.addBuilding("50.70"));
		
		assertEquals(null, map.addBuilding("50.7065"));
		
		data.deleteBuilding("50.70");
	
	}
	
	
	@Test
	public void removeBuilding() {
		
		Building build = new Building("50.80");
		


		
		assertEquals(OnError.UnSuccessfulremovedFromDatabase, map.removeBuilding("50.34"));
		
		map.getBuildings().add(build);

		data.addBuilding("50.80", "", "");
		//assertEquals(build, map.addBuilding("50.80"));
		
		assertEquals(OnError.SuccessfulRemoved, map.removeBuilding("50.80"));
		
		data.addBuilding("88.88", "", "");
		assertEquals(OnError.NumberDoesNotExist, map.removeBuilding("88.88"));

		
		assertEquals(OnError.UnSuccessfulremovedFromDatabase, map.removeBuilding("88.88"));

		data.deleteBuilding("50.80");

	
	}
	
	
	@Test
	public void searchBuigldingByNumber() {

		map.getBuildings().add(searchedBuilding);
		map.loadBuilding(searchedBuilding.getNumber(), searchedBuilding.getName(), searchedBuilding.getAddress());
		
		assertEquals(searchedBuilding, map.searchBuildingByNumber("50.90") );	
		assertEquals(null, map.searchBuildingByNumber("55.50"));
		
	}
		
		
//	
//	@Test
//	public void assigVertexToBuiglding() {
//		
//
//		Vertex vert = new Vertex();
//		
//		
//		assertEquals(vert, map.assignVertexToBuildingEntrances(vert, searchedBuilding));
//		
//		
//		assertEquals(map.assignVertexToBuildingEntrances(vert, searchedBuilding), map.assignVertexToBuildingEntrances(vert, searchedBuilding));
//		
//		Vertex ver = new Vertex(0, 0, 1, 1);
//		assertNull(	map.assignVertexToBuildingEntrances(vert, searchedBuilding));
//	}
	
	
	@Test
	public void changeBuilding() {
		
		map.getBuildings().add(searchedBuilding);
		
		Building asserts = map.changeBuilding("50.90", "newName", "newAdress");
		
		data.addBuilding("50.34", "", "");
		
		Building two = map.changeBuilding("50.34", "", "");
		
		assertEquals(asserts, map.changeBuilding("50.90", "newName", "newAdress"));
		
		assertEquals(asserts, map.changeBuilding(asserts.getNumber(), asserts.getName(), asserts.getAddress()));
		
		assertEquals(two, map.changeBuilding("50.34", "", ""));
		
		assertEquals(asserts, map.changeBuilding("50.90", "newName", "newAdress"));
		
		assertEquals(asserts, map.changeBuilding("50.90", "45654", "34236526"));
		
		data.deleteBuilding("50.90");
		




		
	}
	
	
	@Test
	public void getEntrances() {
		
		map.getBuildings().add(searchedBuilding);

		assertEquals(searchedBuilding.getEntrances(), map.getEntrances(searchedBuilding.getNumber()));
		
		assertEquals(null, map.getEntrances("50.57"));
	
	}
	
	
	@Test
	public void setBluePrinted() {
		
		map.getBuildings().add(searchedBuilding);
		assertEquals(true, map.setBlueprinted(searchedBuilding.getNumber(), true));
		
		assertEquals(false, map.setBlueprinted("50.66", true));
	}
	
	
	
	
	
	
	
	
	
	
		
		
		
	@Test
	public void searchBuiglding() {
		
		//map.getBuildings().add(searchedBuilding);
	    map.addBuilding("50.90");

		//data.addBuilding(searchedBuilding.getNumber(), searchedBuilding.getName(), searchedBuilding.getAddress());
		assertEquals(searchedBuilding, map.searchBuilding("50.90"));
		
		
		assertEquals(null, map.searchBuilding("wow"));
		

		

		
	}
	
	
	
	@Test
	public void getVertexIdOfBuilding() {
		
		assertEquals(0, map.getVertexIdofRoom("50.34", ""));
		
	}

	
	@Test
	public void loadBuiglding() {
		
		Building newBuild = map.loadBuilding("50.50", "", "");
		Building test = map.loadBuilding("50.34", "", "Am Fasanengarten 5a/b, 76131 Karlsruhe");
		
		
		assertEquals(null, map.loadBuilding("50.444", "", ""));
		
		Building bil = map.loadBuilding("50.44", "", "hallo");
		
		assertEquals(bil, bil);
		
		
		assertEquals(null, map.loadBuilding("50.34", "", "Am Fasanengarten 5a/b, 76131 Karlsruhe"));
		assertEquals(test,  test );
	    
		assertEquals(newBuild, newBuild);
		
	}
	
	
	@Test
	public void checkIfNumberExist() {
		
		
		map.getBuildings().add(searchedBuilding);


		assertEquals(OnError.NumberDoesNotExist, map.checkIfNumberExist("50.54"));
		
		assertEquals(OnError.NumberExist, map.checkIfNumberExist(searchedBuilding.getNumber()));

	
	}
	
	
	@Test
	public void loadBuildingsFromDatabase() {

		Building building = new Building("50.50", "", "");
		data.addBuilding("50.50", "", "");
		assertEquals(building, map.loadBuilding("50.50", "", ""));
		
	
	}
	
	
	@Test
	public void getBuildings() {
		
		assertEquals(map.getBuildings(), map.getBuildings());
	
	}
	
	@Test
	public void getBuildingListFromDataBase() {
		
		ArrayList<String[]> testList = map.getBuildingList();
		
		assertEquals(testList, map.getBuildingList());
	
	}
	
	@Test
	public void toSTring() {
		
		assertEquals(map.toString(), map.toString());
	
	}
	
	@After
	public void aftert() {
		data.forgetDB();
	}

	
	
	
	
	
	
	
	

}
