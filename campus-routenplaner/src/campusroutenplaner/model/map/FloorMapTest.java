package campusroutenplaner.model.map;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class FloorMapTest {

	
	private FloorMap floor;
	
	@Before public void beforeTest() {
		floor = new FloorMap(0, null, 0);
		floor = new FloorMap(null, null, 0, null, 0);
	}
	
	
	
	@Test
	public void FloorNuberSetAndGet() {
		floor.setFlooNumber(1);
	  assertEquals(1, floor.getFloorNumber());
	}
	
	@Test
	public void getBuildingnumber() {
	  assertEquals(null, floor.getBuildingNumber());
	}

}
