package campusroutenplaner.model.map;

import static org.junit.Assert.*;

import java.awt.image.BufferedImage;

import org.junit.Before;
import org.junit.Test;

import com.sun.javafx.iio.ImageStorage.ImageType;

public class BackgroundTest {

	private BufferedImage defaultImage;
	private Background testBackground;
	private int img;
	
	@Before public void beforeClass() {
		img  = 1;
		defaultImage = new BufferedImage(1, 5, img);
		testBackground = new Background();
		testBackground = new Background(0.1, 5.3, defaultImage);
		testBackground = new Background(0.1, 5.3, defaultImage, 5);

	}
	

	@Test
	public void getHigh() {
		 assertEquals(0.1, testBackground.getHight(), 0.1);
	}
	
	@Test
	public void setHigh() {
		testBackground.setHight(0.2);
		 assertEquals(0.2, testBackground.getHight(), 0.2);
	}
	
	
	@Test
	public void getWidth() {
		 assertEquals(5.3, testBackground.getWidth(), 5.3);
	}
	

	@Test
	public void setWidth() {
		testBackground.setWidth(5.4);
		 assertEquals(5.4, testBackground.getWidth(), 5.4);
	}
	
	@Test
	public void getScale() {
		 assertEquals(5, testBackground.getScale(), 5);
	}
	
	
	@Test
	public void setScale() {
		testBackground.setScale(6);
		 assertEquals(6, testBackground.getScale(), 6);
	}
	
	@Test
	public void getPicture() {
	   assertEquals(defaultImage, testBackground.getPicture());
	}
	
	@Test
	public void setPicture() {
		testBackground.setPicture(defaultImage);
	   assertEquals(defaultImage, testBackground.getPicture());
	}
	
}
