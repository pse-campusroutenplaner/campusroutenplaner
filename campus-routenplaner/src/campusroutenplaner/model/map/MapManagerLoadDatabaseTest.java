package campusroutenplaner.model.map;

import static org.junit.Assert.*;

import java.io.IOException;
import org.junit.*;
import campusroutenplaner.model.database.Database;

public class MapManagerLoadDatabaseTest {
	static Database db;
	static MapManager mm;
	static CampusMap cm;
	@BeforeClass
	public static void setUpBeforeClass(){
		db = new Database("test/model/loadDB.db");
		cm = new CampusMap(db);
		mm = new MapManager(cm, db);
	}
	@SuppressWarnings("deprecation")
	@Test
	public void loadDatabaseTest() throws IOException {
		mm.loadDatabase();
		assertEquals(8, cm.getBuildingList().size());
		assertNotNull(mm.getMap(4));
		assertEquals(1.6, cm.getBackground().getScale(), 0);
		assertEquals(167,cm.getGraph().getVertices().size());
		assertEquals(210,cm.getGraph().getEdges().size());
		assertTrue(cm.getGraph().getEdge(86).isBlocked());
		assertEquals("Test", ((RoomType) mm.getMap(4).getGraph().getVertex(246).getType()).getAliases().get(0));
	}
	
    @After
	public void tearDown(){
    	
    }
	@AfterClass
	public static void cleanUp(){
	
	}
}
