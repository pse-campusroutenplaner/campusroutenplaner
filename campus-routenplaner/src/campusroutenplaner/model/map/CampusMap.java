package campusroutenplaner.model.map;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Pattern;

import campusroutenplaner.Logger;
import campusroutenplaner.model.database.Database;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;


public class CampusMap extends Map {

	private LinkedList<Building> buildings;
	private Database database;

	/**
	 *
	 * @param graph
	 * @param backgroundImage
	 * @param id
	 * @param buildings
     */
	/*public CampusMap(Graph graph, Background backgroundImage, int id, LinkedList<Building> buildings) {
		super(graph, backgroundImage, id);
		this.buildings = buildings;
	}*/

	/**
	 * 
	 * @param database
	 */
	public CampusMap(Database database) {
		super(new Graph(), new Background(), 0);
		this.buildings = new LinkedList<>();
		this.database = database;
	}
	
	
	/**
	 * This method search in the List of buildings for building with the handover number.
	 * In case that no building were found or the handover number was not correct, it will be returned null.
	 * @param buildingNumber
	 * @return the building, or null if not found
	 */
	public Building searchBuildingByNumber(String buildingNumber) {
		
		if ( (Pattern.matches("\\d\\d\\.\\d\\d" , buildingNumber) ) ) {
			for(int i = 0; i < buildings.size(); i++) {
				if (buildingNumber.equals(buildings.get(i).getNumber())) {
					return buildings.get(i);
				}
			}
	    	
	    }
		return null;
	}
	
	

	
	
	/**
	 * With this method it can be added a new buidling, in case that all handover parameters are correct and not used.
	 * In case that any parameter is not correct or already exist, it will be returned a null
	 * @param number
	 * @param name
	 * @param address
	 * @return building
	 */
/*	public Building addBuilding (String number, String name, String address) {
	        
	        if ( (Pattern.matches("\\d\\d\\.\\d\\d" , number)) && (checkIfNumberExist(number) == OnError.NumberDoesNotExist)) {
	         
	            if ( (Pattern.matches("[a-zA-Z]+", name)) && (!database.checkIfBuildingNameExist(name))) {
	            
	              if (!database.checkIfBuildingAddressExist(address)) {
	            	  if ( database.addBuilding(number, name, address) ) {

	            		  Building building = new Building(number, name, address);
	            		  buildings.add(building);
	            		  System.out.println("Created:  "+ building);
	            		  return building;
	                   }
	               }  
	            }
	        }
	        return null;
	    }
	*/
	/**
	 * With this method it can be added a new buidling, in case that all handover parameters are correct and not used.
	 * In case that any parameter is not correct or already exist, it will be returned a null
	 * @param number
	 * @param name
	 * @param address
	 * @return building
	 */
	protected Building loadBuilding (String number, String name, String address) {

		if ( (Pattern.matches("\\d\\d\\.\\d\\d" , number)) && (checkIfNumberExist(number) != OnError.NumberExist)) {
			
	                  Building building = new Building(number, name, address);
	                  buildings.add(building);
                      System.out.println("Created: "+ building);
	                  return building;
	             
	        }
	        return null;
	    }
	
	/**
	 * With this method it can be added a new buidling, in case that all handover parameters are correct and not used.
	 * In case that any parameter is not correct or already exist, it will be returned a null
	 * @param number
	 * @return buidling
	 */
	public Building addBuilding (String number) {
		
		
		if ( (Pattern.matches("\\d\\d\\.\\d\\d" , number)) && (checkIfNumberExist(number) == OnError.NumberDoesNotExist)) {

            String name = new String();
			String address = new String();
			
			if ( database.addBuilding(number, name, address) ) {

				Building building = new Building(number, name, address);
				buildings.add(building);
				System.out.println("Created: "+ building);
				return building;
		   }
		}
		
		return null;

	}
	
	
	/**
	 * Check, if building with such number already exist, then remove the building from database.If the remove was successful
	 * then remove it from the list of buildings,only then it will be return 1;
	 * Otherweise it will be returned UnSuccessfulRemoved;
	 * @param buildingNumber
	 * @return OnError
	 */
	public OnError removeBuilding(String buildingNumber) {
		

			 if (database.deleteBuilding(buildingNumber)) {

				 for (int i = 0; i < buildings.size(); i++) {
					 if (buildings.get(i).getNumber().equals(buildingNumber)) {

						  buildings.remove(i);
						 return OnError.SuccessfulRemoved;
					 }
				 }




				 return OnError.NumberDoesNotExist;
		      }
		      return OnError.UnSuccessfulremovedFromDatabase;

	}
	
	
	/**
	 * If the vertex is already assigned to building, then it will be returned null.
	 * Otherweise change the type of handover vertex, then add the vertex in the list of entrances of the building.
	 * @param vertex
	 * @param building
	 * @return Vertex
	 */
	public Vertex assignVertexToBuildingEntrances(Vertex vertex, Building building) {

		
	    if (!database.checkIfVertexIsAssignToBuilding(vertex.getId()) ) {
	    	
	    	double buildingNumberConverted = Double.parseDouble(building.getNumber());
	    	
		  if (database.assignEntranceToBuilding(vertex.getId(), buildingNumberConverted) ) {
			
			  VertexType type = new EntranceType(building.getNumber(), vertex);
			  vertex.setType(type);

			  if(vertex.getMapId() == 0){
				  building.getEntrances().add(vertex);
			  }

			  System.out.println("Der Representant und die zugehörigen Verbindungen sind zu erstellen");
			  return vertex;
		   }
		 }
		
		return null;
	}
	
	/**
	 * First it must be checked, if the handover parameter are equals, unequal or null. In case that are null or equal(to the saved parameters), then the parameter from the building remain unchanged.
	 * In case that are unequal to the saved parameters, then they will be overwrited with the handover parameters.
	 * The changes must be saved in database.
	 * @param buildingNumber
	 * @param name
	 * @param address
	 * @return toChangeBuilding
	 */
	public Building changeBuilding(String buildingNumber, String name, String address) {
	
		Building toChangeBuilding =  searchBuildingByNumber(buildingNumber);
		
		  if ( (name != null) && (!database.checkIfBuildingNameExist(name)) && (Pattern.matches("[a-zA-Z]+", name)) ) {
			  
			 if (database.changeBuildingName(buildingNumber, name) ) {
				 toChangeBuilding.setName(name);
			 }
	      }
		
			if ( (address != null) && (!database.checkIfBuildingAddressExist(address)) ) {
			   if (database.changeBuildingAddress(buildingNumber, address)) {
				   toChangeBuilding.setAddress(address);
			   }
			}
		
		return toChangeBuilding;
	}
	
	
	/**
	 * The Parameters buildingNumber and room are passed to methods from database
	 * In case that no such room exist, it will be returned 0,because numbers for vertices start by 1.
	 * @param buildingNumber
	 * @param room
	 * @return int
	 */
    public int getVertexIdofRoom(String buildingNumber, String room) {


		System.out.println("Searching for room name " + room +"  in buildingNumber "+buildingNumber+ " in Database");
		return database.getVertexIdOfRoom(buildingNumber, room);

	}
	
	/**
	 *  The parameter searchedString is passed to method from database
	 *  In case that no such building are found, then it will be returned null.
	 *  @return Building
	 */
	public Building searchBuilding(String searchedString) {
	   
		
		if (Pattern.matches("\\d\\d.\\d\\d", searchedString) || (Pattern.matches("[a-zA-Z]+", searchedString)) ) {
		
			String numberOfBuilding = database.searchBuilding(searchedString);
		
			if (!numberOfBuilding.equals("")) {
			
				return searchBuildingByNumber(numberOfBuilding) ;	
			}
		}
		
		
	 	 return null;
	
	}
	

	/**
	 * @return List of buildingNumbers (List<String>)
	 */
	 public ArrayList<String[]> getBuildingList() {
	   return database.getBuildings();
	 }
	
	/**
	 * Find the building with the handover parameter buildingNumber.Then return List of entrances for the building.
	 * In case that the handover parameter corresponds to no building, it will be returned null.
	 * @param buildingNumber
	 * @return List<Vertex> (entrances of building)
	 */
	public LinkedList<Vertex> getEntrances(String buildingNumber) {
		

		for (int i = 0; i < buildings.size(); i++) {
			if (buildings.get(i).getNumber().equals(buildingNumber)) {
				Building building = buildings.get(i);
				return building.getEntrances();
			}
		}
		return null;
		
	}



	public boolean setBlueprinted(String buildingNumber, boolean blueprinted){
        
		Building build = searchBuildingByNumber(buildingNumber);

        if(build == null){
            System.out.println("Cannot set blueprinted to nonexisting building: " + buildingNumber);
            return false;
            
        }

        build.setBlueprinted(blueprinted);
        return true;
    }
	
	/**
	 * check if the handover parameter already exist as building number
	 * @param number
	 * @return boolean
	 */
	public OnError checkIfNumberExist(String number) {
		for (int i = 0; i < buildings.size(); i++) {
			if ( number.equals(buildings.get(i).getNumber()) ) {
				return OnError.NumberExist;
			}
		}
		return OnError.NumberDoesNotExist;
	}
	
	/**
	 * use in the tests
	 * @return
	 */
	public LinkedList<Building> getBuildings() {
		return buildings;
	}

	@Override
	public String toString() {
		return "CampusMap [buildings =" + buildings + "  " + "defaultBuilding" + "]";
	}
	
	
	





}

