package campusroutenplaner.model.map;




import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import org.junit.*;

import campusroutenplaner.model.database.Database;
import campusroutenplaner.model.map.*;

import java.util.Collections;
import java.util.LinkedList;


/**
 * Created by nmladenov on 6/29/16.
 */
public class MapManagerTest {
	
	private static MapManager mapMan;


    private LinkedList<Vertex> addedVertex;
    private LinkedList<Edge> addedEdges;

	private Vertex actual;
	private Vertex expected;
	private VertexType type;
	

	@BeforeClass
	public static void setUpBeforeClass(){

		Database database = new Database("test/model/mapManagerTestKit.db");
		CampusMap campus = new CampusMap(database);
		mapMan = new MapManager(campus, database);	
		mapMan.loadDatabase();

       // mapMan.addFloor("50.34", 0);
       // mapMan.addFloor("50.34", 1);

      //  System.out.println(mapMan.get);

	}


	@Before
    public void setUp(){



	    addedVertex = new LinkedList<>();
	    addedEdges = new LinkedList<>();
    }

	@Test
    public void addVertexToCampus() {

	    Vertex vert = mapMan.addVertex(0, 0, 0);


        assertEquals(vert, mapMan.getVertex(0, vert.getId()));

    }

    @Test
    public void addVertexToFloor(){
        Vertex vert = mapMan.addVertex(1, 0, 0);



        assertEquals(vert, mapMan.getVertex(1, vert.getId()));
    }


    @Test
    public void addVertexToNonExistingFloor(){
        Vertex vert = mapMan.addVertex(4, 0, 0);

        assertNull(vert);
    }



    @Test
    public void deleteVertex(){
        Vertex vert = mapMan.addVertex(0, 0, 0);



        assertEquals(vert.getId(), mapMan.deleteVertex(0, vert.getId()));

    }

    @Test
    public void deleteRoomWithAliases(){

        Vertex vert = mapMan.addVertex(1, 0, 0);
        LinkedList<String> aliases = new LinkedList<>();
        aliases.add("Dase");
        aliases.add("mahne");

        mapMan.changeVertex(vert.getMapId(), vert.getId(), 0, 0, new RoomType("213", aliases));

        mapMan.deleteVertex(vert.getMapId(), vert.getId());
        assertNull(mapMan.getVertex(vert.getMapId(), vert.getId()));

    }


    @Test
    public void deleteNonExisting(){
        assertEquals(-1, mapMan.deleteVertex(0, 0));
    }


    @Test
    public void deleteWithEdges(){
        Vertex vert1 = mapMan.addVertex(0, 0, 0);
        Vertex vert2 = mapMan.addVertex(0, 0, 0);



        Edge edge = mapMan.addEdge(vert1.getMapId(), vert1.getId(), vert2.getMapId(), vert2.getId());

        assertEquals(-1, mapMan.deleteVertex(0, vert1.getId()));
        assertEquals(-1, mapMan.deleteVertex(0, vert2.getId()));
    }








    // -----------------ADD EDGE----------------

    @Test
    public void addEdgeInvalidMapIds(){
        assertNull(mapMan.addEdge(-1, -1, -1, -1));
    }


    @Test
    public void addEdgeNoVertices(){
        assertNull(mapMan.addEdge(0, -1, 0, -1));
    }

    @Test
    public void addExistingEdge(){
        Vertex vert1 = mapMan.addVertex(0, 0, 0);
        Vertex vert2 = mapMan.addVertex(0, 0, 0);



        Edge edge = mapMan.addEdge(vert1.getMapId(), vert1.getId(), vert2.getMapId(), vert2.getId());

        assertNull(mapMan.addEdge(vert1.getMapId(), vert1.getId(), vert2.getMapId(), vert2.getId()));
    }



    @Test
    public void addEdgeNotStairsOrElevator(){
        Vertex down = mapMan.addVertex(1, 0, 0);
        Vertex up = mapMan.addVertex(2, 0, 0);

        assertNull(mapMan.addEdge(down.getMapId(), down.getId(), up.getMapId(), up.getId()));
    }


    @Test
    public void addEdgeTwoStairs(){
        Vertex down = mapMan.addVertex(1, 0, 0);
        mapMan.changeVertex(down.getMapId(), down.getId(), 0, 0, new StairsType());
        Vertex up = mapMan.addVertex(2, 0, 0);
        mapMan.changeVertex(up.getMapId(), up.getId(), 0, 0, new StairsType());

        assertNotNull(mapMan.addEdge(down.getMapId(), down.getId(), up.getMapId(), up.getId()));

        assertNotNull(up.getEdgeBetween(down));
        assertNotNull(down.getEdgeBetween(up));

    }

    @Test
    public void addEdgeTwoElevator(){
        Vertex down = mapMan.addVertex(1, 0, 0);
        mapMan.changeVertex(down.getMapId(), down.getId(), 0, 0, new ElevatorType());
        Vertex up = mapMan.addVertex(2, 0, 0);
        mapMan.changeVertex(up.getMapId(), up.getId(), 0, 0, new ElevatorType());


        assertNotNull(mapMan.addEdge(down.getMapId(), down.getId(), up.getMapId(), up.getId()));

        assertNotNull(up.getEdgeBetween(down));
        assertNotNull(down.getEdgeBetween(up));
    }


    @Test
    public void addEdgeElevAndStairs(){
        Vertex down = mapMan.addVertex(1, 0, 0);
        mapMan.changeVertex(down.getMapId(), down.getId(), 0, 0, new ElevatorType());
        Vertex up = mapMan.addVertex(2, 0, 0);
        mapMan.changeVertex(up.getMapId(), up.getId(), 0, 0, new StairsType());


        assertNull(mapMan.addEdge(down.getMapId(), down.getId(), up.getMapId(), up.getId()));


    }

    @Test
    public void addEdgeStairsAndElev(){
        Vertex down = mapMan.addVertex(1, 0, 0);
        mapMan.changeVertex(down.getMapId(), down.getId(), 0, 0, new StairsType());
        Vertex up = mapMan.addVertex(2, 0, 0);
        mapMan.changeVertex(up.getMapId(), up.getId(), 0, 0, new ElevatorType());


        assertNull(mapMan.addEdge(down.getMapId(), down.getId(), up.getMapId(), up.getId()));


    }


    @Test
    public void addEdgeentranceOnSecondFloor(){
        Vertex entranceIn = mapMan.addVertex(2, 0, 0);
        mapMan.changeVertex(entranceIn.getMapId(), entranceIn.getId(), 0,0, new EntranceType("50.34"));
        Vertex entranceCampus = mapMan.addVertex(0, 0, 0);
        mapMan.changeVertex(entranceCampus.getMapId(), entranceCampus.getId(), 0, 0, new EntranceType("50.34"));


        assertNull(mapMan.addEdge(entranceCampus.getMapId(), entranceCampus.getId(), entranceIn.getMapId(), entranceIn.getId()));

        assertNull(mapMan.addEdge(entranceIn.getMapId(), entranceIn.getId(), entranceCampus.getMapId(), entranceCampus.getId()));

    }




    /**
     *Adding edge between two campus vertices
     */
    @Test
    public void addEdgeEntranceCampusAndCampus(){

        Vertex vert1 = mapMan.addVertex(0, 0, 0);
        mapMan.changeVertex(vert1.getMapId(), vert1.getId(), 0, 0, new EntranceType("50.34"));
        Vertex vert2 = mapMan.addVertex(0, 0, 0);
        mapMan.changeVertex(vert2.getMapId(), vert2.getId(), 0, 0, new EntranceType("50.34"));


        assertNull(mapMan.addEdge(vert1.getMapId(), vert1.getId(), vert2.getMapId(), vert2.getId()));
    }


    @Test
    public void addEdgeEntranceSecondFloor1(){
        Vertex entrance = mapMan.addVertex(0, 0, 0);
        mapMan.changeVertex(entrance.getMapId(), entrance.getId(), 0, 0, new EntranceType("50.34"));
        Vertex floor = mapMan.addVertex(2, 0, 0);
        mapMan.changeVertex(floor.getMapId(), floor.getId(), 0, 0, new EntranceType("50.34"));

        assertNull(mapMan.addEdge(entrance.getMapId(), entrance.getId(), floor.getMapId(), floor.getId()));
        assertNull(mapMan.addEdge(entrance.getMapId(), entrance.getId(), floor.getMapId(), floor.getId()));
    }






    @Test
    public void addEdgeEntranceValid1(){
        Vertex entranceCampus = mapMan.addVertex(0, 0, 0);
        mapMan.changeVertex(entranceCampus.getMapId(), entranceCampus.getId(), 0, 0, new EntranceType("50.34"));
        Vertex entranceBuild = mapMan.addVertex(1, 0, 0);
        mapMan.changeVertex(entranceBuild.getMapId(), entranceBuild.getId(), 0, 0, new EntranceType("50.34"));


        Edge edge = mapMan.addEdge(entranceBuild.getMapId(), entranceBuild.getId(), entranceCampus.getMapId(), entranceCampus.getId());

        assertNotNull(edge);
        assertEquals(edge.getLength(), 0, 1);



        assertEquals(entranceBuild, ((EntranceType) entranceCampus.getType()).getRepresentant());
        assertEquals(entranceCampus, ((EntranceType) entranceBuild.getType()).getRepresentant());

    }

    @Test
    public void addEdgeEntranceValid2(){
        Vertex entranceCampus = mapMan.addVertex(0, 0, 0);
        mapMan.changeVertex(entranceCampus.getMapId(), entranceCampus.getId(), 0, 0, new EntranceType("50.34"));
        Vertex entranceBuild = mapMan.addVertex(1, 0, 0);
        mapMan.changeVertex(entranceBuild.getMapId(), entranceBuild.getId(), 0, 0, new EntranceType("50.34"));

        Edge edge = mapMan.addEdge(entranceCampus.getMapId(), entranceCampus.getId(), entranceBuild.getMapId(), entranceBuild.getId());

        assertNotNull(edge);
        assertEquals(edge.getLength(), 0, 1);



        assertEquals(entranceBuild, ((EntranceType) entranceCampus.getType()).getRepresentant());
        assertEquals(entranceCampus, ((EntranceType) entranceBuild.getType()).getRepresentant());
    }


	


    @Test
    public void deleteEdgeNotValidMapId(){
        assertEquals(-1, mapMan.deleteEdge(-1, 0));
    }

    @Test
    public void deleteEdgeNotValidId(){
        assertEquals(-1, mapMan.deleteEdge(0,-1));
    }



    @Test
    public void deleteEdgeDifferentIds(){
        Vertex down = mapMan.addVertex(1, 0, 0);
        mapMan.changeVertex(down.getMapId(), down.getId(), 0, 0, new StairsType());
        Vertex up = mapMan.addVertex(2, 0, 0);
        mapMan.changeVertex(up.getMapId(), up.getId(), 0, 0, new StairsType());


        Edge edge = mapMan.addEdge(down.getMapId(), down.getId(), up.getMapId(), up.getId());

        assertNotEquals(-1, mapMan.deleteEdge(up.getMapId(), edge.getId()));
    }



    @Test
    public void deleteEdgeEntrances(){
        Vertex entranceCampus = mapMan.addVertex(0, 0, 0);
        mapMan.changeVertex(entranceCampus.getMapId(), entranceCampus.getId(), 0, 0, new EntranceType("50.34"));
        Vertex entranceBuild = mapMan.addVertex(1, 0, 0);
        mapMan.changeVertex(entranceBuild.getMapId(), entranceBuild.getId(), 0, 0, new EntranceType("50.34"));


        Edge edge = mapMan.addEdge(entranceCampus.getMapId(), entranceCampus.getId(), entranceBuild.getMapId(), entranceBuild.getId());


        assertNotEquals(-1, mapMan.deleteEdge(0, edge.getId()));

        assertNull(((EntranceType) entranceBuild.getType()).getRepresentant());
        assertNull(((EntranceType) entranceCampus.getType()).getRepresentant());


    }



    //--------------Change Vertex-----------

    @Test
    public void changeVertexValid(){
        Vertex vert = mapMan.addVertex(0, 0, 0);

        vert = mapMan.changeVertex(0, vert.getId(), 1, 1, new DefaultType());

        Vertex expected = new Vertex(1, 1, vert.getId(), new DefaultType(), 0);

        assertEquals(expected, vert);
    }

    @Test
    public void changeVertexNoMapIdNoId(){
        assertNull(mapMan.changeVertex(-1, 0, 1, 1, new DefaultType()));
        assertNull(mapMan.changeVertex(0,-1 , 1, 1, new DefaultType()));
    }



    @Test
    public void changeVertexDeleteEdgesByStairs(){

        Vertex down = mapMan.addVertex(1, 0, 0);
        mapMan.changeVertex(down.getMapId(), down.getId(), 0, 0, new StairsType());
        Vertex up = mapMan.addVertex(2, 0, 0);
        mapMan.changeVertex(up.getMapId(), up.getId(), 0, 0, new StairsType());

            Edge edge = mapMan.addEdge(down.getMapId(), down.getId(), up.getMapId(), up.getId());

            mapMan.changeVertex(up.getMapId(), up.getId(), 0,0,  new DefaultType());

            assertNull(up.getEdgeBetween(down));


    }



    @Test
    public void changeVertexDeleteEdgesByElevator(){
        Vertex down = mapMan.addVertex(1, 0, 0);
        mapMan.changeVertex(down.getMapId(), down.getId(), 0, 0, new StairsType());
        Vertex up = mapMan.addVertex(2, 0, 0);
        mapMan.changeVertex(up.getMapId(), up.getId(), 0, 0, new StairsType());

        Edge edge = mapMan.addEdge(down.getMapId(), down.getId(), up.getMapId(), up.getId());

        mapMan.changeVertex(up.getMapId(), up.getId(), 0,0,  new DefaultType());

        assertNull(up.getEdgeBetween(down));

    }



    @Test
    public void changeVertexNoDeletedEdges(){

        Vertex down = mapMan.addVertex(1, 0, 0);
        mapMan.changeVertex(down.getMapId(), down.getId(), 0, 0, new ElevatorType());
        Vertex up = mapMan.addVertex(2, 0, 0);
        mapMan.changeVertex(up.getMapId(), up.getId(), 0, 0, new ElevatorType());

        Edge edge = mapMan.addEdge(down.getMapId(), down.getId(), up.getMapId(), up.getId());

        mapMan.changeVertex(up.getMapId(), up.getId(), 1 ,1,  new ElevatorType());

        assertNotNull(up.getEdgeBetween(down));

    }


    @Test
    public void changeVertexChangeRepres(){
        Vertex entranceCampus = mapMan.addVertex(0, 0, 0);
        mapMan.changeVertex(entranceCampus.getMapId(), entranceCampus.getId(), 0, 0, new EntranceType("50.34"));
        Vertex entranceBuild = mapMan.addVertex(1, 0, 0);
        mapMan.changeVertex(entranceBuild.getMapId(), entranceBuild.getId(), 0, 0, new EntranceType("50.34"));


        Edge edge = mapMan.addEdge(entranceCampus.getMapId(), entranceCampus.getId(), entranceBuild.getMapId(), entranceBuild.getId());

        System.out.println(entranceBuild);
        System.out.println(entranceCampus);

        mapMan.changeVertex(entranceBuild.getMapId(), entranceBuild.getId(), 1 ,1,  new DefaultType());


        assertNull(((EntranceType) entranceCampus.getType()).getRepresentant());

        assertNull(entranceBuild.getEdgeBetween(entranceCampus));

    }

    @Test
    public void changeVertexPos(){
        Vertex source = mapMan.addVertex(0, 0, 0);
        Vertex dest = mapMan.addVertex(0, 0, 0);

        Edge edge = mapMan.addEdge(source.getMapId(), source.getId(), dest.getMapId(), dest.getId());


        mapMan.changeVertex(dest.getMapId(), dest.getId(), 5, 5, new DefaultType());

        assertNotEquals(edge.getLength(), dest.getEdgeBetween(source).getId());

    }
    
    
    @Test
    public void changeVertexToRoomOnCampus(){
    	Vertex vert = mapMan.addVertex(0, 0, 0);

    	vert = mapMan.changeVertex(vert.getMapId(), vert.getId(), 0, 0, new RoomType());
    	
    	assertNull(vert);
    }



    @Test
    public void changeVertexToEntrance(){
        Vertex vert = mapMan.addVertex(0, 0, 0);

        vert = mapMan.changeVertex(vert.getMapId(), vert.getId(), 0, 0, new EntranceType("50.34"));

        assertNotNull(vert);

        assertTrue(((CampusMap) mapMan.getMap(0)).searchBuildingByNumber("50.34").getEntrances().contains(vert));
    }

    @Test
    public void changeVertexFromRoom(){
        int floorId = mapMan.addFloor("50.34", 5);

        Vertex vertex = mapMan.addVertex(floorId, 0 , 0);

        RoomType type = new RoomType("213");
        type.addAlias("niki");
        mapMan.changeVertex(floorId, vertex.getId(), 0, 0, type);




        Vertex actual = mapMan.changeVertex(floorId, vertex.getId(), 0, 0, new DefaultType());
        assertEquals(new DefaultType(), actual.getType());



    }



    // --------------- ADD FLOOR TEST
    
    @Test
    public void addFloorTest(){
    	int mapId = mapMan.addFloor("00.00", 0);
    	
    	assertNotNull(mapMan.getMap(mapId));
    }
    
    @Test
    public void deleteFloorCampusMap(){
    	assertEquals(-1, mapMan.removeFloor(0));
    }
    
    @Test
    public void deleteFloorNormal(){
    	int mapId =  mapMan.addFloor("00.00", 0);
    	
    	assertEquals(0, mapMan.removeFloor(mapId));


    }






    @Test
    public void changeBackgorundNoBgTest(){
        assertNull(mapMan.changeBackground(10, null, 1, 1, 0.8));
    }

    @Test
    public void changeBackgroundTest(){
        assertNotNull(mapMan.changeBackground(1, null, 2, 2, 0.8 ));
    }



    @Test
    public void deleteNoBackgroundNoBg(){
      mapMan.deleteBackground(7);
    }

    @Test
    public void deleteBackground(){
        mapMan.changeBackground(7, null, 2, 2, 0.8 );

        mapMan.deleteBackground(7);

        assertNull(mapMan.getBackground(7));
    }





    @Test
    public void addBackground(){
        int id = mapMan.addFloor("50.34", 7);

        mapMan.saveBackgroundImage(id, null);

        assertNotNull(mapMan.getBackground(id));

        mapMan.removeFloor(id);
    }





    @Test
    public void changeEdgeError(){
        assertNull(mapMan.changeEdge(-1, -1, 2, true, true));
        assertNull(mapMan.changeEdge(0, -1, 2, true, true));
    }


    @Test
    public void changeEdgeValid(){
        Vertex source = mapMan.addVertex(0, 0, 0);
        Vertex dest = mapMan.addVertex(0, 0, 0);

        Edge edge = mapMan.addEdge(source.getMapId(), source.getId(), dest.getMapId(), dest.getId());


        assertNotNull(mapMan.changeEdge(edge.getSource().getMapId(), edge.getId(), 25, true, true));
    }



    @Test
    public void getVerticesTest(){


       int mapId =  mapMan.addFloor("50.34", 5);

        LinkedList<Integer> verticesExpected = new LinkedList<>();
        verticesExpected.add(mapMan.addVertex(mapId, 0, 0).getId());
        verticesExpected.add(mapMan.addVertex(mapId, 0, 0).getId());
        verticesExpected.add(mapMan.addVertex(mapId, 0, 0).getId());

        //Collections.reverse(verticesExpected);

        verticesExpected.removeAll(mapMan.getVertices(mapId));

        assertEquals( new LinkedList<>(), verticesExpected );

        mapMan.removeFloor(mapId);
    }

    @Test
    public void getVerticesErrorTest(){
        assertNull(mapMan.getVertices(7));
    }


    @Test
    public void getEdgesTestErrorTest(){
        assertNull(mapMan.getEdgeList(7));
    }

    @Test
    public void getEdgesTest(){
        int mapId =  mapMan.addFloor("50.34", 5);
        Vertex source;
        Vertex dest;
        LinkedList<Integer> edges = new LinkedList<>();
        source = mapMan.addVertex(mapId, 0, 0);
        dest = mapMan.addVertex(mapId, 0, 0);

        edges.add(mapMan.addEdge(source.getMapId(), source.getId(), dest.getMapId(), dest.getId()).getId());

        source = mapMan.addVertex(mapId, 0, 0);
        dest = mapMan.addVertex(mapId, 0, 0);



       // Collections.reverse(edges);
        assertEquals(edges, mapMan.getEdgeList(mapId));
        mapMan.removeFloor(mapId);
    }

    @Test
    public void getMapTest(){
        int mapId =  mapMan.addFloor("50.34", 5);

        assertEquals(mapId, mapMan.getMap(mapId).getMapId());
        mapMan.removeFloor(mapId);
    }

    @Test
    public void getMapTestError(){
        int mapId =  17;

        assertNull(mapMan.getMap(mapId));
    }
    
    @Test
    public void getEdgeTest() {
//    	Vertex source = new Vertex(0, 0, 1, 0);
//    	Vertex destination = new Vertex(1, 1, 2, 0);
    	Vertex source = mapMan.addVertex(0, 0, 0);
    	Vertex destination = mapMan.addVertex(0, 1, 1);

    	
    	Edge testEdge = mapMan.addEdge(0, source.getId(), 0, destination.getId());
    	
    	assertEquals(testEdge, mapMan.getEdge(0, testEdge.getId())) ;
    	assertNull(mapMan.getEdge(44, testEdge.getId())) ;

    	
    }




    @After
	public void tearDown(){
	    for(Vertex vert : addedVertex){
	        if(vert == null){
	            continue;
            }

	        mapMan.deleteVertex(vert.getMapId(), vert.getId());
        }

        for(Edge edge : addedEdges){
            if(edge == null){
                continue;
            }

            mapMan.deleteEdge(edge.getSource().getMapId(), edge.getId());
        }

		actual = null;
		expected = null;
		type = null;
	}
	
	
	
	
	
	@AfterClass
	public static void cleanUp(){
		mapMan = null;
	
	}



}
