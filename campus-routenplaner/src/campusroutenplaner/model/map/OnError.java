package campusroutenplaner.model.map;

public enum OnError {
NumberExist, NumberDoesNotExist, ToRemoveFavoriteNameEqualsNull, ToRemoveFavoriteParametersEqualsNull, ToAddFavoriteNameEqualsNull, ToAddFavoriteParametersEqualsNull, SuccessfulRemoved, UnSuccessfulRemoved, SuccessfulAdd, UnSuccessfulAdd, SuccessfulLogin, UnSuccessfulLogin, SuccessfulChangePassword, UnSuccessfulChangePassword, SuccessfulChangeName, UnSuccessfulChangeName, UnSuccessfulremovedFromDatabase, SuccessfulAddIdenfinite, BluePrintedCannotBeSet, BluePrintedSuccessfullSet, UnSuccessfulAddFromDataBase;
}
