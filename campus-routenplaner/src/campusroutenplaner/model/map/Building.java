package campusroutenplaner.model.map;

import java.util.ArrayList;
import java.util.LinkedList;

public class Building {

	private String number;
	private String name;
	private String address;
	private LinkedList <ArrayList<Integer>> floors;
	private LinkedList<Vertex> entrances;
	private boolean blueprinted;
	
	
	
	
	public Building(String number, String name, String address) {
		this.number = number;
		this.name = name;
		this.address = address;
		this.floors = new LinkedList<ArrayList<Integer>>();
		this.entrances = new LinkedList<Vertex>();
		this.blueprinted = false;
		
	}

	public Building(String number) {
		this.number = number;
		this.floors = new LinkedList<ArrayList<Integer>>();
		this.entrances = new LinkedList<Vertex>();
		this.address = new String();
		this.floors = new LinkedList<ArrayList<Integer>>();
		this.entrances = new LinkedList<Vertex>();
		this.blueprinted = false;
	}
	
	public Building(String number, String name) {
		this.number = number;
		this.name = name;
	}
	

	/**
	 * adds the
	 * @param floor in this building in the right position
     */
	protected void addFloor(FloorMap floor) {



        int i = 0;

        while(i < floors.size() && floor.getFloorNumber() > floors.get(i).get(1)){
            i++;
        }
        


        ArrayList<Integer> newFloor = new ArrayList<>();

        newFloor.add(floor.getMapId());
        newFloor.add(floor.getFloorNumber());

        floors.add(i, newFloor);

	}

	public String getNumber() {
		return number;
	}
	
	public void addEntrance(Vertex vertex) {
	    entrances.add(vertex);
	}

	protected void setNumber(String number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	protected void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	protected void setAddress(String address) {
		this.address = address;
	}
	

	/**
	 *
	 * @return first is the mapId, second is floorNumber
	 * <p>
	 *     Last floor is the last element, first floor, is first element
	 * </p>
     */
	public LinkedList<ArrayList<Integer>> getFloors() {
		return floors;
	}

	protected void setFloors(LinkedList<ArrayList<Integer>> floors) {
		this.floors = floors;
	}

	public LinkedList<Vertex> getEntrances() {
		return entrances;
	}

	protected void setEntrances(LinkedList<Vertex> entrances) {
		this.entrances = entrances;
	}

	public boolean isBlueprinted() {
		return blueprinted;
	}

	protected void setBlueprinted(boolean blueprinted) {
        System.out.println("Setting blueprinted to " + blueprinted);
        this.blueprinted = blueprinted;
	}

	@Override
	public String toString() {
		return "Building [number = " + number + ", name = " + name + ", address = " + address + ", floors = " + floors
				+ ", entrances = " + entrances + ", blueprinted = " + blueprinted + "]";
	}
	


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Building other = (Building) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (blueprinted != other.blueprinted)
			return false;
		if (entrances == null) {
			if (other.entrances != null)
				return false;
		} else if (!entrances.equals(other.entrances))
			return false;
		if (floors == null) {
			if (other.floors != null)
				return false;
		} else if (!floors.equals(other.floors))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		return true;
	}


	
}
