package campusroutenplaner.model.map;

import java.awt.image.BufferedImage;

public class Background {
	private double hight;
	private double width;
	private double scale;
	private BufferedImage picture;


	
	
	/**
	 * @param x
	 * @param y
	 * @param zoom
	 * @param picture
	 */
	public Background(double high, double width, BufferedImage picture) {
		//super();
		this.hight = high;
		this.width = width;
		this.picture = picture;
		scale = 1;
	}
	
	
	public Background(double high, double width, BufferedImage picture, double scale) {
		//super();
		this.hight = high;
		this.width = width;
		this.picture = picture;
		this.scale = scale;
	}
	
	
	
	
	public double getHight() {
		return hight;
	}



	protected void setHight(double hight) {
		this.hight = hight;
	}


	public double getWidth() {
		return width;
	}



	protected void setWidth(double width) {
		this.width = width;
	}



	public double getScale() {
		return scale;
	}



	protected void setScale(double scale) {
		this.scale = scale;
	}



	public Background(){
		picture = new BufferedImage(66,66, BufferedImage.TYPE_INT_RGB);
		scale = 1;
	}
	
	public BufferedImage getPicture() {
		return picture;
	}
	
	
	protected void setPicture(BufferedImage picture) {
		this.picture = picture;
		
	}
	
	
	


	
	
	
}
