package campusroutenplaner.model.map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by nmlad on 8/4/16.
 */
public class StairsTypeTest {

    StairsType stairs;


    @Before
    public void setUp(){
        stairs = new StairsType();
    }

    @Test
    public void getTypeToStringTest(){
        assertEquals(stairs.getType(), "Stairs");
        assertEquals(stairs.toString(), "stairs");
    }


    @Test
    public void equalsStairs(){
        VertexType temp = new StairsType();

        assertTrue(stairs.equals(temp));
    }


    @Test
    public void equalsOther(){
        VertexType temp = new DefaultType();

        assertFalse(stairs.equals(temp));
    }




    @After
    public void cleanUp(){
        stairs = null;
    }
}
