package campusroutenplaner.model.map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by nmlad on 8/4/16.
 */
public class DefaultTypeTest {


    VertexType def;

    @Before
    public void setUp(){
        def = new DefaultType();
    }


    @Test
    public void getTypeToStringTest(){
        assertEquals("Default", def.getType());
        assertEquals("default", def.toString());
    }


    @Test
    public void equalsStairs(){
        VertexType temp = new DefaultType();

        assertTrue(def.equals(temp));
    }


    @Test
    public void equalsOther(){
        VertexType temp = new StairsType();

        assertFalse(def.equals(temp));
    }

    @After
    public void cleanUp(){
        def = null;
    }
}
