package campusroutenplaner.model.map;
 
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import campusroutenplaner.model.favorites.FavoriteManagerTest;
 
@RunWith(value = Suite.class)
@Suite.SuiteClasses(value = { BackgroundTest.class, BuildingTest.class,
CampusMapTest.class, DefaultTypeTest.class, EdgeTest.class,
ElevatorTypeTest.class, EntranceTypeTest.class , FavoriteManagerTest.class, FloorMapTest.class, GraphTest.class, MapManagerTest.class, MapManagerLoadDatabaseTest.class, 
MapTest.class, RoomTypeTest.class, StairsTypeTest.class, VertexTest.class})

public class AllTest {
 
    public AllTest() {
    }
}