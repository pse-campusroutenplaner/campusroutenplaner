package campusroutenplaner.model.map;

public class DefaultType extends VertexType{

	@Override
	public String toString() {

		return "default";
	}


	public String getType(){
		return "Default";
	}

	@Override
	public boolean equals(Object o) {
		return o instanceof DefaultType;
	}
}
