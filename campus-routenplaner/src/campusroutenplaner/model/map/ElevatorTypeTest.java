package campusroutenplaner.model.map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * 
 * Created by nmlad on 8/4/16.
 */
public class ElevatorTypeTest {

    VertexType elevator;

    @Before
    public void setUp(){
        elevator = new ElevatorType();
    }


    @Test
    public void getTypeToStringTest(){
        assertEquals("Elevator", elevator.getType());
        assertEquals("elevator", elevator.toString());
    }


    @Test
    public void equalsStairs(){
        VertexType temp = new ElevatorType();

        assertTrue(elevator.equals(temp));
    }


    @Test
    public void equalsOther(){
        VertexType temp = new DefaultType();

        assertFalse(elevator.equals(temp));
    }

    @After
    public void cleanUp(){
        elevator = null;
    }
}
