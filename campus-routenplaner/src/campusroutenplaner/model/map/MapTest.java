package campusroutenplaner.model.map;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MapTest {

	private FloorMap map;
	private Graph graph;
	private Background backgroundImage;
	private int mapId;
	
	
	@Before public void beforeTest() {
	    Graph graph = new Graph();
	    map =  new FloorMap(null, backgroundImage, mapId, null, mapId);
	    
	}
	
	
	
	
	
	@Test
	public void test() {
		assertEquals(null, map.getGraph());
	}

	@Test
	public void getbackground() {
		assertEquals(backgroundImage, map.getBackground());
	}
	
	@Test
	public void setbackground() {
		map.setBackground(backgroundImage);
		assertEquals(backgroundImage, map.getBackground());
	}
	
	
	@Test
	public void getmapID() {
		assertEquals(0, map.getMapId());
	}
	
	
	@Test
	public void setmapID() {
		map.setMapId(1);
		assertEquals(1, map.getMapId());
	}
	
	@Test
	public void setgraph() {
		map.setGraph(graph);
		assertEquals(null, map.getGraph());
	}
	
	
	
}
