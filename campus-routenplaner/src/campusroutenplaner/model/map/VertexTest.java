package campusroutenplaner.model.map;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.LinkedList;

import campusroutenplaner.model.map.EntranceType;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class VertexTest {

    private Vertex vertex;
	

    @Test
    public void emptyConstructorTest(){
        vertex = new Vertex();

        assertEquals(0, vertex.getId());
        assertEquals(0 , vertex.getMapId());
        assertEquals(new LinkedList<Edge>(), vertex.getNeighbours());
        assertEquals(new DefaultType(), vertex.getType());
        assertEquals(0.0, vertex.getXPos(), 1);
        assertEquals(0.0, vertex.getYPos(), 1);
    }


    @Test
    public void normalValidConstructorTest(){
        vertex = new Vertex(0, 0, 0, 0);

        assertEquals(0, vertex.getId());
        assertEquals(0 , vertex.getMapId());
        assertEquals(new LinkedList<Edge>(), vertex.getNeighbours());
        assertEquals(new DefaultType(), vertex.getType());
        assertEquals(0.0, vertex.getXPos(), 1);
        assertEquals(0.0, vertex.getYPos(), 1);
    }

    @Test
    public void normalNonValidConstrucotrTest(){
        vertex = new Vertex(-1, -1, -1, -1);


        assertEquals(0, vertex.getId());
        assertEquals(0 , vertex.getMapId());
        assertEquals(new LinkedList<Edge>(), vertex.getNeighbours());
        assertEquals(new DefaultType(), vertex.getType());
        assertEquals(0.0, vertex.getXPos(), 1);
        assertEquals(0.0, vertex.getYPos(), 1);
    }



    @Test
    public void extendetValidConstructorTest(){
        vertex = new Vertex(0, 0, 0, new DefaultType(), 0);

        assertEquals(0, vertex.getId());
        assertEquals(0 , vertex.getMapId());
        assertEquals(new LinkedList<Edge>(), vertex.getNeighbours());
        assertEquals(new DefaultType(), vertex.getType());
        assertEquals(0.0, vertex.getXPos(), 1);
        assertEquals(0.0, vertex.getYPos(), 1);
    }


    @Test
    public void extendetNonValidConstructorTest(){
        vertex = new Vertex(-1, -1, -1, null, -1);

        assertEquals(0, vertex.getId());
        assertEquals(0 , vertex.getMapId());
        assertEquals(new LinkedList<Edge>(), vertex.getNeighbours());
        assertEquals(new DefaultType(), vertex.getType());
        assertEquals(0.0, vertex.getXPos(), 1);
        assertEquals(0.0, vertex.getYPos(), 1);
    }



    @Test
    public void setValidXposTest(){
        vertex = new Vertex();

        vertex.setXPos(5);
        assertEquals(5, vertex.getXPos(), 1);
    }


    @Test
    public void setNotValidXposTest(){
        vertex = new Vertex();

        vertex.setXPos(-1);
        assertEquals(0, vertex.getXPos(), 1);
    }

    @Test
    public void setValidYPosTest(){
        vertex = new Vertex();

        vertex.setYPos(5);
        assertEquals(5, vertex.getYPos(), 1);
    }


    @Test
    public void setNonValidYPosTest(){
        vertex = new Vertex();

        vertex.setYPos(-1);
        assertEquals(0, vertex.getYPos(), 1);
    }


    @Test
    public void testConstructor2(){
        vertex = new Vertex();

        Vertex vertexExpected = new Vertex(0, 0, 0, new DefaultType(), 0);

        assertEquals(vertexExpected, vertex);
    }





    @Test
    public void setTypeTest(){
        vertex = new Vertex();

        vertex.setType(new StairsType());

        assertEquals(new StairsType(), vertex.getType());

    }



    @Test
    public void getEdgeBetweenNullVertex(){
        vertex = new Vertex();
        assertNull(vertex.getEdgeBetween(null));
    }


    @Test
    public void getEdgeBetweenValid(){
        vertex = new Vertex();

        Vertex vertexNeigh = new Vertex(0,0,1,0);
        Edge edge = new Edge(0, vertexNeigh, vertex);



        vertexNeigh.getNeighbours().add(edge);
        vertex.getNeighbours().add(edge);

        assertEquals(edge, vertex.getEdgeBetween(vertexNeigh));
    }

    @Test
    public void toStringTest(){


        vertex = new Vertex();

        Edge edge = new Edge(0, vertex, new Vertex(0, 0, 1, 0));

        vertex.getNeighbours().add(edge);

        assertEquals("Vertex [id=0, xPos=0.0, yPos=0.0, type=default, mapId=0, neighbourVert=[1,], neighEdges=[0,]", vertex.toString());
    }

    @Test
    public void getEdgeBetweenExistingNoEdge(){
        vertex = new Vertex();
        assertNull(vertex.getEdgeBetween(new Vertex()));
    }

    @Test
    public void setNullValidTypeTest(){
        vertex = new Vertex();

        vertex.setType(null);

        assertEquals(new DefaultType(), vertex.getType() );
    }


	
	@After
    public void cleanUp(){
	    vertex = null;
    }

}
