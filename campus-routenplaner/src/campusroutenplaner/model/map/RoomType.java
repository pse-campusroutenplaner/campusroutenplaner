package campusroutenplaner.model.map;


import java.util.LinkedList;

public class RoomType extends VertexType {

	private String name;
    private LinkedList<String> aliases;

	
	/**
	 * new roomType with 
	 * @param name
	 */
	public RoomType(String name) {

		if(name != null){
            this.name = name;
        }else{
            this.name = "";
        }

        aliases = new LinkedList<>();
    }


    /**
     * new roomType with
     * @param name
     */
    public RoomType(String name, LinkedList<String> aliases) {

        if(name != null){
            this.name = name;
        }else{
            this.name = "";
        }

        if(aliases == null){
            this.aliases = new LinkedList<>();
        }else{
            this.aliases = aliases;
        }

    }






    public RoomType(){

		this.name = "";
        aliases = new LinkedList<>();
    }
	
	
	
	/**
	 * setter for 
	 * @param name
	 */
	protected void setName(String name) {

        if(name != null){
            this.name = name;
        }
	}

	/**
	 * getter for 
	 * @return name
	 */
	public String getName() {
		return name;
	}


    /**
     * adds an alias to this room !with a number!
     * @param alias; alias should not be null or empty string, otherwise it will not be saved
     *
     */
	public void addAlias(String alias){
	    if(name.equals("")){
	       return;
        }


        if(alias != null && !alias.equals("")){
            aliases.add(alias);
        }
    }


   public RoomType setAliases(LinkedList<String> aliases){
        if(name.equals("")){
            System.out.println("alias to number empty");
            return this;
        }

        if(aliases == null){
         //   System.out.println("Aliases is null");
            return this;
        }


        this.aliases = aliases;
        return this;
    }


	public LinkedList<String> getAliases(){
	    return aliases;
    }


	@Override
	public String toString() {

		return "room: " + name+ ", Aliases: "+ aliases.toString();
	}


    public String getType(){
        return "Room";
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RoomType)) return false;

        RoomType roomType = (RoomType) o;

        if (!getName().equals(roomType.getName())) return false;
        return getAliases().equals(roomType.getAliases());

    }


}