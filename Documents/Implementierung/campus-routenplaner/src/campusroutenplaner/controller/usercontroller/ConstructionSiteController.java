package campusroutenplaner.controller.usercontroller;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.Set;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import campusroutenplaner.model.constructionsites.ConstructionSite;
import campusroutenplaner.model.constructionsites.ConstructionSiteManager;
import campusroutenplaner.model.database.Database;
import campusroutenplaner.model.map.Edge;
import campusroutenplaner.model.map.OnError;

public class ConstructionSiteController {

   private ConstructionSiteManager constrMan;
   private Database database;
	
   /**
    * Constructor Controller
    * @param constrMan
    */
	public ConstructionSiteController(ConstructionSiteManager constrMan) {
		this.constrMan = constrMan;
	}
	
	/**
	 * call up the method loadConstructionSitesFromDatabase from ConstuctionSiteManager
	 * @return ObservableList<ConstructionSite>
	 */
	public ObservableList<ConstructionSite> requestLoadConstructionSitesFromDatabase() {
		return constrMan.loadConstructionSitesFromDatabase();
	}
	

	/**
	 * call up the method addConstructionSite from ConstructionSiteManager.
	 * @param beginDate
	 * @param endDate
	 * @param edgeId
	 * @return OnError
	 */
	public OnError requestAddConstructionSite(int edgeId) {
		
	    if (!database.checkEdgeBlocked(edgeId) ) {
	    	return constrMan.addConstructionSite(edgeId);
	    }
	    return OnError.UnSuccessfulAdd;
		
	}
	
	/**
	 * call up the method changeConstructionSite from ConstructionSiteManager.
	 * @param constructionSiteId
	 * @param fromDate
	 * @param toDate
	 * @param edgeId
	 * @return ConstructionSite
	 */
	public ConstructionSite requestChangeConstructionSite(int constructionSiteId, LocalDate fromDate, LocalDate toDate, int edgeId) {
		
		return constrMan.changeConstructionSite(constructionSiteId, fromDate, toDate, edgeId);
	}
	
	
	/**
	 * call up the method getConstructionSite from ConstructionSiteManager.
	 * @param constructionId
	 * @return ConstructionSite
	 */
	public ConstructionSite requestGetConstructionSite(int constructionId) {
		return constrMan.getConstructionSite(constructionId);
	}
	
	/**
	 * call up the method removeConstructionSite from ConstructionSiteManager.
	 * @param constructionSiteId
	 * @return Enum
	 */ 
	public OnError requestRemoveConstructionSide(int constructionSiteId) {
		return constrMan.removeConstructionSite(constructionSiteId);
	}
	
	/**
	 * call up the method getConstructionSites from ConstructionManager
	 * @return
	 */
	public ObservableList<ConstructionSite> requestGetConstructionSite() {
		return constrMan.getConstructionSites();
	}
	
	/**
	 * call up the method getConstructionSiteId from the class ConstructionSiteManager
	 * @param edgeId
	 * @return int
	 */
	public int requestGetConstructionSiteId(int edgeId) {
		
			return constrMan.getConstructionSiteId(edgeId); 
		
	}
	
	
}
