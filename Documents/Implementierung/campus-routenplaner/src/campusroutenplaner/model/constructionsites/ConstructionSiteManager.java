package campusroutenplaner.model.constructionsites;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.sun.javafx.geom.Edge;


import campusroutenplaner.model.database.Database;
import campusroutenplaner.model.map.OnError;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;



public class ConstructionSiteManager {

	private ConstructionSite construct;
	private Database database;
	private ObservableList<ConstructionSite> constructionSites;
	
	/**
	 * Constructor manager
	 * @param construct
	 * @param database
	 */
	public ConstructionSiteManager(ConstructionSite construct, Database database) {
		this.construct = construct;
		this.database = database;
	}
	
	
	/**
	 * load all saved consstructionsites from the database.
	 * before return, convert every constructionsite from String to SimpleStringProperty and then add in the list that will be returned.
	 * @return LinkedList
	 */
	public ObservableList<ConstructionSite> loadConstructionSitesFromDatabase() {
		
		ObservableList<ConstructionSite> toReturn = FXCollections.observableArrayList();
		
		for ( int i = 0; i < database.getConstructionSites().size(); i++) {
			for (int j = 0; j < database.getConstructionSites().get(j).length; j++) {
				
				int id = Integer.parseInt(database.getConstructionSites().get(j)[0]);
				int edgeId = Integer.parseInt(database.getConstructionSites().get(j)[3]);
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MMM-dd");
			    LocalDate beginDate = LocalDate.parse(database.getConstructionSites().get(j)[1], dtf);
			    LocalDate expDate = LocalDate.parse(database.getConstructionSites().get(j)[2]);

				toReturn.add(new ConstructionSite(id, beginDate, expDate, edgeId));
				
			}
		}
		
		return toReturn;
	}
	
	
	/**
	 * create a default start and enddate for the new constructionsite. then check if the commited id already exist.
	 * if not add in database, then in the local list with all constructionsites and then return SuccessfulAdd.
	 * otherweise return Unsuccessfulsadd 
	 * @param edgeId
	 * @return Enum
	 */
	public OnError addConstructionSite(int edgeId) {
		Edge edge;
		

		LocalDate startDate,expDate;

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MMM-dd");
		LocalDate defaultStartDate = LocalDate.parse("0-0-0", formatter);
        LocalDate defaultExpDate = LocalDate.parse("0-0-0", formatter);


      	int id = 0;
      	
		if (database.checkEdgeBlocked(edgeId)) { 
		
			 id = database.addConstrSite(defaultStartDate.toString(), defaultExpDate.toString(), edgeId);
		 }
			
		if ( id != 0) {
				constructionSites.add(new ConstructionSite(id, defaultStartDate, defaultExpDate, edgeId));
				// edge.blocked = true!!!!!!
				return OnError.SuccessfulAdd;
		}

		return OnError.UnSuccessfulAdd;
	}
	
	
	/**
	 * If startdate and expDate are not equals null then add constructionsite in database .
	 * it will be returned the id from the new constructionsite. if unequal 0 then add new constructionsite in the local 
	 * list with the constructionsites and return successfulAdd.
	 * Otherweise return UnsuccessfulAdd
	 * @param startDate
	 * @param expDate
	 * @param edgeId
	 * @return Enum
	 */
	public OnError addConStructionSite(LocalDate startDate, LocalDate expDate, int edgeId) {
		
		int id = 0;
		
		if ( (!database.checkEdgeBlocked(edgeId)) && (!startDate.equals(null)) && (!expDate.equals(null)) ) {
			 id = database.addConstrSite(startDate.toString(), expDate.toString(), edgeId);
	    }
		
		if ( id != 0) {
			
			constructionSites.add(new ConstructionSite(id, startDate, expDate, edgeId));
			return OnError.SuccessfulAdd;
		}
		
		return OnError.UnSuccessfulAdd;
		
	}
	
	/**
	 * change startdate, expdate or edge to constructionsite.
	 * it will be searched for the constructionsite with the commited id,if found then it will be proofed ,if the new parameters are equals the defaultparameters or null.
	 * In both cases nothing to change.Otherweise override old pamaters. Changes must be saved both in the database and in the local list.
	 * if no such constructionsite is found ,return null.
	 * @param toChangeConstructionSiteId
	 * @param startDate
	 * @param expDate
	 * @param edgeId
	 * @return ConstructionSite
	 */
	public ConstructionSite changeConstructionSite(int toChangeConstructionSiteId, LocalDate startDate, LocalDate expDate, int edgeId) {
		

		 ConstructionSite toChangeConstructionSite = getConstructionSite(toChangeConstructionSiteId);
		 
		 
		if(!toChangeConstructionSite.equals(null)) {
		
			if ( (!toChangeConstructionSite.getBeginDate().equals(startDate)) && (!startDate.equals(null)) ) {
				if (database.changeConstructionSiteStartDate(startDate.toString(), toChangeConstructionSite.getId())) {
					toChangeConstructionSite.setBeginDate(startDate);
				}
			}
		
			if ( (!toChangeConstructionSite.getEndDate().equals(expDate)) && (!expDate.equals(null))  ) {
				if (database.changeConstructionSiteExpDate(expDate.toString(), toChangeConstructionSite.getId()) ) {
					toChangeConstructionSite.setEndDate(expDate);
				}
			}
		
		}
		
		return null;
	}
	
	/**
	 * search in the list with constructionSites . if no constructionsite with such id found,return null.
	 * @param constructionsiteId
	 * @return ConstructionSite
	 */
	public ConstructionSite getConstructionSite(int constructionsiteId) {
		
		for (ConstructionSite temp :constructionSites) {
			if (temp.getId() == constructionsiteId) {
				return temp;
			}
		}
		return null;
		
	}
	
	/**
	 * if constructionsite removed from database then remove from the list with constructionSite and the return SuccessfulRemoved
	 * Otherweise return UnSuccessfulremoved.
	 * @param constructionsiteId
	 * @return Enum
	 */
	public OnError removeConstructionSite(int constructionsiteId) {
		
		
		if (database.deleteConstrSite(constructionsiteId)) {
			for (ConstructionSite temp : constructionSites) {
				if (temp.getId() == constructionsiteId) {
					constructionSites.remove(temp);
					return OnError.SuccessfulRemoved;
				}
			}
		}
		
		return OnError.UnSuccessfulRemoved;

	}
	
	/**
	 * list with constructionsites local
	 * @return
	 */
	public ObservableList<ConstructionSite> getConstructionSites() {
	
		return constructionSites;
	}
	
	
	/**
	 * Get construtionSiteId.in case that constructionsite with such edge already exist, it will be returned the id from constructionsite.
	 * Otherweise it will be returned -1. 
	 * @param edge
	 * @return int 
	 */
	public int getConstructionSiteId(int edgeId) {
		
		for ( ConstructionSite temp :constructionSites) {
			if (temp.getEdgeId() == edgeId) {
				return temp.getId();
			}
		}
		return -1;
		
		
	}
	
	
	
	
	
	
	
	
}
