#Deltes all aux files, generated from Texmaker and -studio

#!/bin/bash
razh=( log aux toc snm nav out bcf run.xml synctex.gz vrb project class ipr iws iml )
echo Deleting:
for i in "${razh[@]}"
do 
	find . -name "*."$i"" -type f 
	find . -name "*."$i"" -type f -delete
done

rm -rf Documents/Pflichtenheft/Pflichtenheft.pdf
